function tabList(list, view, n, action) {
	var firstN = n - 1;

	$(view + ' > ul > li').hide();
	$(view + ' > ul > li:eq(' + firstN + ')').show();
	$(list + '> ul > li').eq(firstN).addClass('on');

	$(list + "> ul > li:not('.url')").on(action, function() {
		var maxN = $(list).find('li').length;
		var thisN = $(this).index();

		if ($(this).hasClass('off') == false) {
			$(list).find('li').each(function(e) {
				if ($(this).hasClass('off') == true) {
				} else if ($(this).hasClass('hide') == true) {
				} else {
					$(this).removeClass('on');
				}
			});

			$(this).addClass('on');
			$(view + ' > ul > li').hide();
			$(view + '> ul > li:eq(' + thisN + ')').show();
		}
		return false;
	});
}

(function($) {
	// jquery
	var $dom = $(document),
		$body = $('body'),
		isMobile = false;

	function enquirePc() {
		isMobile = false;
		$body.removeClass('is-mobile');
	}

	function enquireMobile() {
		isMobile = true;
		$body.addClass('is-mobile');
	}

	function initMenu() {
		const toggleButton = $('.sidebar-toggle');
		const sideBar = $('.sidebar');
		const moreMenu = $('.menu-toggle');
		const moreMenuVertical = $('.menu-toggle.vertical');
		const moreMenuOverlap = $('.menu-toggle-overlap');

		$(document).on('click','.sidebar-toggle', function() {
			sideBar.toggleClass('toggled');
		});

		$(document).on('click','.menu-toggle', function() {
			let targetMenu = $(this).closest('.card-footer').find('.more-menu');
			targetMenu.toggleClass('toggled');
		});

		$(document).on('click','.menu-toggle.vertical', function() {
			let targetMenu = $(this).next('.more-menu');
			targetMenu.toggleClass('toggled');
		});

		$(document).on('click','.menu-toggle-overlap', function() {
			let targetMenu = $(this).next('.more-menu-overlap');
			targetMenu.toggleClass('toggled');
		});
	}

	function initSlickMenu() {
		$('.slick-menu').slick({
			arrows: false,
			infinite: false,
			slidesToShow: 6,
			swipeToSlide: true,
			// variableWidth: true,
			responsive: [
				{
					breakpoint: 567,
					settings: {
						slidesToShow: 4
					}
				}
			]
		});
	}

	function initCalendar() {
		// datepicker
		$.datepicker.regional['ko'] = {
			showOn: 'button',
			showButtonPanel: false,
			buttonImage: '../assets/images/svg/input-calendar.svg',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd',
			buttonText: '선택',
			yearSuffix: '년',
			monthNamesShort: [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12' ],
			monthNames: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ],
			dayNamesMin: [ '일', '월', '화', '수', '목', '금', '토' ],
			dayNames: [ '일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일' ]
		};
		$.datepicker.setDefaults($.datepicker.regional['ko']);

		var options = {
			showOn: 'button',
			showButtonPanel: false,
			buttonImage: '../assets/images/svg/input-calendar.svg',
			buttonImageOnly: true
		};
		$('.input-month').monthpicker(options);
	}

	function initMobileToggle() {
		$('.button-search').on('click', function() {
			var $this = $(this);
			var target = $('.mobile-toggle-wrap');
			var hasTarget = $this.data('target');
			if (hasTarget) return;
			// if (typeof $this.data('target') !== 'undefined') return;
			target.toggleClass('toggled');
		});

		$('.toggle-search').on('click', function() {
			var $this = $(this);
			var target = $this.data('target');
			$('.' + target).toggleClass('toggled');
		});
	}

	function initTableClick() {
		$('.ul-click .ul-click-li').on('click', function() {
			const target = $(this);
			target.parent().next().slideToggle();
		});
		$('.tr-click').on('click', function() {
			const target = $(this);
			target.next().toggle();
		});
		$('.button-filter').on('click', function() {
			const target = $('.filter-type');
			target.toggleClass('toggled');
		});
		$('.tooltip-info').on('click', function(e) {
			e.preventDefault();
			const target = $(this);
			target.toggleClass('toggled');
		});
	}

	function initTableColspanAuto() {
		$.each($('[class="colspan-auto"]'), function(index, value) {
			var table = $(this).closest('table'); // Get Table
			var siblings = $(this).closest('tr').find('th:visible, td:visible').not('[class="colspan-auto"]').length; // Count colspan siblings
			var numCols = table.find('tr').first().find('th:visible, td:visible').length; // Count visible columns
			$(this).attr('colspan', numCols.toString() - siblings); // Update colspan attribute
		});
	}

	$dom.ready(function() {
		initMenu();
		initCalendar();
		initMobileToggle();
		initTableClick();
		initTableColspanAuto();

		if ($body.hasClass('body-login')) {
			tabList('.tabList', '.tabView', 1, 'click');
		}

		$(window).resize(function() {
			if (this.resizeTO) {
				clearTimeout(this.resizeTO);
			}
			this.resizeTO = setTimeout(function() {
				$(this).trigger('resizeEnd');
			}, 100);
		});

		$(window).on('resizeEnd', function() {
			setTimeout(function() {
				initTableColspanAuto();
			}, 500);
		});

		enquire.register('screen and (max-width: 576px)', {
			match: function() {
				enquireMobile();
			},
			unmatch: function() {
				enquirePc();
			}
		});
	});
})(jQuery);
