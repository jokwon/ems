function numberChk(event) {
    event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if ((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 9) {
        return;
    } else {
        return false;
    }
}

function formatMobile(phoneNum) {
    if(isMobile(phoneNum)) {
        var rtnNum;
        var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
        var myArray;
        if(regExp.test(phoneNum)){
            myArray = regExp.exec(phoneNum);
            rtnNum = myArray[1]+''+myArray[2]+''+myArray[3];
            return rtnNum;
        } else {
            return phoneNum;
        }
    } else {
        return phoneNum;
    }
}

function isMobile(v) {
    var phoneNum = $("#"+v).val();
    var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
    var myArray;
    if(regExp.test(phoneNum)){
        myArray = regExp.exec(phoneNum);
        return true;
    } else {
        alert("모바일 형식이 잘못 되었습니다.");
        $("#"+v).focus();
        return false;
    }
}

function isMobile2(phoneNum,num) {
    var regExp =/(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
    var myArray;
    if(regExp.test(phoneNum)){
        myArray = regExp.exec(phoneNum);
        return true;
    } else {
        alert(num+"번 선생님의 모바일 형식이 잘못 되었습니다.");
        return false;
    }
}

function isEmail(v) {
    var email = $("#"+v).val();
    var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
    if (regExp.test(email)) {
        return true;
    } else {
        alert("이메일 형식이 잘못 되었습니다.");
        $("#"+v).focus();
        return false;
    }
}

function isEmail2(email,num) {
    var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
    if (regExp.test(email)) {
        return true;
    } else {
        alert(num+"번 선생님의 이메일 형식이 잘못 되었습니다.");
        return false;
    }
}

function check_null(v,name){
    if($("#"+v).val() == "")
    {
        alert(name + " 입력하세요");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function check_select(v,name){
    if($("#"+v).val() == "")
    {
        alert(name + " 선택하세요.");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function check_cnt(v,name){
    if($("#"+v).val() <= 0)
    {
        alert(name + " 확인해주세요.");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function check_nullAndSpace(v,name){
    if($.trim($("#"+v).val()) == "")
    {
        alert(name + " 입력하세요");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function check_num(v,name,min,max) {
    var j = $("#"+v).val();
    if(j.length < min || j.length > max) {
        if (min == max) alert(name + ' ' + min + ' 자 이어야 합니다');
        else            alert(name + ' ' + min + ' ~ ' + max + ' 자 이내로 입력하셔야 합니다');
        $("#"+v).focus();
        return false;
    }
    return true;
}

function checkNumber(v,name) {
    var RegExpN = /^[0-9]*$/;
    if(!RegExpN.test($("#"+v).val()))
    {
        alert(name + " 숫자만 입력해주세요.");
        $("#"+v).val('');
        $("#"+v).focus();
        return false;
    }
    return true;
}

function checkEngOrNum(v, name) {
    var RegExp = /^[a-zA-Z0-9_]*$/i; //영문숫자 포함
    if(!RegExp.test($("#"+v).val()))
    {
        alert(name + " 영문과 숫자만 입력해주세요.");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function checkEngAndNum(v, name) {
    var RegExpE = /[a-zA-Z]/i;
    var RegExpN = /[0-9]/;
    if(!RegExpE.test($("#"+v).val()) || !RegExpN.test($("#"+v).val()))
    {
        alert(name + " 영문과 숫자를 조합하여 입력해주세요.");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function checkEngOrNum2(v, name) {
    var RegExp = /^.*(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*+=]).*$/i;
    if(!RegExp.test($("#"+v).val()))
    {
        alert(name + " 영문과 숫자,특수문자를 조합하여 입력해주세요.");
        $("#"+v).focus();
        return false;
    }
    return true;
}


function checkEngAndNum2(v, name) {
    var RegExpE = /[a-zA-Z]/i;
    var RegExpN = /[0-9]/;
    var RegExpS = /[!@#$%^*+=-]/;

    if(!RegExpE.test($("#"+v).val()) || !RegExpN.test($("#"+v).val()) || !RegExpS.test($("#"+v).val()))
    {
        alert(name + " 영문과 숫자,특수문자를 조합하여 입력해주세요.");
        $("#"+v).focus();
        return false;
    }
    return true;
}

function checkSame(v1, v2, name1, name2)
{
    if($("#"+v1).val() != $("#"+v2).val())
    {
        alert(name1 + " " + name2 + " 일치하지 않습니다.");
        $("#"+v2).focus();
        return false;
    }
    return true;
}

function check_checkbox(v,name){

    if($("input:checkbox[id='"+v+"']").is(":checked") == false)
    {
        alert(name);
        $("#"+v).focus();
        return false;
    }
    return true;
}

function login_redirect(msg){

    if( window.is_login != 'on' ) {
        if( msg )
            alert(msg);

        location.replace('/login?redirect='+encodeURIComponent(location.href));

        return false;
    }

    return true;
}

function sendPopup(args, params) {
    var def = {
        'method': 'get',
        'target': 'formPopup'
    };
    params = params || {};

    if( typeof args == 'object' ) {

        jQuery.each(args, function(i, v){
            def[i] = v;
        });

    } else {
        def.action = args;
    }

    if( !def.action ) {
        alert('팝업 URL이 누락 되었습니다.');
    } else {

        var windowPopup = window.open('about:blank', def.target, 'top=0,left=0,width=300,height=200,scrollbars=yes,toolbar=no');
        if( !windowPopup ) {
            alert('팝업차단 해제 후 다시 시도해주세요.');
            windowPopup.close();
        } else {
            jQuery('head', windowPopup.document).append("<title>불러오는중..</title>");
            jQuery('body', windowPopup.document)
                .css({
                    'margin': 0
                    ,'padding': 0
                    ,'overflow': 'hidden'
                })
                .html("<div style='padding:0;margin:0;width:100%;text-align: center;overflow: hidden;line-height:150px;'><h2>로드중..</h2></div>");

            var newForm = jQuery('<form />');
            newForm.attr(def);
            newForm.appendTo('body');

            jQuery.each(params, function(i, v){

                newForm.append('<input type="hidden" name="'+ i +'" class="'+ i +'" value="'+ v +'" />');

            });


            newForm.submit().remove();
        }

        return windowPopup;
    }

    return void(0);
}

function changeSido(target,selv,chv) {
    if( !target )
        return;

    $(target).find("option").remove();
    $(target).prepend("<option value=''>구/군</option>");

    if(selv == "") return;

    for(var i=0;i<const_area[selv].length;i++) {
        var selectedv = "";
        if(const_area[selv][i] == chv) selectedv = "selected";

        $(target).append("<option value='"+const_area[selv][i]+"' "+selectedv+">"+const_area[selv][i]+"</option>");
    }
}

function getSidoList(target, sidov) {
    if( !target )
        return;

    $(target).find("option").remove();
    $(target).prepend("<option value=''>시/도</option>");

    for(var i=0;i<const_area.length;i++) {
        var selectedv = "";

        if(const_area[i] == sidov) selectedv = "selected";

        $(target).append("<option value='"+const_area[i]+"' "+selectedv+">"+const_area[i]+"</option>");
    }
}

window.publicLayer = {
    'useLayer': false,
    'isResize': true,
    'o': '_layer_',
    'v': '_layerOveray_',
    'w': jQuery(window).width()*0.8,
    'h': jQuery(window).height()*0.8,
    'reset': function(){
        jQuery("._layers_").remove();

        this.useLayer = false;
        this.w = jQuery(window).width()*0.8;
        this.h = jQuery(window).width()*0.8;
        this.o = '_layer_';
        this.v = '_layerOveray_';
    },
    'resize': function(){
        if( this.useLayer ) {
            if( this.isResize ) {
                jQuery('.' + this.o).css({
                    'top': ( jQuery(window).scrollTop() + (jQuery(window).height() - parseInt(this.h)) / 2 ) + 'px',
                    'left': ( jQuery(window).width() / 2 ) - ( parseInt(this.w) / 2 ) + 'px'
                });
            }


            jQuery('.'+this.v).css({
                'width': jQuery(document).width()+'px',
                'height': jQuery(document).height()+'px'
            });
        }
    },
    'setCss': function(options){
        if( options )
            jQuery('.'+this.o).css(options);
    },
    'setHtml': function(addHtml){
        jQuery('body').append( '<div class="'+ this.o +' _layers_"></div>' );
        jQuery('body').append( '<div class="'+ this.v +' _layers_"></div>' );

        jQuery('.'+this.v).css({
            'width': jQuery(document).width()+'px',
            'height': jQuery(document).height()+'px',
            'position': 'absolute',
            'top': '0',
            'left': '0',
            'z-index': '100',
            'filter': 'alpha(opacity=50)',
            '-khtml-opacity': '0.5',
            '-moz-opacity': '0.5',
            'opacity': '0.5',
            'background-color': '#000',
            'display': 'none'
        });

        jQuery('.'+this.o).css({
            'top': ( jQuery(window).scrollTop() + (jQuery(window).height() - this.h) / 2 )+'px',
            'left': ( jQuery(window).width() / 2 ) - ( this.w / 2 )+'px',
            'width': this.w,
            'height': this.h,
            'position': 'absolute',
            'padding': '0px',
            'background-color': '#fff',
            'border-radius': '10px',
            '-moz-border-radius': '10px',
            '-webkit-border-radius': '10px',
            'overflow': 'hidden',
            'cursor': 'default',
            'z-index': '101',
            'display': 'none'
        });

        jQuery('.'+this.o).html(addHtml);
    },
    'setSize': function(w, h, resize){
        this.isResize = resize != false ? true : false;
        this.w = w || jQuery(window).width()*0.8;
        this.h = h || jQuery(window).height()*0.8;
    },
    'init': function(){
        this.useLayer = true;
        jQuery('._layers_').show();

        jQuery(window).resize(function(){ publicLayer.resize(); });
        jQuery(window).scroll(function(){ publicLayer.resize(); });
        jQuery(document).on('click touchstart', '.publicLayerCloseBtn', function(e){
            e.preventDefault();
            publicLayer.reset();
        });
    }
};

window.queryString = {
    'setArr' : [],
    'init': function(){
        var URI = location.href.split("?");
        if(URI.length > 1){
            URI = URI[1].split("&");
            for(var i in URI)
                if(URI[i])
                    this.setArr[this.setArr.length] = { key: URI[i].split("=")[0], value: URI[i].split("=")[1] };
        }
    },
    'getParam': function(getKey){
        var getValue = '';
        if(this.setArr.length > 0){
            for(var i in this.setArr)
                if(this.setArr[i].key == getKey)
                    getValue = this.setArr[i].value;
        }

        return getValue;
    }
};

window.delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

