<?

?>
<script type="application/javascript">

    $(document).ready(function() {
        $("#searchTextV").keydown(function (key) {
            if(key.keyCode == 13){
                searchText();
            }
        });
    });

    function selectType(sympoCd, eventStatus, type, privateDesc, symposiumInfo) {
    	if(privateDesc == 'N'){
			alert('관리자가 개인정보 활용 동의서 설정 작업을 완료해야 사전등록 진행이 가능합니다.');return;
        }
        if(symposiumInfo == 'N'){
        	alert('관리자가 신청서 설정 작업을 완료해야 사전등록 진행이 가능합니다.');return;            
        }
    	if(eventStatus == 'Y') {
            alert('사전등록 대기중에는 신청서 작성이 불가능 합니다.');return;
        } else if(eventStatus == 'EI') {
            alert('심포지엄 진행중에는 신청서 작성이 불가능 합니다.');return;
        } else if(eventStatus == 'EI2' || eventStatus == 'EI3') {
            alert('심포지엄 대기중에는 신청서 작성이 불가능 합니다.');return;
    	} else if(eventStatus == 'EN') {
            alert('심포지엄이 종료되어 신청서 작성이 불가능 합니다.');return;
        } else {
            var param = {
                sympoCd: sympoCd,
                type: type
            };

            $.ajax({
                type: 'POST',
                url: '<?=HOME_DIR?>/popup/selectType/',
                data: param,
                async: false,
                success: function(data) {
                     if(data != null) {
                    	 $("#pop_layer2").html(data);
                         $("#modalSelect").modal();
                     }
                }
			});
        }
    }

    var symPage = 1;

    function moreSymposium() {
        symPage += 1;

        var html = '';
        var searchSortVal = $("#searchSortV option:selected").val();
        var listSortVal = $("#listSortV option:selected").val();
        var searchTypeVal = $("#searchTypeV option:selected").val();
        var searchTextVal = $('#searchTextV').val();
        var searchGubunVal = $('#searchGubunVal').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/symposium/symposiumAjax",
            dataType: "json",
            data: {
                page: symPage,
                searchSortVal: searchSortVal,
                listSortVal: listSortVal,
                searchTypeVal : searchTypeVal,
                searchTextVal: searchTextVal,
                searchGubunVal: searchGubunVal
            },
            success: function (data) {
            	if(data.length != 0) {
                    for (var key in data) {
                    	html += '<div class="col-12">';
                        	html += '<div class="card">';
                        		html += '<div class="card-header">';
                        			html += '<div class="card-tag" id="'+data[key]['sympoCd']+'gorupview">';
                        			var groupNames = data[key]['groupNames'];
                        			if(groupNames != null) {
                            			var groupNamesArr = groupNames.split(',');
                            			for (var i = 0; i < groupNamesArr.length; i++) {
                                			if (groupNamesArr[i] != null && groupNamesArr[i] != '') {
                                    			html += '<button type="button" class="tag tag-brand">'+groupNamesArr[i]+'</button>';
                                			}
                            			}
                        			}
                        			html += '</div>';
                        			html += '<div class="card-info">';
                        			if(data[key]['sympoType'] == 'LIVE') {
                            			html += '<button type="button" id="' + data[key]['sympoCd'] + '_sympoType" class="tag tag-live">' + data[key]['sympoType'] + '</span>';
                        			} else {
                            			html += '<button type="button" id="' + data[key]['sympoCd'] + '_sympoType" class="tag tag-demand">' + data[key]['sympoType'] + '</span>';
                        			}
                        			if (data[key]['eventStatus'] != 'EN') {
                            			html += '<button type="button" class="tag tag-count d-none d-md-inline-block">심포지엄 D-'
                            		if(data[key]['dDay'] != '' && data[key]['dDay'] != null) {
                            			html += ''+data[key]['dDay']+'';
                            		} else {
                                		html += 'day';
                            		}
                            			html += '</button>';
                            			html += '<button type="button" class="tag tag-count d-inline-block d-md-none">D-';
                            		if(data[key]['dDay'] != '' && data[key]['dDay'] != null) {
                            			html += ''+data[key]['dDay']+'';
                            		} else {
                                		html += '0';
                            		}
                            			html += '</button>';
                        			}
                        			html += '</div>';
                        		html += '</div>';
                        	html += '<div class="card-body">';
                        	if (data[key]['eventStatus'] == 'EN') {
                            	html += '<div class="tag tag-info end" id="'+data[key]['sympoCd']+'_eventPreTime">사전등록 기간 | '+data[key]['eventPreRegStartDateTime']+'('+data[key]['eventPreRegStartWeek']+') 00:00 ~ '+data[key]['eventPreRegEndDateTime']+'('+data[key]['eventPreRegEndWeek']+') 23:59</div>';
                        	} else {
                        		html += '<div class="tag tag-info ing" id="'+data[key]['sympoCd']+'_eventPreTime">사전등록 기간 | '+data[key]['eventPreRegStartDateTime']+'('+data[key]['eventPreRegStartWeek']+') 00:00 ~ '+data[key]['eventPreRegEndDateTime']+'('+data[key]['eventPreRegEndWeek']+') 23:59</div>';
                        	}
                        			html += '<h4 class="card-body-title"><a href="<?=HOME_DIR?>/symposium/sympo_detail/'+data[key]['sympoCd']+'">'
									if(data[key]['gubun'] == 'N'){
										html += '<button type="button" class="tag tag-end">OFF</button>'
									}
									html += data[key]['eventName']+'</a></h4>';
                        			html += '<small class="small-date">'+data[key]['eventStartDate']+'('+data[key]['eventStartWeek']+') '+data[key]['eventStartTime']+' ~ '+data[key]['eventEndDate']+'('+data[key]['eventEndWeek']+') '+data[key]['eventEndTime']+'<em>|</em>'+data[key]['presenter']+'</small>';
			                        html += '<ul class="ul-icon">';
            			            	html += '<li>';
                        					html += '<a href="<?=HOME_DIR?>/symposium/sympo_enrollment/'+data[key]['sympoCd']+'">';
                        						html += '<dl>';
                        							html += '<dt>';
                        								html += '<figure>';
                        									html += '<i class="demo-icon icon-calendar-check-o"></i>';
                        								html += '</figure>';
                        								html += '사전등록완료';
                        							html += '</dt>';
                        							html += '<dd>'+data[key]['applicantCount']+'</dd>';
                        						html += '</dl>';
                        					html += '</a>';
                        				html += '</li>';
                        				html += '<li>';
                        					html += '<a href="<?=HOME_DIR?>/symposium/sympo_attend_complete/'+ data[key]['sympoCd']+'">';
                        						html += '<dl>';
                        							html += '<dt>';
                        								html += '<figure>';
						                        			html += '<i class="demo-icon icon-envelope-open"></i>';
						                        		html += '</figure>';
                        								html += '패스코드 발급완료';
                        							html += '</dt>';
                        							html += '<dd>'+data[key]['passcodeYcount']+'</dd>';
                        						html += '</dl>';
                        					html += '</a>';
                        				html += '</li>';
                        			html += '</ul>';
                        		html += '</div>';
                        		html += '<div class="card-footer">';
	                        		html += '<div></div><div class="text-center">';
			                        	<?php if($setting['btn3Yn'] == 'Y') {?>
			                        		html += '<button type="button" class="button" onclick="javascript:selectType(\''+data[key]['sympoCd']+'\',\''+data[key]['eventStatus']+'\',\'hcp\',\'';
			                        		if(data[key]['privateDesc'] == null || data[key]['privateDesc']==''){
				                        		html += 'N';
				                        	}else{
												html += 'Y';
											}
											html += '\',\'';
											if(data[key]['symposiumInfo']==null || data[key]['symposiumInfo']==''){
												html += 'N';
											}else{
												html += 'Y';
											}
											html += '\');">방문등록</button>';
			                        	<?php }?>
			                        	<?php if($setting['btn1Yn'] == 'Y') {?>
			                        		html += '<button type="button" class="button" onclick="javascript:selectType(\''+data[key]['sympoCd']+'\',\''+data[key]['eventStatus']+'\',\'psr\',\'';
			                        		if(data[key]['privateDesc'] == null || data[key]['privateDesc']==''){
				                        		html += 'N';
				                        	}else{
												html += 'Y';
											}
											html += '\',\'';
											if(data[key]['symposiumInfo']==null || data[key]['symposiumInfo']==''){
												html += 'N';
											}else{
												html += 'Y';
											}
											html += '\');">예비등록</button>';
			                        	<?php }?>
			                        	html += '</div>';
			                        	<?php if($authority == 'pm' || $authority == 'mcm' || $authority == 'op') { ?>
			                        	html += '<div class="button-setting-wrap">';
			                        		html += '<a href="javascript:sympoMod(\''+data[key]['sympoCd']+'\');" class="button button-setting">setting</a>';
			                        	html += '</div>';
			                        	<?php }?>
		                        html += '</div>';
	                        html += '</div>';
                        html += '</div>';
                    }

                    $('#tbody').append(html);

                    if(data.length < 5) {
                        $('#more').hide();
                    }

                } else {
                    alert('데이터가 없습니다.');
                }
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function searchSort(obj) {
        var val = $(obj).val();
        $('#searchSortVal').val(val);
        $('#form').submit();
    }

    function listSort(obj) {
        var val = $(obj).val();
        $('#listSortVal').val(val);
        $('#form').submit();
    }

    function sympoMod(sympoCd) {
        var param = {
            sympoCd: sympoCd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/symposiumModify/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $("#modalSymposiumSetting").modal();
                 }
            }
       });
    }

    function searchText() {
        var val1 = $('#searchTextV').val();
        $('#searchTextVal').val(val1);
        var val2 = $('#searchTypeV option:selected').val();
        $('#searchTypeVal').val(val2);
        $('#form').submit();
    }

    function searchGubun(val) {
        $('#searchGubunVal').val(val);
        $('#form').submit();
    }

    </script>

<form id="form" action="<?=HOME_DIR?>/symposium/sympo_list" method="get">
    <input type="hidden" id="searchSortVal" name="searchSortVal" value="<?=$searchSortVal?>"/>
    <input type="hidden" id="listSortVal" name="listSortVal" value="<?=$listSortVal?>"/>
    <input type="hidden" id="searchTypeVal" name="searchTypeVal" value="<?=$searchTypeVal?>"/>
    <input type="hidden" id="searchTextVal" name="searchTextVal" value="<?=$searchTextVal?>"/>
    <input type="hidden" id="searchGubunVal" name="searchGubunVal" value="<?=$searchGubunVal?>"/>
</form>

<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
		    <button type="button" class="sidebar-toggle"><span></span></button>
		    <h4 class="title">심포지엄</h4>
		</div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	                <form action="">
	                    <div class="filter-wrap filter-type">
	                        <div class="drawer-wrap">
	                        	<?php if($authority == 'pm' || $authority == 'mcm' || $authority == 'op') {?>
	                            <div class="box-round">
	                                <h5 class="box-title">구분</h5>
	                                <label class="radio"><input type="radio" name="radio1" value="1" onclick="javascript:searchGubun('A');" <?php if($searchGubunVal == 'A') {?> checked <?php }?>><span></span>전체</label>
	                                <label class="radio"><input type="radio" name="radio1" value="2" onclick="javascript:searchGubun('Y');" <?php if($searchGubunVal == 'Y') {?> checked <?php }?>><span></span>ON</label>
	                                <label class="radio"><input type="radio" name="radio1" value="3" onclick="javascript:searchGubun('N');" <?php if($searchGubunVal == 'N') {?> checked <?php }?>><span></span>OFF</label>
	                            </div>
	                            <?php } ?>
	                            <em></em>
	                            <div class="select-wrap">
	                                <select name="searchSortV" id="searchSortV" class="custom-select ml-0 ml-md-2" style="width: auto;" onchange="searchSort(this);">
	                                        <option value="all" <?php if($searchSortVal == 'all') {?>selected<?php }?>>심포지엄 진행상태</option>
	                                        <option value="Y" <?php if($searchSortVal == 'Y') {?>selected<?php }?>>사전등록 대기중</option>
	                                        <option value="I" <?php if($searchSortVal == 'I') {?>selected<?php }?>>사전등록 진행중</option>
	                                        <option value="EI2" <?php if($searchSortVal == 'EI3' || $searchSortVal == 'EI2') {?>selected<?php }?>>심포지엄 대기중</option>
	                                        <option value="EI" <?php if($searchSortVal == 'EI') {?>selected<?php }?>>심포지엄 진행중</option>
	                                        <option value="EN" <?php if($searchSortVal == 'EN') {?>selected<?php }?>>심포지엄 종료</option>
	                                </select>
	                                <select name="listSortV" id="listSortV" class="custom-select ml-0 ml-md-2" style="width: auto;" onchange="listSort(this);">
	                                    <?php if($listSortVal == 'all') {?>
	                                        <option value="all" selected>심포지엄 시작기준</option>
	                                        <option value="desc">시작일 내림차순</option>
	                                        <option value="asc">시작일 오름차순</option>
	                                    <?php } else if($listSortVal == 'desc') {?>
	                                        <option value="all">심포지엄 시작기준</option>
	                                        <option value="desc" selected>시작일 내림차순</option>
	                                        <option value="asc">시작일 오름차순</option>
	                                    <?php } else if($listSortVal == 'asc') {?>
	                                        <option value="all">심포지엄 시작기준</option>
	                                        <option value="desc">시작일 내림차순</option>
	                                        <option value="asc" selected>시작일 오름차순</option>
	                                    <?php }?>	
	                                </select>
	                            </div>
	                        </div>
	                        <div class="search-wrap">
	                            <div class="input-group">
	                            	<div class="input-group-prepend">
                                        <select name="searchTypeV" id="searchTypeV" class="custom-select" style="width: auto;">
                                            <option value="eventName" <?php if($searchTypeVal == 'eventName') { ?> selected <?php }?>>심포지엄명</option>
                                            <option value="brandName" <?php if($searchTypeVal == 'brandName') { ?> selected <?php }?>>브랜드명</option>
                                        </select>
                                    </div>
	                                <input type="text" class="form-control" id="searchTextV" placeholder="검색어를 입력하세요." aria-label="검색어를 입력하세요." aria-describedby="button-addon-search" value="<?=$searchTextVal?>">
	                                <div class="input-group-append">
	                                    <button class="btn" type="button" id="button-addon-search" onclick="javascript:searchText();">검색</button>
	                                </div>
	                            </div>
	                            <button type="button" class="button button-filter"></button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="line-title">총 <?=$sympoCount['count']?>건</h4>
	            </div>
	        </div>
	        <div class="row" id="tbody">
	        	<?php if(count($sympo_list) > 0) {
					foreach ($sympo_list as $sympo_key => $row) { ?>
	                <div class="col-12">
	                    <div class="card">
	                        <div class="card-header">
	                            <div class="card-tag" id="<?=$row['sympoCd']?>gorupview">
	                            	<?php
	                                $groupNamesArr = explode(",", $row['groupNames']);
	                                foreach ($groupNamesArr as $groupName) {
	                                    if($groupName != null && $groupName != '') { ?>
	                                <button type="button" class="tag tag-brand"><?=$groupName?></button>
	                                <?php }
	                    			} ?>
	                            </div>
	                            <div class="card-info">
	                                <?php if($row['sympoType'] == 'LIVE') {?>
	                                	<button type="button" id="<?=$row['sympoCd']?>_sympoType" class="tag tag-live"><?=$row['sympoType']?></button>
	                                <?php } else {?>
	                                	<button type="button" id="<?=$row['sympoCd']?>_sympoType" class="tag tag-demand"><?=$row['sympoType']?></button>
	                                <?php }?>
	                            </div>
	                        </div>
	                        <div class="card-body">
	                    	<?php if($row['eventStatus'] == 'EN') {?>
	                            <div class="tag tag-info end" id="<?=$row['sympoCd']?>_eventPreTime">사전등록 기간 | <?=$row['eventPreRegStartDateTime']?>(<?=$row['eventPreRegStartWeek']?>) 00:00 ~ <?=$row['eventPreRegEndDateTime']?>(<?=$row['eventPreRegEndWeek']?>) 23:59</div>
	                        <?php } else {?>
	                            <div class="tag tag-info ing" id="<?=$row['sympoCd']?>_eventPreTime">사전등록 기간 | <?=$row['eventPreRegStartDateTime']?>(<?=$row['eventPreRegStartWeek']?>) 00:00 ~ <?=$row['eventPreRegEndDateTime']?>(<?=$row['eventPreRegEndWeek']?>) 23:59</div>
	                        <?php } ?>
	                            <h4 class="card-body-title"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$row['sympoCd']?>"><?php if($row['gubun'] == 'N') {?><button type="button" class="tag tag-end">OFF</button><?php }?>  <?=$row['eventName']?></a></h4>
	                            <small class="small-date"><?=$row['eventStartDate']?>(<?=$row['eventStartWeek']?>) <?=$row['eventStartTime']?> ~ <?=$row['eventEndDate']?>(<?=$row['eventEndWeek']?>) <?=$row['eventEndTime']?> <em>|</em> <?=$row['presenter']?></small>
	                            <ul class="ul-icon">
	                                <li>
	                                    <a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$row['sympoCd']?>">
	                                        <dl>
	                                            <dt>
	                                                <figure>
	                                                    <i class="demo-icon icon-calendar-check-o"></i>
	                                                </figure>
	                                                사전등록완료
	                                            </dt>
	                                            <dd><?=$row['applicantCount']?></dd>
	                                        </dl>
	                                    </a>
	                                </li>
	                                <li>
	                                    <a href="<?=HOME_DIR?>/symposium/sympo_attend_complete/<?=$row['sympoCd']?>">
	                                        <dl>
	                                            <dt>
	                                                <figure>
	                                                    <i class="demo-icon icon-envelope-open"></i>
	                                                </figure>
	                                                패스코드 발급완료
	                                            </dt>
	                                            <dd><?=$row['passcodeYcount']?></dd>
	                                        </dl>
	                                    </a>
	                                </li>
	                            </ul>
	                        </div>
	
	                        <div class="card-footer">
	                        	<?php if($authority == 'pm' || $authority == 'mcm' || $authority == 'op') {?>
	                            <div></div>
	                            <?php }?>
	                            <div class="text-center">
	                            <?php if($row['btn3Yn'] == 'Y') {?>
	                                <button type="button" class="button" onclick="javascript:selectType('<?=$row['sympoCd']?>', '<?=$row['eventStatus']?>','hcp','<?php if($row['privateDesc']==null || $row['privateDesc']==''){?>N<?php }else{?>Y<?php }?>','<?php if($row['symposiumInfo']==null || $row['symposiumInfo']==''){?>N<?php }else{?>Y<?php }?>');">방문등록</button>
	                            <?php } ?>
	                    		<?php if($row['btn1Yn'] == 'Y') {?>
	                                <button type="button" class="button" onclick="javascript:selectType('<?=$row['sympoCd']?>', '<?=$row['eventStatus']?>','psr','<?php if($row['privateDesc']==null || $row['privateDesc']==''){?>N<?php }else{?>Y<?php }?>','<?php if($row['symposiumInfo']==null || $row['symposiumInfo']==''){?>N<?php }else{?>Y<?php }?>');">예비등록</button>
	                            <?php } ?>
	                            </div>
	                            <?php if($authority == 'pm' || $authority == 'mcm' || $authority == 'op') {?>
	                            <div class="button-setting-wrap">
	                                <a href="javascript:sympoMod('<?=$row['sympoCd']?>');" class="button button-setting">setting</a>
	                            </div>
	                            <?php }?>
	                        </div>
	                    </div>
	                </div>
	            <?php  }
	    		} else { ?>
	    		<div class="col-12">
                    <div class="card">
                    	<div class="card-body">
                    		<p style="text-align:center; margin-top: 20px;">심포지엄이 없습니다.</p>
	                   	</div>
	            	</div>
	            </div>
	    		<?php }?>
	        </div>
	        <?php if(count($sympo_list) >= 5) {?>
	    	<div class="row">
				<div class="col-12 text-center">
					<a href="javascript:moreSymposium();" class="button gray" id="more">리스트 추가 보기</a>
				</div>
			</div>
			<?php }?>
	    </div>
	</section>
</main>