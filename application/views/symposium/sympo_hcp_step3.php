<?php 

?>
<body id="body" class="body site pre-registration page-default is-header" aria-expanded="false">
	<script src="<?=HOME_DIR?>/js/vendor/signature_pad.min.js"></script>
	<script>

	    $(document).ready(function() {
	    	// signature
	        const canvas = document.getElementById('signature-pad');
	        let signaturePad = null;
	
	        $('#next').click(function() {
	            var cnt = $('input:checkbox:checked').length;
	            var $target = $('.form-error-msg');
	            if (cnt != 1) {
	                $target.fadeIn();
	                setTimeout(function() {
	                    $target.fadeOut();
	                }, 3000);
	                return false;
	            } else {
	                if (signaturePad == null) {
	                    $('#modalSignature').modal('show');
	                } else {
	                    return true;
	                }
	                return false;
	            }
	        });
	        $('input[type="checkbox"]').on('click', function() {
	            var $target = $('.form-error-msg');
	            $target.hide();
	        });

	
	        function resizeCanvas() {
	            console.log('canvas resize');
	            // When zoomed out to less than 100%, for some very strange reason,
	            // some browsers report devicePixelRatio as less than 1
	            // and only part of the canvas is cleared then.
	            var ratio = Math.max(window.devicePixelRatio || 1, 1);
	            canvas.width = canvas.offsetWidth * ratio;
	            canvas.height = canvas.offsetHeight * ratio;
	            canvas.getContext("2d").scale(ratio, ratio);
	        }
	
	        function drawSignature() {
	            signaturePad = new SignaturePad(canvas);
	        }
	
	        window.onresize = resizeCanvas;
	        // resizeCanvas();
	        document.getElementById('undo').addEventListener('click', function() {
	            signaturePad.clear();
	        });
	
	
	        $('button[data-dismiss="modal"]').on('click', function() {
	            signaturePad = null;
	        });
	        $('#modalSignature').on('shown.bs.modal', function(e) {
	            resizeCanvas();
	            drawSignature();
	        })
	
	        document.getElementById('check-sign').addEventListener('click', function() {
	            if (signaturePad.isEmpty()) {
	                var $errorMsg = $('.error-sign');
	                $errorMsg.show();
	                setTimeout(function() {
	                    $errorMsg.hide();
	                }, 1000);
	            } else {
	                var data = signaturePad.toDataURL('image/png');
	                // save signature
	                $('#canvas_img').val(data);
	                alert('서명되었습니다.');
	                $('#modalSignature').modal('hide');

	                var invitation = $('#invitation').val();

	                $.ajax({
	                    type : 'post',
	                    url : '<?=HOME_DIR?>/symposium/getCheckInvitation/',
	                    data : {
							invitation : invitation
						},
	                    dataType : 'json',
	                    success : function(result){

		                    if(result == 0){
		                    	var queryString = $("form[name=hcpform]").serialize() ;
		    	                $.ajax({
		    	                    type : 'post',
		    	                    url : '<?=HOME_DIR?>/symposium/hcp_step4/',
		    	                    data : queryString,
		    	                    dataType : 'json',
		    	                    success : function(result){
		    	                    	$('#applicant_cdL').val(result['applicant_cd']);
		    	                    	$("#hcpCompleteForm").submit();
		    	                    	
		    	                    },
		    	                });
		                    }else {
		                    	alert('만료된 초대장입니다.');
		                    	window.history.forward();
		                    }
	                    }
	                });
	            }
	        });
	    });
	
	    function prev(){
	
	    	 $("#hcpform").attr("action", "<?=HOME_DIR?>/symposium/hcp_step2/");
	    		
			 $("#hcpform").submit();
	    }
	
	</script>
    <div id="page" class="page" aria-expanded="false">
        <div id="bg-over"></div>
        <header id="masthead" class="site-header" aria-expanded="false">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="site-header-wrap">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <nav id="site-navigation" class="main-navigation">
                                            <div class="menu-primary-container">
                                                <ul id="primary-top" class="primary-top nav-menu" aria-expanded="false">
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#" class="main-menu-item menu-link href-empty">개인정보 수집 및 활용 동의서</a>
                                                        <ul class="nav-menu-sub">
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:privacyPop();" class="sub-menu-item menu-link">개인정보 수집 및 이용에 대한 동의</a>
                                                            </li>
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:termPop();" class="sub-menu-item menu-link">제 3자 제공에 대한 동의</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <h2>사전등록 신청서</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="ul-sign-step">
                            <li class="item">
                                <span class="is-pc">01. 참석자 기본 정보</span>
                                <span class="is-mobile">01</span>
                            </li>
                            <li class="item active">
                                <span class="is-pc">02. 개인정보 처리동의</span>
                                <span class="is-mobile">02</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <form method="post" id="hcpCompleteForm" name="hcpCompleteForm" action="<?=HOME_DIR?>/symposium/hcp_complete/">
					<input type="hidden" id="applicant_cdL" name="applicant_cdL" value=""/>
					<input type="hidden" id="applicant_nameL" name="applicant_nameL" value="<?=$applicant_name?>"/>
					<input type="hidden" id="sympo_cdL" name="sympo_cdL" value="<?=$sympo_cd?>"/>
					<input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
				</form>
				<form method="post" id="hcpform" name="hcpform" action="">
					<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
					<input type="hidden" id="applicant_name" name="applicant_name" value="<?=$applicant_name?>"/>
					<input type="hidden" id="applicant_hospital" name="applicant_hospital" value="<?=$applicant_hospital?>"/>
					<input type="hidden" id="applicant_dep" name="applicant_dep" value="<?=$applicant_dep?>"/>
					<input type="hidden" id="applicant_hp" name="applicant_hp" value="<?=$applicant_hp?>"/>
					<input type="hidden" id="applicant_email" name="applicant_email" value="<?=$applicant_email?>"/>
					<input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
					<input type="hidden" id="canvas_img" name="canvas_img" value=""/>
                    <div class="row mb-5">
                        <div class="col-12">
                            <section class="privacy">
                                <h3>2. 개인정보 및 고유식별정보의 제 3자 제공에 대한 동의</h3>
                            </section>
                            <section class="privacy">
                                <figure class="mb-0">
                                    <img src="<?=HOME_DIR?><?=$setting['private_img']?>" alt="개인정보 수집 동의">
                                </figure>
                            </section>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-12">
                            <label class="checkbox"><input type="checkbox" class="cv" name="c1"><span></span> 개인정보의 <strong>제3자 제공</strong>에 대한 설명을 모두 이해하고, 이에 동의하십니까?</label>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-12">
                            <div class="form-error-msg">
                                <i class="icon-demo icon-info-circled"></i>개인정보 제 3자 제공 동의에 체크 해주셔야 합니다.
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <ul class="ul-button-wrap">
                                <li><a href="javascript:prev();" class="button button-black-outline">이전단계</a></li>
                            	<li><button id="next" class="button button-default">다음단계</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	    <div class="modal fade signature" id="modalSignature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered modal-md">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <div>
	                        <h2 class="modal-title">서명입력</h2>
	                    </div>
	                </div>
	                <div class="modal-body">
	                    <div class="wrapper">
	                        <button id="undo" type="button" class="button sign-undo"></button>
	                        <canvas id="signature-pad" class="signature-pad">
	                        </canvas>
	                        <div class="error-sign">
	                            <div class="msg">
	                                서명후 진행하세요.
	                            </div>
	                        </div>
	                    </div>
	                    <small><i class="demo-icon icon-info-circled"></i> 서명 후 '확인'을 누르시면 사전등록 신청이 완료됩니다.</small>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="button button-black-outline" data-dismiss="modal" aria-label="Close">취소</button>
	                    <button type="button" class="button button-default" id="check-sign">확인</button>
	                </div>
	            </div>
	        </div>
	    </div>
	
	    <!-- https://github.com/szimek/signature_pad -->
	    