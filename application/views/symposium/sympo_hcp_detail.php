<?php

?>

<body id="body" class="body site pre-registration page-default is-header" aria-expanded="false">
	<script>
	    function goHome(){
	    	$("#hcpform").attr("action", "<?=HOME_DIR?>/symposium/hcp_home/");
		    $("#hcpform").submit();
	    }
	</script>
    <div id="page" class="page" aria-expanded="false">
        <div id="bg-over"></div>
		<header id="masthead" class="site-header" aria-expanded="false">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="site-header-wrap">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <nav id="site-navigation" class="main-navigation">
                                            <div class="menu-primary-container">
                                                <ul id="primary-top" class="primary-top nav-menu" aria-expanded="false">
                                                    <li class="main-menu-item menu-item">
                                                        <a href="javascript:goHome();" class="main-menu-item menu-link">HOME</a>
                                                    </li>
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#" class="main-menu-item menu-link href-empty">개인정보 수집 및 활용 동의서</a>
                                                        <ul class="nav-menu-sub">
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:privacyPop();" class="sub-menu-item menu-link">개인정보 수집 및 이용에 대한 동의</a>
                                                            </li>
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:termPop();" class="sub-menu-item menu-link">제 3자 제공에 대한 동의</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <h2>사전등록 신청서</h2>
                        <p class="lead">등록내용 확인</p>
                    </div>
                </div>
                <form id="hcpform" name="hcpform" action="" method="post">
					<input type="hidden" id="applicant_cd" name="applicant_cd" value="<?=$applicant_cd?>"/>
				    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
				    <input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
				</form>
                <div class="row">
                    <div class="col-12">
                        <h5 class="font-color-deep-red">참석자 기본 정보</h5>
                        <dl class="dl-list form-registration">
                            <dt>성명</dt>
                            <dd><?= $applicant['applicantName']?></dd>
                            <dt>병원명</dt>
                            <dd><?= $applicant['applicantHospital']?></dd>
                            <dt>진료과</dt>
                            <dd><?php if($applicant['applicantDep'] != null && $applicant['applicantDep'] != ''){ ?><?= $applicant['applicantDep']?><?php }else{?>-<?php }?></dd>
                            <dt>모바일</dt>
                            <dd><?= $applicant['applicantHp']?></dd>
                            <dt>이메일</dt>
                            <dd><?php if($applicant['applicantEmail'] != null && $applicant['applicantEmail'] != ''){ ?><?= $applicant['applicantEmail']?><?php }else{?>-<?php }?></dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                   		<button type="button" class="button" onclick="javascript:goHome();">확인</button>
                    </div>
                </div>
            </div>
        </div>