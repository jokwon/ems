<?php

?>
<script type="application/javascript">

	$(document).ready(function() {

		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
		
	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(0);
	    swiper.el.childNodes[1].children[0].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });
	});

	function selectType(sympoCd, eventStatus, type, privateDesc, symposiumInfo) {
        if(privateDesc == 'N'){
			alert('관리자가 개인정보 활용 동의서 설정 작업을 완료해야 사전등록 진행이 가능합니다.');return;
        }
        if(symposiumInfo == 'N'){
        	alert('관리자가 신청서 설정 작업을 완료해야 사전등록 진행이 가능합니다.');return;            
        }
		if(eventStatus == 'Y') {
            alert('사전등록 대기중에는 신청서 작성이 불가능 합니다.');return;
        } else if(eventStatus == 'EI') {
            alert('심포지엄 진행중에는 신청서 작성이 불가능 합니다.');return;
        } else if(eventStatus == 'EI2' || eventStatus == 'EI3') {
            alert('심포지엄 대기중에는 신청서 작성이 불가능 합니다.');return;
    	} else if(eventStatus == 'EN') {
            alert('심포지엄이 종료되어 신청서 작성이 불가능 합니다.');return;
        } else {
            var param = {
                sympoCd: sympoCd,
                type: type
            };

            $.ajax({
                type: 'POST',
                url: '<?=HOME_DIR?>/popup/selectType/',
                data: param,
                async: false,
                success: function(data) {
                     if(data != null) {
                    	 $("#pop_layer2").html(data);
                         $("#modalSelect").modal();
                     }
                }
			});
        }
    }

    function sympoMod(sympoCd) {
        var param = {
            sympoCd: sympoCd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/symposiumModify/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $("#modalSymposiumSetting").modal();
                 }
            }
		});

    }
</script>
    
<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
		    <button type="button" class="sidebar-toggle"><span></span></button>
		    <h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12" id="<?=$symposium['sympoCd']?>gorupview">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="card">
	                    <div class="card-body">
	                        <div class="p-0 p-md-4">
	                            <h5 class="font-color-blue">심포지엄 상세정보</h5>
	                            <dl class="dl-info">
	                                <dt>심포지엄 상태</dt>
	                                <?php if($symposium['eventStatus'] == 'I') {?>
	                                	<dd id="<?=$symposium['sympoCd']?>_sympoStatus">사전등록 진행중</dd>
	                                <?php } else if($symposium['eventStatus'] == 'Y') {?>
	                                	<dd id="<?=$symposium['sympoCd']?>_sympoStatus">사전등록 대기중</dd>
	                                <?php } else if($symposium['eventStatus'] == 'EI') {?>
	                                	<dd id="<?=$symposium['sympoCd']?>_sympoStatus">심포지엄 진행중</dd>
	                                <?php } else if($symposium['eventStatus'] == 'EI3' || $symposium['eventStatus'] == 'EI2') {?>
                                		<dd id="<?=$symposium['sympoCd']?>_sympoStatus">심포지엄 진행중</sdd>
	                                <?php } else if($symposium['eventStatus'] == 'EN') {?>
	                                	<dd id="<?=$symposium['sympoCd']?>_sympoStatus">심포지엄 종료</dd>
	                                <?php }?>
	                                <dt>심포지엄 진행방식</dt>
	                                <dd><?=$symposium['sympoType']?></dd>
	                                <dt>심포지엄 URL</dt>
	                                <dd><a href="<?=$symposium['domain']?>" target="_blank"><?=$symposium['domain']?></a></dd>
	                                <dt>심포지엄 진행일정</dt>
	                                <dd><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?></dd>
	                                <dt>사전등록 기간</dt>
	                                <dd id="<?=$symposium['sympoCd']?>_eventPreTime"><?php if($symposium['eventPreRegStartDateTime'] != null && $symposium['eventPreRegStartDateTime'] != ''){?><?=$symposium['eventPreRegStartDateTime']?>(<?=$symposium['eventPreRegStartWeek']?>) 00:00 ~ <?=$symposium['eventPreRegEndDateTime']?>(<?=$symposium['eventPreRegEndWeek']?>) 23:59<?php } else {?> -<?php }?></dd>
	                                <dt>심포지엄 발표자</dt>
	                                <dd class="mb-4" id="<?=$symposium['sympoCd']?>_location"><?php if($symposium['presenter'] != null && $symposium['presenter'] != ''){?><?=$symposium['presenter']?><?php } else {?> -<?php }?></dd>
	                                <dt>사전등록완료</dt>
	                                <dd><?=$symposium['applicantCount']?></dd>
	                                <dt>패스코드 발급완료</dt>
	                                <dd><?=$symposium['passcodeYcount']?></dd>
	                               
	                            </dl>
	                        </div>
	                    </div>
	                    <div class="card-footer">
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                            <div></div>
	                        <?php }?>
	                        <div class="text-center">
	                        	<?php if($symposium['btn3Yn'] == 'Y') {?>
	                        		<button type="button" class="button" onclick="javascript:selectType('<?=$symposium['sympoCd']?>', '<?=$symposium['eventStatus']?>','hcp','<?php if($symposium['privateDesc']==null || $symposium['privateDesc']==''){?>N<?php }else{?>Y<?php }?>','<?php if($symposium['symposiumInfo']==null || $symposium['symposiumInfo']==''){?>N<?php }else{?>Y<?php }?>');">방문등록</button>
	                            <?php }?>
	                            <?php if($symposium['btn1Yn'] == 'Y') {?>
	                            	<button type="button" class="button" onclick="javascript:selectType('<?=$symposium['sympoCd']?>', '<?=$symposium['eventStatus']?>','psr','<?php if($symposium['privateDesc']==null || $symposium['privateDesc']==''){?>N<?php }else{?>Y<?php }?>','<?php if($symposium['symposiumInfo']==null || $symposium['symposiumInfo']==''){?>N<?php }else{?>Y<?php }?>');">예비등록</button>
	                            <?php }?>
	                        </div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                        <div class="button-setting-wrap">
	                            <a href="javascript:sympoMod('<?=$symposium['sympoCd']?>');" class="button button-setting">setting</a>
	                        </div>
	                        <?php }?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>