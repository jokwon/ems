<?php

?>
<script type="application/javascript">

	var checkBoxes ="";

	$(document).ready(function() {

		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
		
	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(2);
	    swiper.el.childNodes[1].children[2].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });
	
	    checkBoxes = document.querySelectorAll("input[class=cv]");
	
	    $('.input-date').datepicker({
	        maxDate: "0",
	        showOn: 'button',
	        showButtonPanel: false,
	        buttonImage: '<?=HOME_DIR?>/images/icon/calendar.svg',
	        buttonImageOnly: true,
	        showMonthAfterYear: true,
	        dateFormat: 'yy-mm-dd',
	    });
	});

	function allChk() {
        if($("#all").prop("checked")){
            $("input[name=c]").prop("checked",true);
            checkboxCount();
        }else{
            $("input[name=c]").prop("checked",false);
            checkboxCount();
        }
    }

	function checkboxCount()
    {
    	var cnt = 0;
        var target = document.querySelector('.div-table-footer');
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked === true) {
                cnt++;
            }
        }
        (cnt > 0) ? target.classList.add('active'): target.classList.remove('active');
    }

	function sort(sortWhere, sortType) {
        $('#searchText').val($('#sText').val());
        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);
        $('#symform').submit();
    }

    function dupleInspection(type,dupleType) {
        $('#dupleInspec').val(type);
        $('#dupleInspecType').val(dupleType);
        $('#symform').submit();
    }

    function searchTextgo() {

        var scRegDtSt = $('#scRegDtSt').val();
        var scRegDtEd = $('#scRegDtEd').val();

        if(scRegDtSt == '' && scRegDtEd == '') {

        } else {
            if(scRegDtSt == '' || scRegDtEd == '') {
                if(scRegDtSt == '') {
                    alert('시작일을 입력하세요');
                    $('#scRegDtSt').focus();
                    return;
                }
                if(scRegDtEd == '') {
                    alert('종료일을 입력하세요');
                    $('#scRegDtEd').focus();
                    return;
                }
            }
        }

        $('#dupleInspec').val('');
        $('#symform').submit();
    }

    function dateCheck(inThis){

	    if($("#scRegDtSt").val()!="" && $("#scRegDtEd").val()!=""){
	        if($("#scRegDtSt").val() > $("#scRegDtEd").val()){
	            alert("검색 시작 날짜는 마지막날짜 보다 작아야 합니다.");
	            inThis.value="";
	            inThis.focus();
	        }
	    }
	
	    var eventEndDate = '<?=$symposium['eventEndDate']?>';
	    if(eventEndDate.replace(/\./g, '-') <= $("#scRegDtEd").val() || eventEndDate.replace(/\./g, '-') <= $("#scRegDtSt").val()) {
	        alert("검색 날짜는 심포지엄 진행 마지막 날짜 보다 작아야 합니다.");
	        inThis.value="";
	        inThis.focus();
	    }
	}
    
    function excelDown(obj) {
    	var sympoCd 		= '<?=$sympoCd?>';
    	var fileType 		= $(obj).val();
        var scRegDtSt 		= $('#scRegDtSt').val();
        var scRegDtEd 		= $('#scRegDtEd').val();
        var sortType 		= $('#sortType').val();
        var sortWhere 		= $('#sortWhere').val();
        var searchType 		= $('#searchType').val();
        var searchText 		= $('#searchText').val();
        var dupleInspecType	= $('#dupleInspecType').val();
        var dupleInspec 	= $('#dupleInspec').val();
        var passcode 			= '';
        var cancel 			= 'Y';
        var passcodeType 			= 'cancel';

        var param = {
	        sympoCd: sympoCd,
	        fileType: fileType,
	        scRegDtSt: scRegDtSt,
	        scRegDtEd: scRegDtEd,
	        sortType: sortType,
	        sortWhere: sortWhere,
	        searchType: searchType,
	        searchText: searchText,
	        dupleInspecType: dupleInspecType,
	        dupleInspec: dupleInspec,
	        passcode: passcode,
	        cancel: cancel,
	        passcodeType: passcodeType
        };

        if(fileType != '') {
            $.post('<?=HOME_DIR?>/symposium/applicantExcelAjax/', param, function (data) {
                window.location = data;
            });
        }
    }

    var page = 1;
    function moreBtn() {
        page += 1;

        var html = '';
        var searchType = $('#searchType').val();
        var searchText = $('#searchText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();
        var scRegDtSt = $('#scRegDtSt').val();
        var scRegDtEd = $('#scRegDtEd').val();
        var dupleInspec = $('#dupleInspec').val();
        var dupleInspecType = $('#dupleInspecType').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/symposium/sympo_attend_cancelAjax",
            dataType: "json",
            data: {
                page: page,
                sympoCd: '<?=$sympoCd?>',
                searchType: searchType,
                searchText: searchText,
                sortWhere: sortWhere,
                sortType: sortType,
                scRegDtSt: scRegDtSt,
                scRegDtEd: scRegDtEd,
                dupleInspec: dupleInspec,
                dupleInspecType: dupleInspecType
            },
            success: function (data) {
                if(data.length != 0) {

                	var dupleHp = $('#tbody > tr:nth-last-child(1) > td:nth-child(7)').text();
                    var dupleEmail = $('#tbody > tr:nth-last-child(1) > td:nth-child(11)').text();
                    var dupleInspec = '<?=$dupleInspec?>';
                    var dupleInspecType = '<?=$dupleInspecType?>';
                    var prevdupleCount = 0;

                    for (var key in data) {

                    	if(dupleInspecType == 'applicant_hp')
                        {
	                    	if (dupleInspec == 'Y' && data[key]['hpDupleCount'] >= 2 && dupleHp != '' && dupleHp != data[key]['applicantHp']) {
	                            dupleHp = data[key]['applicantHp'];
	                            prevdupleCount = data[key]['hpDupleCount'];
	                            html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                        } else {
	                            if (dupleInspec == 'Y' && key != 0 && prevdupleCount != data[key]['hpDupleCount']) {
	                            	html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                            }
	                            dupleHp = data[key]['applicantHp'];
	                            prevdupleCount = data[key]['hpDupleCount'];
	                        }
	
	                    	if (dupleInspec == 'Y' && data[key]['hpDupleCount'] >= 2) {
	                    	html += '<tr class="tbody-overlap">';
	                        } else {
	                        html += '<tr>';
	                        }
                        }else if(dupleInspecType == 'applicant_email') {
                        	if (dupleInspec == 'Y' && data[key]['emailDupleCount'] >= 2 && dupleEmail != '' && dupleEmail.toLowerCase() != data[key]['applicantEmail'].toLowerCase()) {
                        		dupleEmail = data[key]['applicantEmail'];
	                            prevdupleCount = data[key]['emailDupleCount'];
	                            html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                        } else {
	                            if (dupleInspec == 'Y' && key != 0 && prevdupleCount != data[key]['emailDupleCount']) {
	                            	html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                            }
	                            dupleEmail = data[key]['applicantEmail'];
	                            prevdupleCount = data[key]['emailDupleCount'];
	                        }
	
	                    	if (dupleInspec == 'Y' && data[key]['emailDupleCount'] >= 2) {
	                    	html += '<tr class="tbody-overlap">';
	                        } else {
	                        html += '<tr>';
	                        }
                        } else {
							html += '<tr>';
                        }
                    	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
                        	html += '<td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="'+data[key]['applicantCd']+'" onchange="checkboxCount();"><span></span></label></td>';
                        <?php } ?>
                        html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">';
                        if(data[key]['passcode'] == 'N') {
	                        html += '<em class="point point-red mr-2"></em>미발급';
	                    } else {
	                    	html += '<em class="point point-green mr-2"></em>발급';
	                    }                        
                        html += '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd'] +'\');">'
                        if(data[key]['regiComplete'] == 'N'){
                        	html += 'N'
                        }else if(data[key]['regiComplete'] == 'Y'){
                        	html += 'Y'
                        }
						html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'+data[key]['applicantName'];
                        if(data[key]['comment'] != null && data[key]['comment'] != '') {
	                        html += '<i class="demo-icon icon-commenting-o"></i>';
	                    }
	                    html += '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'+data[key]['applicantHospital']+'</td>';
                        html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">';
						if(data[key]['applicantHp'] != null && data[key]['applicantHp'] != "")
						{
							html += data[key]['applicantHp']; 
						}else {
							html += '-';
						}
						html += '</td>';
						html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd']+'\');">';
						if(data[key]['officeName'] != null && data[key]['officeName'] != "")
						{
							html += data[key]['officeName']; 
						}else {
							html += '-';
						}
						html += '</td>';
						html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'+data[key]['creatorName']+' '+data[key]['creatorAuthority'].toUpperCase()+'</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['canceldate']+'\');">'+ data[key]['canceldate']+'</td>';
    					html += '<td style="display: none;">'+data[key]['applicantEmail']+'</td>';
                        html += '</tr>';
                    }

                    $('#tbody').append(html);

                    if(data.length < 10) {
                        $('#more').hide();
                    }

                } else {
                    alert('데이터가 없습니다.');
                }

                checkBoxes = document.querySelectorAll("input[class=cv]");
                
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function drawerDown(applicantCd) {
        
        var param = {
        		applicantCd: applicantCd,
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/applicantDetail/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                	 $('#modalDetail').modal();
                 }
            }
		});
    }

    function passcodeDelete(sympoCd) {

        var applicantCds = '';
        $("input[name=c]:checked").each(function() {
            applicantCds += $(this).val() + ';';
        });

        var param = {
            sympoCd: sympoCd,
            applicantCds: applicantCds,
        };

        if (applicantCds != null && applicantCds != '') {
        	$.ajax({
                type: 'POST',
                url: '<?=HOME_DIR?>/popup/passcodeDelete/',
                data: param,
                async: false,
                success: function(data) {
                     if(data != null) {
                    	 $("#pop_layer3").html(data);
                         $('#modalSend').modal();
                     }
                }
        	});
        } else {
            alert('선택 대상이 없습니다.');
        }
    }

    function passcodeRestore(sympoCd) {

        var applicantCds = '';
        $("input[name=c]:checked").each(function() {
            applicantCds += $(this).val() + ';';
        });

        var param = {
            sympoCd: sympoCd,
            applicantCds: applicantCds,
        };

        if (applicantCds != null && applicantCds != '') {
        	$.ajax({
                type: 'POST',
                url: '<?=HOME_DIR?>/popup/passcodeRestore/',
                data: param,
                async: false,
                success: function(data) {
                     if(data != null) {
                    	 $("#pop_layer3").html(data);
                         $('#modalSend').modal();
                     }
                }
        	});
        } else {
            alert('선택 대상이 없습니다.');
        }
    }
    function back(){
        window.location.href = "<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>";
    }
</script>

<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="export-wrap">
	                    <div class="d-flex justify-content-between w-100">
	                    	<div class="d-inline-flex align-items-center">
	                    		<button class="button button-back" onclick="back()"><i class="demo-icon icon-left"></i></button>
                                <h5 class="title d-inline mr-1">참가취소 <span class="font-color-red"><?=$attentcount['cnt']?></span>명</h5>
                                <h6 class="font-weight-normal d-inline-flex mr-2 mb-0">(<span class="d-none d-md-inline-block">모바일</span><span class="d-inline-block d-md-none mr-1">M</span><span class="font-color-red"><?=$dupleHpCount['cnt']?></span>|<span class="d-none d-md-inline-block">이메일</span><span class="d-inline-block d-md-none mr-1">E</span><span class="font-color-red"><?=$dupleEmailCount['cnt']?></span>)</h6>
                                <button type="button" class="button button-mid muted d-none d-sm-inline-block mr-1" onclick="javascript:dupleInspection('Y','applicant_hp');">모바일중복</button>
                                <button type="button" class="button button-mid muted d-none d-sm-inline-block" onclick="javascript:dupleInspection('Y','applicant_email');">이메일중복</button>
                            </div>
                            <div class="d-inline-flex d-sm-none position-relative">
                            	<button type="button" class="button button-mid muted mr-1 d-inline-block d-sm-none menu-toggle-overlap">중복</button>
                                <nav class="more-menu-overlap">
                                	<ul>
                                    	<li class="item"><a href="#" class="link small" onclick="javascript:dupleInspection('Y','applicant_hp');">모바일 중복</a></li>
                                        <li class="item"><a href="#" class="link small" onclick="javascript:dupleInspection('Y','applicant_email');">이메일 중복</a></li>
                                    </ul>
                                </nav>
                                <button type="button" class="button button-search"></button>
                            </div>
	                    </div>
	                    <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                    <div class="export-select-wrap">
	                        <select class="custom-select" style="width: auto;" onchange="excelDown(this);">
	                            <option value="">EXPORT</option>
	                            <option value="csv">CSV</option>
	                            <option value="excel">EXCEL</option>
	                        </select>
	                    </div>
	                    <?php }?>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px mobile-toggle-wrap">
	            <div class="col-12">
	                <form id="symform" name="symform" action="<?=HOME_DIR?>/symposium/sympo_attend_cancel/<?=$symposium['sympoCd']?>" method="post">
		                <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$symposium['sympoCd']?>"/>
		                <input type="hidden" id="applicantCds" name="applicantCds" value=""/>
		                <input type="hidden" id="sortType" name="sortType" value="<?=$sortType?>"/>
		                <input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere?>"/>
		                <input type="hidden" id="dupleInspec" name="dupleInspec" value="<?=$dupleInspec?>"/>
		                <input type="hidden" id="dupleInspecType" name="dupleInspecType" value="<?=$dupleInspecType?>"/>
	                    <div class="filter-wrap md-column">
	                        <div>
	                            <div class="box-round">
	                                <h5 class="box-title">등록기간</h5>
	                                <div class="div-date" id="dateStart">
	                                    <input type="text" id="scRegDtSt" name="scRegDtSt" class="input-date" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$scRegDtSt?>">
	                                </div>
	                                <div class="div-date mx-1" id="blockAnd">~</div>
	                                <div class="div-date" id="dateEnd">
	                                    <input type="text" id="scRegDtEd" name="scRegDtEd" class="input-date" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$scRegDtEd?>">
	                                </div>
	                            </div>
	                        </div>
	                        <div>
	                            <div class="input-group search-wrap">
	                                <div class="input-group-prepend">
	                                    <select name="searchType" id="searchType" class="custom-select" style="width: auto;">
	                                        <option value="applicant_name" <?php if($searchType == 'applicant_name') { ?> selected <?php }?>>성명</option>
		                                    <option value="applicant_hospital" <?php if($searchType == 'applicant_hospital') { ?> selected <?php }?>>병원명</option>
		                                    <option value="applicant_hp" <?php if($searchType == 'applicant_hp') { ?> selected <?php }?>>모바일</option>
		                                    <option value="office_name" <?php if($searchType == 'office_name') { ?> selected <?php }?>>담당 영업소</option>
		                                    <option value="creator_name" <?php if($searchType == 'creator_name') { ?> selected <?php }?>>등록 담당자</option>
	                                    </select>
	                                </div>
	                                <input type="text" class="form-control" id="searchText" name="searchText" placeholder="검색어를 입력하세요." aria-label="검색어를 입력하세요." aria-describedby="button-addon-search" value="<?=$searchText?>">
	                                <div class="input-group-append">
	                                    <button class="btn" type="button" id="button-addon-search" onclick="javascript:searchTextgo();">검색</button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	        <div class="row mt-3">
	        	<div class="col-12">
	            	<h6 class="letter-spacing-1"><i class="demo-icon icon-info-circled"></i>현재 해당 심포지엄은 참가신청 중복 가능 설정 입니다.</h6>
	                <table class="table text-center">
	                    <colgroup>
	                    	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
                        	<col style="width: 5%;">
	                        <col style="width: 10%;">
	                        <col style="width: 10%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <col style="width: 15%;" class="hidden-mobile">
	                        <col style="width: 15%;">
	                        <col style="width: 10%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <col style="width: 15%;" class="hidden-mobile">
                			<?php } else {?>
	                        <col style="width: 10%;">
	                        <col style="width: 10%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <col style="width: 15%;" class="hidden-mobile">
	                        <col style="width: 20%;">
	                        <col style="width: 10%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <col style="width: 15%;" class="hidden-mobile">
							<?php }?>
	                    </colgroup>
	                    <thead>
	                    	 <tr>
	                    	 	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
	                            <th><label class="checkbox"><input type="checkbox" id="all" name="all" class="check-all cv" onclick="allChk()"><span></span></label></th>
	                            <?php } ?>
	                            <th>패스코드</th>
	                            <th class="hidden-mobile">서명</th>
	                            <?php if($sortWhere == 'applicant_name' && $sortType == 'desc') { ?>
	                            	<th><a href="javascript:sort('applicant_name', 'asc');">성명<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            	<th><a href="javascript:sort('applicant_name', 'desc');">성명<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
	                            <?php } ?>
	                            <th class="hidden-mobile">병원명</th>
                            	<th>모바일</th>
	                            <th class="hidden-mobile">영업소</th>
	                            <th>등록 담당자</th>
	                            <?php if($sortWhere == 'canceldate' && $sortType == 'desc') { ?>
	                            	<th class="hidden-mobile"><a href="javascript:sort('canceldate', 'asc');">참가취소일<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            	<th class="hidden-mobile"><a href="javascript:sort('canceldate', 'desc');">참가취소일<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
	                            <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody id="tbody">
	                    	<?php if($attentList != null && count($attentList) > 0) {
			                $dupleHp = "";
			                $dupleEmail = "";
			                $prevdupleCount = 0;
			
			                foreach ($attentList as $key => $item) {
			                    ?>
			                <?php if($dupleInspecType == 'applicant_hp') {?>
				                <?php if ($dupleInspec == 'Y' && $item['hpDupleCount'] >= 2 && $dupleHp != "" && $dupleHp != $item['applicantHp']) {
				                    $dupleHp = $item['applicantHp'];
				                    $prevdupleCount = $item['hpDupleCount']; ?>
				                    <tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php } else {
				                    if ($dupleInspec == 'Y' && $key != 0 && $prevdupleCount != $item['hpDupleCount']) { ?>
				                	<tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php }
				                    $dupleHp = $item['applicantHp'];
				                    $prevdupleCount = $item['hpDupleCount'];
				                }?>
		                        <?php if ($dupleInspec == 'Y' && $item['hpDupleCount'] >= 2) { ?>
		                        
		                        	<tr class="tbody-overlap">
		                        <?php } else { ?>
	                            	<tr>
	                            <?php } ?>
                            <?php } else if($dupleInspecType == 'applicant_email'){?>
                            	<?php if ($dupleInspec == 'Y' && $item['emailDupleCount'] >= 2 && $dupleEmail != "" && $dupleEmail != $item['applicantEmail']) {
                            		$dupleEmail = $item['applicantEmail'];
				                    $prevdupleCount = $item['emailDupleCount']; ?>
				                    <tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php } else {
				                    if ($dupleInspec == 'Y' && $key != 0 && $prevdupleCount != $item['emailDupleCount']) { ?>
				                	<tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php }
				                	$dupleEmail = $item['applicantEmail'];
				                    $prevdupleCount = $item['emailDupleCount'];
				                }?>
		                        <?php if ($dupleInspec == 'Y' && $item['emailDupleCount'] >= 2) { ?>
		                        
		                        <tr class="tbody-overlap">
		                        <?php } else { ?>
	                            <tr>
	                            <?php } ?>
                            <?php } else { ?>
                            	<tr>
				            <?php }?>
				            	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
				            	<td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="<?= $item['applicantCd'] ?>" onchange="checkboxCount();"><span></span></label></td>
				            	<?php } ?>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if($item['passcode'] == 'N'){ ?>
	                            	<em class="point point-red mr-2"></em>미발급
	                            <?php }else if($item['passcode'] == 'Y'){?>
	                            	<em class="point point-green mr-2"></em>발급
	                            <?php }?>
	                            </td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if($item['regiComplete'] == 'N'){ ?>
	                            	N
	                            <?php }else if($item['regiComplete'] == 'Y'){?>
	                            	Y
	                            <?php }?>
	                            </td>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if($item['comment'] != null && $item['comment'] != '') { ?>
								<?=$item['applicantName'] ?><i class="demo-icon icon-commenting-o"></i>
								<?php } else { ?>
								<?=$item['applicantName'] ?>
								<?php } ?>
								</td>
								<td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?= $item['applicantHospital'] ?></td>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?php if($item['applicantHp'] != null && $item['applicantHp'] != ''){?><?= $item['applicantHp'] ?><?php }else{?>-<?php }?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?php if($item['officeName'] != null && $item['officeName'] != ''){?><?= $item['officeName'] ?><?php }else{?>-<?php }?></td>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?= $item['creatorName'] ?> <?= strtoupper($item['creatorAuthority']) ?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if ($item['canceldate'] != null && $item['canceldate'] != "") { ?><?= $item['canceldate'] ?><?php } else { ?>-<?php } ?>
	                            </td>
	                            <td style="display: none;"><?= $item['applicantEmail'] ?></td>
	                        </tr>
	                        <?php } ?>
						<?php } else {?>
							<tr><td colspan="10">등록 리스트가 존재하지 않습니다.</td></tr>
						<?php }?>
	                    </tbody>
	                </table>
	                <div class="div-table-footer">
                        <button type="button" class="tag tag-icon" onclick="passcodeRestore('<?=$symposium['sympoCd']?>');"><span><img src="<?=HOME_DIR?>/images/icon/restore.svg" alt="참가 복원"></span>참가 복원</button>
                        <?php if($authority == 'op' ) {?>
                        <button type="button" class="tag tag-icon" onclick="passcodeDelete('<?=$symposium['sympoCd']?>');"><span><img src="<?=HOME_DIR?>/images/icon/delete.svg" alt="삭제"></span>삭제</button>
                        <?php }?>
                    </div>
	            </div>
	        </div>
	        <?php if($attentList != null && count($attentList) >= 10) {?>
            <div class="row mt-3">
				<div class="col-12 text-center">
			    	<a href="javascript:;" id="more" class="button gray" onclick="moreBtn();">리스트 추가 보기</a>
			    </div>
			</div>
			<?php }?>
	    </div>
	</section>