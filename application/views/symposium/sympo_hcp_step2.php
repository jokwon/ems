<?php 

?>
<body id="body" class="body site pre-registration page-default is-header" aria-expanded="false">
	<script>
		$(document).ready(function() {
	        $('input[type="checkbox"]').on('click', function() {
	            var $target = $('.form-error-msg');
	            $target.hide();
	        })
	    });
	
		function next(){
	        var personalInfoNc = $("input:checkbox[name='personalInfoNc']").is(":checked");
	        var personalInfoSe = $("input:checkbox[name='personalInfoSe']").is(":checked");

	        if(personalInfoNc && personalInfoSe){
	        	$("#hcpform").attr("action", "<?=HOME_DIR?>/symposium/hcp_step3/");
	 			$("#hcpform").submit();	
	        } else {
	        	var $target = $('.form-error-msg');
	        	$target.fadeIn();
	            setTimeout(function() {
	                $target.fadeOut();
	            }, 3000);
	                    
		    }
	    }
	
	    function prev(){
	    	 $("#hcpform").attr("action", "<?=HOME_DIR?>/symposium/hcp_step1/");
			 $("#hcpform").submit();
	    }
	</script>
    <div id="page" class="page" aria-expanded="false">
        <div id="bg-over"></div>
        <header id="masthead" class="site-header" aria-expanded="false">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="site-header-wrap">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <nav id="site-navigation" class="main-navigation">
                                            <div class="menu-primary-container">
                                                <ul id="primary-top" class="primary-top nav-menu" aria-expanded="false">
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#" class="main-menu-item menu-link href-empty">개인정보 수집 및 활용 동의서</a>
                                                        <ul class="nav-menu-sub">
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:privacyPop();" class="sub-menu-item menu-link">개인정보 수집 및 이용에 대한 동의</a>
                                                            </li>
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:termPop();" class="sub-menu-item menu-link">제 3자 제공에 대한 동의</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <h2>사전등록 신청서</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="ul-sign-step">
                            <li class="item">
                                <span class="is-pc">01. 참석자 기본 정보</span>
                                <span class="is-mobile">01</span>
                            </li>
                            <li class="item active">
                                <span class="is-pc">02. 개인정보 처리동의</span>
                                <span class="is-mobile">02</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <form method="post" id="hcpform" name="hcpform" action="">
					<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
					<input type="hidden" id="applicant_name" name="applicant_name" value="<?=$applicant_name?>"/>
					<input type="hidden" id="applicant_hospital" name="applicant_hospital" value="<?=$applicant_hospital?>"/>
					<input type="hidden" id="applicant_dep" name="applicant_dep" value="<?=$applicant_dep?>"/>
					<input type="hidden" id="applicant_hp" name="applicant_hp" value="<?=$applicant_hp?>"/>
					<input type="hidden" id="applicant_email" name="applicant_email" value="<?=$applicant_email?>"/>
					<input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
                    <div class="row mb-5">
                        <div class="col-12">
                            <section class="privacy">
                                <h3>개인정보 수집 및 활용 동의서</h3>
                                <p class="mb-5"><?=$setting['private_desc']?></p>
                                <h4>1. 개인정보의 수집 및 이용에 대한 동의</h4>
                                <h5>① 수집 및 이용목적:</h5>
                                <div class="indent">
                                    <p><?=$setting['private_collection']?></p>
                                    <h6>■ 필수 : </h6>
                                    <p><?=$setting['private_collection_nc']?></p>
                                    <h6>■ 선택 : </h6>
                                    <p class="mb-4"><?=$setting['private_collection_se']?></p>
                                </div>

                                <h5>② 수집항목:</h5>
                                <div class="indent">
                                    <h6>■ 필수 :</h6>
                                    <p><?=$setting['private_item_nc']?></p>
                                    <h6>■ 선택 :</h6>
                                    <p class="mb-4"><?=$setting['private_item_se']?></p>
                                </div>
                                <h5 class="underline">③ 보유 및 이용기간 :</h5>
                                <p class="underline mb-4"><?=$setting['private_period']?></p>
                                <h5>④ 거부권 및 거부에 따른 불이익:</h5>
                                <p><?=$setting['private_disadvantage']?></p>
                            </section>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-12">
                            <label class="checkbox"><input type="checkbox" class="cv" name="personalInfoNc" value='Y'><span></span> 개인정보의 <strong>필수적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</label>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-12">
                            <label class="checkbox"><input type="checkbox" class="cv" name="personalInfoSe" value='Y'><span></span> 개인정보의 <strong>선택적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</label>
                        </div>
                        <div class="col-12">
                            <div class="form-error-msg">
                                <i class="icon-demo icon-info-circled"></i>개인정보 필수적 수집 / 선택적 수집 이용에 관한 동의를 모두 체크 해주셔야 합니다.
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <ul class="ul-button-wrap">
                                <li><a href="javascript:prev();" class="button button-black-outline">이전단계</a></li>
                            	<li><a href="javascript:next();" class="button button-default">다음단계</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>