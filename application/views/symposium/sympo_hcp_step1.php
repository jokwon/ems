<?php

?>
<body id="body" class="body site pre-registration page-default is-header" aria-expanded="false">
	<script type="application/javascript">
        function next() {
        	var applicant_name = $('#applicant_name').val();
            var applicant_hospital = $('#applicant_hospital').val();
            var applicant_dep = $('#applicant_dep').val();
			var applicant_email = $('#applicant_email').val();
			var applicant_hp = $('#applicant_hp').val();

            
            if(!check_null("applicant_name","성명을")) return;
            if(applicant_name.length > 40){
            	alert('성명을 40자 이하로 입력해주세요.');return;
            }
            if(!check_null("applicant_hospital","병원명을")) return;
            if(applicant_hospital.length > 40){
            	alert('병원명을 40자 이하로 입력해주세요.');return;
            }
            if(!check_null("applicant_dep","진료과를")) return;
            if(applicant_dep.length > 10){
            	alert('진료과를 10자 이하로 입력해주세요.');return;
            }
            if(!check_null("applicant_hp","모바일을")) return;
            if(applicant_hp != null && applicant_hp != ""){
            	if(!isMobile("applicant_hp")) return;
            }
            if(applicant_email != null && applicant_email != ""){
            	if(!isEmail("applicant_email")) return;
            	if(applicant_email.length > 40){
                	alert('이메일을 40자 이하로 입력해주세요.');return;
                }
            }

            $('#hcpform').submit();
        }

		function goHome(){
		    $("#hcpHomeform").submit();
	    }
	</script>
    <div id="page" class="page" aria-expanded="false">
        <div id="bg-over"></div>
        <header id="masthead" class="site-header" aria-expanded="false">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="site-header-wrap">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <nav id="site-navigation" class="main-navigation">
                                            <div class="menu-primary-container">
                                                <ul id="primary-top" class="primary-top nav-menu" aria-expanded="false">
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#" class="main-menu-item menu-link href-empty">개인정보 수집 및 활용 동의서</a>
                                                        <ul class="nav-menu-sub">
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:privacyPop();" class="sub-menu-item menu-link">개인정보 수집 및 이용에 대한 동의</a>
                                                            </li>
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:termPop();" class="sub-menu-item menu-link">제 3자 제공에 대한 동의</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-12 text-center">
                        <h2>사전등록 신청서</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="ul-sign-step">
                            <li class="item active">
                                <span class="is-pc">01. 참석자 기본 정보</span>
                                <span class="is-mobile">01</span>
                            </li>
                            <li class="item">
                                <span class="is-pc">02. 개인정보 처리동의</span>
                                <span class="is-mobile">02</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <form method="post" id="hcpHomeform" name="hcpHomeform" action="<?=HOME_DIR?>/symposium/hcp_home">
				    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
				    <input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
				</form>
                <form method="post" id="hcpform" name="hcpform" action="<?=HOME_DIR?>/symposium/hcp_step2">
					<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
					<input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
	                <div class="row">
	                    <div class="col-12">
	                        <div class="form-group">
	                            <label for="s1">성명(*)</label>
	                            <input type="text" class="form-control" name="applicant_name" id="applicant_name" value="<?=$applicant_name?>" placeholder="e.g. 홍길동">                             
	                        </div>
	                        <div class="form-group">
	                            <label for="s2">병원명(*)</label>
	                            <input type="text" class="form-control" name="applicant_hospital" id="applicant_hospital" value="<?=$applicant_hospital?>" placeholder="e.g. 한국병원">
	                        </div>
	                        <div class="form-group">
	                            <label for="s3">진료과(*)</label>
	                            <input type="text" class="form-control" name="applicant_dep" id="applicant_dep" value="<?=$applicant_dep?>" placeholder="e.g. 소화기내과">
	                        </div>
	                        <div class="form-group">
	                            <label for="s4">모바일(*)</label>
	                            <input type="text" class="form-control" name="applicant_hp" id="applicant_hp" value="<?=$applicant_hp?>" placeholder="e.g. 01011112222">
	                        </div>
	                        <div class="form-group">
	                            <label for="s5">이메일</label>
	                            <input type="email" class="form-control" name="applicant_email" id="applicant_email" value="<?=$applicant_email?>" placeholder="e.g. support@webinars.co.kr">
	                        </div>
	                    </div>
	                </div>
                </form>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <ul class="ul-button-wrap">
                            <li></li>
                            <li><a href="javascript:next();" class="button button-default">다음단계</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>