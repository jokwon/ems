<?php

?>
<script type="application/javascript">
    $(document).ready(function() {

    	var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
		
    	var swiper = new Swiper('.swiper-container', {
            slidesPerView: 'auto',
            slidesPerView: preView,
            spaceBetween: 0,
            allowTouchMove: false,
            navigation: {
                nextEl: '.custom-swiper-button-next',
                prevEl: '.custom-swiper-button-prev',
            },
            breakpoints: {
                567: {
                    slidesPerView: 3,
                    allowTouchMove: true,
                },
            }
        });
        swiper.slideTo(2);
        swiper.el.childNodes[1].children[2].classList.add('highlight');

        $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });

        // date picker
        $('.input-date').datepicker({
            maxDate: "0",
            showOn: 'button',
            showButtonPanel: true,
            buttonImage: '../assets/images/icon/calendar.svg',
            buttonImageOnly: true,
            dateFormat: 'yy-mm-dd',
        });
        
        $("#sText").keydown(function (key) {
            if(key.keyCode == 13){
                searchText();
            }
        });
    });

    function passcodeAllComplete(sympoCd) {
		var readyCount = <?=$passcodeNcount['cnt']?>;

		if(readyCount == 0)	{
			alert('패스코드 미발급 목록이 없습니다.');
			return;
		}

        var param = {
            sympoCd: sympoCd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/passcodeAllComplete/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer3").html(data);
                     $('#modalSend').modal();
                 }
            }
		});
    }

    function passcodeAllRestore(sympoCd) {
    	var readyCount = <?=$passcodeCcount['cnt']?>;

		if(readyCount == 0)	{
			alert('취소 목록이 없습니다.');
			return;
		}
        
        var param = {
            sympoCd: sympoCd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/passcodeAllRestore/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer3").html(data);
                     $('#modalSend').modal();
                 }
            }
		});
    }

    function searchDate() {
        $('#symform').submit();
    }

</script>
<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12" id="<?=$symposium['sympoCd']?>gorupview">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="export-wrap">
	                    <div class="d-flex justify-content-between w-100">
	                        <div>
	                        	<h6 class="title d-inline mr-2">패스코드 발급완료 <span class="font-color-red"><?=$passcodeYcount['cnt']?></span>명</h6>
                                <h6 class="title d-inline mr-2">패스코드 미발급 <span class="font-color-red"><?=$passcodeNcount['cnt']?></span>명</h6>
                                <em class="br-break-sm" style="height: 5px;"></em>
                                <h6 class="title d-inline mr-2">참가 취소 <span class="font-color-red"><?=$passcodeCcount['cnt']?></span>명</h6>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-3">
                <div class="col-12">
                    <h6 class="letter-spacing-1"><i class="demo-icon icon-info-circled"></i>현재 해당 심포지엄은 참가신청 중복 가능 설정입니다.</h6>
                </div>
            </div>
	        <div class="row">
            <div class="col-12">
                <table class="table text-center">
	                    <colgroup>
	                        <col style="width: 30%">
	                        <col style="width: 25%">
	                        <col style="width: 25%">
	                        <col style="width: 20%">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <th>진행단계</th>
	                            <th>등록현황</th>
	                            <th>모바일 중복</th>
	                            <th>일괄진행</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<tr class="cursor-pointer">
                                <td onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_ready/<?=$symposium['sympoCd']?>');"><strong>패스코드 미발급[<?=$passcodeNcount['cnt']?>]</strong></td>
                                <td onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_ready/<?=$symposium['sympoCd']?>');"><?=$passcodeNcount['cnt']?></td>
                                <td onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_ready/<?=$symposium['sympoCd']?>');"><?=$dupleNHpCount['cnt']?></td>
                                <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
		                        	<td><button type="button" class="button button-md" role="button" onclick="passcodeAllComplete('<?=$symposium['sympoCd']?>');">전체 발급</button></td>
	                        	<?php } else {?><td> - </td><?php }?>
                            </tr>
                            <tr onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_complete/<?=$symposium['sympoCd']?>');" class="cursor-pointer">
                                <td><strong>패스코드 발급완료[<?=$passcodeYcount['cnt']?>]</strong></td>
                                <td><?=$passcodeYcount['cnt']?></td>
                                <td><?=$dupleYHpCount['cnt']?></td>
                                <td><?=$passcodeYcount['cnt']?></td>
                            </tr>
                            <tr class="cursor-pointer">
                                <td onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_cancel/<?=$symposium['sympoCd']?>');"><strong>참가 취소[<?=$passcodeCcount['cnt']?>]</strong></td>
                                <td onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_cancel/<?=$symposium['sympoCd']?>');"><?=$passcodeCcount['cnt']?></td>
                                <td onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_cancel/<?=$symposium['sympoCd']?>');"><?=$dupleCHpCount['cnt']?></td>
                                <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
		                        	<td><button type="button" class="button button-md" role="button" onclick="passcodeAllRestore('<?=$symposium['sympoCd']?>');">참가복원</button></td>
	                        	<?php } else {?><td> - </td><?php }?>
                                
                            </tr>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</section>