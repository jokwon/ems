<?php 

?>
<style>
    .button-download {
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: #333333;
        padding: 0 10px;
    }

    .button-download img {width: 18px; margin-right: 5px;}
    .sign-tab-div {position: absolute; left: 210px; top: 10px;} 

    @media (max-width: 1600px) {
        .amv-btn {display: none;}
    }
   
    @media (min-width: 481px) and (max-width: 767px) {
        .button-download {display: none;}
        .sign-tab-div {position: static;}
        .sign-tab-div ul.ul-tab li, .tm-tab-div ul.ul-tab li {width: calc(100% / 3);}
    }
    @media (max-height: 375px) {.button-download {display: none;}}
    @media (min-width: 320px) and (max-width: 480px) {
        .button-download {display: none;}
        .sign-tab-div {position: static;}
    }
</style>

<script type="application/javascript">

	$(document).ready(function() {

		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>

	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(1);
	    swiper.el.childNodes[1].children[1].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });
	
	    checkBoxes = document.querySelectorAll("input[class=cv]");
	
	    $("#sText").keydown(function (key) {
	        if(key.keyCode == 13){
	            searchText();
	        }
	    });
	});

    function searchSortPasscode(type) {
        $('#passcode').val(type);
        $('#symform').submit();
    }

    function searchSortRc(type) {
        $('#regiComplete').val(type);
        $('#symform').submit();
    }

    function searchText() {
        $('#dupleInspec').val('');
        $('#searchText').val($('#sText').val());
        $('#searchType').val($('#sType').val());
        $('#symform').submit();
    }

    function excelDown(obj) {
    	var sympoCd 		= '<?=$sympoCd?>';
    	var fileType 		= $(obj).val();
        var scRegDtSt 		= '';
        var scRegDtEd 		= '';
        var sortType 		= $('#sortType').val();
        var sortWhere 		= $('#sortWhere').val();
        var searchType		= $('#sType').val();
        var searchText 		= $('#sText').val();
        var dupleInspecType = $('#dupleInspecType').val();
        var dupleInspec 	= $('#dupleInspec').val();
        var passcode 			= $('#passcode').val();
        var cancel 			= 'N';
        var passcodeType 			= 'enrollment';
        
        var param = {
        	sympoCd: sympoCd,
        	fileType: fileType,
    	    scRegDtSt: scRegDtSt,
    	    scRegDtEd: scRegDtEd,
    	    sortType: sortType,
    	    sortWhere: sortWhere,
    	    searchType: searchType,
    	    searchText: searchText,
    	    dupleInspecType: dupleInspecType,
    	    dupleInspec: dupleInspec,
    	    passcode: passcode,
    	    cancel: cancel,
    	    passcodeType: passcodeType
        };

        if(fileType != '') {

            $.post('<?=HOME_DIR?>/symposium/applicantExcelAjax/', param, function (data) {
                window.location = data;
            });
        }
    }

    var page = 1;
    function moreBtn() {
        page += 1;

        var html = '';
        var searchType = $('#sType').val();
        var searchText = $('#sText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();
        var passcode = $('#passcode').val();
        var regiComplete = $('#regiComplete').val();
        var dupleInspec = $('#dupleInspec').val();
        var dupleInspecType = $('#dupleInspecType').val();


        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/symposium/sympo_enrollmentAjax",
            dataType: "json",
            data: {
                page: page,
                sympoCd: '<?=$sympoCd?>',
                searchText: searchText,
                searchType: searchType,
                sortWhere: sortWhere,
                sortType: sortType,
                passcode: passcode,
                regiComplete: regiComplete,
                dupleInspec: dupleInspec,
                dupleInspecType: dupleInspecType
            },
            success: function (data) {
                if(data.length != 0) {

                    var dupleHp = $('#tbody > tr:nth-last-child(1) > td:nth-child(6)').text();
                    var dupleEmail = $('#tbody > tr:nth-last-child(1) > td:nth-child(10)').text();
                    var dupleInspec = '<?=$dupleInspec?>';
                    var dupleInspecType = '<?=$dupleInspecType?>';
                    var prevdupleCount = 0;

                    for (var key in data) {

                    	//휴대폰 중복 클릭시
                    	if(dupleInspecType == 'applicant_hp')
                        {
	                    	if (dupleInspec == 'Y' && data[key]['hpDupleCount'] >= 2 && dupleHp != '' && dupleHp != data[key]['applicantHp']) {
	                            dupleHp = data[key]['applicantHp'];
	                            prevdupleCount = data[key]['hpDupleCount'];
	                            html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                        } else {
	                            if (dupleInspec == 'Y' && key != 0 && prevdupleCount != data[key]['hpDupleCount']) {
	                            	html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                            }
	                            dupleHp = data[key]['applicantHp'];
	                            prevdupleCount = data[key]['hpDupleCount'];
	                        }
	
	                    	if (dupleInspec == 'Y' && data[key]['hpDupleCount'] >= 2) {
	                    	html += '<tr class="tbody-overlap">';
	                        } else {
	                        html += '<tr>';
	                        }
                        }// 이메일 중복 클릭시
                        else if(dupleInspecType == 'applicant_email') {
                        	if (dupleInspec == 'Y' && data[key]['emailDupleCount'] >= 2 && dupleEmail != '' && dupleEmail.toLowerCase() != data[key]['applicantEmail'].toLowerCase()) {
                        		dupleEmail = data[key]['applicantEmail'];
	                            prevdupleCount = data[key]['emailDupleCount'];
	                            html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                        } else {
	                            if (dupleInspec == 'Y' && key != 0 && prevdupleCount != data[key]['emailDupleCount']) {
	                            	html += '<tr><td style="height: 5px; padding: 1px;"></td></tr>';
	                            }
	                            dupleEmail = data[key]['applicantEmail'];
	                            prevdupleCount = data[key]['emailDupleCount'];
	                        }
	
	                    	if (dupleInspec == 'Y' && data[key]['emailDupleCount'] >= 2) {
	                    	html += '<tr class="tbody-overlap">';
	                        } else {
	                        html += '<tr>';
	                        }
                        } else {
							html += '<tr>';
                        }
                        
                        html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">';
                        if(data[key]['passcode'] == 'N') {
	                        html += '<em class="point point-red mr-2"></em>미발급';
	                    } else {
	                    	html += '<em class="point point-green mr-2"></em>발급';
	                    }                        
                        html += '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd'] +'\');">'
                        if(data[key]['regiComplete'] == 'N'){
                        	html += 'N'
                        }else if(data[key]['regiComplete'] == 'Y'){
                        	html += 'Y'
                        }
                        html += '</td>'
						html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'+data[key]['applicantName'];
                        if(data[key]['comment'] != null && data[key]['comment'] != '') {
	                        html += '<i class="demo-icon icon-commenting-o"></i>';
	                    }
	                    html += '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'+data[key]['applicantHospital']+'</td>';
                        html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">';
						if(data[key]['applicantHp'] != null && data[key]['applicantHp'] != "")
						{
							html += data[key]['applicantHp']; 
						}else {
							html += '-';
						}
						html += '</td>';
						html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd']+'\');">';
						if(data[key]['officeName']!=null && data[key]['officeName']!=''){
							html += data[key]['officeName']
						} else {
							html+='-'
						}
						html += '</td>';
						html += '<td onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'+data[key]['creatorName']+' '+data[key]['creatorAuthority'].toUpperCase()+'</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['applicantCd']+'\');">'
    					html += data[key]['applicantRegdate']+'<small class="small-date">'+data[key]['updatedate']+'</small>'; 
    					html += '</td>';
    					html += '<td style="display: none;">'+data[key]['applicantEmail']+'</td>';
                        html += '</tr>';
                    }
                    $('#tbody').append(html);

                    if(data.length < 10) {
                        $('#more').hide();
                    }
                } else {
                    alert('데이터가 없습니다.');
                }
                
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function sort(sortWhere, sortType) {
        $('#searchText').val($('#sText').val());

        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);

        $('#symform').submit();

    }

    function dupleInspection(type,dupleType) {
        $('#dupleInspec').val(type);
        $('#dupleInspecType').val(dupleType);
        $('#symform').submit();
    }

    function drawerDown(applicantCd) {

        var param = {
        		applicantCd: applicantCd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/applicantDetail/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                	 $('#modalDetail').modal();
                 }
            }
		});
    }

    function allSignDownload(signCount) {
        if(signCount == 0){
            alert('다운받을 수 있는 서명이 없습니다.');
        } else {
        	if(confirm('등록 리스트 수가 많을 경우 시간이 다소 걸릴 수 있습니다. 전체 서명 다운로드를 진행하시겠습니까?')) {
        	location.href='<?=HOME_DIR?>/symposium/signAll?sympo_cd=<?=$symposium['sympoCd']?>';
        	}
        }
    }
</script>

<form id="symform" name="symform" action="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>" method="post">
	<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$symposium['sympoCd']?>" /> 
	<input type="hidden" id="applicantCds" name="applicantCds" value="" /> 
	<input type="hidden" id="searchType" name="searchType" value="<?=$searchType?>" /> 
	<input type="hidden" id="searchText" name="searchText" value="<?=$searchText?>" /> 
	<input type="hidden" id="sortType" name="sortType" value="<?=$sortType?>" /> 
	<input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere?>" /> 
	<input type="hidden" id="passcode" name="passcode" value="<?=$passcode?>" />
	<input type="hidden" id="regiComplete" name="regiComplete" value="<?=$regiComplete?>" />  
	<input type="hidden" id="dupleInspec" name="dupleInspec" value="<?=$dupleInspec?>" />
	<input type="hidden" id="dupleInspecType" name="dupleInspecType" value="<?=$dupleInspecType?>"/>
</form>

<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12" id="<?=$symposium['sympoCd']?>gorupview">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	               	
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="export-wrap">
	                	<div class="d-flex justify-content-between w-100">
	                    	<div class="d-inline-flex align-items-center">
                                <h5 class="title d-inline mr-1">등록 <span class="font-color-red"><?=$regilistcount['cnt']?></span>명</h5>
                                <h6 class="font-weight-normal d-inline-flex mr-2 mb-0">(<span class="d-none d-md-inline-block">모바일</span><span class="d-inline-block d-md-none mr-1">M</span><span class="font-color-red"><?=$dupleHpCount['cnt']?></span>|<span class="d-none d-md-inline-block">이메일</span><span class="d-inline-block d-md-none mr-1">E</span><span class="font-color-red"><?=$dupleEmailCount['cnt']?></span>)</h6>
                                <button type="button" class="button button-mid muted d-none d-sm-inline-block mr-1" onclick="javascript:dupleInspection('Y','applicant_hp');">모바일중복</button>
                                <button type="button" class="button button-mid muted d-none d-sm-inline-block mr-1" onclick="javascript:dupleInspection('Y','applicant_email');">이메일중복</button>
                            </div>
                            <div class="d-inline-flex d-sm-none position-relative">
                            	<button type="button" class="button button-mid muted mr-1 d-inline-block d-sm-none menu-toggle-overlap">중복</button>
                                <nav class="more-menu-overlap">
                                	<ul>
                                    	<li class="item"><a href="#" class="link small" onclick="javascript:dupleInspection('Y','applicant_hp');">모바일 중복</a></li>
                                        <li class="item"><a href="#" class="link small" onclick="javascript:dupleInspection('Y','applicant_email');">이메일 중복</a></li>
                                    </ul>
                                </nav>
                                <button type="button" class="button button-search"></button>
                            </div>
	                    </div>
	                    <div class="mr-1">
	                    	<button type="button" class="button button-download ml-1" onclick="allSignDownload('<?=$signCount['cnt']?>')"><img src="/images/svg/u10.svg" alt=""> 전체서명</button>
	                    </div>
	                    <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                    <div class="export-select-wrap">
	                        <select name="s1" id="s1" class="custom-select" style="width: auto;" onchange="excelDown(this);">
	                            <option value="">EXPORT</option>
	                            <option value="csv">CSV</option>
	                            <option value="excel">EXCEL</option>
	                        </select>
	                    </div>
	                    <?php }?>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px mobile-toggle-wrap">
	            <div class="col-12">
		            <div class="filter-wrap md-column">
				        <div class="tm-tab-div">
				            <ul class="ul-tab">
				                <li class="<?php if($passcode == 'All' || $passcode == '') { ?> item active <?php } else {?> item <?php } ?>"><a href="javascript:searchSortPasscode('All');" class="link">패스코드</a></li>
				                <li class="<?php if($passcode == 'N') { ?> item active <?php } else {?> item <?php } ?>"><a href="javascript:searchSortPasscode('N');" class="link">미발급</a></li>
				                <li class="<?php if($passcode == 'Y') { ?> item active <?php } else {?> item <?php } ?>"><a href="javascript:searchSortPasscode('Y');" class="link">발급</a></li>
				            </ul>
				        </div>
				        <div class="sign-tab-div">
				            <ul class="ul-tab">
				                <li class="<?php if($regiComplete == 'All' || $regiComplete == '') { ?> item active <?php } else {?> item <?php } ?>"><a href="javascript:searchSortRc('All');" class="link">서명전체</a></li>
				                <li class="<?php if($regiComplete == 'Y') { ?> item active <?php } else {?> item <?php } ?>"><a href="javascript:searchSortRc('Y');" class="link">있음</a></li>
				                <li class="<?php if($regiComplete == 'N') { ?> item active <?php } else {?> item <?php } ?>"><a href="javascript:searchSortRc('N');" class="link">없음</a></li>
				            </ul>
				        </div>
				        <div>
				            <div class="input-group search-wrap">
				                <div class="input-group-prepend">
				                    <select name="sType" id="sType" class="custom-select" style="width: auto;">
				                        <option value="applicant_name" <?php if($searchType == 'applicant_name') { ?> selected <?php }?>>성명</option>
				                        <option value="applicant_hospital" <?php if($searchType == 'applicant_hospital') { ?> selected <?php }?>>병원명</option>
				                        <option value="applicant_hp" <?php if($searchType == 'applicant_hp') { ?> selected <?php }?>>모바일</option>
				                        <option value="office_name" <?php if($searchType == 'office_name') { ?> selected <?php }?>>담당 영업소</option>
				                        <option value="creator_name" <?php if($searchType == 'creator_name') { ?> selected <?php }?>>등록 담당자</option>
				                    </select>
				                </div>
				                <input type="text" class="form-control" placeholder="검색어를 입력하세요." id="sText" name="sText" aria-label="검색어를 입력하세요." aria-describedby="button-addon-search" value="<?=$searchText?>">
				                <div class="input-group-append">
				                    <button class="btn" type="button" id="button-addon-search" onclick="javascript:searchText();">검색</button>
				                </div>
				            </div>
				        </div>
		            </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <table class="table text-center">
	                    <colgroup>
	                        <col style="width: 10%;">
	                        <col style="width: 10%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <col style="width: 20%;" class="hidden-mobile">
	                        <col style="width: 15%;">
	                        <col style="width: 10%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <col style="width: 15%;" class="hidden-mobile">
	                    </colgroup>
	                    <thead>
	                    	 <tr>
	                            <th>패스코드</th>
	                            <th class="hidden-mobile">서명</th>
	                            <?php if($sortWhere == 'applicant_name' && $sortType == 'desc') { ?>
	                            	<th><a href="javascript:sort('applicant_name', 'asc');">성명<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            	<th><a href="javascript:sort('applicant_name', 'desc');">성명<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
	                            <?php } ?>
	                            <th class="hidden-mobile">병원명</th>
                            	<th>모바일</th>
	                            <th class="hidden-mobile">영업소</th>
	                            <th>등록 담당자</th>
	                            <?php if($sortWhere == 'createdate' && $sortType == 'desc') { ?>
	                            	<th class="hidden-mobile"><a href="javascript:sort('createdate', 'asc');">등록일 / 수정일<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            	<th class="hidden-mobile"><a href="javascript:sort('createdate', 'desc');">등록일 / 수정일<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
	                            <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody id="tbody" >
	                    	<?php
								if ($regilist != null && count ( $regilist ) > 0) {
									$dupleHp = "";
									$dupleEmail = "";
									$prevdupleCount = 0;
																		
									foreach ( $regilist as $key => $item ) {
							?>
							<?php if($dupleInspecType == 'applicant_hp') {?>
				                <?php if ($dupleInspec == 'Y' && $item['hpDupleCount'] >= 2 && $dupleHp != "" && $dupleHp != $item['applicantHp']) {
				                    $dupleHp = $item['applicantHp'];
				                    $prevdupleCount = $item['hpDupleCount']; ?>
				                    <tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php } else {
				                    if ($dupleInspec == 'Y' && $key != 0 && $prevdupleCount != $item['hpDupleCount']) { ?>
				                	<tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php }
				                    $dupleHp = $item['applicantHp'];
				                    $prevdupleCount = $item['hpDupleCount'];
				                }?>
		                        <?php if ($dupleInspec == 'Y' && $item['hpDupleCount'] >= 2) { ?>
		                        
		                        <tr class="tbody-overlap">
		                        <?php } else { ?>
	                            <tr>
	                            <?php } ?>
                            <?php } else if($dupleInspecType == 'applicant_email'){?>
                            	<?php if ($dupleInspec == 'Y' && $item['emailDupleCount'] >= 2 && $dupleEmail != "" && $dupleEmail != $item['applicantEmail']) {
                            		$dupleEmail = $item['applicantEmail'];
				                    $prevdupleCount = $item['emailDupleCount']; ?>
				                    <tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php } else {
				                    if ($dupleInspec == 'Y' && $key != 0 && $prevdupleCount != $item['emailDupleCount']) { ?>
				                	<tr><td style="height: 5px; padding: 1px;"></td></tr>
				                <?php }
				                	$dupleEmail = $item['applicantEmail'];
				                    $prevdupleCount = $item['emailDupleCount'];
				                }?>
		                        <?php if ($dupleInspec == 'Y' && $item['emailDupleCount'] >= 2) { ?>
		                        
		                        <tr class="tbody-overlap">
		                        <?php } else { ?>
	                            <tr>
	                            <?php } ?>
                            <?php } else { ?>
                            	<tr>
				            <?php }?>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if($item['passcode'] == 'N'){ ?>
	                            	<em class="point point-red mr-2"></em>미발급
	                            <?php }else if($item['passcode'] == 'Y'){?>
	                            	<em class="point point-green mr-2"></em>발급
	                            <?php }?>
	                            </td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if($item['regiComplete'] == 'N'){ ?>
	                            	N
	                            <?php }else if($item['regiComplete'] == 'Y'){?>
	                            	Y
	                            <?php }?>
	                            </td>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if($item['comment'] != null && $item['comment'] != '') { ?>
								<?=$item['applicantName'] ?><i class="demo-icon icon-commenting-o"></i>
								<?php } else { ?>
								<?=$item['applicantName'] ?>
								<?php } ?>
								</td>
								<td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?= $item['applicantHospital'] ?></td>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?php if($item['applicantHp'] != null && $item['applicantHp'] != ''){?><?= $item['applicantHp'] ?><?php }else{?>-<?php }?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?php if($item['officeName'] != null && $item['officeName'] != ''){?><?= $item['officeName'] ?><?php }else{?>-<?php }?></td>
	                            <td onclick="drawerDown('<?= $item['applicantCd'] ?>');"><?= $item['creatorName'] ?> <?= strtoupper($item['creatorAuthority']) ?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $item['applicantCd'] ?>');">
	                            <?php if ($item['applicantRegdate'] != null && $item['applicantRegdate'] != "") { ?>
	                            <?= $item['applicantRegdate'] ?><small class="small-date"><?=$item['updatedate']?></small>
	                            <?php } else { ?>
	                            &nbsp;
	                            <?php } ?>
	                            </td>
	                            <td style="display: none;"><?= $item['applicantEmail'] ?></td>
	                        </tr>
	                        <?php }?>
						<?php } else {?>
							<tr><td colspan="10">등록 리스트가 존재하지 않습니다.</td></tr>
						<?php }?>	
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <?php if($regilist != null && count($regilist) >= 10) {?>
	        <div class="row mt-3">
	            <div class="col-12 text-center">
	                <a href="javascript:;" id="more" class="button gray" onclick="moreBtn();">리스트 추가 보기</a>
	            </div>
	        </div>
	        <?php }?>
	    </div>
	</section>