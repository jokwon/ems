<?php

?>
<script type="application/javascript">

	$(document).ready(function() {
	
		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
		
	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(3);
	    swiper.el.childNodes[1].children[3].classList.add('highlight');
	
	    $(window).resize(function() {
	        setTimeout(function() {
	            swiper.update();
	        }, 500);
	    });
	    
	    // date picker
	    $('.input-date').datepicker({
	        showOn: 'button',
	        showButtonPanel: false,
	        buttonImage: '<?=HOME_DIR?>/images/icon/calendar.svg',
	        buttonImageOnly: true,
	        showMonthAfterYear: true,
	        dateFormat: 'yy-mm-dd',
	    });
	
	    $('.drawer-up').on('click', function() {
	        var $this = $(this);
	        if ($this.hasClass('only-mobile') && !$('body.is-mobile').length) return false;
	        $this.next().toggle();
	    })
	});

    function dateCheck(inThis){

	    if($("#scRegDtSt").val()!="" && $("#scRegDtEd").val()!=""){
	        if($("#scRegDtSt").val() > $("#scRegDtEd").val()){
	            alert("검색 시작 날짜는 마지막날짜 보다 작아야 합니다.");
	            inThis.value="";
	            inThis.focus();
	        }
	    }
	
	    var eventEndDate = '<?=$symposium['eventEndDate']?>';
	    if(eventEndDate.replace(/\./g, '-') <= $("#scRegDtEd").val() || eventEndDate.replace(/\./g, '-') <= $("#scRegDtSt").val()) {
	        alert("검색 날짜는 심포지엄 진행 마지막 날짜 보다 작아야 합니다.");
	        inThis.value="";
	        inThis.focus();
	    }
	}

    function reportSel(obj) {
        var val = $(obj).val();
        if(val == 'type1') {
            window.location = '<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>';
        } else if(val == 'type2') {
            window.location = '<?=HOME_DIR?>/symposium/sympo_reportManager/<?=$symposium['sympoCd']?>';
        } else {
            window.location = '<?=HOME_DIR?>/symposium/sympo_reportDate/<?=$symposium['sympoCd']?>';
        }
    }

    function sort(sortWhere, sortType) {
        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);
        search();
    }

    function search() {

        var scRegDtSt = $('#scRegDtSt').val();
        var scRegDtEd = $('#scRegDtEd').val();

        if(scRegDtSt == '' && scRegDtEd == '') {

        } else {
            if(scRegDtSt == '' || scRegDtEd == '') {
                if(scRegDtSt == '') {
                    alert('시작일을 입력하세요');
                    $('#scRegDtSt').focus();
                    return;
                }
                if(scRegDtEd == '') {
                    alert('종료일을 입력하세요');
                    $('#scRegDtEd').focus();
                    return;
                }
            }
        }

        $('#sform').submit();
    }

    function excelDown(obj) {
        var sympoCd = '<?=$symposium['sympoCd']?>';
        var scRegDtSt = $('#scRegDtSt').val();
        var scRegDtEd = $('#scRegDtEd').val();
        var member_id = $('#member_id').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();
        var fileType = $(obj).val();

        var param = {
            sympoCd: sympoCd,
            scRegDtSt: scRegDtSt,
            scRegDtEd: scRegDtEd,
            member_id: member_id,
            sortWhere: sortWhere,
            sortType: sortType,
            fileType: fileType
        };

        if(fileType != '') {
            $.post('<?=HOME_DIR?>/symposium/officeReportExcelAjax/', param, function (data) {
                window.location = data;
            });
        }
    }
</script>

<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12" id="<?=$symposium['sympoCd']?>gorupview">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	            	<form name="sform" id="sform" action="<?=HOME_DIR?>/symposium/sympo_reportOffice/<?=$symposium['sympoCd']?>" method="post">
				                	<input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere?>"/>
			                    	<input type="hidden" id="sortType" name="sortType" value="<?=$sortType?>"/>
	                    <div class="filter-wrap xl-column">
	                        <div class="d-inline-flex">
	                            <div class="form-inline w-100">
	                                <div class="input-group">
	                                    <div class="input-group-prepend">
	                                        <div class="prepend-title red">보고서 TYPE</div>
	                                    </div>
	                                    <select name="searchType" id="searchType" class="custom-select" style="width: auto;" onchange="reportSel(this);">
	                                        <option value="type1">심포지엄</option>
				                            <option value="type2" selected>담당 영업소별</option>
				                            <option value="type3">등록일별</option>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="d-inline-block d-md-none position-relative">
	                                <img src="<?=HOME_DIR?>/images/png/empty.png" style="width:40px; height: 40px;" class="ml-1" alt="">
	                                <button type="button" class="button button-search big position-absolute toggle-search" data-target="search-zone" style="top: 0; right: 0;"></button>
	                            </div>
	                        </div>
	                        <div class="search-zone md-hidden">
	                            <div class="form-inline">
		                                <div class="box-round mr-0 mr-sm-1">
		                                    <div class="div-date" id="dateStart">
		                                        <input type="text" class="input-date" id="scRegDtSt" name="scRegDtSt" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$scRegDtSt?>">
		                                    </div>
		                                    <div class="div-date mx-1" id="blockAnd">~</div>
		                                    <div class="div-date" id="dateEnd">
		                                        <input type="text" class="input-date" id="scRegDtEd" name="scRegDtEd" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$scRegDtEd?>">
		                                    </div>
		                                </div>
		                                <div class="input-group mt-1 mt-sm-0">
		                                    <input type="text" class="form-control" placeholder="검색어를 입력하세요." aria-label="검색어를 입력하세요." aria-describedby="button-addon-search" id="searchText" name="searchText" value="<?=$searchText?>">
		                                    <div class="input-group-append">
		                                        <button class="btn" type="submit" id="button-addon-search" onclick="javascript:search();">검색</button>
		                                    </div>
		                                </div>
		                            <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                                <div class="export-select-wrap ml-1">
	                                    <select name="s2" id="s2" class="custom-select white-mode" style="width: auto;" onchange="excelDown(this);">
	                                        <option value="">EXPORT</option>
	                                        <option value="csv">CSV</option>
	                                        <option value="excel">EXCEL</option>
	                                    </select>
	                                </div>
	                                <?php }?>
	                            </div>
	                        </div>
	                    </div>
                    </form>
	            </div>
	        </div>
	        <div class="row mt-3">
	            <div class="col-12">
	                <table class="table text-center">
	                    <colgroup>
	                        <col style="width: 20%;">
	                        <col style="width: 20%;" class="hidden-mobile">
	                        <col style="width: 20%;" class="hidden-mobile">
	                        <col style="width: 20%;">
	                        <col style="width: 20%;">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                        	<?php if($sortWhere == 'office_name' && $sortType == 'desc') { ?>
	                            <th><a href="javascript:sort('office_name', 'asc');" class="tbl_sort">담당 영업소<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            <th><a href="javascript:sort('office_name', 'desc');" class="tbl_sort">담당 영업소<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th> 
	                            <?php }?>
	                            <th class="hidden-mobile">예비등록</th>
	                            <th class="hidden-mobile">방문등록</th>
	                            <?php if($sortWhere == 'regiCount' && $sortType == 'desc') { ?>
	                            <th><a href="javascript:sort('regiCount', 'asc');" class="tbl_sort">사전등록완료<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            <th><a href="javascript:sort('regiCount', 'desc');" class="tbl_sort">사전등록완료<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th> 
	                            <?php }?>
	                            <?php if($sortWhere == 'passcodeYCount' && $sortType == 'desc') { ?>
	                            <th><a href="javascript:sort('passcodeYCount', 'asc');" class="tbl_sort">패스코드 발급완료<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            <th><a href="javascript:sort('passcodeYCount', 'desc');" class="tbl_sort">패스코드 발급완료<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th> 
	                            <?php }?>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php if($officeReport != null && count($officeReport) > 0) {
	                    		foreach ($officeReport as $item) {?>
			                        <tr class="drawer-up only-mobile">
			                            <td><?php if($item['office_name'] != null || $item['office_name'] = ''){?><?=$item['office_name'] ?><?php } else {?>-<?php }?></td>
			                            <td class="hidden-mobile"><?=$item['regiPSRCount'] ?></td>
			                            <td class="hidden-mobile"><?=$item['regiHCPCount'] ?></td>
			                            <td class="font-color-red"><?=$item['regiCount'] ?></td>
			                            <td class="font-color-red"><?=$item['passcodeYCount'] ?></td>
			                        </tr>
			                        <tr class="drawer-down">
			                            <td colspan="auto" class="colspan-auto">
			                                <div class="ul-info-wrap">
			                                    <ul class="ul-info ml-4 line-no">
			                                        <li>
			                                            <dl>
			                                                <dt>총 사전등록 완료자수</dt>
			                                                <dd><?=$item['regiCount'] ?></dd>
			                                                <dt>예비등록</dt>
			                                                <dd><?=$item['regiPSRCount'] ?></dd>
			                                                <dt>방문등록</dt>
			                                                <dd><?=$item['regiHCPCount'] ?></dd>
			                                            </dl>
			                                        </li>
			                                    </ul>
			                                </div>
			                            </td>
			                        </tr>
		                        <?php } ?>
						<?php } else {?>
							<tr><td colspan="10">리스트가 존재하지 않습니다.</td></tr>
						<?php }?>	
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</section>