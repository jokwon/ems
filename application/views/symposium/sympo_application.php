<?php

?>
<script type="application/javascript">

	var firstSlick = true;

	$(document).ready(function() {

		$('.modal').on('shown.bs.modal', function(e) {
            if (firstSlick) {
                firstSlick = false;
                $('.slick-wrap').on('init', function(event, slick) {
                    $('.slick-item').eq(0).addClass('active-item');
                });
                $('.slick-wrap').on('afterChange', function(event, slick, currentSlide) {
                    $('.slick-item').removeClass('active-item');
                    $(this).find('.slick-item').eq(currentSlide).addClass('active-item');
                })

                $('.slick-wrap').slick({
                    arrows: false,
                    dots: true,
                    infinite: true,
                    fade: true,
                    rows: 0,
                    autoplay: true,
                    swipeToSlide: true,
                    speed: 500,
                    autoplaySpeed: 7000,
                });
            }
        });

		$(document).on('click','#file_inp0',function() {
	        $('#imgFile').click();
	    });
		$('#imgFile').change(function() {
            $('#file_inp00').html(this.files[0].name);
            $('#fileDel').val("");
        });

		$(document).on('click','#content_file_inp1',function() {
	        $('#content_file1').click();
	    });
		$('#content_file1').change(function() {
            $('#content_file_inp11').html(this.files[0].name);
            $('#fileDelCF1').val("");
        });

		$(document).on('click','#content_file_inp2',function() {
	        $('#content_file2').click();
	    });
		$('#content_file2').change(function() {
            $('#content_file_inp22').html(this.files[0].name);
            $('#fileDelCF2').val("");
        });
	
	    $(document).on('click','#file_inp1',function() {
	        $('#imgFile1').click();
	    });
	    $('#imgFile1').change(function() {
            $('#file_inp11').html(this.files[0].name);
            $('#fileDel1').val("");
        });

	    $(document).on('click','#file_inp2',function() {
	        $('#imgFile2').click();
	    });
	    $('#imgFile2').change(function() {
            $('#file_inp22').html(this.files[0].name);
            $('#fileDel2').val("");
        });
	    
	    $(document).on('click','#file_inp3',function() {
	        $('#imgFile3').click();
	    });
	    $('#imgFile3').change(function() {
            $('#file_inp33').html(this.files[0].name);
            $('#fileDel3').val("");
        });
	    
	    $(document).on('click','#file_inp4',function() {
	        $('#imgFile4').click();
	    });
	    $('#imgFile4').change(function() {
            $('#file_inp44').html(this.files[0].name);
            $('#fileDel4').val("");
        });

	    var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
	    
	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(4);
	    swiper.el.childNodes[1].children[4].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });
	});

    function templateUpdate() {

        if($("#file_inp00").text()=="Choose a file…") {
        	alert("사전등록페이지를 활성화 하시려면 페이지 상단 배경 이미지의 이미지 파일을 첨부해주세요");
        	return;
        }

    	if(!check_null("agency_info","심포지엄 준비 사무국 정보를")) return;
        if(!check_null("symposium_info","심포지엄 안내문을")) return;
        
        if($(":input:checkbox[name=template_onoff]").is(":checked")) {

	        if($(":input:checkbox[name=onoff1]").is(":checked")) {
	        	$("#onoff1").val("Y");
	        	if($("#file_inp11").text()=="Choose a file…") {
	            	alert("사전등록페이지를 활성화 하시려면 심포지엄 관련 정보1의 이미지 파일을 첨부해주세요");
	            	return;
	            }
	        } else {
	        	$("#onoff1").val("N");
	        	$('#PreRegiSelect1').val("");
	        }
	        if($(":input:checkbox[name=onoff2]").is(":checked")) {
	        	$("#onoff2").val("Y");
	        	if($("#file_inp22").text()=="Choose a file…") {
	            	alert("사전등록페이지를 활성화 하시려면 심포지엄 관련 정보2의 이미지 파일을 첨부해주세요");
	            	return;	
	            }
	        } else {
	        	$("#onoff2").val("N");
	        	$('#PreRegiSelect2').val("");
	        }
	        if($(":input:checkbox[name=onoff3]").is(":checked")) {
	        	$("#onoff3").val("Y");
	        	if($("#file_inp33").text()=="Choose a file…") {
	            	alert("사전등록페이지를 활성화 하시려면 심포지엄 관련 정보3의 이미지 파일을 첨부해주세요");
	            	return;	
	            }
	        } else {
	        	$("#onoff3").val("N");
	        	$('#PreRegiSelect3').val("");
	        }
	        if($(":input:checkbox[name=onoff4]").is(":checked")) {
	        	$("#onoff4").val("Y");
	        	if($("#file_inp44").text()=="Choose a file…") {
	            	alert("사전등록페이지를 활성화 하시려면 심포지엄 관련 정보4의 이미지 파일을 첨부해주세요");
	            	return;
	            }
	        } else {
	        	$("#onoff4").val("N");
	        	$('#PreRegiSelect4').val("");
	        }
			for(var i=1; i<5; i++){
				if($('#onoff'+i).val() == "Y") {
					if($('#PreRegiSelect'+i).val() == "" || $('#PreRegiSelect'+i).val() == null ) {
						alert("순번이 선택되지 않은 슬라이드 이미지가 있습니다.")
						return;
					}
				}
				if($('#onoff'+i).val() != "N") {
					for(var j=1; j<5; j++){
						if(i==j) {
							continue;
						}
						if($('#PreRegiSelect'+i).val() == $('#PreRegiSelect'+j).val()) {
							alert("활성화 되어있는 사전등록 페이지 중 순서가 중복된 값이 있습니다.");
							return;	
						}
					}
				}
			}
        }

        if($(":input:checkbox[name=content_onoff1]").is(":checked")) {
        	if(!check_null("content_subject1","컨텐츠 영역1의 영역 제목을")) return;
        	
        	$("#content_onoff1").val("Y");
        	if($("#content_file_inp11").text()=="Choose a file…") {
            	alert("사전등록페이지를 활성화 하시려면 컨텐츠 영역1의 이미지 파일을 첨부해주세요");
            	return;
            }
        } else {
        	$("#content_onoff1").val("N");
        }

        if($(":input:checkbox[name=content_onoff2]").is(":checked")) {
        	if(!check_null("content_subject2","컨텐츠 영역2의 영역 제목을")) return;
        	
        	$("#content_onoff2").val("Y");
        	if($("#content_file_inp22").text()=="Choose a file…") {
            	alert("사전등록페이지를 활성화 하시려면 컨텐츠 영역2의 이미지 파일을 첨부해주세요");
            	return;
            }
        } else {
        	$("#content_onoff2").val("N");
        }

		$('#temFrm').attr('action', '<?=HOME_DIR?>/settings/template_upload_exec');
		$('#temFrm').submit();
	}

    function fileDelete(num) {
        if(num == 'basic'){
        	$('#imgFile').val('');
        	$('#file_inp00').html('Choose a file…');
            $('#fileDel').val('Y');
        } else if (num == 'content_subject1') {
        	$('#content_file1').val('');
        	$('#content_file_inp11').html('Choose a file…');
            $('#fileDelCF1').val('Y');
        } else if (num == 'content_subject2') {
        	$('#content_file2').val('');
        	$('#content_file_inp22').html('Choose a file…');
            $('#fileDelCF2').val('Y');
        } else {
        	$('#imgFile'+num).val('');
        	$('#file_inp'+num+num).html('Choose a file…');
            $('#fileDel'+num).val('Y');
        }
    }
</script>
<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
                <div class="col-12">
                    <div class="filter-wrap">
                        <div>
                            <ul class="ul-tab">
                                <li class="item active"><a href="javascript:move('symposium', '<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>');" class="link">사전등록 페이지</a></li>
                                <li class="item"><a href="javascript:move('symposium', '<?=HOME_DIR?>/symposium/sympo_personalInfo/<?=$symposium['sympoCd']?>');" class="link">개인정보 활용동의</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
			<form name="temFrm" id="temFrm" method="post" enctype="multipart/form-data">
				<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$symposium['sympoCd']?>">
                <input type="hidden" id="fileDel" name="fileDel" value="">
                <input type="hidden" id="fileDel1" name="fileDel1" value="">
                <input type="hidden" id="fileDel2" name="fileDel2" value="">
                <input type="hidden" id="fileDel3" name="fileDel3" value="">
                <input type="hidden" id="fileDel4" name="fileDel4" value="">
                <input type="hidden" id="fileDelCF1" name="fileDelCF1" value="">
                <input type="hidden" id="fileDelCF2" name="fileDelCF2" value="">
		        <div class="row mt-3">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">페이지 상단 배경 이미지</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <label>이미지 등록</label>
		                            <p class="small mb-1">이미지는 2560x350픽셀 사이즈, 파일크기 2MB 이하를 권장합니다.(반응형에 따라 중앙정렬)</p>
		                            <div class="d-flex">
		                                <input type="file" name="file" class="inputFile" id="imgFile" aria-invalid="false">
									    <label id="file_inp0">
									    	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
									            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
									            </path>
									        </svg>
									    <?php if($setting['filename'] == "" || $setting['filename'] == null){?> 
									    	<span id="file_inp00">Choose a file…</span>
									    <?php } else { ?>
									    	<span id="file_inp00"><?=$setting['filename']?><?=$setting['fileext']?></span>
									    <?php }?> 
									    </label>
									    <button type="button" class="button button-empty" onclick="javascript:fileDelete('basic');" onsubmit="return false"></button>
									    <button type="button" class="button button-download ml-1" <?php if($setting['filename'] != null && $setting['filename'] != ""){?>onclick="location.href='<?=HOME_DIR?>/symposium/download_file?template_filename=<?=$setting['filename']?><?=$setting['fileext']?>'"<?php }else{?>onclick="alert('저장된파일이 없습니다.')" <?php }?> onsubmit="return false"></button>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">심포지엄 준비 사무국 정보(에이전시)</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group mb-0">
		                            <textarea name="agency_info" id="agency_info" style="height: 120px;" class="form-control"><?=$setting['agency_info']?></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">심포지엄 안내문(기본값)</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group mb-0">
		                            <textarea name="symposium_info" id="symposium_info" style="height: 120px;" class="form-control"><?=$setting['symposium_info']?></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header d-flex justify-content-between">
		                        <h6 class="card-title">심포지엄 관련 정보(이미지 슬라이드)</h6>
		                        <div class="align-self-start" style="height: 30px;">
		                            <label class="switch">
		                                <input type="checkbox" name="template_onoff" id="template_onoff" <?if($setting['template_onoff'] == 'Y') {?> checked <?}?> value="Y">
		                                <span class="slider"></span>
		                            </label>
		                        </div>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <label>이미지 등록</label>
		                            <p class="small mb-1">등록 이미지는 1024 X free 픽셀 사이즈, 파일크기 2MB 이하를 권장합니다.</p>
		                            <?php 
				                    $i = 1;
				                    foreach ( $templateList as $key => $item ) { ?>
		                            <div class="d-flex align-items-center mb-2">
		                                <label class="switch mr-1">
		                                    <input type="checkbox" name="onoff<?=$i?>" id="onoff<?=$i?>" <?if($item['onoff'] == 'Y') {?> checked <?}?> value="Y">
		                                    <span class="slider"></span>
		                                </label>
		                                <input type="hidden" id="temp_cd<?=$i?>" name="temp_cd<?=$i?>" value="<?=$item['setting_template_cd']?>" />
					                    <select class="custom-select" id="PreRegiSelect<?=$i?>" name="PreRegiSelect<?=$i?>" style="width: auto;">
						                    <option value=""  <?php if($item['sub_num'] == null) { ?> selected <?php }?>>선택</option>
						                    <option value="1" <?php if($item['sub_num'] == '1') { ?> selected <?php }?>>1번째</option>
						                    <option value="2" <?php if($item['sub_num'] == '2') { ?> selected <?php }?>>2번째</option>
						                    <option value="3" <?php if($item['sub_num'] == '3') { ?> selected <?php }?>>3번째</option>
						                    <option value="4" <?php if($item['sub_num'] == '4') { ?> selected <?php }?>>4번째</option>
						                </select>
		                                <input type="file" name="file<?=$i?>" class="inputFile" id="imgFile<?=$i?>" aria-invalid="false">
									    <label id="file_inp<?=$i?>">
									    	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
									            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
									            </path>
									        </svg>
									    <?php if($item['filename'] == "" || $item['filename'] == null){?> 
									    	<span id="file_inp<?=$i?><?=$i?>">Choose a file…</span>
									    <?php } else { ?>
									    	<span id="file_inp<?=$i?><?=$i?>"><?=$item['filename']?><?=$item['fileext']?></span>
									    <?php }?> 
									    </label>
									    <button type="button" class="button button-empty" onclick="javascript:fileDelete(<?=$i?>);" onsubmit="return false"></button>
									    <button type="button" class="button button-download ml-1" <?php if($item['filename'] != null && $item['filename'] != ""){?>onclick="location.href='<?=HOME_DIR?>/symposium/download_file?template_filename=<?=$item['filename']?><?=$item['fileext']?>'"<?php }else{?>onclick="alert('저장된파일이 없습니다.')" <?php }?> onsubmit="return false"></button>
									</div>
									<?php
									$i++; }?>	        
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header d-flex justify-content-between">
		                        <h6 class="card-title">컨텐츠 영역 1</h6>
		                        <div class="align-self-start" style="height: 30px;">
		                            <label class="switch">
		                                <input type="checkbox" name="content_onoff1" id="content_onoff1" <?if($setting['content_onoff1'] == 'Y') {?> checked <?}?> value="Y">
		                                <span class="slider"></span>
		                            </label>
		                        </div>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <label for="s11">영역 제목</label>
		                            <input type="text" class="form-control" name="content_subject1" id="content_subject1" value="<?=$setting['content_subject1']?>" placeholder="e.g. 인사말">
		                        </div>
		
		                        <div class="form-group mb-0">
		                            <label>이미지 등록</label>
		                            <p class="small mb-1">등록 이미지는 1024 X free 픽셀 사이즈, 파일크기 2MB 이하를 권장합니다.</p>
		                            <div class="d-flex">
		                                <input type="file" name="content_file1" class="inputFile" id="content_file1" aria-invalid="false">
									    <label id="content_file_inp1">
									    	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
									            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
									            </path>
									        </svg>
									    <?php if($setting['content_filename1'] == "" || $setting['content_filename1'] == null){?> 
									    	<span id="content_file_inp11">Choose a file…</span>
									    <?php } else { ?>
									    	<span id="content_file_inp11"><?=$setting['content_filename1']?><?=$setting['content_fileext1']?></span>
									    <?php }?> 
									    </label>
									    <button type="button" class="button button-empty" onclick="javascript:fileDelete('content_subject1');" onsubmit="return false"></button>
									    <button type="button" class="button button-download ml-1" <?php if($setting['content_filename1'] != null && $setting['content_filename1'] != ""){?>onclick="location.href='<?=HOME_DIR?>/symposium/download_file?template_filename=<?=$setting['content_filename1']?><?=$setting['content_fileext1']?>'"<?php }else{?>onclick="alert('저장된파일이 없습니다.')" <?php }?> onsubmit="return false"></button>
		                            </div>
		                        </div>
		
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header d-flex justify-content-between">
		                        <h6 class="card-title">컨텐츠 영역 2</h6>
		                        <div class="align-self-start" style="height: 30px;">
		                            <label class="switch">
		                            	<input type="checkbox" name="content_onoff2" id="content_onoff2" <?if($setting['content_onoff2'] == 'Y') {?> checked <?}?> value="Y">
		                                <span class="slider"></span>
		                            </label>
		                        </div>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <label for="s21">영역 제목</label>
		                            <input type="text" class="form-control" name="content_subject2" id="content_subject2" value="<?=$setting['content_subject2']?>" placeholder="e.g. 인사말">
		                        </div>
		
		                        <div class="form-group mb-0">
		                            <label>이미지 등록</label>
		                            <p class="small mb-1">등록 이미지는 1024 X free 픽셀 사이즈, 파일크기 2MB 이하를 권장합니다.</p>
		                            <div class="d-flex">
		                                <input type="file" name="content_file2" class="inputFile" id="content_file2" aria-invalid="false">
									    <label id="content_file_inp2">
									    	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
									            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
									            </path>
									        </svg>
									    <?php if($setting['content_filename2'] == "" || $setting['content_filename2'] == null){?> 
									    	<span id="content_file_inp22">Choose a file…</span>
									    <?php } else { ?>
									    	<span id="content_file_inp22"><?=$setting['content_filename2']?><?=$setting['content_fileext2']?></span>
									    <?php }?> 
									    </label>
									    <button type="button" class="button button-empty" onclick="javascript:fileDelete('content_subject2');" onsubmit="return false"></button>
									    <button type="button" class="button button-download ml-1" <?php if($setting['content_filename2'] != null && $setting['content_filename2'] != ""){?>onclick="location.href='<?=HOME_DIR?>/symposium/download_file?template_filename=<?=$setting['content_filename2']?><?=$setting['content_fileext2']?>'"<?php }else{?>onclick="alert('저장된파일이 없습니다.')" <?php }?> onsubmit="return false"></button>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
	                <div class="col-12 text-center">
	                    <button type="button" class="button" onclick="javascript:templateUpdate();">저장하기</button>
	                </div>
	            </div>
	        </form>
	    </div>
	</section>
