<body id="body" class="body site pre-registration page-default is-header" aria-expanded="false">
	<script>
	    function goHome(){
	    	$("#hcpform").attr("action", "<?=HOME_DIR?>/symposium/hcp_home/");
		    $("#hcpform").submit();
	    }
	
		function goDetail(){
			$("#hcpform").attr("action", "<?=HOME_DIR?>/symposium/hcp_detail/");
		    $("#hcpform").submit();
	    }
	</script>
	<form id="hcpform" name="hcpform" action="" method="post">
		<input type="hidden" id="applicant_cd" name="applicant_cd" value="<?=$applicant_cd?>"/>
	    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
	    <input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
	</form>
    <div id="page" class="page" aria-expanded="false">
        <div id="bg-over"></div>
        <header id="masthead" class="site-header" aria-expanded="false">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="site-header-wrap">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <nav id="site-navigation" class="main-navigation">
                                            <div class="menu-primary-container">
                                                <ul id="primary-top" class="primary-top nav-menu" aria-expanded="false">
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#" class="main-menu-item menu-link href-empty">개인정보 수집 및 활용 동의서</a>
                                                        <ul class="nav-menu-sub">
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:privacyPop();" class="sub-menu-item menu-link">개인정보 수집 및 이용에 대한 동의</a>
                                                            </li>
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:termPop();" class="sub-menu-item menu-link">제 3자 제공에 대한 동의</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
            <div class="container">
                <div class="row">
                    <div class="col-12 justify-content-center align-items-center">
                        <div class="msg-congratulation">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-10 col-lg-8">
                                    <figure>
                                        <img src="<?=HOME_DIR?>/images/registration/sign.svg" alt="싸인">
                                    </figure>
                                    <h4><?=$applicant_name?> 선생님 감사합니다.</h4>
                                    <h3><?=$symposium['eventName']?></h3>
                                    <p class="py-2"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><br><?=$symposium['presenter']?><br></p>
                                    <p>해당 심포지엄에 대한 사전등록 신청이 정상적으로 완료되었습니다.</p>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-10 col-lg-8">
                                    <ul class="ul-button-wrap">
                                        <li><a href="javascript:goDetail();" class="button button-black-outline">등록내역확인</a></li>
                                        <li><a href="javascript:goHome();" class="button button-black-outline">Home</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>