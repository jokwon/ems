<?php

?>
<script type="application/javascript">

	var firstSlick = true;

	$(document).ready(function() {

		$('.modal').on('shown.bs.modal', function(e) {
            if (firstSlick) {
                firstSlick = false;
                $('.slick-wrap').on('init', function(event, slick) {
                    $('.slick-item').eq(0).addClass('active-item');
                });
                $('.slick-wrap').on('afterChange', function(event, slick, currentSlide) {
                    $('.slick-item').removeClass('active-item');
                    $(this).find('.slick-item').eq(currentSlide).addClass('active-item');
                })

                $('.slick-wrap').slick({
                    arrows: false,
                    dots: true,
                    infinite: true,
                    fade: true,
                    rows: 0,
                    autoplay: true,
                    swipeToSlide: true,
                    speed: 500,
                    autoplaySpeed: 7000,
                });
            }
        });

		$(document).on('click','#file_inp0',function() {
	        $('#imgFile').click();
	    });
		$('#imgFile').change(function() {
            $('#file_inp00').html(this.files[0].name);
            $('#fileDel').val("");
        });

		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
	    
	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(4);
	    swiper.el.childNodes[1].children[4].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });

	});

    function templateUpdate() {

        if(!check_null("private_desc","개인정보 수집 및 활용동의서 안내문을")) return;
        if(!check_null("private_collection","수집 및 이용목적을")) return;
        if(!check_null("private_collection_nc","수집 및 이용목적의 필수항목을")) return;
        if(!check_null("private_collection_se","수집 및 이용목적의 선택항목을")) return;
        if(!check_null("private_item_nc","수집항목의 필수항목을")) return;
        if(!check_null("private_item_se","수집항목의 선택항목을")) return;
        if(!check_null("private_period"," 보유 및 이용기간을")) return;
        if(!check_null("private_disadvantage","거부권 및 거부에 따른 불이익을")) return;
        if($("#file_inp00").text()=="Choose a file…") {
        	alert("개인정보 수집 및 고유식별 정보의 제 3자 제공에 대한 동의의 이미지 파일을 첨부해주세요");
        	return;
        }
        
		$('#temFrm').attr('action', '<?=HOME_DIR?>/settings/template_upload_private');
		$('#temFrm').submit();
	}

    function fileDelete() {
		$('#imgFile').val('');
		$('#file_inp00').html('Choose a file…');
		$('#fileDel').val('Y');
    }

</script>
<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
                <div class="col-12">
                    <div class="filter-wrap">
                        <div>
                            <ul class="ul-tab">
                                <li class="item"><a href="javascript:move('symposium', '<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>');" class="link">사전등록 페이지</a></li>
                                <li class="item active"><a href="javascript:move('symposium', '<?=HOME_DIR?>/symposium/sympo_personalInfo/<?=$symposium['sympoCd']?>');" class="link">개인정보 활용동의</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
			<form name="temFrm" id="temFrm" method="post" enctype="multipart/form-data">
				<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$symposium['sympoCd']?>">
                <input type="hidden" id="fileDel" name="fileDel" value="">
		        <div class="row mt-3">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">개인정보 수집 및 활용동의서 안내문</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group mb-0">
		                            <textarea name="private_desc" id="private_desc" style="height: 150px;" class="form-control"><?=$setting['private_desc']?></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row mt-3">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">1. 개인정보 수집 및 이용에 대한 동의 문구</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <h6>① 수집 및 이용목적</h6>
		                            <textarea name="private_collection" id="private_collection" style="height: 150px;" class="form-control"><?=$setting['private_collection']?></textarea>
		                        </div>
		                        <div class="form-group">
		                            <label for="s2">■ 필수</label>
		                            <textarea name="private_collection_nc" id="private_collection_nc" style="height: 150px;" class="form-control"><?=$setting['private_collection_nc']?></textarea>
		                        </div>
		                        <div class="form-group">
		                            <label for="s3">■ 선택</label>
		                            <textarea name="private_collection_se" id="private_collection_se" style="height: 150px;" class="form-control"><?=$setting['private_collection_se']?></textarea>
		                        </div>
		                        <div class="form-group">
		                            <h6>② 수집항목</h6>
		                            <label for="s4">■ 필수</label>
		                            <textarea name="private_item_nc" id="private_item_nc" style="height: 150px;" class="form-control"><?=$setting['private_item_nc']?></textarea>
		                        </div>
		                        <div class="form-group">
		                            <label for="s5">■ 선택</label>
		                            <textarea name="private_item_se" id="private_item_se" style="height: 150px;" class="form-control"><?=$setting['private_item_se']?></textarea>
		                        </div>
		                        <div class="form-group">
		                            <h6>③ 보유 및 이용기간</h6>
		                            <textarea name="private_period" id="private_period" style="height: 150px;" class="form-control"><?=$setting['private_period']?></textarea>
		                        </div>
		                        <div class="form-group mb-0">
		                            <h6>④ 거부권 및 거부에 따른 불이익</h6>
		                            <textarea name="private_disadvantage" id="private_disadvantage" style="height: 150px;" class="form-control"><?=$setting['private_disadvantage']?></textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row mt-3">
		            <div class="col-12">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">개인정보 수집 및 고유식별 정보의 제 3자 제공에 대한 동의</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group mb-0">
		                            <label>이미지 등록</label>
		                            <p class="small mb-1">등록 이미지는 1024 X free 픽셀 사이즈, 파일크기 2MB 이하를 권장합니다.</p>
		                            <div class="d-flex">
		                                <input type="file" name="file" class="inputFile" id="imgFile" aria-invalid="false">
		                                <label id="file_inp0"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
		                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
		                                        </path>
		                                    </svg> 
		                                    <?php if($setting['private_filename'] == "" || $setting['private_filename'] == null){?> 
										    	<span id="file_inp00">Choose a file…</span>
										    <?php } else { ?>
										    	<span id="file_inp00"><?=$setting['private_filename']?><?=$setting['private_fileext']?></span>
										    <?php }?>
		                                </label>
		                                <button type="button" class="button button-empty" onclick="javascript:fileDelete();" onsubmit="return false"></button>
		                                <button type="button" class="button button-download ml-1" <?php if($setting['private_filename'] != null && $setting['private_filename'] != ""){?>onclick="location.href='<?=HOME_DIR?>/symposium/download_file?template_filename=<?=$setting['private_filename']?><?=$setting['private_fileext']?>'"<?php }else{?>onclick="alert('저장된파일이 없습니다.')" <?php }?> onsubmit="return false"></button>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12 text-center">
		                <button type="button" class="button" onclick="javascript:templateUpdate();">저장하기</button>
		            </div>
		        </div>
	        </form>
	    </div>
	</section>
