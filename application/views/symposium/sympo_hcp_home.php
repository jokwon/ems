<?php

?>
<body id="body" class="body site pre-registration" aria-expanded="false">
	<script>

		function goHome(){
		    $("#hcpHomeform").attr("action", "<?=HOME_DIR?>/symposium/hcp_home/");
		    $("#hcpHomeform").submit();
	    }

		function regist(){
			var applicant_cd = '<?=$applicant_cd?>';
			if(applicant_cd != null && applicant_cd != ''){
				alert('이미 사전등록이 완료되었습니다.');
				return;
			}
			
		    $("#hcpHomeform").attr("action", "<?=HOME_DIR?>/symposium/hcp_step1/");
		    $("#hcpHomeform").submit();
	    }

	    function goDetail(){
		    if($('#applicant_cd').val() == null || $('#applicant_cd').val() == '')
		    {
			    alert('사전등록 내역이 없습니다.');
		    } else{
				$("#hcpDetail").submit();
		    }
	    }
	</script>
	<form id="hcpHomeform" name="hcpHomeform" action="" method="post">
	    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
	    <input type="hidden" id="applicant_cd" name="applicant_cd" value="<?=$applicant_cd?>"/>
	    <input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
	</form>
	<form id="hcpDetail" name="hcpDetail" action="<?=HOME_DIR?>/symposium/hcp_detail/" method="post">
		<input type="hidden" id="applicant_cd" name="applicant_cd" value="<?=$applicant_cd?>"/>
	    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympo_cd?>"/>
	    <input type="hidden" id="invitation" name="invitation" value="<?=$invitation?>"/>
	</form>
    <div id="page" class="page" aria-expanded="false">
        <div id="bg-over"></div>
        <header id="masthead" class="site-header" aria-expanded="false">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="site-header-wrap">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Menu<span></span></button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <nav id="site-navigation" class="main-navigation">
                                            <div class="menu-primary-container">
                                                <ul id="primary-top" class="primary-top nav-menu" aria-expanded="false">
                                                    <li class="main-menu-item menu-item">
                                                        <a href="javascript:goHome();" class="main-menu-item menu-link">HOME</a>
                                                    </li>
                                                    <?php if($setting['content_onoff1'] == 'Y') {?>
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#<?= $setting['content_subject1']?>" class="main-menu-item menu-link quick-go"><?= $setting['content_subject1']?></a>
                                                    </li>
                                                    <?php }?>
                                                    <?php if($setting['content_onoff2'] == 'Y') {?>
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#<?= $setting['content_subject2']?>" class="main-menu-item menu-link quick-go"><?= $setting['content_subject2']?></a>
                                                    </li>
                                                    <?php }?>
                                                    <li class="main-menu-item menu-item">
                                                        <a href="#" class="main-menu-item menu-link href-empty">개인정보 수집 및 활용 동의서</a>
                                                        <ul class="nav-menu-sub">
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:privacyPop();" class="sub-menu-item menu-link">개인정보 수집 및 이용에 대한 동의</a>
                                                            </li>
                                                            <li class="sub-menu-item menu-item">
                                                                <a href="javascript:termPop();" class="sub-menu-item menu-link">제 3자 제공에 대한 동의</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="main-menu-item menu-item">
                                                        <a href="javascript:goDetail();" class="main-menu-item menu-link">사전등록 내용확인</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="div-button-wrap">
                            <button class="button" onclick="regist()">사전등록 신청</button>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="content" class="site-content">
            <section class="top-hero">
                <div class="bg" style="background-image: url('<?=HOME_DIR?><?=$setting['application_img']?>')"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="title-top"><?=$symposium['eventName']?></h2>
                            <p><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?></p>
                            <p class="mb-0"><?=$symposium['presenter']?></p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="row my-5">
                    <div class="col-12">
                        <h4 class="title-bar">심포지엄 안내문</h4>
                        <p><?=$setting['symposium_info']?></p>
                        <a href="javascript:;" class="button button-black-link" onclick="regist()">사전등록 신청</a>
                    </div>
                </div>
                <?php if($setting['template_onoff'] == 'Y') {?>
                <div class="row">
                    <div class="col-12">
                        <h4 class="title-bar">심포지엄 관련정보</h4>
                        <div class="slide-wrap" style="margin-bottom:40px">
                            <div class="slick-wrap">
                            	<?php 
			                    $flag = 0;
			                    foreach ( $templateList as $key => $item ) { 
			                    	if($item['onoff'] == 'N'){
			                    		$flag++;
			                    	}
			                    }
			                    if($flag != 4){
				                    foreach ( $templateList as $key => $item ) { 
				                    	if($item['onoff'] == 'Y'){
				                    ?>
				                    <div class="slick-item">
				                    	<figure>
				                    <?php if($item['application_img'] != null && $item['application_img'] != '') {?>
							            	<img src="<?=HOME_DIR?><?=$item['application_img']?>">
							        <?php } else {?>
							           		<img src="<?=HOME_DIR?><?=$item['application_img']?>">
							        <?php }?>
							        	</figure>
				                    </div>
									<?php }
				                    }
								}else{ ?>
									<div class="slick-item">
										<figure>
											<img src="<?=HOME_DIR?>/images/application/bg-1.jpg" alt="심포지엄 관련정보">
										</figure>
				                    </div>
								<?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
                <?php if($setting['content_onoff1'] == 'Y') {?>
                <div class="row" id="<?= $setting['content_subject1']?>">
                    <div class="col-12">
                        <h4 class="title-bar"><?=$setting['content_subject1']?></h4>
                        <figure>
                            <img src="<?=HOME_DIR?><?=$setting['content_img1']?>" class="border border-secondary">
                        </figure>
                    </div>
                </div>
                <?php }?>
                <?php if($setting['content_onoff2'] == 'Y') {?>
                <div class="row" id="<?=$setting['content_subject2']?>">
                    <div class="col-12">
                        <h4 class="title-bar"><?=$setting['content_subject2']?></h4>
                        <figure>
                            <img src="<?=HOME_DIR?><?=$setting['content_img2']?>" class="border border-secondary">
                        </figure>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>