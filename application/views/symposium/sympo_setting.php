<?php
?>
<style>
    .sympo-setbox {min-height: 75px !important;}
    .ft-wt-bd {font-weight: 500;}
</style>
<script type="application/javascript">
	
	$(document).ready(function() {

		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
		
	    var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(5);
	    swiper.el.childNodes[1].children[5].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });
	});

    function sympoSettingUpdate() {

        var btn1Yn = $(":input:checkbox[name=btn1Yn]:checked").val();
        var btn3Yn = $(":input:checkbox[name=btn3Yn]:checked").val();
        
        if(btn1Yn != 'Y') {
            btn1Yn = 'N'
        }
        if(btn3Yn != 'Y') {
            btn3Yn = 'N'
        }

        var param = {
            sympo_cd: '<?=$symposium['sympoCd']?>',
            btn1Yn: btn1Yn,
            btn3Yn: btn3Yn,
        };

        $.post('<?=HOME_DIR?>/settings/settingSympoUpdate', param, function(data) {
            alert('저장되었습니다.');
            window.location.reload();
        });

       
    }
</script>
<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12" id="<?=$symposium['sympoCd']?>gorupview">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-3">
	            <div class="col-12 col-md-6">
	                <div class="card">
	                    <div class="card-header">
	                        <h6 class="card-title">등록기능 활성화</h6>
	                    </div>
	                    <div class="card-body">
	                        <table class="table text-center mb-0">
	                            <colgroup>
	                                <col style="width: 50%">
	                                <col style="width: 50%">
	                            </colgroup>
	                            <tbody>
	                                <tr>
	                                    <td>개별등록</td>
	                                    <td>
	                                        <label class="switch">
	                                            <input type="checkbox" name="btn1Yn" id="btn1Yn" <?if($row['btn1Yn'] == 'Y') {?> checked <?}?> value="Y">
	                                            <span class="slider"></span>
	                                        </label>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>방문등록</td>
	                                    <td>
	                                        <label class="switch">
	                                            <input type="checkbox" name="btn3Yn" id="btn3Yn" <?if($row['btn3Yn'] == 'Y') {?> checked <?}?> value="Y">
	                                            <span class="slider"></span>
	                                        </label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12 text-center">
	                <button type="button" class="button" onclick="javascript:sympoSettingUpdate();">저장하기</button>
	            </div>
	        </div>
		</div>
	</section>