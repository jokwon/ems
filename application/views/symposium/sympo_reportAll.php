<?php

?>
<script type="application/javascript">

	$(document).ready(function() {

		var preView = 0;
		<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
		preView = 6;
		<?php } else if($authority == 'ag') {?>
		preView = 5;
		<?php } else if($authority == 'mr') {?>
		preView = 4;
		<?php }?>
	
		var swiper = new Swiper('.swiper-container', {
	        slidesPerView: 'auto',
	        slidesPerView: preView,
	        spaceBetween: 0,
	        allowTouchMove: false,
	        navigation: {
	            nextEl: '.custom-swiper-button-next',
	            prevEl: '.custom-swiper-button-prev',
	        },
	        breakpoints: {
	            567: {
	                slidesPerView: 3,
	                allowTouchMove: true,
	            },
	        }
	    });
	    swiper.slideTo(3);
	    swiper.el.childNodes[1].children[3].classList.add('highlight');

	    $(window).resize(function() {
            setTimeout(function() {
                swiper.update();
            }, 500);
        });
	
	    // date picker
	    $('.input-date').datepicker({
	        showOn: 'button',
	        showButtonPanel: false,
	        buttonImage: '<?=HOME_DIR?>/images/icon/calendar.svg',
	        buttonImageOnly: true,
	        showMonthAfterYear: true,
	        dateFormat: 'yy-mm-dd',
	    });
	});
	
	
	var randomScalingFactor = function() {
	    return Math.round(Math.random() * 100);
	};
	
	var configPie = {
	    type: 'pie',
	    data: {
	        datasets: [{
	            data: [
	            	<?=$report['regiPSRCount']?>,
	            	<?=$report['regiHCPCount']?>,
	            ],
	            backgroundColor: [
	                '#e27d5f',
	                '#84cdc9',
	            ],
	            borderWidth: 0,
	        }],
	        labels: [
	            '예비등록',
	            '방문등록',
	        ]
	    },
	    options: {
	        responsive: true,
	        maintainAspectRatio: false,
	        legend: {
	            display: true,
	            position: 'right',
	            labels: {
	                boxWidth: 10,
	            }
	        },
	    }
	};
	
	var configBar = {
	    type: 'bar',
	    data: {
	        datasets: [{
	            data: [
	            	<?foreach ($reportDays as $key => $item) {?>
	            	<?=$item['count']?>,
	                <?}?>
	            ],
	            backgroundColor: '#40b3a2',
	        }],
	        labels: [
	        	<?foreach ($reportDays as $key => $item) {?>
	        	'<?=$item['date']?>',
	            <?}?>
	        ]
	    },
	    options: {
	        responsive: true,
	        maintainAspectRatio: false,
	        legend: {
	            display: false,
	        },
	        layout: {
	            padding: {
	                left: 0,
	                right: 0,
	                top: 0,
	                bottom: 0,
	            }
	        }
	    }
	};
	
	window.onload = function() {
	    Chart.defaults.global.legend.labels.usePointStyle = true;
	    var ctxPie = document.getElementById('chart-pie').getContext('2d');
	    var ctxBar = document.getElementById('chart-bar').getContext('2d');
	    window.myPie = new Chart(ctxPie, configPie);
	    window.myBar = new Chart(ctxBar, configBar);
	};


    function reportSel(obj) {
        var val = $(obj).val();
        if(val == 'type1') {
            window.location = '<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>';
        } else if(val == 'type2') {
            window.location = '<?=HOME_DIR?>/symposium/sympo_reportOffice/<?=$symposium['sympoCd']?>';
        } else {
            window.location = '<?=HOME_DIR?>/symposium/sympo_reportDate/<?=$symposium['sympoCd']?>';
        }
    }

    function dateCheck(inThis){

	    if($("#scRegDtSt").val()!="" && $("#scRegDtEd").val()!=""){
	        if($("#scRegDtSt").val() > $("#scRegDtEd").val()){
	            alert("검색 시작 날짜는 마지막날짜 보다 작아야 합니다.");
	            inThis.value="";
	            inThis.focus();
	        }
	    }
	
	    var eventEndDate = '<?=$symposium['eventEndDate']?>';
	    if(eventEndDate.replace(/\./g, '-') <= $("#scRegDtEd").val() || eventEndDate.replace(/\./g, '-') <= $("#scRegDtSt").val()) {
	        alert("검색 날짜는 심포지엄 진행 마지막 날짜 보다 작아야 합니다.");
	        inThis.value="";
	        inThis.focus();
	    }
	}

    function search() {

        var scRegDtSt = $('#scRegDtSt').val();
        var scRegDtEd = $('#scRegDtEd').val();

        if(scRegDtSt == '' && scRegDtEd == '') {

        } else {
            if(scRegDtSt == '' || scRegDtEd == '') {
                if(scRegDtSt == '') {
                    alert('시작일을 입력하세요');
                    $('#scRegDtSt').focus();
                    return;
                }
                if(scRegDtEd == '') {
                    alert('종료일을 입력하세요');
                    $('#scRegDtEd').focus();
                    return;
                }
            }
        }

        $('#sform').submit();
    }
</script>

<main class="main">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">심포지엄</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12" id="<?=$symposium['sympoCd']?>gorupview">
	            	<?php
					$groupNamesArr = explode(",", $symposium['groupNames']);
                    foreach ($groupNamesArr as $groupName) {
                    	if($groupName != null && $groupName != '') { ?>
	                		<button type="button" class="tag tag-brand"><?=$groupName?></button>
	                <?php }
                    } ?>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h4 class="symposium-title include-tag">
	                <?php if($symposium['sympoType'] == 'LIVE') {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-live"><?=$symposium['sympoType']?></button>
	                <?php } else {?>
	                	<button type="button" id="<?=$symposium['sympoCd']?>_sympoType" class="tag tag-demand"><?=$symposium['sympoType']?></button>
	                <?php }?>
	                <?=$symposium['eventName']?>
	                </h4>
	                <small class="small-date"><?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?><em>|</em> <?=$symposium['presenter']?></small>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$symposium['sympoCd']?>"><?php echo HOME_DIR?>상세정보</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>">등록관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_attend/<?=$symposium['sympoCd']?>">참가관리</a></div>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>">보고서</a></div>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm' || $authority == 'ag') {?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_application/<?=$symposium['sympoCd']?>">신청서관리</a></div>
	                        <?php }?>
	                        <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm'){?>
	                        <div class="swiper-slide"><a href="<?=HOME_DIR?>/symposium/sympo_setting/<?=$symposium['sympoCd']?>">설정관리</a></div>
	                        <?php }?>
	                    </div>
	                    <div class="swiper-button custom-swiper-button-next"></div>
	                    <div class="swiper-button custom-swiper-button-prev"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <form name="sform" id="sform" action="<?=HOME_DIR?>/symposium/sympo_reportAll/<?=$symposium['sympoCd']?>" method="post">
	                    <div class="filter-wrap md-column">
	                        <div class="d-inline-flex">
	                            <div class="input-group">
	                                <div class="input-group-prepend">
	                                    <div class="prepend-title red">보고서 TYPE</div>
	                                </div>
	                                <select name="searchType" id="searchType" class="custom-select" style="width: auto;" onchange="reportSel(this);">
	                                    <option value="type1">심포지엄</option>
	                                    <option value="type2">담당 영업소별</option>
	                                    <option value="type3">등록일별</option>
	                                </select>
	                            </div>
	                            <div class="d-inline-block d-md-none position-relative">
	                                <img src="/assets/images/png/empty.png" style="width:40px; height: 40px;" class="ml-1" alt="">
	                                <button type="button" class="button button-search big position-absolute toggle-search" data-target="search-zone" style="top: 0; right: 0;"></button>
	                            </div>
	                        </div>
	                        <div class="search-zone md-hidden">
	                            <div class="input-group">
	                                <div class="box-round">
	                                    <div class="div-date" id="dateStart">
	                                        <input type="text" id="scRegDtSt" name="scRegDtSt" class="input-date" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$scRegDtSt?>">
	                                    </div>
	                                    <div class="div-date mx-1" id="blockAnd">~</div>
	                                    <div class="div-date" id="dateEnd">
	                                        <input type="text" id="scRegDtEd" name="scRegDtEd" class="input-date" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$scRegDtEd?>">
	                                    </div>
	                                </div>
	                                <div class="input-group-append">
	                                    <button class="btn" type="submit" id="button-addon-search" onclick="javascript:search();">검색</button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <ul class="ul-icon no-icon">
	                    <li onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>');" style="cursor: pointer;">
                            <dl>
                                <dt>사전등록완료</dt>
                                <dd class="font-color-red"><?=$report['regiCount']?></dd>
                            </dl>
	                    </li>
	                    <li onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_complete/<?=$symposium['sympoCd']?>');" style="cursor: pointer;">
                            <dl>
                                <dt>패스코드 발급완료</dt>
                                <dd><?=$report['passcodeCount']?></dd>
                            </dl>
	                    </li>
	                    <li onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_attend_cancel/<?=$symposium['sympoCd']?>');" style="cursor: pointer;">
	                        <dl>
	                            <dt>참가취소</dt>
	                            <dd><?=$report['cancelCount']?></dd>
	                        </dl>
	                    </li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <ul class="ul-icon divide mt-2">
	                    <li onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>');" style="cursor: pointer;">
	                        <dl>
	                            <dt>예비등록</dt>
	                            <dd class="font-color-blue"><?=$report['regiPSRCount']?></dd>
	                        </dl>
	                    </li>
	                    <li onclick="move('symposium', '<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$symposium['sympoCd']?>');" style="cursor: pointer;">
	                        <dl>
	                            <dt>방문등록</dt>
	                            <dd class="font-color-blue"><?=$report['regiHCPCount']?></dd>
	                        </dl>
	                    </li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h5 class="line-title">
	                    사전등록 방식별 사전등록완료자 현황
	                    <a href="#" class="tooltip-info outline-none">
	                        <i class="demo-icon icon-info-circled" data-toggle="tooltip" data-placement="top" title="Tooltip on top"></i>
	                        <div class="tooltip-box">사전모집중인 심포지엄 현항만 보실 수 있습니다.</div>
	                    </a>
	                </h5>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <div class="box-wrap p-5">
	                	<div style="height: 350px;">
	                    	<canvas id="chart-pie"></canvas>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <h5 class="line-title mt-1">
	                    일별 사전등록완료 현황
	                    <a href="#" class="tooltip-info outline-none">
	                        <i class="demo-icon icon-info-circled" data-toggle="tooltip" data-placement="top" title="Tooltip on top"></i>
	                        <div class="tooltip-box">일별 심포지엄에 사전등록한 완료자 현황만 보실 수 있습니다.</div>
	                    </a>
	                </h5>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12">
	                <div class="box-wrap p-5">
	                	<div style="height: 350px;">
	                    	<canvas id="chart-bar"></canvas>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>