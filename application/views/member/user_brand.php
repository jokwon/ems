<?php
?>

<script type="application/javascript">

	var checkBoxes =""

	$(document).ready(function() 
	{
    	checkBoxes = document.querySelectorAll("input[type=checkbox]");
    });

	function allChk() 
	{
        if($("#all").prop("checked")){
            $("input[name=c]").prop("checked",true);
            checkboxCount()
        }else{
            $("input[name=c]").prop("checked",false);
            checkboxCount()
        }
    }
    
    function checkboxCount()
    {
    	var cnt = 0;
        var target = document.querySelector('.div-table-footer');
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked === true) {
                cnt++;
            }
        }
        (cnt > 0) ? target.classList.add('active'): target.classList.remove('active');
    }

    
    function brandAdd() 
    {
        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/brandAdd/',
            data: null,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $('#modalAdd').modal()
                 }
            }
		});
    }

    function excelDown(obj) 
    {
        var searchType = $('#sType').val();
        var searchText = $('#sText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();
        var fileType = $(obj).val();

        var param = {
            searchType: searchType,
            searchText: searchText,
            sortWhere: sortWhere,
            sortType: sortType,
            fileType: fileType
        };

        if(fileType != '') {
            $.post('<?=HOME_DIR?>/member/brandGroupExcelAjax/', param, function (data) {
                window.location = data;
            });
        }
    }

    function searchText() 
    {
        $('#searchText').val($('#sText').val());
        $('#searchType').val($('#sType').val());
        $('#grpform').submit();
    }

    function sort(sortWhere, sortType) 
    {
        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);
        $('#grpform').submit();
    }

    var page = 1;
    function more() 
    {
        page += 1;

        var html = '';
        var searchType = $('#searchType').val();
        var searchText = $('#searchText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/member/user_brandAjax",
            dataType: "json",
            data: {
                page: page,
                searchType: searchType,
                searchText: searchText,
                sortWhere: sortWhere,
                sortType: sortType,
            },
            success: function (data) {

                if(data.length != 0) {

                    for (var key in data) {
                    	html += '<tr>';
                    	<?php if($authority == 'op') {?>
                    	html += '<td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="' + data[key]['brand_group_cd'] + '" onchange="checkboxCount();"><span></span></label></td>';
                        <?php }?>
                        html += '<td><a href="<?=HOME_DIR?>/member/user_brand_detail?brand_group_cd=' + data[key]['brand_group_cd'] + '">' + data[key]['brand_group_name'] + ' [' + data[key]['pCount'] + ']</a></td>';
                        html += '<td>' + data[key]['org_name'] + '</td>';
                        html += '<td class="hidden-mobile">' + data[key]['createdate'] + '<small class="small-date">(' + data[key]['updatedate'] + ')</small></td>';
                        html += '</tr>';
                    }

                    $('#tbody').append(html);

                    if(data.length < 10) {
                        $('#more').hide();
                    }
                } else {
                    alert('데이터가 없습니다.');
                }
                
                checkBoxes = document.querySelectorAll("input[class=cv]");
                
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function brandGroupDel() 
    {
        var brandGroupCds = '';
        $("input[name=c]:checked").each(function () {
            brandGroupCds += $(this).val() + ';';
        });

        if(brandGroupCds == null || brandGroupCds == '') {
            alert('브랜드계정을 선택하셔야 합니다.');
            return;
        }

        if(confirm('삭제 하시겠습니까?')) {
            $('#brandGroupCds').val(brandGroupCds);

            $('#grpform').attr('method', 'post');
            $('#grpform').attr('action', '<?=HOME_DIR?>/member/brandGroupDel');
            $('#grpform').submit();
        }
    }
</script>

<form id="grpform" name="grpform" action="<?=HOME_DIR?>/member/user_brand" method="post">
    <input type="hidden" id="brandGroupCds" name="brandGroupCds" value=""/>
    <input type="hidden" id="searchType" name="searchType" value="<?=$searchType?>"/>
    <input type="hidden" id="searchText" name="searchText" value="<?=$searchText?>"/>
    <input type="hidden" id="sortType" name="sortType" value="<?=$sortType?>"/>
    <input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere?>"/>
</form>

<main class="main menu-address">
	<nav aria-label="breadcrumb">
	    <div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">사용자 관리</h4>
	    <div class="div">
	</nav>
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	                <div class="div-tab-warp">
	                    <ul>
	                        <li class="item"><a class="link" href="javascript:move('member', '<?=HOME_DIR?>/member/user_all/');">전체사용자</a></li>
	                        <li class="item active"><a class="link" href="javascript:move('member', '<?=HOME_DIR?>/member/user_brand/');">브랜드 계정</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="filter-wrap md-column align-items-center">
	                    <div class="d-inline-flex align-items-center">
	                        <h5 class="title m-0 mr-3">총 <?=$brandGroupCount['cnt'] ?>건</h5>
	                        <?php if($authority == 'op') {?>
	                        <button type="button" class="button button-responsive" onclick="brandAdd()">그룹 추가</button>
	                        <?php }?>
	                    </div>
	                    <div class="d-inline-block d-md-none position-relative mt-0">
	                        <button type="button" class="button button-search position-absolute toggle-search" data-target="search-zone" style="bottom: 0; right: 0;"></button>
	                    </div>
	                    <div class="search-zone md-hidden d-md-inline-flex">
	                        <div class="input-group search-wrap">
	                            <div class="input-group-prepend">
	                                <select name="sType" id="sType" class="custom-select" style="width: auto;">
	                                    <option value="brand_group_name" <?php if($searchType == 'brand_group_name') { ?> selected <?php }?> >브랜드계정</option>
	                                    <option value="org_name" <?php if($searchType == 'org_name') { ?> selected <?php }?> >회사계정</option>
	                                </select>
	                            </div>
	                            <input type="text" class="form-control form-responsive" placeholder="검색어 입력" aria-label="검색어 입력" id="sText" name="sText" aria-describedby="button-addon-search" value="<?=$searchText?>">
	                            <div class="input-group-append">
	                                <button class="btn" type="button" id="button-addon-search" onclick="javascript:searchText();">검색</button>
	                            </div>
	                        </div>
	                        <div class="export-select-wrap ml-1">
	                            <select class="custom-select white-mode" style="width: auto;" onchange="excelDown(this);">
	                                <option value="">EXPORT</option>
	                                <option value="csv">CSV</option>
	                                <option value="excel">EXCEL</option>
	                            </select>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-3">
	            <div class="col-12">
	                <table class="table text-center">
	                    <colgroup>
		                    <?php if($authority == 'op') {?>
		                        <col style="width: 10%;">
		                        <col style="width: 30%;">
		                        <col style="width: 30%;">
		                        <col style="width: 30%;" class="hidden-mobile">
		                    <?php } else {?>
		                        <col style="width: 35%;">
		                        <col style="width: 35%;">
		                        <col style="width: 30%;" class="hidden-mobile">
		                    <?php }?>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                        	<?php if($authority == 'op') {?>
	                            <th><label class="checkbox"><input type="checkbox" id="all" name="all" class="check-all cv" onclick="allChk()"><span></span></label></th>
	                            <?php }?>
	                            <th>브랜드계정</th>
	                            <th>회사계정</th>
	                            <th class="hidden-mobile">
	                            <?php if($sortWhere == 'createdate' && $sortType == 'desc') { ?>
			                        <a href="javascript:sort('createdate', 'asc');" class="tbl_sort">등록일 / 수정일<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a>
			                    <?php } else { ?>
			                        <a href="javascript:sort('createdate', 'desc');" class="tbl_sort">등록일 / 수정일<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a>
			                    <?php } ?>
			                    </th>
	                        </tr>
	                    </thead>
	                    <tbody id="tbody">
	                    <?php if(count($brandGroupList) > 0) { ?>
	    					<?php foreach ($brandGroupList as $key => $row) { ?>
	                        <tr>
	                        	<?php if($authority == 'op') {?>
	                            <td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="<?=$row['brand_group_cd'] ?>" onchange="checkboxCount();"><span></span></label></td>
	                            <?php }?>
	                            <td><a href="<?=HOME_DIR?>/member/user_brand_detail?brand_group_cd=<?=$row['brand_group_cd'] ?>"><?=$row['brand_group_name'] ?>[<?=$row['pCount'] ?>]</a></td>
	                            <td><?=$row['org_name'] ?></td>
	                            <td class="hidden-mobile">
	                                <?=$row['createdate'] ?>
	                                <small class="small-date">(<?=$row['updatedate'] ?>)</small>
	                            </td>
	                        </tr>
	                        <?php } ?>
						<?php } else {?>
							<tr><td colspan="10">브랜드가 존재하지 않습니다.</td></tr>
						<?php }?>
	                    </tbody>
	                </table>
	                <div class="div-table-footer">
	                    <button type="button" class="tag tag-icon delete" onclick="brandGroupDel();"><span><img src="<?=HOME_DIR?>/images/icon/delete.svg" alt="삭제"></span>삭제</button>
	                </div>
	            </div>
	        </div>
	        <?php if(count($brandGroupList) >= 10) {?>
	        <div class="row">
	            <div class="col-12 text-center">
	                <a href="javascript:;" id="more" class="button gray" onclick="javascript:more();">리스트 추가 보기</a>
	            </div>
	        </div>
	        <?php }?>
	    </div>
	</section>