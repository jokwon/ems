<?php

?>

<script type="text/javascript">

	$(document).ready(function() {
	    tabList(".tabList", '.tabView', 1, "click");
	})
	
    function idSearch(){
		if(!check_null("member_name1","이름을")) return ;
        if(!check_null("member_hp","모바일을")) return ;
        var member_name = $('#member_name1').val();
        var member_hp = $('#member_hp').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/member/idSearch_ajax",
            dataType: "json",
            async: false,
            data: {
                member_name: member_name,
                member_hp: member_hp
            },
            success: function (data) {
                $('#idresult1').hide();
                $('#idresult2').hide();
                if(data.list.length > 0) {
                    var html = '';
                    for(var i=0;i<data.list.length;i++) {
                        html += '<p class="login_txt">가입하신 이메일 계정은 [<span id="resultID">'+data.list[i].member_id+'</span>] 입니다.</p>';
                    }
                    $('#idresult1').html(html);
                    $('#idresult1').show();
                } else {
                    $('#idresult2').show();
                }
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

	function pwSearch() {
        if(!check_null("member_name2","이름을")) return ;
        if(!check_null("member_email","이메일을")) return ;
        var member_name = $("#member_name2").val();
        var member_email = $("#member_email").val();

        if(!isEmail("member_email")) return;

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/member/idSearchEmail",
            dataType: "json",
            async: false,
            data: {
                member_name: member_name,
                member_email: member_email
            },
            success: function (data) {
                if(data.result == "ok") {
                    alert("이메일이 발송되었습니다.");
                    window.location = '<?=HOME_DIR?>/login';
                } else if(data.result == "not") {
                    alert("일치하는 아이디가 없습니다.");
                } else {
                    alert("이메일 발송에 실패하였습니다.");
                }
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

</script>
    <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="site-brand text-center">
                                    <img src="<?=HOME_DIR?>/images/logo/logo-white.svg" alt="ASOS">
                                </h1>
                                <div class="tabList">
                                    <ul>
                                        <li class="item">
                                            <a href="#" class="link">가입 계정 찾기</a>
                                        </li>
                                        <li class="item">
                                            <a href="#" class="link">비밀번호 찾기</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tabView">
                                    <ul>
                                        <li>
										<div class="form-group">
										    <input type="text" class="form-control form-control-lg" name="member_name1" id="member_name1" placeholder="이름을 입력하세요.">
										</div>
										<div class="form-group">
										    <input type="text" class="form-control form-control-lg" name="member_hp" id="member_hp" placeholder="모바일 번호를 입력하세요." onkeydown="return numberChk(event);">
										</div>
										<div id="idresult1" style="display: none;">
										    <p class="login_txt">가입하신 이메일 계정은 [<span id="resultID">이메일@이메일.com</span>] 입니다.</p>
										</div>
										<div id="idresult2" style="display: none;">
										    <p class="login_txt" style="color: red;">입력하신 정보와 일치하는 계정이 없습니다.</p>
										</div>
										<div class="form-group mt-3">
										    <button type="button" class="button w-100 button-lg" onclick="javascript:idSearch();">계정 찾기</button>
										</div>
                                            <p class="mb-5 text-center">
                                                <a href="<?=HOME_DIR?>/">로그인 화면 가기</a>
                                            </p>
                                        </li>
                                        <li>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-lg" name="member_name2" id="member_name2" placeholder="이름을 입력하세요.">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control form-control-lg" name="member_email" id="member_email" placeholder="이메일 주소를 입력하세요.">
                                            </div>
                                            <small>가입시 등록한 정보를 입력하시고 <strong>[비밀번호 찾기]</strong> 버튼을 눌러주세요. 가입되었던 이메일로
                                                    임시비밀번호를 발송해 드립니다.</small>
                                            <div class="form-group mt-3">
                                                <button type="button" class="button w-100 button-lg" onclick="javascript:pwSearch();">비밀번호 찾기</button>
                                            </div>
                                            <p class="mb-5 text-center">
                                                <a href="<?=HOME_DIR?>/">로그인 화면 가기</a>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h6>솔루션 사무국</h6>
                                <p class="small opacity-50">
                                    사용과 관련된 문의 사항은 하단의 메일 및 전화번호로 연락 주시기 바랍니다.
                                </p>
                                <dl class="dl-list">
                                    <dt>이메일:</dt>
                                    <dd>
                                        <a href="mailto:support@webinars.co.kr" class="opacity-50">support@webinars.co.kr</a>
                                    </dd>
                                    <dt>전화번호:</dt>
                                    <dd>
                                        <a href="tel:02-6342-6830" class="opacity-50"><?=$dashboard_num1?></a>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>

</html>
