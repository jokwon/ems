<?php

?>


<script type="application/javascript">
    var certification = '';

    function updateMem(){
        var email = '<?=$member_email?>';

        if(!check_null("member_pass","비밀번호을")) return ;

        if($('#newPw1').val() != null && $('#newPw1').val() != '') {
            if($('#newPw1').val() != $('#newPw2').val()) {
                alert('새로운 비밀번호를 확인해주세요.');
                return ;
            }

            if($('#newPw2').val() == null || $('#newPw2').val() == '') {
                alert('새로운 비밀번호를 확인을 입력 해주세요.');
                return ;
            }
        }

        $("#joinfrm").submit();
    }

    function pwCheck(obj) {

        var pw = $('#newPw1').val();
        var pwchk = $(obj).val();

        if(pw != pwchk) {
            $('#pwConfirm').html('새로운 비밀번호가 일치하지 않습니다.');
            $('#pwConfirm').css('color','red');
        } else {
            $('#pwConfirm').html('새로운 비밀번호가 일치합니다.');
            $('#pwConfirm').css('color','blue');
        }
    }

</script>


<main class="main menu-address">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">마이페이지</h4>
	    </div>
	</nav>
	
	<section class="section">
		<form name="joinfrm" id="joinfrm" method="post" action="<?=HOME_DIR?>/member/mypageUpdate">
            <input type="hidden" id="member_cd" name="member_cd" value="<?=$member_cd?>"/>
		    <div class="container-fluid p-0">
		        <div class="row">
		            <div class="col-12 col-md-6">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">나의 기본정보</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <label for="member_id">이메일</label>
		                            <input type="text" class="form-control" name="member_id" id="member_id" value="<?=$row['member_id'] ?>" readonly>
		                        </div>
		                        <div class="form-group">
		                            <label for="member_name">이름</label>
		                            <input type="text" class="form-control" name="member_name" id="member_name" value="<?=$row['member_name'] ?>" readonly>
		                        </div>
		                        <div class="form-group">
		                            <label for="member_org">회사명</label>
		                            <input type="text" class="form-control" name="member_org" id="member_org" value="<?=$row['member_org'] ?>" placeholder="e.g. 웨비나스">
		                        </div>
		                        <div class="form-group">
		                            <label for="member_dep">부서명</label>
		                            <input type="text" class="form-control" name="member_dep" id="member_dep" value="<?=$row['member_dep'] ?>" placeholder="e.g. 마케팅부">
		                        </div>
		                        <div class="form-group">
		                            <label for="member_team">팀명</label>
		                            <input type="text" class="form-control" name="member_team" id="member_team" value="<?=$row['member_team'] ?>" placeholder="e.g. 마케팅 1팀">
		                        </div>
		                        <div class="form-group">
                                    <label for="i6-1">담당 영업소</label>
                                    <input type="text" class="form-control" name="office_name" id="office_name" value="<?=$row['office_name'] ?>" readonly>
                                </div>
		                        <div class="form-group mb-0">
		                            <label for="member_brand">브랜드 계정</label>
		                            <input type="text" class="form-control" name="member_brand" id="member_brand" value="<?=$row['brand_names']?>" readonly>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div class="col-12 col-md-6">
		                <div class="card">
		                    <div class="card-header">
		                        <h6 class="card-title">나의 추가정보</h6>
		                    </div>
		                    <div class="card-body">
		                        <div class="form-group">
		                            <label for="member_hp">모바일</label>
		                            <input type="text" class="form-control" name="member_hp" id="member_hp" value="<?=$row['member_hp'] ?>" onkeydown="return numberChk(event);">
		                        </div>
		                        <div class="form-group">
		                            <label for="member_pass">비밀번호</label>
		                            <input type="password" class="form-control" name="member_pass" id="member_pass">
		                        </div>
		                        <div class="form-group">
		                            <label for="newPw1">새로운 비밀번호</label>
		                            <input type="password" class="form-control" name="newPw1" id="newPw1">
		                        </div>
		                        <div class="form-group">
		                            <label for="newPw2">새로운 비밀번호 확인</label><small><span id="pwConfirm"></span></small></dt>
		                            <input type="password" class="form-control" name="newPw2" id="newPw2" onkeyup="pwCheck(this);">
		                        </div>
		                        <div class="form-group mb-0">
		                            <input type="checkbox" class="custom-checkbox" name="agree" id="agree" checked disabled>
		                            <label for="c1"><a href="#n" class="popup" onclick="termPop()" style="text-decoration: underline;">사이트 이용약관</a>,
		                            <a href="#n" class="popup" onclick="privacyPop()"  style="text-decoration: underline;">개인정보처리방침</a>에 대한 안내를 읽고 이에 동의합니다.</label>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col-12 text-center">
		                <button type="button" class="button button-sm" onclick="javascript:updateMem();">수정하기</button>
		            </div>
		        </div>
		    </div>
		</form>
	</section>
