<?php
?>

<script type="application/javascript">

	var checkBoxes =""

    $(document).ready(function() {

        checkBoxes = document.querySelectorAll("input[class=cv]");

        $(document).on('click','.drawer-up' ,function() {
            var $this = $(this);
            $this.next().toggle();
        })
        
        $(document).on('click','.custom-select',function(e) {
            e.stopPropagation();
        })

    });

	function allChk() {
        if($("#all").prop("checked")){
            $("input[name=c]").prop("checked",true);
            checkboxCount()
        }else{
            $("input[name=c]").prop("checked",false);
            checkboxCount()
        }
    }
    
    function checkboxCount() {
    	var cnt = 0;
        var target = document.querySelector('.div-table-footer');
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked === true) {
                cnt++;
            }
        }
        (cnt > 0) ? target.classList.add('active'): target.classList.remove('active');
    }

    function excelDown(obj) {
        var brand_group_cd = $('#brand_group_cd').val();
        var searchType = $('#sType').val();
        var searchText = $('#sText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();
        var authType = $('#authType').val();
        var fileType = $(obj).val();

        var param = {
            brand_group_cd: brand_group_cd,
            searchType: searchType,
            searchText: searchText,
            sortWhere: sortWhere,
            sortType: sortType,
            authType: authType,
            fileType: fileType
        };

        if(fileType != '') {
            $.post('<?=HOME_DIR?>/member/brandGroupPeopleExcelAjax/', param, function (data) {
                window.location = data;
            });
        }
    }

    function searchSort(type) {
        $('#authType').val(type);
        $('#bgmform').submit();
    }

    function searchText() {
        $('#searchText').val($('#sText').val());
        $('#searchType').val($('#sType').val());
        $('#bgmform').submit();
    }

    function sort(sortWhere, sortType) {
        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);
        $('#bgmform').submit();
    }

    var page = 1;
    function more() {
        page += 1;

        var html = '';
        var searchType = $('#searchType').val();
        var searchText = $('#searchText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();
        var authType = $('#authType').val();
        var brand_group_cd = $('#brand_group_cd').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/member/user_brand_detailAjax",
            dataType: "json",
            data: {
                page: page,
                brand_group_cd: brand_group_cd,
                searchType: searchType,
                searchText: searchText,
                sortWhere: sortWhere,
                sortType: sortType,
                authType: authType
            },
            success: function (result) {

                if(result.memberList.length != 0) {

                    var data = result.memberList;

                    for (var key in data) {
                    	html += '<tr>';
                        html += '<td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="' + data[key]['member_cd'] + ':' + data[key]['emailConfirm'] + '" onchange="checkboxCount();"><span></span></label></td>';
                        html += '<td onclick="drawerDown(\''+data[key]['member_cd']+'\');">' + data[key]['member_name'] + '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['member_cd']+'\');">' + data[key]['member_email'] + '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['member_cd']+'\');">' + data[key]['member_org'] + '</td>';
                        html += '<td class="hidden-mobile" onclick="drawerDown(\''+data[key]['member_cd']+'\');">' + data[key]['brand_count'] + '개</td>';
                        html += '<td onclick="drawerDown(\''+data[key]['member_cd']+'\');">'
            			if(data[key]['office_name'] != null && data[key]['office_name'] != "")
            			{
            				html += data[key]['office_name']; 
            			}else {
            				html += '-';
            			}
            			html += '</td>';
            			html += '<td onclick="drawerDown(\''+data[key]['member_cd']+'\');">';
                        if(data[key]['emailConfirm'] == 'true') {
                            html += '<small class="font-color-blue">활성화</small>';
                            html += '<small class="small-date d-none d-md-block">(' + data[key]['member_regdate'] + ')</small>';
                        } else if(data[key]['emailConfirm'] == 'add') {
                            html += '<small class="font-color-red">계정등록</small>';
                        } else if(data[key]['emailConfirm'] == 'mail') {
                            html += '<small class="font-color-red">비활성화</small>';
                            html += '<small class="small-date d-none d-md-block">(' + data[key]['mail_date'] + ')</small>';
                        } else if(data[key]['emailConfirm'] == 'false') {
                            html += '<small class="font-color-red">가입요청</small>';
                        }
                        html += '</td>';
                        html += '<td onclick="drawerDown(\''+data[key]['member_cd']+'\');">'+data[key]['authority'].toUpperCase()+'</td>';
                        html += '</tr>';
                    }

                    $('#tbody').append(html);

                    if(data.length < 10) {
                        $('#more').hide();
                    }
                } else {
                    alert('데이터가 없습니다.');
                }

                checkBoxes = document.querySelectorAll("input[class=cv]");
                
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function brandGroupMemberDel() {

        var memberCds = '';
        var user_authority = '<?=$authority?>'
        var flag = false;

        $("input[name=c]:checked").each(function () {
            var valArr = $(this).val().split(":");
            if(valArr[0] == 'MEM_0000000000000000')
            {
                alert('admin 계정은 삭제할 수 없습니다.');
                flag = true;
                return;
            }

            var param = {
                    member_cd: valArr[0]
            };

            $.ajax({
                type: 'POST',
                url: '<?=HOME_DIR?>/popup/getAuthority/',
                data: param,
                async: false,
                success: function(data) {
                    data = JSON.parse(data);
                	if(user_authority == 'pm' && (data[0]['authority'] == 'mcm' || data[0]['authority'] == 'op')){
                    	alert('자신보다 높은 등급의 사용자는 삭제할 수 없습니다.');
                    	flag = true;
                    	return;
            		}
                }
    		});
    		
            memberCds += valArr[0] + ';';
        });

        if(flag)
        	return;

        if(memberCds == null || memberCds == '') {
            alert('사용자를 선택하셔야 합니다.');
            return;
        }

        if(confirm('삭제 하시겠습니까?')) {
            $('#memberCds').val(memberCds);
            $('#bgmform').attr('method', 'post');
            $('#bgmform').attr('action', '<?=HOME_DIR?>/member/brandGroupMemberDel');
            $('#bgmform').submit();
        }
    }

    function brandInclude() {
        var brand_group_cd = $('#brand_group_cd').val();
        var param = {
            brand_group_cd: brand_group_cd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/brandInclude/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $('#modalAdd').modal();
                 }
            }
		});
    }

    function back(){
        window.location.href = "<?=HOME_DIR?>/member/user_brand/";
    }

    function drawerDown(memberCd) {
        var param = {
        		memberCd: memberCd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/memberDetail/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                	 $('#modalDetail').modal();
                 }
            }
		});
    }
    
</script>

<form id="bgmform" name="bgmform" action="<?=HOME_DIR?>/member/user_brand_detail" method="post">
    <input type="hidden" id="memberCds" name="memberCds" value=""/>
    <input type="hidden" id="brand_group_cd" name="brand_group_cd" value="<?=$brandGroup['brand_group_cd']?>"/>
    <input type="hidden" id="searchType" name="searchType" value="<?=$searchType?>"/>
    <input type="hidden" id="searchText" name="searchText" value="<?=$searchText?>"/>
    <input type="hidden" id="sortType" name="sortType" value="<?=$sortType?>"/>
    <input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere?>"/>
    <input type="hidden" id="authType" name="authType" value="<?=$authType?>"/>
</form>

<form id="duplefrm" name="duplefrm" action="<?=HOME_DIR?>/member/excelDupleDown" method="post">
    <input type="hidden" id="data" name="data" value=""/>
</form>

<main class="main menu-address">
	<nav aria-label="breadcrumb">
	    <div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">사용자 관리</h4>
	    <div class="div">
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	                <div class="div-tab-warp">
	                    <ul>
	                    	<li class="item"><a class="link" href="javascript:move('member', '<?=HOME_DIR?>/member/user_all/');">전체사용자</a></li>
	                        <li class="item active"><a class="link" href="javascript:move('member', '<?=HOME_DIR?>/member/user_brand/');">브랜드 계정</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px">
	            <div class="col-12">
	                <div class="export-wrap">
	                    <div class="d-flex justify-content-between w-100">
	                        <div class="d-inline-flex align-items-center">
	                            <button class="button button-back" onclick="back()"><i class="demo-icon icon-left"></i></button>
	                            <div class="text-center float-none mr-3">
	                                총 <strong><?=$brandGroupPeopleCount['cnt']?></strong>명 (<strong><?=$brandGroup['brand_group_name']?></strong> )
	                            </div>
	                            <button type="button" class="button button-responsive" onclick="javascript:brandInclude();">추가</button>
	                        </div>
	                        <div class="d-inline-block d-sm-none line-height-0">
	                            <button type="button" class="button button-search"></button>
	                        </div>
	                        <div class="export-select-wrap">
	                            <select class="custom-select white-mode" style="width: auto;" onchange="excelDown(this);">
	                                <option value="">EXPORT</option>
	                                <option value="csv">CSV</option>
	                                <option value="excel">EXCEL</option>
	                            </select>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-10px mobile-toggle-wrap">
	            <div class="col-12">
		            <div class="filter-wrap md-column">
		                <div>
		                    <ul class="ul-tab">
		                        <li class="<?php if($authType == 'All') { ?> item active <?php }else {?> item <?php }?>"><a href="javascript:searchSort('All');" class="link">전체보기</a></li>
	                            <li class="<?php if($authType == 'mcm') { ?> item active <?php }else {?> item <?php }?>"><a href="javascript:searchSort('mcm');" class="link">MCM</a></li>
	                            <li class="<?php if($authType == 'pm') { ?> item active <?php }else {?> item <?php }?>"><a href="javascript:searchSort('pm');" class="link">PM</a></li>
	                            <li class="<?php if($authType == 'mr') { ?> item active <?php }else {?> item <?php }?>"><a href="javascript:searchSort('mr');" class="link">MR</a></li>
	                            <li class="<?php if($authType == 'ag') { ?> item active <?php }else {?> item <?php }?>"><a href="javascript:searchSort('ag');" class="link">AG</a></li>
		                    </ul>
		                </div>
		                <div>
		                    <div class="input-group search-wrap">
		                        <div class="input-group-prepend">
		                            <select name="sType" id="sType" class="custom-select" style="width: auto;">
		                                <option value="member_name" <?php if($searchType == 'member_name') { ?> selected <?php }?> >성명</option>
			                            <option value="member_email" <?php if($searchType == 'member_email') { ?> selected <?php }?> >이메일</option>
			                            <option value="member_hp" <?php if($searchType == 'member_hp') { ?> selected <?php }?>>모바일</option>
			                            <option value="member_org" <?php if($searchType == 'member_org') { ?> selected <?php }?>>회사명</option>
			                            <option value="member_dep" <?php if($searchType == 'member_dep') { ?> selected <?php }?>>부서명</option>
			                            <option value="office_name" <?php if($searchType == 'office_name') { ?> selected <?php }?>>담당 영업소</option>
		                            </select>
		                        </div>
		                        <input type="text" class="form-control" placeholder="검색어를 입력하세요." id="sText" name="sText" aria-label="검색어를 입력하세요." aria-describedby="button-addon-search" value="<?=$searchText?>">
		                        <div class="input-group-append">
		                            <button class="btn" type="button" id="button-addon-search" onclick="javascript:searchText();">검색</button>
		                        </div>
		                    </div>
		                </div>
		            </div>
	            </div>
	        </div>
	        <div class="row mt-3">
	            <div class="col-12">
	                <table class="table text-center">
	                    <colgroup>
	                        <col style="width: 5%;">
                            <col style="width: 10%;">
                            <col style="width: 20%;" class="hidden-mobile">
                            <col style="width: 15%;" class="hidden-mobile">
                            <col style="width: 10%;" class="hidden-mobile">
                            <col style="width: 10%;">
                            <col style="width: 20%;">
                            <col style="width: 10%;">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <th><label class="checkbox"><input type="checkbox" name="all" id="all" class="check-all cv" onclick="allChk()"><span></span></label></th>
	                            <?php if($sortWhere == 'member_name' && $sortType == 'desc') { ?>
	                            	<th><a href="javascript:sort('member_name', 'asc');">성명<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
				                    <th><a href="javascript:sort('member_name', 'desc');">성명<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
				                <?php } ?>
	                            <th class="hidden-mobile">이메일</th>
	                            <th class="hidden-mobile">회사명</th>
	                            <th class="hidden-mobile">브랜드</th>
	                            <th>담당 영업소</th>
	                            <th>계정상태</th>
	                            <th>권한</th>
	                        </tr>
	                    </thead>
	                    <tbody id="tbody">
	                    <?php if(count($brandGroupPeopleList) > 0) { ?>
	            			<?php foreach ($brandGroupPeopleList as $key => $row) { ?>
	                        <tr>
	                            <td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="<?=$row['member_cd'] ?>:<?=$row['emailConfirm']?>" onchange="checkboxCount();"><span></span></label>
	                            </td>
	                            <td onclick="drawerDown('<?= $row['member_cd'] ?>');"><?=$row['member_name'] ?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $row['member_cd'] ?>');"><?=$row['member_email'] ?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $row['member_cd'] ?>');"><?=$row['member_org'] ?></td>
	                            <td class="hidden-mobile" onclick="drawerDown('<?= $row['member_cd'] ?>');"><?=$row['brand_count'] ?>개</td>
	                            <td onclick="drawerDown('<?= $row['member_cd'] ?>');"><?php if($row['office_name'] != null && $row['office_name'] != '') { ?><?=$row['office_name'] ?><?php }else{?>-<?php }?></td>
	                            <td onclick="drawerDown('<?= $row['member_cd'] ?>');">
	                            <?php if($row['emailConfirm'] == 'true') { ?>
	                                <small class="font-color-blue">활성화</small>
	                                <small class="small-date d-none d-md-block">(<?=$row['member_regdate'] ?>)</small>
	                            <?php } else if($row['emailConfirm'] == 'add') { ?>
	                            	<small class="font-color-red">계정등록</small>
	                            <?php } else if($row['emailConfirm'] == 'false') { ?>
	                            	<small class="font-color-red">가입요청</small>
	                            <?php } else if($row['emailConfirm'] == 'mail') { ?>
	                            	<small class="font-color-red">비활성화</small>
	                                <small class="small-date d-none d-md-block">(<?=$row['mail_date'] ?>)</small>
	                            <?php } ?>
	                            </td>
	                            <td onclick="drawerDown('<?= $row['member_cd'] ?>');"><?=strtoupper($row['authority'])?></td>
	                        </tr>
	                        <?php } ?>
	        			<?php } else {?>
							<tr><td colspan="10">사용자가 존재하지 않습니다.</td></tr>
						<?php }?>
	                    </tbody>
	                </table>
	                <div class="div-table-footer">
	                    <button type="button" class="tag tag-icon delete" onclick="javascript:brandGroupMemberDel();"><span><img src="<?=HOME_DIR?>/images/icon/delete.svg" alt="삭제"></span>삭제</button>
	                </div>
	            </div>
	        </div>
	        <?php if(count($brandGroupPeopleList) >= 10) {?>
	        <div class="row">
	            <div class="col-12">
	                <div class="text-center">
	                    <a href="javascript:more();" id="more" class="button gray">리스트 추가 보기</a>
	                </div>
	            </div>
	        </div>
	        <?php }?>
	    </div>
	</section>