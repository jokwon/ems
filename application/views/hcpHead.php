<?php 
header("Progma:no-cache");
header("Cache-Control:no-cache,must-revalidate");
?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no, address=no, email=no">
    <title>EMS</title>
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/slick/slick.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/css/main.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/css/main-asos.css">
    <script src="<?=HOME_DIR?>/js/jquery-3.4.1.min.js"></script>
    <script src="<?=HOME_DIR?>/js/formcheck.js?v=<?=time()?>"></script>
    <style>
        html,
        body {
            background-color: white;
            height: 100%;
        }
    </style>
    <script type="text/javascript">

        function move(type, url) {
            window.location = url;
        }

        function privacyPop() {
            $('#modalPrivacy').modal();
        }

        function termPop() {
        	$('#modalAgreement').modal();
        }

        $('input[type="text"]').keydown(function() {
        	  if (event.keyCode === 13) {
        	    event.preventDefault();
        	  };
        	});
    </script>
</head>
