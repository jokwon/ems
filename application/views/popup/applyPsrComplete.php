<?php
?>

<script type="application/javascript">

    function newAdd() {
        var sympoCd = '<?=$sympoCd?>';
        var param = {
            sympoCd: sympoCd,
            type: 'psr'
        };
        
        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/selectType/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                	 $('#modalSignSubmit').modal("hide");
                     $(".modal-backdrop").remove();
                     $('#modalSelect').modal();
                 }
            }
		});
    }

</script>



<div class="modal fade" id="modalSignSubmit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">예비등록 신청서</h5>
                    <small>등록하실 분의 정보를 정확하게 입력해 주세요.</small>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h5 class="pt-5 pb-1">등록 완료하였습니다.</h5>
                <p class="mb-5">이외에 추가 등록을 원하신다면<br>추가 등록 버튼을 선택해 주세요.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="button button-modal" onclick="javascript:newAdd();">추가등록</button>
                <button type="button" class="button muted" data-dismiss="modal" aria-label="Close">닫기</button>
            </div>
        </div>
    </div>
</div>