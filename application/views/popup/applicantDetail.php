<?php 
	$authority = $this->session->userdata('authority');
?>
<script type="text/javascript">

	function applicantDetail(applicantCd) {

        var param = {
        	applicantCd: applicantCd
        };

        $("#modalDetail").modal("hide");
        $(".modal-backdrop").remove();

        $.post('<?=HOME_DIR?>/popup/applicantDetail', param, function(data) {
            $("#pop_layer2").html(data);
            $('#modalDetail').modal();
        });
    }

	function applicantManagerDetail(applicantCd) {

        var param = {
        	applicantCd: applicantCd
        };

        $("#modalDetail").modal("hide");
        $(".modal-backdrop").remove();

        $.post('<?=HOME_DIR?>/popup/applicantManagerDetail', param, function(data) {
            $("#pop_layer2").html(data);
            $('#modalManager').modal();
        });
    }

	function applicantDetailEdit(applicantCd) {

        var param = {
        	applicantCd: applicantCd
        };

        $("#modalDetail").modal("hide");
        $(".modal-backdrop").remove();

        $.post('<?=HOME_DIR?>/popup/applicantDetailEdit', param, function(data) {
            $("#pop_layer2").html(data);
            $('#modalDetailEdit').modal();
        });
    }

	function signAdd(applicantCd){
		var flag = confirm('서명을 추가하시겠습니까?');

		if(flag){

			var applicant_cd = '<?=$applicant['applicantCd']?>';

			var param = {
					applicant_cd: applicant_cd
		    };
		    
			$("#modalDetail").modal("hide");
		    $(".modal-backdrop").remove();
		    
	        $.post('<?=HOME_DIR?>/popup/agreeStep1', param, function(data) {
	          	$("#pop_layer2").html(data);
	            $('#agreeStep1').modal();
	        });
		}
	}

</script>

<style>
	.wordbreak {}
</style>

<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">등록정보 상세보기</h5>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="ul-button mb-4">
                    <li class="active">
                        <a href="javascript:" class="button-modal" nclick="applicantDetail('<?= $applicant['applicantCd'] ?>')">참가자 기본정보</a>
                    </li>
                    <li>
                        <a href="javascript:" class="button-modal" onclick="applicantManagerDetail('<?= $applicant['applicantCd'] ?>')">담당자 등록 정보</a>
                    </li>
                </ul>
                <h6 class="color-red">사전등록 정보</h6>
                <ul class="ul-list-normal">
                    <li>
                        <span>성명</span><span><?if($applicant['applicantName'] != null && $applicant['applicantName'] != ''){?><?=$applicant['applicantName']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>병원명</span><span><?if($applicant['applicantHospital'] != null && $applicant['applicantHospital'] != ''){?><?=$applicant['applicantHospital']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>진료과</span><span><?if($applicant['applicantDep'] != null && $applicant['applicantDep'] != ''){?><?=$applicant['applicantDep']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>모바일</span><span><?if($applicant['applicantHp'] != null && $applicant['applicantHp'] != ''){?><?=$applicant['applicantHp']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>이메일</span><span><?if($applicant['applicantEmail'] != null && $applicant['applicantEmail'] != ''){?><?=$applicant['applicantEmail']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>개인정보 서명</span><span><?if($applicant['filename'] != null && $applicant['filename'] != ''){?><a href="<?=HOME_DIR?>/symposium/pdfDownload?applicant_cd=<?=$applicant['applicantCd']?>"><?=$symposium['eventName']?>_<?=$applicant['applicantName']?>_<?=$applicant['applicantHospital']?>.pdf</a><?php }else{?><a href="javascript:signAdd('<?=$applicant['applicantCd']?>')" class="form-group-right-link" style="color: #b60c33">추가서명 받기</a><?php }?></span>
                    </li>
                    <li>
                        <span>메모</span><span style="word-break:break-all;"><?if($applicant['comment'] != null && $applicant['comment'] != ''){?><?=$applicant['comment']?><?php }else{?>-<?php }?></span>
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">닫기</button>
                <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'ag') {?>
                <button type="button" class="button button-modal" onclick="applicantDetailEdit('<?= $applicant['applicantCd'] ?>')">정보수정</button>
				<?php }?>
            </div>
        </div>
    </div>
</div>