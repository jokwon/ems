<?php

?>
<script type="application/javascript">
    function send() {

        var subject = $('#subject').val();
        var content = $('#content').val();

        if(subject == null || subject =='') {
            alert('제목을 입력하세요.');
            return;          
        }
        if(content == null || content =='') {
            alert('제목을 입력하세요.');
            return;          
        }

        var param = {
            subject: subject,
            content: content,
            customer_name: '<?=$customer_name?>'
        };
        $.post('<?=HOME_DIR?>/popup/inquireComplete/', param, function(data) {
            alert('문의하신 내용이 접수되었습니다.');
        });
    }
</script>

<div class="modal fade" id="modalInquire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">문의하기</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="subject">제목(*)</label>
                    <input type="text" class="form-control" name="subject" id="subject" value="" placeholder="문의하실 내용의 제목을 입력해 주세요.">
                </div>
                <div class="form-group">
                    <label for="content">문의내용(*)</label>
                    <textarea name="content" id="content" style="height: 100px;" class="form-control" placeholder="문의하실 내용을 입력해 주세요."></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                <button type="button" onclick="javascript:send();" class="button" rule="button" data-dismiss="modal" aria-label="Close">확인</button>
            </div>
        </div>
    </div>
</div>
          