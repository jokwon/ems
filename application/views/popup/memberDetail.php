<?php 
	$authority = $this->session->userdata('authority');
?>
<script type="text/javascript">

	function memberDetail(memberCd) {

        var param = {
        		memberCd: memberCd
        };

        $("#modalDetail").modal("hide");
        $(".modal-backdrop").remove();

        $.post('<?=HOME_DIR?>/popup/memberDetail', param, function(data) {
            $("#pop_layer2").html(data);
            $('#modalDetail').modal();
        });
    }

	function memberManagerDetail(memberCd) {

        var param = {
        		memberCd: memberCd
        };

        $("#modalDetail").modal("hide");
        $(".modal-backdrop").remove();

        $.post('<?=HOME_DIR?>/popup/memberManagerDetail', param, function(data) {
            $("#pop_layer2").html(data);
            $('#modalManager').modal();
        });
    }

	function memberDetailEdit(memberCd) {

        var param = {
        	memberCd: memberCd
        };

        $("#modalDetail").modal("hide");
        $(".modal-backdrop").remove();

        $.post('<?=HOME_DIR?>/popup/memberDetailEdit', param, function(data) {
            $("#pop_layer2").html(data);
            $('#modalDetailEdit').modal();
        });
    }

</script>

<style>
	.wordbreak {}
</style>

<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">사용자정보 상세보기</h5>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="ul-button mb-4">
                    <li class="active">
                        <a href="javascript:" class="button-modal" nclick="memberDetail('<?= $member['member_cd'] ?>')">사용자 기본정보</a>
                    </li>
                    <li>
                        <a href="javascript:" class="button-modal" onclick="memberManagerDetail('<?= $member['member_cd'] ?>')">사이트 이용정보</a>
                    </li>
                </ul>
                <ul class="ul-list-normal">
                    <li>
                        <span>성명</span><span><?if($member['member_name'] != null && $member['member_name'] != ''){?><?=$member['member_name']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>모바일</span><span><?if($member['member_hp'] != null && $member['member_hp'] != ''){?><?=$member['member_hp']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>이메일</span><span><?if($member['member_email'] != null && $member['member_email'] != ''){?><?=$member['member_email']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>회사명</span><span><?if($member['member_org'] != null && $member['member_org'] != ''){?><?=$member['member_org']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>부서명</span><span><?if($member['member_dep'] != null && $member['member_dep'] != ''){?><?=$member['member_dep']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>팀명</span><span><?if($member['member_team'] != null && $member['member_team'] != ''){?><?=$member['member_team']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>권한</span><span><?if($member['authority'] != null && $member['authority'] != ''){?><?=strtoupper($member['authority'])?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>담당영업소</span><span><?php if($member['office_name'] != null && $member['office_name'] != ''){?><?=$member['office_name']?><?php }else{?>-<?php }?></span>
                    </li>
                    <li>
                        <span>브랜드 계정</span><span><?php if($member['brand_names'] != null && $member['brand_names'] != ''){ ?><?=$member['brand_names']?><?php } else {?>-<?php }?></span>
                    </li>
                    <br>
                    <input type="checkbox" class="custom-checkbox" <?php if($member['agree'] == 'Y') { ?> checked <?php } ?> value="Y" disabled>
	            	<label for="regist_agree"><em><a href="javascript:termPop();">사이트 이용약관</a>, <a href="javascript:privacyPop();">개인정보 처리방침</a></em> 에 대한 안내를 읽고 이에 동의합니다.</label>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">닫기</button>
                <?php if($authority == 'op' || ($authority == 'mcm' && $member['authority'] != 'op') || ($authority == 'pm' && $member['authority'] != 'op' && $member['authority'] != 'mcm') ) {?>
                <button type="button" class="button button-modal" onclick="memberDetailEdit('<?= $member['member_cd'] ?>')">정보수정</button>
				<?php }?>
            </div>
        </div>
    </div>
</div>