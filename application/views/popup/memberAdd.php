<?php
	$authority = $this->session->userdata('authority');
?>
<script type="application/javascript">

	$('input[type="text"]').keydown(function() {
		if (event.keyCode === 13) {
	    	event.preventDefault();
	  	};
	});

    function member_request() {
        var member_name = $('#member_name').val();
        var member_email = $('#member_email').val();
        var member_org = $('#member_org').val();
        var member_dep = $('#member_dep').val();
        var member_team = $('#member_team').val();
        var member_hp = $('#member_hp').val();
        var authority = $('#authority').val();
        var office_name = $('#office_name').val();
        var brand_group_cds = '';

        if(!check_null("member_name","성명을")) return;
        if(!check_null("member_email","이메일을")) return;
        if(!check_null("member_org","회사명을")) return;
        if(!check_null("member_dep","부서명을")) return;
        if(!check_null("member_team","팀명을")) return;
        if(authority == 'mr'){
        	if(!check_null("office_name","담당 영업소를")) return;
        }
        if(!isEmail("member_email")) return;

        if($('#member_hp').val() != null && $('#member_hp').val() != '') {
            if(!isMobile("member_hp")) return;
        }

        var emailCheckVal = $('#emailCheckVal').val();
        if(emailCheckVal == 'N') {
            alert("이메일 형식이 잘못 되었습니다.");
            $("#member_email").focus();
            return;
        }

        var url = '<?=HOME_DIR?>/member/member_request1';

        $('select[name=brand_group]').each(function() {
            brand_group_cds += $(this).val() + ';';
        });

        var param = {
            member_name: member_name,
            member_email: member_email,
            member_hp: member_hp,
            member_org: member_org,
            member_dep: member_dep,
            member_team: member_team,
            authority: authority,
            office_name: office_name,
            brand_group_cds: brand_group_cds
        };

        $.post(url, param, function() {
            $('#modalAdd').modal('hide');
            alert('등록 하였습니다.');
            window.location.reload();
        });
    }

    function rowDel(obj){
        $(obj).parent().parent().remove();
    }

    function rowAdd(obj){
        var html = '';

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/popup/memeberGroupList",
            dataType: "json",
            data: {},
            success: function (data) {
            	html += '<div class="row mb-1">';
                html += '<div class="col-9 pr-1">';
                html += '<select name="brand_group" id="brand_group" class="custom-select" style="width: 100%; height: 40px;">';
                html += '<option value="">브랜드 계정을 선택하세요.</option>';
                for (var key in data) {
                    html += '<option value="'+data[key]['brand_group_cd']+'">'+data[key]['brand_group_name']+'</option>';
                }
                html += '</select>';
                html += '</div>';
                html += '<div class="col-3 pl-1">';
                html += '<button class="button gray w-100" onclick="rowDel(this)">삭제</button>';
                html += '</div>';
                html += '</div>';
                $(obj).parent().parent().before(html);
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function emailCheck(obj) {
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
        if (!regExp.test($('#member_email').val())) {
            $('#emailConfirm').html('이메일 형식이 잘못 되었습니다.');
            $('#emailConfirm').css('color','red');
            $('#emailCheckVal').val('N');
            return ;
        }

        var member_email = $(obj).val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/member/emailCheck",
            dataType: "json",
            data: {
                member_email: member_email
            },
            success: function (data) {
                if(member_email != null && member_email != '') {
                    if(data.result > 0) {
                        $('#emailConfirm').html('이미 가입된 이메일 주소입니다.');
                        $('#emailConfirm').css('color','red');
                        $('#emailCheckVal').val('N');
                    } else {
                        $('#emailConfirm').html('사용 가능한 이메일입니다.');
                        $('#emailConfirm').css('color','blue');
                        $('#emailCheckVal').val('Y');
                    }
                } else {
                    $('#emailConfirm').html('');
                    $('#emailCheckVal').val('N');
                }
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

</script>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5 class="modal-title" id="exampleModalCenterTitle">사용자정보 추가</h5>
                        <small>사용자 정보를 정확하게 입력해 주세요.</small>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="member_name">성명(*)</label>
                            <input type="text" class="form-control" name="member_name" id="member_name" placeholder="e.g. 홍길동">
                        </div>
                        <div class="form-group">
                            <label for="member_hp">모바일(*)</label>
                            <input type="tel" class="form-control" name="member_hp" id="member_hp" placeholder="e.g. 01011112222" onkeydown="return numberChk(event);">
                        </div>
                        <div class="form-group">
                            <label for="s3">이메일(*)<small id="emailConfirm"></small></label>
                            <input type="email" class="form-control" name="member_email" id="member_email" placeholder="e.g. support@webinars.co.kr" onkeyup="emailCheck(this, event);">
                            <input type="hidden" id="emailCheckVal" value="N"/>
                        </div>
                        <div class="form-group">
                            <label for="s4">회사명(*)</label>
                            <input type="text" class="form-control" name="member_org" id="member_org" placeholder="e.g. 웨비나스">
                        </div>
                        <div class="form-group">
                            <label for="s5">부서명(*)</label>
                            <input type="text" class="form-control" name="member_dep" id="member_dep" placeholder="e.g. 영업부">
                        </div>
                        <div class="form-group">
                            <label for="s6">팀명(*)</label>
                            <input type="text" class="form-control" name="member_team" id="member_team" placeholder="e.g. 영업1팀">
                        </div>
                        <div class="form-group">
                            <label for="s7">권한</label>
                            <select name="authority" id="authority" class="custom-select">
                                <option value="ag">AG</option>
                                <option value="mr" selected>MR</option>
                                <option value="pm">PM</option>
                                <?php if($authority == 'mcm' || $authority == 'op'){?>
                                <option value="mcm">MCM</option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="s8">담당 영업소</label>
                            <select name="office_name" id="office_name" class="custom-select">
                                <option value="" selected>선택</option>
                                <?php foreach ($office_list as $office){?>
                                	<option value="<?=$office['office_name']?>"><?=$office['office_name']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="form-group mb-0">
                            <label>브랜드계정 설정</label>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <button type="button" class="button muted w-100" onclick="rowAdd(this);">브랜드 계정 추가</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button" onclick="javascript:member_request();">작성완료</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
