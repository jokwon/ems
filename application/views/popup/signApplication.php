<?php


?>
<!-- https://github.com/szimek/signature_pad -->
    <script src="<?=HOME_DIR?>/js/vendor/signature_pad.min.js"></script>
    <script>
        $(document).ready(function() {
            // signature
            const canvas = document.getElementById('signature-pad');
            let signaturePad = null;

            $('form').submit(function() {
                var cnt = $('input:checkbox:checked').length;
                var $target = $('.form-error-msg');
                if (cnt != 1) {
                    $target.fadeIn();
                    setTimeout(function() {
                        $target.fadeOut();
                    }, 3000);
                    return false;
                } else {
                    if (signaturePad == null) {
                        $('#modalSignature').modal('show');
                    } else {
                        return true;
                    }
                    return false;
                }
            });
            $('input[type="checkbox"]').on('click', function() {
                var $target = $('.form-error-msg');
                $target.hide();
            })


            function resizeCanvas() {
                console.log('canvas resize');
                // When zoomed out to less than 100%, for some very strange reason,
                // some browsers report devicePixelRatio as less than 1
                // and only part of the canvas is cleared then.
                var ratio = Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * ratio;
                canvas.height = canvas.offsetHeight * ratio;
                canvas.getContext("2d").scale(ratio, ratio);
            }

            function drawSignature() {
                signaturePad = new SignaturePad(canvas);
            }

            window.onresize = resizeCanvas;
            // resizeCanvas();
            document.getElementById('undo').addEventListener('click', function() {
                signaturePad.clear();
            });


            $('button[data-dismiss="modal"]').on('click', function() {
                signaturePad = null;
            });
            $('#modalSignature').on('shown.bs.modal', function(e) {
                resizeCanvas();
                drawSignature();
            })

            document.getElementById('check-sign').addEventListener('click', function() {
                if (signaturePad.isEmpty()) {
                    var $errorMsg = $('.error-sign');
                    $errorMsg.show();
                    setTimeout(function() {
                        $errorMsg.hide();
                    }, 1000);
                } else {
                    var data = signaturePad.toDataURL('image/png');
                    // save signature
                    // check console.log
                    console.log(data);
                    alert('서명되었습니다.');
                    $('#modalSignature').modal('hide');
                    $('#button-submit').trigger('click');
                }
            });
        });
    </script>

<div class="modal fade signature" id="modalSignature" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h2 class="modal-title">서명입력</h2>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="wrapper">
                        <button id="undo" type="button" class="button sign-undo"></button>
                        <canvas id="signature-pad" class="signature-pad">
                        </canvas>
                        <div class="error-sign">
                            <div class="msg">
                                서명후 진행하세요.
                            </div>
                        </div>
                    </div>
                    <small><i class="demo-icon icon-info-circled"></i> 서명 후 '확인'을 누르시면 사전등록 신청이 완료됩니다.</small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button button-black-outline" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button button-default" id="check-sign" onclick="next();">확인</button>
                </div>
            </div>
        </div>
    </div>

