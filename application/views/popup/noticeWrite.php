<?php

?>

<script language="javascript">

	$(document).ready(function() {
	    $('#file_inp0').click(function() {
	        $('#siteFile').click();
	    });
	    $('#siteFile').change(function() {
            $('#file_inp').html(this.files[0].name);
        });
	});

	$('input[type="text"]').keydown(function() {
		if (event.keyCode === 13) {
	    	event.preventDefault();
	  	};
	});

    function sendit(){

        if(!check_null("notice_title","제목을")) return;
        if(!check_null("notice_content","내용을")) return;

        var brand_group_cds = '';
        var flag = true;

        $('select[name=brand_group]').each(function() {
            if($(this).val() == '' || $(this).val() == null){
                alert('브랜드를 선택해 주세요');
                flag = false;
                return;
            }

            brand_group_cds += $(this).val() + ';';
        });

        if(flag){
	        $('#brand_group_cds').val(brand_group_cds);
	        
	        if($('#brand_group_cds').val() == null || $('#brand_group_cds').val() == ''){
				alert('한개 이상의 브랜드를 설정해야 합니다.');
				return;
	        }
	        $("#noticeFrm").submit();
        }
    }

    function fileDelete() {
        $('#file_inp').html('Choose a file…');
        $('#siteFile').val('');
        $('#attfDel').val('Y');
    }

    function rowAdd(obj){
        var html = '';

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/popup/memeberGroupList",
            dataType: "json",
            data: {},
            success: function (data) {
            	html += '<div class="row mb-1">';
                html += '<div class="col-9 pr-1">';
                html += '<select name="brand_group" id="brand_group" class="custom-select" style="width: 100%; height: 40px;">';
                html += '<option value="">브랜드 계정을 선택하세요.</option>';
                for (var key in data) {
                    html += '<option value="'+data[key]['brand_group_cd']+'">'+data[key]['brand_group_name']+'</option>';
                }
                html += '</select>';
                html += '</div>';
                html += '<div class="col-3 pl-1">';
                html += '<button class="button gray w-100" onclick="rowDel(this)">삭제</button>';
                html += '</div>';
                html += '</div>';
                $(obj).parent().parent().before(html);
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function rowDel(obj){
        $(obj).parent().parent().remove();
    }
</script>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">공지사항</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="noticeFrm" id="noticeFrm" method="post" action="<?=HOME_DIR?>/notice/write_exec" enctype="multipart/form-data">
            	<input type="hidden" name="brand_group_cds" id="brand_group_cds" value="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="s3">제목(*)</label>
                        <input type="text" class="form-control" name="notice_title" id="notice_title" placeholder="제목을 입력하세요.">
                    </div>
                    <div class="form-group">
                        <label for="s4">내용(*)</label>
                        <textarea name="notice_content" id="notice_content" style="height: 250px;" class="form-control"></textarea>
                    </div>
                    <div class="form-group mb-0">
                    	<label for="s4">첨부파일</label>
                    	<small class="small mb-1">(단, 16MB 이하의 이미지 파일, PDF 파일만 등록 가능)</small>
                        <div class="row mt-1">
                            <div class="col-12">
                                <input type="file" name="file" class="inputFile" id="siteFile" aria-invalid="false">
                                <label id="file_inp0"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
                                        </path>
                                    </svg> 
										<span id="file_inp">Choose a file…</span>
                                </label>
                                <button type="button" class="button button-empty" onclick="javascript:fileDelete();" onsubmit="return false"></button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <label>브랜드계정 설정(*)</label>
                        <div class="row mb-1">
                            <div class="col-12">
                                <button class="button muted w-100" type="button" onclick="rowAdd(this)">브랜드 계정 추가</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button" onclick="javascript:sendit();">등록하기</button>
                </div>
            </form>
        </div>
    </div>
</div>

