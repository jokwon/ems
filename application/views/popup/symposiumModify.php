<?php
	$authority = $this->session->userdata('authority');
?>
<script type="application/javascript">

	$(document).ready(function() {
	
	    $('.input-date').datepicker({
	        showOn: 'button',
	        showButtonPanel: false,
	        buttonImage: '<?=HOME_DIR?>/images/icon/calendar.svg',
	        buttonImageOnly: true,
	        showMonthAfterYear: true,
	        dateFormat: 'yy-mm-dd',
	    });
	
	    var eventPreRegStartDateTime= '<?=$symposium['eventPreRegStartDateTime']?>';
	    var eventPreRegEndDateTime= '<?=$symposium['eventPreRegEndDateTime']?>';
	
	    if(eventPreRegStartDateTime != '0000.00.00') {
	        $("#scRegDtSt").val(eventPreRegStartDateTime.replace(/\./g, '-'));
	    }
	
	    if(eventPreRegEndDateTime != '0000.00.00') {
	        $("#scRegDtEd").val(eventPreRegEndDateTime.replace(/\./g, '-'));
	    }
	});
	
	function dateChecksympo(inThis){
	    if($("#scRegDtSt").val()!="" && $("#scRegDtEd").val()!=""){
	        if($("#scRegDtSt").val() >= $("#scRegDtEd").val()){
	            alert("사전등록 종료일이 사전등록 시작일과 같거나 늦어야 합니다. ");
	            inThis.value="";
	            inThis.focus();
	        }
	    }
	
	    var eventEndDate = '<?=$symposium['eventEndDate']?>';
	    if(eventEndDate.replace(/\./g, '-') <= $("#scRegDtEd").val()) {
	        alert("사전등록 종료일이 심포지엄 진행일 마지막 날짜 보다 빨라야 합니다. ");
	        inThis.value="";
	        inThis.focus();
	    }
	}

    function sympoUpdate(sympoCd) {
        var sympoType = $(":input:radio[name=sympoType]:checked").val();
        var gubun 	  = $(":input:radio[name=gubun]:checked").val();
        var scRegDtSt = $("#scRegDtSt").val();
        var scRegDtEd = $("#scRegDtEd").val();
        var presenter = $("#presenter").val();

        var brand_group_cds = '';

        <?php if($authority == 'mcm' || $authority == 'op') {?>
	        $('select[name=brand_group]').each(function() {
	            brand_group_cds += $(this).val() + ';';
	        });
	    <?php }?>

        var param = {
            sympoCd: sympoCd,
            sympoType: sympoType,
            gubun: gubun,
            eventPreRegStartDateTime: scRegDtSt,
            eventPreRegEndDateTime: scRegDtEd,
            presenter: presenter,
            <?php if($authority == 'mcm' || $authority == 'op') {?>
            brand_group_cds: brand_group_cds
            <?php }?>
        };

        $.post('<?=HOME_DIR?>/symposium/symposiumUpdate/', param, function (data) {
            alert('업데이트 하였습니다.');
            $('#modalSymposiumSetting').modal("hide");
            window.location.reload();
        });
    }

    function rowAdd(obj){
        var html = '';

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/popup/BrandGroupAllList",
            dataType: "json",
            data: {},
            success: function (data) {
            	html += '<div class="row mb-1">';
                html += '<div class="col-9 pr-1">';
                html += '<select name="brand_group" id="brand_group" class="custom-select" style="width: 100%; height: 40px;">';
                html += '<option value="">브랜드 계정을 선택하세요.</option>';
                for (var key in data) {
                    html += '<option value="'+data[key]['brand_group_cd']+'">'+data[key]['brand_group_name']+'</option>';
                }
                html += '</select>';
                html += '</div>';
                html += '<div class="col-3 pl-1">';
                html += '<button class="button gray w-100" onclick="rowDel(this)">삭제</button>';
                html += '</div>';
                html += '</div>';
                $(obj).parent().parent().before(html);
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function rowDel(obj){
        $(obj).parent().parent().remove();
    }
</script>



<div class="modal fade" id="modalSymposiumSetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">심포지엄 정보설정</h5>
                    <small>심포지엄 정보를 정확하게 입력해 주세요.</small>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label>심포지엄 활성화</label>
                        <div class="mt-1 d-flex align-items-center">
                            <label class="radio"><input type="radio" name="gubun" value="Y" <?php if($symposium['gubun'] == 'Y') {?>checked <?php }?>><span></span>ON</label>
                            <label class="radio"><input type="radio" name="gubun" value="N"<?php if($symposium['gubun'] == 'N') {?>checked <?php }?>><span></span>OFF</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="eventName">심포지엄 제목</label>
                        <input type="text" class="form-control" name="eventName" id="eventName" value="<?=$symposium['eventName']?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="eventDate">심포지엄 진행일정</label>
                        <input type="text" class="form-control" name="eventDate" id="eventDate" value="<?=$symposium['eventStartDate']?>(<?=$symposium['eventStartWeek']?>) <?=$symposium['eventStartTime']?> ~ <?=$symposium['eventEndDate']?>(<?=$symposium['eventEndWeek']?>) <?=$symposium['eventEndTime']?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>심포지엄 구분</label>
                        <div class="mt-1 d-flex align-items-center">
                            <label class="radio"><input type="radio" name="sympoType" value="LIVE" <?if($symposium['sympoType'] == 'LIVE') {?>checked<?}?>><span></span>Live</label>
                            <label class="radio"><input type="radio" name="sympoType" value="On Demand" <?if($symposium['sympoType'] == 'On Demand') {?>checked<?}?>><span></span>On Demand</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>사전등록 진행일정</label>
                        <div class="row">
                            <div class="col-6 pr-1">
                                <div class="date-wrap">
                                    <div class="box-round w-100">
                                        <h5 class="box-title"><span class="d-none d-md-block">시작일시</span><span class="d-block d-md-none">시작</span></h5>
                                        <div class="div-date" id="dateStart">
                                            <input type="text" class="input-date" id="scRegDtSt" name="scRegDtSt" title="시작일시" placeholder="yyyy-mm-dd" onchange="dateChecksympo(this)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 pl-1">
                                <div class="date-wrap">
                                    <div class="box-round w-100">
                                        <h5 class="box-title"><span class="d-none d-md-block">종료일시</span><span class="d-block d-md-none">종료</span></h5>
                                        <div class="div-date" id="dateStart">
                                            <input type="text" id="scRegDtEd" name="scRegDtEd" title="종료일시"  class="input-date" placeholder="yyyy-mm-dd" onchange="dateChecksympo(this)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="presenter">심포지엄 발표자</label>
                        <input type="tel" class="form-control" name="presenter" id="presenter" value="<?=$symposium['presenter']?>" placeholder="e.g. 홍길동 외 OO명">
                    </div>
                    <?php if($authority == 'mcm' || $authority == 'op') {?>
	                    <div class="form-group mb-0">
	                        <label>브랜드계정 설정</label>
	                        <div class="row">
	                            <div class="col-12">
	                            <?php if(count($sympo_to_brand) > 0) {
		                        foreach ($sympo_to_brand as $sympobrand) :
		                        if($sympobrand['default_brand_chk'] == 'Y') { ?>
	                                <button value="<?=$sympobrand['brand_group_name'] ?>" class="tag tag-brand w-100" disabled><?=$sympobrand['brand_group_name'] ?></button>
	                            <?php } ?>
		                        <?php endforeach; ?>
		                        <?php } ?>
	                            </div>
	                        </div>
	                        <?php if(count($sympo_to_brand) > 0) {
	                        foreach ($sympo_to_brand as $sympobrand) :
	                            if($sympobrand['default_brand_chk'] != 'Y') { ?>
		                            <div class="row mb-1">
		                                <div class="col-9 pr-1">
		                                    <select name="brand_group" id="brand_group" class="custom-select" style="width: 100%; height: 40px;">
		                                        <option value="">브랜드 계정을 선택하세요.</option>
		                                        <?php foreach($brandGroupList as $brand) { ?>
		                                        	<?php if($brand['brand_group_cd'] == $sympobrand['brand_group_cd']) { ?>
		                                        		<option value="<?=$brand['brand_group_cd']?>" selected><?=$brand['brand_group_name']?></option>
		                                        	<?php } else { ?>
				                                        <option value="<?=$brand['brand_group_cd']?>"><?=$brand['brand_group_name']?></option>
				                                    <?php } ?>
				                                <?php } ?>
		                                    </select>
		                                </div>
		                                <div class="col-3 pl-1">
		                                    <button class="button gray w-100" onclick="rowDel(this)">삭제</button>
		                                </div>
		                            </div>
	                        	<?php } ?>
	                        <?php endforeach; ?>
	                    	<?php } ?>
	                        <div class="row mt-1">
	                            <div class="col-12">
	                                <button class="button muted w-100" type="button" onclick="rowAdd(this)">브랜드 계정 추가</button>
	                            </div>
	                        </div>
	                    </div>
                    <?php }?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button" onclick="javascript:sympoUpdate('<?=$symposium['sympoCd']?>');">정보수정</button>
                </div>
            </form>
        </div>
    </div>
</div>