<?php

?>

<script type="application/javascript">

	$( document ).ready(function() {
	    $('#file_inp0').click(function() {
	        $('#excelfile').click();
	    });
	    $('#excelfile').change(function() {
            $('#file_inp').html(this.files[0].name);
        });
	});

    function excelInsert(){

    	if($("#file_inp").text() == 'Choose a file…')
        {
            alert('파일을 선택해 주세요');
            return;
        }
        if(confirm("등록하시겠습니까?")){
            $("#eFrm").submit();
        }
    }

    function fileDelete() {
        $('#file_inp').html('Choose a file…');
        $('#fileDel').val('Y');
    }
</script>


<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">주소록 대량 업로드</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="eFrm" id="eFrm" method="post" action="<?=HOME_DIR?>/address/excel_upload_exec" enctype="multipart/form-data">
        	<input type="hidden" id="locationType" name="locationType" value="<?=$locationType?>"/>
                <div class="modal-body">
                    <div>
                        <input type="file" name="file1" class="inputFile" id="excelfile" aria-invalid="false">
                        <label id="file_inp0"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
                                </path>
                            </svg> <span id="file_inp">Choose a file…</span>
                        </label>
                        <button type="button" class="button button-empty" onclick="fileDelete()"></button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button" onclick="excelInsert()">대량업로드</button>
                </div>
            </form>
        </div>
    </div>
</div>
