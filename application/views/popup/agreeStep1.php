<?php

?>
<style>
    label.checkbox span {margin-right: 3px;}
    .modal-body {padding-bottom: 0 !important;}
    .modal-footer {border-top: 1px solid #f2f1ee !important; padding-top: 20px !important; padding-bottom: 22px !important;}
</style>
<script type="application/javascript">

	function agreeStep2() {

		var applicant_cd = '<?=$applicant_cd?>';
		var personalInfoNc = $("input:checkbox[name='personalInfoNc']").is(":checked");
        var personalInfoSe = $("input:checkbox[name='personalInfoSe']").is(":checked");

        if(personalInfoNc && personalInfoSe){
        	var param = {
    	            applicant_cd: applicant_cd
    	        };
    	    
    	    $("#agreeStep1").modal("hide");
    	    $(".modal-backdrop").remove();
    	    
            $.post('<?=HOME_DIR?>/popup/agreeStep2', param, function(data) {
              	$("#pop_layer2").html(data);
                $('#agreeStep1').modal();
            });
        } else {
        	alert("개인정보 처리에 모두 동의해주세요.")
	    }
	}
</script>

<div class="modal fade privacy" id="agreeStep1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h2 class="modal-title">개인정보 처리동의서 (1)</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
	            <section class="privacy">
	                <h3>개인정보 수집 및 활용 동의서</h3>
	                <p class="mb-5"><?=$setting['private_desc']?></p>
	                <h4>1. 개인정보의 수집 및 이용에 대한 동의</h4>
	                <h5>① 수집 및 이용목적:</h5>
	                <div class="indent">
	                    <p><?=$setting['private_collection']?></p>
	                    <h6>■ 필수 : </h6>
	                    <p><?=$setting['private_collection_nc']?></p>
	                    <h6>■ 선택 : </h6>
	                    <p class="mb-4"><?=$setting['private_collection_se']?></p>
	                </div>
	                <h5>② 수집항목:</h5>
	                <div class="indent">
	                    <h6>■ 필수 :</h6>
	                    <p><?=$setting['private_item_nc']?></p>
	                    <h6>■ 선택 :</h6>
	                    <p class="mb-4"><?=$setting['private_item_se']?></p>
	                </div>
	                <h5 class="underline">③ 보유 및 이용기간 :</h5>
	                <p class="underline mb-4"><?=$setting['private_period']?></p>
	                <h5>④ 거부권 및 거부에 따른 불이익:</h5>
	                <p><?=$setting['private_disadvantage']?></p>
	            </section>
	            <div class="row mb-2">
	                <div class="col-12">
	                    <label class="checkbox"><input type="checkbox" class="cv" name="personalInfoNc" value='Y'><span></span> 개인정보의 <strong>필수적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</label>
	                </div>
	            </div>
	            <div class="row mb-5">
	                <div class="col-12">
	                    <label class="checkbox"><input type="checkbox" class="cv" name="personalInfoSe" value='Y'><span></span> 개인정보의 <strong>선택적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</label>
	                </div>
	            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                <button type="button" class="button button-modal" onclick="agreeStep2()">다음단계</button>
            </div>
        </div>
    </div>
</div>


