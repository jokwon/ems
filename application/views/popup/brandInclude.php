<?php

?>
<script type="application/javascript">

	$('input[type="text"]').keydown(function() {
		if (event.keyCode === 13) {
	    	event.preventDefault();
	  	};
	});

    function memSearch() {
        var brand_group_cd = '<?=$brand_group_cd ?>';
        var searchType = $('#psearchType').val();
        var searchText = $('#psearchText').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/popup/brandIncludeMemSearch",
            dataType: "json",
            data: {
                brand_group_cd: brand_group_cd,
                searchType: searchType,
                searchText: searchText
            },
            success: function (data) {
                var html = '';
                if(data.length > 0){
	                for (var key in data) {
	                    html += '<tr>';
	                    html += '<td><label class="checkbox"><input type="checkbox" name="mem_chk2" value="'+data[key]['member_cd']+'"/><span></span></label></td>';
	                    html += '<td>'+data[key]['member_name']+'</td>';
	                    html += '<td>'+data[key]['member_org']+'</td>';
	                    html += '<td>'+data[key]['member_dep']+'</td>';
	                    html += '<td>'+data[key]['member_team']+'</td>';
	                    html += '<td>'+data[key]['authority'].toUpperCase()+'</td>';
	                    html += '</tr>';
	                }
                } else {
                	html += '<tr>';
                    html += '<td colspan="6"> 검색결과가 없습니다.</td>';
                    html += '</tr>';
                }

                $('#popTbody').html(html);
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function allChk2() {
        if($("#all2").prop("checked")){
            $("input[name=mem_chk2]").prop("checked",true);
        }else{
            $("input[name=mem_chk2]").prop("checked",false);
        }
    }

    function brandIncludeMemSelect() {
        var brand_group_cd = '<?=$brand_group_cd ?>';

        var memberCds = '';
        $("input[name=mem_chk2]:checked").each(function () {
            memberCds += $(this).val() + ';';
        });

        var param = {
            brand_group_cd: brand_group_cd,
            memberCds: memberCds
        };

        $.post('<?=HOME_DIR?>/popup/brandIncludeMemSelect/', param, function(result) {
            alert('추가 하였습니다.');
            $('#modalAdd').modal('hide');
            window.location.reload();
        });
    }
</script>

<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">사용자 그룹 설정하기</h5>
                    <small>리스트에 추가할 사용자를 선택하세요.</small>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="s1">사용자검색</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <select name="psearchType" id="psearchType" class="custom-select" style="width: auto;">
                                    <option value="member_name">성명</option>
                                    <option value="member_org">회사명</option>
                                    <option value="member_dep">부서명</option>
                                    <option value="member_team">팀명</option>
                                </select>
                            </div>
                            <input type="text" class="form-control" id="psearchText" name="psearchText" placeholder="검색어를 입력하세요." aria-label="검색어를 입력하세요.">
                            <div class="input-group-append">
                                <button class="btn" type="button" id="button-addon-search" onclick="javascript:memSearch();">검색</button>
                            </div>
                        </div>
                    </div>
                    <table class="table text-center mb-0">
                        <colgroup>
                            <col style="width: 5%">
                            <col style="width: 20%">
                            <col style="width: 20%">
                            <col style="width: 20%">
                            <col style="width: 20%">
                            <col style="width: 15%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th><label class="checkbox"><input type="checkbox" name="all2" id="all2" class="check-all cm" onclick="allChk2()"><span></span></label></th>
                                <th>성명</th>
                                <th>회사명</th>
                                <th>부서명</th>
                                <th>팀명</th>
                                <th>권한</th>
                            </tr>
                        </thead>
                        <tbody id="popTbody">
                            
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button" onclick="javascript:brandIncludeMemSelect();">선택완료</button>
                </div>
            </form>
        </div>
    </div>
</div>