<?php

?>

<script type="text/javascript" src="<?=HOME_DIR?>/js/common.js"></script>
<script type="application/javascript">
    function attentApply() {
        if(confirm("대상자를 등록리스트에서 영구삭제 하시겠습니까?")){
	        var applicantCds = '<?=$applicantCds?>';
	        if(applicantCds != null && applicantCds != '') {
	            $('#popform').submit();
	        }
        }
    }
</script>

<form id="popform" name="popform" action="<?=HOME_DIR?>/symposium/sympo_attend_Del" method="post">
    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympoCd?>"/>
    <input type="hidden" id="applicantCds" name="applicantCds" value="<?=$applicantCds?>"/>
</form>

<div class="modal fade" id="modalSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">참가자 삭제</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <p class="p-4 text-center mb-0">
                        참가자 삭제를 진행하겠습니까?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button" onclick="attentApply();">승인</button>
                </div>
            </form>
        </div>
    </div>
</div>

