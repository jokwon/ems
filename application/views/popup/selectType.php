<?php


?>

<script type="application/javascript">

    function searchSelect(enrollmentType) {
        var sympoCd = '<?=$sympoCd?>';
		var type = '<?=$type?>';

        var param = {
            sympoCd: sympoCd,
            type: type,
            enrollmentType : enrollmentType
        };

        $("#modalSelect").modal("hide");
        $(".modal-backdrop").remove();

        if(type =='hcp') {
    		$.ajax({
                type : 'post',
                url : '<?=HOME_DIR?>/symposium/getInvitationCode/',
                dataType : 'json',
                success : function(result){
                	$('#invitation').val(result);
                	alert("팝업이 허용되지 않은 경우 사전등록 페이지가 열리지 않습니다.");
                	hcpSubmit.submit();
                },
            });
    	} else if(type=='psr'){
    		$.post('<?=HOME_DIR?>/popup/applyPsr', param, function(data) {
    			$("#pop_layer2").html(data);
                $("#modalSign").modal();
	        });
    	}
        
    }

</script>

<form method="post" id="hcpSubmit" action="<?=HOME_DIR?>/symposium/hcp_home" target="_blank">
	<input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympoCd?>"/>
	<input type="hidden" id="invitation" name="invitation" value=""/>
</form>

<div class="modal fade" id="modalSelect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title"><?=$type == 'hcp' ? '방문등록' : '예비등록'?></h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <button type="button" class="button muted w-100 mb-3 button-modal" onclick="searchSelect('handWriting')">고객정보 수기 등록</button>
            </div>
        </div>
    </div>
</div>
