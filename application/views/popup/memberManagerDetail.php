<script type="text/javascript">

	function memberDetail(memberCd) {
	
	    var param = {
	    		memberCd: memberCd
	    };
	
	    $("#modalDetail").modal("hide");
	    $(".modal-backdrop").remove();
	
	    $.post('<?=HOME_DIR?>/popup/memberDetail', param, function(data) {
	        $("#pop_layer2").html(data);
	        $('#modalDetail').modal();
	    });
	}
	
	function memberManagerDetail(memberCd) {
	
	    var param = {
	    		memberCd: memberCd
	    };
	
	    $("#modalDetail").modal("hide");
	    $(".modal-backdrop").remove();
	
	    $.post('<?=HOME_DIR?>/popup/memberManagerDetail', param, function(data) {
	        $("#pop_layer2").html(data);
	        $('#modalManager').modal();
	    });
	}

</script>

<div class="modal fade" id="modalManager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">사용자정보 상세보기</h5>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <ul class="ul-button mb-4">
                        <li>
                            <a href="javascript:" class="button-modal" onclick="memberDetail('<?= $member['member_cd'] ?>')">사용자 기본정보</a>
                        </li>
                        <li  class="active">
                            <a href="javascript:" class="button-modal" onclick="memberManagerDetail('<?= $member['member_cd'] ?>')">사이트 이용정보</a>
                        </li>
                    </ul>
                    <ul class="ul-list-normal">
                        <li>
                            <span>계정상태</span><span><?php if($member['emailConfirm'] == 'true') { ?>활성화<?php } else if($member['emailConfirm'] == 'add') { ?>계정등록<?php } else if($member['emailConfirm'] == 'mail') { ?>비활성화<?php } else if($member['emailConfirm'] == 'false') { ?>가입요청<?php } ?></span>
                        </li>
                        <li>
                            <span>담당 브랜드명</span><span><?php if($member['member_brand'] != null && $member['member_brand'] != '') { ?><?=$member['member_brand']?><?php } else { ?>-<?php } ?></span>
                        </li>
                        <li>
                            <span>계정등록 일시</span><span><?=$member['createdate'] ?></span>
                        </li>
                        <li>
                            <span>가입 신청서 담당자</span><span><?php if($member['regi_manager'] != '' ) { ?><?=$member['regi_manager']?><?php } else { ?>본인작성<?php } ?></span>
                        </li>
                        <li>
                            <span>활성화 메일 전송</span><span><?php if($member['appr_status'] == 'Y' ) { ?>발송 <?php } else { ?>미발송<?php } ?></span>
                        </li>
                        <li>
                            <span>활성화 메일 전송일</span><span><?php if($member['appr_date'] != null && $member['appr_date'] != '') { ?><?=$member['appr_date']?><?php } else { ?>-<?php } ?></span>
                        </li>
                        <li>
                            <span>활성화 메일 완료일</span><span><?php if($member['emailConfirm'] == 'true') { ?><?=$member['member_regdate'] ?><?php } else { ?>-<?php } ?></span>
                        </li>
                        <li>
                            <span>가입 승인 담당자</span><span><?php if($member['appr_name'] != null && $member['appr_name'] != '') { ?><?=$member['appr_name']?> <?= strtoupper($member['appr_authority']) ?><?php } else { ?>-<?php } ?></span>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">닫기</button>
                </div>
            </form>
        </div>
    </div>
</div>