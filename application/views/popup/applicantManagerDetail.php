<script type="text/javascript">

	function applicantDetail(applicantCd) {
		
	    var param = {
	    	applicantCd: applicantCd
	    };
	
	    $("#modalManager").modal("hide");
	    $(".modal-backdrop").remove();
	
	    $.post('<?=HOME_DIR?>/popup/applicantDetail', param, function(data) {
	        $("#pop_layer2").html(data);
	        $('#modalDetail').modal();
	    });
	}
	
	function applicantManagerDetail(applicantCd) {
	
	    var param = {
	    	applicantCd: applicantCd
	    };
	
	    $("#modalManager").modal("hide");
	    $(".modal-backdrop").remove();
	
	    $.post('<?=HOME_DIR?>/popup/applicantManagerDetail', param, function(data) {
	        $("#pop_layer2").html(data);
	        $('#modalManager').modal();
	    });
	}

</script>

<div class="modal fade" id="modalManager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">담당자 등록정보 상세보기</h5>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <ul class="ul-button mb-4">
                        <li>
                            <a href="javascript:" class="button-modal" onclick="applicantDetail('<?= $applicant['applicantCd'] ?>')">참가자 기본정보</a>
                        </li>
                        <li  class="active">
                            <a href="javascript:" class="button-modal" onclick="applicantManagerDetail('<?= $applicant['applicantCd'] ?>')">담당자 등록 정보</a>
                        </li>
                    </ul>
                    <h6 class="color-red">등록 활동 정보</h6>
                    <ul class="ul-list-normal">
                        <li>
                            <span>사전등록 담당자</span><span><?=$applicant['creatorName']?> <?= strtoupper($applicant['creatorAuthority']) ?></span>
                        </li>
                        <li>
                            <span>담당 영업소</span><span><?php if($applicant['creatorOfficeName'] != null && $applicant['creatorOfficeName'] != ''){?><?=$applicant['creatorOfficeName']?><?php }else{?>-<?php }?></span>
                        </li>
                        <li>
                            <span>담당자 연락처</span><span><?=$applicant['creatorHp']?></span>
                        </li>
                        <li>
                            <span>사전등록방식</span>
                            <?php if ($applicant['registType'] == 'PSR') { ?>
                            	<span>예비등록</span>
	                        <?php } else if ($applicant['registType'] == 'HCP') { ?>
	                            <span>방문등록</span>
	                        <?php } ?>
                        </li>
                        <li>
                            <span>사전등록 완료일</span><span><?=$applicant['createdate']?></span>
                        </li>
                        <li>
                            <span>마지막 수정 담당자</span><span><?=$applicant['updaterName']?> <?= strtoupper($applicant['updaterAuthority']) ?></span>
                        </li>
                        <li>
                            <span>등록 최종 수정일</span><span><?=$applicant['updatedate']?></span>
                        </li>
                        <?php if($applicant['passcode'] == 'Y'){?>
                        <li>
                            <span>패스코드 담당자</span><span><?=$applicant['passcodeManagerName']?> <?= strtoupper($applicant['passcodeManagerAuthority']) ?></span>
                        </li>
                        <li>
                            <span>패스코드 발급일</span><span><?=$applicant['passcodedate']?></span>
                        </li>
                        <?php }?>
                        <?php if($applicant['cancel'] == 'Y'){?>
                        <li>
                            <span>참가 취소 담당자</span><span><?=$applicant['cancelManagerName']?> <?= strtoupper($applicant['cancelManagerAuthority']) ?></span>
                        </li>
                        <li>
                            <span>참가 취소일</span><span><?=$applicant['canceldate']?></span>
                        </li>
                        <?php }?>
                        <?php if($applicant['addsignManager'] != null && $applicant['addsignManager'] != ''){?>
                        <li>
                            <span>서명 추가 담당자</span><span><?=$applicant['addsignManagerName']?> <?= strtoupper($applicant['addsignManagerAuthority']) ?></span>
                        </li>
                        <li>
                            <span>서명추가일</span><span><?=$applicant['addsigndate']?></span>
                        </li>
                        <?php }?>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">닫기</button>
                </div>
            </form>
        </div>
    </div>
</div>