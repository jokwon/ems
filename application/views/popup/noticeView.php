<div class="modal fade" id="modalNotice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">공지사항</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table__notice">
                    <colgroup>
                        <col style="width: 25%">
                        <col style="width: 75%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>브랜드계정</th>
                            <td><?=$notice['groupNames']?></td>
                        </tr>
                        <tr>
                            <th>제목</th>
                            <td><?=$notice['noticeTitle']?></td>
                        </tr>
                        <tr>
                            <th>내용</th>
                            <td><?=$notice['noticeContent']?></td>
                        </tr>
                        <tr>
                            <th>첨부파일</th>
                            <?php if($notice['noticeFilename'] == null || $notice['noticeFilename'] == '') {?>
                            	<td>파일없음</td>
                            <?php } else {?>
								<td><a href="<?=HOME_DIR?>/notice/download_file?notice_filename=<?=$notice['noticeFilename']?><?=$notice['noticeFileExt']?>"><?=$notice['noticeFilename']?><?=$notice['noticeFileExt']?></a></td>
							<?php }?>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="button" rule="button" data-dismiss="modal" aria-label="Close">확인</button>
            </div>
        </div>
    </div>
</div>