<?php

?>
<script type="application/javascript">
    function writePsr() {

        var sympoCd = '<?=$sympoCd?>';
        var applicant_name = $('#applicant_name').val();
        var applicant_email = $('#applicant_email').val();
        var applicant_hp = $('#applicant_hp').val();
        var applicant_hospital = $('#applicant_hospital').val();
        var applicant_dep = $('#applicant_dep').val();

        if(!check_null("applicant_name", "성명을")) return;
        if(applicant_name.length > 40){
        	alert('성명을 40자 이하로 입력해주세요.');return;
        }
        if(!check_null("applicant_hospital", "병원명을")) return;
        if(applicant_hospital.length > 40){
        	alert('병원명을 40자 이하로 입력해주세요.');return;
        }
        if(!check_null("applicant_dep", "진료과를")) return;
        if(applicant_dep.length > 10){
        	alert('진료과를 10자 이하로 입력해주세요.');return;
        }
        if(!check_null("applicant_hp", "모바일을")) return;
        if(applicant_hp != null && applicant_hp != "") {
        	if(!isMobile("applicant_hp")) return;
        }
        if(applicant_email != null && applicant_email != "") {
        	if(!isEmail("applicant_email")) return;
        	if(applicant_email.length > 40){
            	alert('이메일을 40자 이하로 입력해주세요.'); return;
            }
        }

        var param = {
        	sympoCd: sympoCd,
            applicant_name: applicant_name,
            applicant_email: applicant_email,
            applicant_hp: applicant_hp,
            applicant_hospital: applicant_hospital,
            applicant_dep: applicant_dep
        };

		$('#modalSign').modal("hide");
        $(".modal-backdrop").remove();
        
		$.post('<?=HOME_DIR?>/popup/applyPsrComplete/', param, function (data) {
            $("#pop_layer2").html(data);
			$('#modalSignSubmit').modal();
		});
    }
</script>

<div class="modal fade" id="modalSign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title">예비등록 신청서</h5>
                    <small>개인정보 활용동의에 대한 동의가 없는 간편 등록 신청입니다.</small>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="s2">성명(*)</label>
                    <input type="text" class="form-control" name="applicant_name" id="applicant_name" placeholder="e.g. 김길동" value="">
                </div>
                <div class="form-group">
                    <label for="s3">병원명(*)</label>
                    <input type="text" class="form-control" name="applicant_hospital" id="applicant_hospital" placeholder="e.g. 한국병원" value="">
                </div>
                <div class="form-group">
                    <label for="s4">진료과(*)</label>
                    <input type="text" class="form-control" name="applicant_dep" id="applicant_dep"  placeholder="e.g. 소아청소년과" value="">
                </div>
                <div class="form-group">
                    <label for="s5">모바일(*)</label>
                    <input type="text" class="form-control" name="applicant_hp" id="applicant_hp" placeholder="e.g. 01011112222" value="">
                </div>
                <div class="form-group">
                    <label for="s6">이메일</label>
                    <input type="text" class="form-control" name="applicant_email" id="applicant_email" placeholder="e.g. support@webinars.co.kr" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                <button type="button" class="button button-modal" onclick="javascript:writePsr();">등록완료</button>
            </div>
        </div>
    </div>
</div>