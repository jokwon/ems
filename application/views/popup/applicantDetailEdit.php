<script type="text/javascript">

	function applicantEdit() {

	    if(confirm("사전등록자 정보를 수정 하시겠습니까?")) {

	    	var applicant_email = $("#applicantEmail").val();
	    	var applicant_hp = $("#applicantHp").val();
	    	var applicant_name = $('#applicantName').val();
	        var applicant_hospital = $('#applicantHospital').val();
	        var applicant_dep = $('#applicantDep').val();
	
	        if(!check_null('applicantName', "성명을")) return;
	        if(applicant_name.length > 40){
            	alert('성명을 40자 이하로 입력해주세요.'); return;
            }
	        if(!check_null('applicantHospital', "병원명을")) return;
	        if(applicant_hospital.length > 40){
            	alert('병원명을 40자 이하로 입력해주세요.');return;
            }
	        if(!check_null('applicantDep', "진료과를")) return;
	        if(applicant_dep.length > 10){
            	alert('진료과를 10자 이하로 입력해주세요.'); return;
            }
			if(applicant_hp == null || applicant_hp == "") {
				alert("모바일은 필수로 작성해야 합니다."); return;
	        }
			if(applicant_hp != null && applicant_hp != "") {
	        	if(!isMobile("applicantHp")) return;
	        }
	        if(applicant_email != null && applicant_email != "") {
	        	if(!isEmail('applicantEmail')) return;
	        	if(applicant_email.length > 40){
	            	alert('이메일을 40자 이하로 입력해주세요.'); return;
	            }
	        }
	
	        $('#applicantFrm').submit();
	    }
	}

	</script>

<div class="modal fade" id="modalDetailEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">참가자 기본정보 수정</h5>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="applicantFrm" id="applicantFrm" method="post" action="<?=HOME_DIR?>/symposium/applicantEdit" enctype="multipart/form-data">
            	<input type="hidden" id="applicantCd" name="applicantCd" value="<?=$applicant['applicantCd']?>"/>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="applicantName">성명(*)</label>
                        <input type="text" class="form-control" name="applicantName" id="applicantName" value="<?=$applicant['applicantName']?>" placeholder="e.g. 홍길동" >
                    </div>
                    <div class="form-group">
                        <label for="applicantHospital">병원명(*)</label>
                        <input type="text" class="form-control" name="applicantHospital" id="applicantHospital" value="<?=$applicant['applicantHospital']?>" placeholder="e.g. 서울성모병원">
                    </div>
                    <div class="form-group">
                        <label for="applicantDep">진료과(*)</label>
                        <input type="text" class="form-control" name="applicantDep" id="applicantDep" value="<?=$applicant['applicantDep']?>" placeholder="e.g. 소화기내과">
                    </div>
                    <div class="form-group">
                        <label for="applicantHp">모바일(*)</label>
                        <input type="text" class="form-control" name="applicantHp" id="applicantHp" value="<?=$applicant['applicantHp']?>" placeholder="e.g. 01011112222">
                    </div>
                    <div class="form-group">
                        <label for="applicantEmail">이메일</label>
                        <input type="email" class="form-control" name="applicantEmail" id="applicantEmail" value="<?=$applicant['applicantEmail']?>" placeholder="e.g. support@webinars.co.kr">
                    </div>
                    <div class="form-group">
                        <label for="commet">메모</label>
                        <textarea name="comment" id="comment" style="height: 100px;" class="form-control"><?=$applicant['comment']?></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button button-modal" onclick="applicantEdit()">정보수정</button
                </div>
            </form>
        </div>
    </div>
</div>