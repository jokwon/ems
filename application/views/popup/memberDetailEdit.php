<?php
	$authority = $this->session->userdata('authority');
?>
<script type="application/javascript">

	function memberEdit() {
		var member_cd = '<?=$member['member_cd']?>';
	    var member_name = $('#member_name').val();
	    var member_org = $('#member_org').val();
	    var member_dep = $('#member_dep').val();
	    var member_team = $('#member_team').val();
	    var member_hp = $('#member_hp').val();
	    var authority = $('#authority').val();
	    var agree = $('#agree'+member_cd).val();
	    var office_name = $('#office_name').val();
	    var brand_group_cds = '';

        if('<?= $this->session->userdata('member_cd')?>' == member_cd) {
            if('<?=$this->session->userdata('authority')?>' != authority) {
    	    	alert('자기자신의 권한은 바꿀 수 없습니다.');
	        	window.location = '<?=HOME_DIR?>/member/user_all';
	        	return;
            }
        }

    	<?php if($authority != 'pm') {?>
		    $('select[name=brand_group]').each(function() {
		        brand_group_cds += $(this).val() + ';';
		    });
	    <?php } ?>
	
	    if(!check_null("member_name","성명을")) return ;
	    if(!check_null("member_org","회사명을")) return ;
	    if(!check_null("member_dep","부서명를")) return ;
	    if(!check_null("member_team","팀명을")) return ;
	    if(!check_null("member_brand","담당브랜드명을")) return ;
	    if(authority == 'mr'){
        	if(!check_null("office_name","담당 영업소를")) return;
        }
	
	    var param = {
	        member_cd: member_cd,
	        member_name: member_name,
	        member_org: member_org,
	        member_dep: member_dep,
	        member_team: member_team,
	        member_hp: member_hp,
	        authority: authority,
	        agree: agree,
	        office_name: office_name,
	        <?php if($authority != 'pm') {?>
	        brand_group_cds: brand_group_cds,
	        <?php } ?>
	    };
	
	    $.post('<?=HOME_DIR?>/member/memberEdit/', param, function(data) {
	        alert('수정 하였습니다.');
	        $('#modalDetailEdit').modal('hide');
	        location.reload();
	    });
	}

    function rowAdd(obj) {
        var html = '';

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/popup/memeberGroupList",
            dataType: "json",
            data: {},
            success: function (data) {
            	html += '<div class="row mb-1">';
                html += '<div class="col-9 pr-1">';
                html += '<select name="brand_group" id="brand_group" class="custom-select" style="width: 100%; height: 40px;">';
                html += '<option value="">브랜드 계정을 선택하세요.</option>';
                for (var key in data) {
                    html += '<option value="'+data[key]['brand_group_cd']+'">'+data[key]['brand_group_name']+'</option>';
                }
                html += '</select>';
                html += '</div>';
                html += '<div class="col-3 pl-1">';
                html += '<button class="button gray w-100" onclick="rowDel(this)">삭제</button>';
                html += '</div>';
                html += '</div>';
                $(obj).parent().parent().before(html);
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function rowDel(obj) {
        $(obj).parent().parent().remove();
    }
</script>

<div class="modal fade" id="modalDetailEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">사용자 기본정보 수정</h5>
                </div>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label>성명(*)</label>
                        <input type="text" class="form-control" name="member_name" id="member_name" value="<?=$member['member_name'] ?>" placeholder="e.g. 홍길동">
                    </div>
                    <div class="form-group">
                        <label>모바일</label>
                        <input type="tel" class="form-control" name="member_hp" id="member_hp" value="<?=$member['member_hp'] ?>" placeholder="e.g. 01011112222">
                    </div>
                    <div class="form-group">
                        <label>이메일(*)</label>
                        <input type="email" class="form-control" name="member_email" id="member_email" value="<?=$member['member_email'] ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>회사명(*)</label>
                        <input type="text" class="form-control" name="member_org" id="member_org" value="<?=$member['member_org'] ?>" placeholder="e.g. 웨비나스">
                    </div>
                    <div class="form-group">
                        <label>부서명(*)</label>
                        <input type="text" class="form-control" name="member_dep" id="member_dep" value="<?=$member['member_dep'] ?>" placeholder="e.g. 영업부">
                    </div>
                    <div class="form-group">
                        <label>팀명(*)</label>
                        <input type="text" class="form-control" name="member_team" id="member_team" value="<?=$member['member_team'] ?>" placeholder="e.g. 영업1팀">
                    </div>
                    <div class="form-group">
                        <label>권한</label>
                        <select name="authority" id="authority" class="custom-select">
                        	<?php if($authority == 'mcm' || $authority == 'op'){?>
                            	<option value="mcm" <?php if($member['authority'] == 'mcm') { ?> selected <?php } ?>>MCM</option>
                            <?php }?>
                            <option value="pm" <?php if($member['authority'] == 'pm') { ?> selected <?php } ?>>PM</option>
                            <option value="mr" <?php if($member['authority'] == 'mr') { ?> selected <?php } ?>>MR</option>
                            <option value="ag" <?php if($member['authority'] == 'ag') { ?> selected <?php } ?>>AG</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>담당 영업소</label>
                        <select name="office_name" id="office_name" class="custom-select">
                            <option value="">선택</option>
                            <?php foreach ($office_list as $office){?>
                            	<option value="<?=$office['office_name']?>" <?php if($member['office_name'] == $office['office_name']) { ?> selected <?php } ?>><?=$office['office_name']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <?php if($authority != 'pm') {?>
                        <div class="form-group">
                            <label>브랜드계정 설정</label>
                            <?php if(count($brandGroupList) > 0) { ?>
			                    <?php foreach ($brandGroupList as $key => $row) { ?>
			                        <?php if($row['brand_group_member_cd'] != null && $row['brand_group_member_cd'] != '') { ?>
		                            <div class="row mb-1">
		                                <div class="col-9 pr-1">
		                                	<select name="brand_group" id="brand_group" class="custom-select" style="width: 100%; height: 40px;">
											<?php foreach($brand_group_All_list as $key => $brand) {?>
												<?php if($brand['brand_group_cd'] == $row['brand_group_cd']) {?>
		                                			<option value="<?=$brand['brand_group_cd']?>" selected><?=$brand['brand_group_name']?></option>
		                                		<?php }else{?>
		                                			<option value="<?=$brand['brand_group_cd']?>"><?=$brand['brand_group_name']?></option>
		                                		<?php }?>
		                                	<?php }?>
		                                	</select>
		                                </div>
		                                <div class="col-3 pl-1">
		                                    <button class="button gray w-100" onclick="rowDel(this);">삭제</button>
		                                </div>
		                            </div>
		                            <?php } ?>
			                    <?php } ?>
			                <?php } ?>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <button type="button" class="button muted w-100" onclick="rowAdd(this);">브랜드 계정 추가</button>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <div class="modal-footer">
                	<button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button button-modal" onclick="memberEdit()">정보수정</button
                </div>
            </form>
        </div>
    </div>
</div>