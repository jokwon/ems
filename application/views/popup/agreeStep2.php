<?php

?>

<style>
    figure img {width: 100%;}
    .pd-btm-0 {padding-bottom: 0 !important; padding-top: 30px !important}
    .pd-tnb {padding: 5px 0 30px; margin-bottom: 0 !important;}
    .modal-body h4 {font-size: 1.125rem; margin-bottom: 0 !important; padding-bottom: 1.5rem;}
    .h4-ftsize {font-size: 1rem !important; margin-bottom: 0 !important; padding-top: 1.5rem; text-align: justify;}
    .modal-body .wrapper {
        position: relative;
        width: 100%;
        height: 300px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid #dadada;
    }
    .modal-body .wrapper::before {
        content: '';
        width: 200px;
        height: 56px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        background-size: 200px 56px;
        opacity: 0.3;
    }
    .modal-body .signature-pad {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 298px;
        z-index: 10;
    }
    .modal-body .wrapper .sign-undo {
        position: absolute;
        z-index: 15;
        right: 0;
        bottom: 0;
        width: 40px;
        height: 40px;
        display: inline-block;
        padding: 0;
        background-color: transparent;
    }
    .modal-body .wrapper .sign-undo::before {
        content: '\f025';
        font-family: "fontello";
        font-size: 1.5rem;
        color: rgba(0, 0, 0, 0.5);
    }
    .modal-body .wrapper .sign-undo:hover::before {color: #000000;}
    .modal-body .wrapper .error-sign {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index: 20;
        display: none;
    }
    .modal-body .wrapper .error-sign .msg {
        height: 100%;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        font-size: 1.125rem;
        color: white;
        font-weight: 500;
    }
    .modal-footer {border-top: 1px solid #f2f1ee !important; padding-top: 20px !important; padding-bottom: 22px !important;}
</style>
<script src="<?=HOME_DIR?>/js/vendor/signature_pad.min.js"></script>
<script>

	$(document).ready(function() {

	    const canvas = document.getElementById('signature-pad');
	    let signaturePad = null;

	    $('#next').click(function() {
	        var cnt = $('input:checkbox:checked').length;
	        if (cnt != 1) {
	        	alert("개인정보 처리에 모두 동의해주세요.");
	        } else {
	            if (signaturePad == null) {
	                $('#modalSignature').modal('show');
	            } else {
	                return true;
	            }
	            return false;
	        }
	    });

	    function resizeCanvas() {
	        var ratio = Math.max(window.devicePixelRatio || 1, 1);
	        canvas.width = canvas.offsetWidth * ratio;
	        canvas.height = canvas.offsetHeight * ratio;
	        canvas.getContext("2d").scale(ratio, ratio);
	    }
	
	    function drawSignature() {
	        signaturePad = new SignaturePad(canvas);
	    }
	
	    window.onresize = resizeCanvas;
	    document.getElementById('undo').addEventListener('click', function() {
	        signaturePad.clear();
	    });
	
	
	    $('button[data-dismiss="modal"]').on('click', function() {
	        signaturePad = null;
	    });
	    $('#modalSignature').on('shown.bs.modal', function(e) {
	        resizeCanvas();
	        drawSignature();
	    })
	
	    document.getElementById('check-sign').addEventListener('click', function() {
	        if (signaturePad.isEmpty()) {
                $(".error-sign").show();
                setTimeout(function() {
                    $(".error-sign").hide();
                }, 1000);
	        } else {
	            var data = signaturePad.toDataURL('image/png');
	            // save signature
	            $('#canvas_img').val(data);
	            alert('서명되었습니다.');
	            $('#modalSignature').modal('hide');
	
	            var queryString = $("form[name=hcpform]").serialize() ;
	            
                $.ajax({
                    type : 'post',
                    url : '<?=HOME_DIR?>/popup/agreeStep3/',
                    data : queryString,
                    dataType : 'json',
                    success : function(result){
                        alert('서명등록이 완료되었습니다.')
                    	window.location.reload();
                    },
                });
	        }
	    });
	});

	function agreeStep1() {
		var param = {
	            applicant_cd: '<?=$applicant_cd?>'
	        };
	    
	    $("#agreeStep1").modal("hide");
	    $(".modal-backdrop").remove();
	    
        $.post('<?=HOME_DIR?>/popup/agreeStep1', param, function(data) {
          	$("#pop_layer2").html(data);
            $('#agreeStep1').modal();
        });
	}

</script>

<form method="post" id="hcpform" name="hcpform" action="">
	<input type="hidden" id="applicant_cd" name="applicant_cd" value="<?=$applicant_cd?>"/>
	<input type="hidden" id="canvas_img" name="canvas_img" value=""/>
</form>

<div class="modal fade privacy" id="agreeStep1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
        	<div class="modal-header">
                <div>
                    <h5 class="modal-title">개인정보 처리동의서 (2)</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-btm-0">
                <h4>2. 개인정보 및 고유식별정보의 제 3자 제공에 대한 동의</h4>
                <section class="privacy">
                    <figure class="mb-0">
                        <img src="<?=HOME_DIR?><?=$setting['private_img']?>" alt="개인정보 수집 동의">
                    </figure>
                </section>
                <h4 class="h4-ftsize">귀하는 위와 같은 개인정보 및 고유식별정보의 제3자 제공을 거부할 수 있습니다. 다만, 개인정보 및 고유식별정보의 제3자 제공에 동의하지 않을 경우 당사의 자사 제품설명회의 참석 및 정보제공, 여행자보험 가입이 제한될 수 있습니다.</h4>
                <div class="row mb-2 pd-tnb">
	                <div class="col-12">
	                    <label class="checkbox"><input type="checkbox" class="cv" name="c1"><span></span> 개인정보의 <strong>제3자 제공</strong>에 대한 설명을 모두 이해하고, 이에 동의하십니까?</label>
	                </div>
	            </div>
            </div>
            <div class="modal-footer">
            	<button type="button" class="button gray" onclick="agreeStep1()">이전단계</button>
                <button type="button" class="button button-modal" id="next">다음단계</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade signature" id="modalSignature" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h2 class="modal-title">서명입력</h2>
                </div>
            </div>
            <div class="modal-body">
                <div class="wrapper">
                    <button id="undo" type="button" class="button sign-undo"></button>
                    <canvas id="signature-pad" class="signature-pad" width="416" height="298" style="touch-action: none;">
                    </canvas>
                    <div class="error-sign">
                        <div class="msg">
                            서명후 진행하세요.
                        </div>
                    </div>
                </div>
                <small><i class="demo-icon icon-info-circled"></i> 서명 후 ‘확인’ 버튼을 누르시면 개인정보 동의서 및 서명이 제출됩니다.</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="button button-black-outline" data-dismiss="modal" aria-label="Close">취소</button>
                <button type="button" class="button button-default" id="check-sign">확인</button>
            </div>
        </div>
    </div>
</div>

