<?php

?>
<script type="application/javascript">

    function attentApply() {
        $('#popform').submit();
    }

</script>

<form id="popform" name="popform" action="<?=HOME_DIR?>/symposium/sympo_attend_AllApply" method="post">
    <input type="hidden" id="sympo_cd" name="sympo_cd" value="<?=$sympoCd?>"/>
</form>

<div class="modal fade" id="modalSend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">패스코드</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <p class="p-4 text-center mb-0">
                        패스코드 발급을 모두 진행하시겠습니까?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="button" class="button" onclick="attentApply();">전체승인</button>
                </div>
            </form>
        </div>
    </div>
</div>