<?php

?>
<script type="application/javascript">

    function brandRegist() {
        let org_id = $('#orgs').val();
        let org_name = $('#orgs option:selected').text();
        let brand_id = $('#brands').val();
        let brand_name = $('#brands option:selected').text();

        let param = {
            org_id: org_id,
            org_name: org_name,
            brand_id: brand_id,
            brand_name: brand_name
        };

        $.post('<?=HOME_DIR?>/popup/brand_group_regist/', param, function(data) {
            $('#modalAdd').modal('hide');
            window.location.reload();
        });
    }

    function orgChange(obj) {
        let val = $(obj).val();

        let param = {
            group_id: val
        };

        $.post('<?=HOME_DIR?>/popup/orgTobrand/', param, function(result) {

            var data = JSON.parse(result);

            let html = '';
            if(data != null && data.length > 0) {
                for(let i=0;i<data.length;i++) {
                    html += '<option value="' + data[i].site_id + '">' + data[i].site_name + '</option>';
                }
            }

            $('#brands').html(html);
        });
    }
</script>


<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title" id="exampleModalCenterTitle">브랜드계정 그룹 추가</h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="orgs">회사계정</label>
                        <select name="orgs" id="orgs" onchange="orgChange(this);" class="custom-select">
                            <option value="">전체</option>
                            <?php if(count($apiGroupList) > 0) {
                       	 		foreach ($apiGroupList as $group) : ?>
                            <option value="<?=$group['group_id']?>"><?=$group['group_name']?></option>
	                                <?php endforeach; ?>
	                        <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="brands">브랜드계정</label>
                        <select name="brands" id="brands" class="custom-select">
                            <option value="">브랜드 계정을 선택하세요.</option>
                            <?php if(count($apiSiteList) > 0) {
	                            foreach ($apiSiteList as $site) : ?>
	                                <option value="<?=$site['site_id']?>"><?=$site['site_name']?></option>
	                            <?php endforeach; ?>
	                        <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button gray" data-dismiss="modal" aria-label="Close">취소</button>
                    <button type="submit" class="button" onclick="javascript:brandRegist();">브랜드 계정 생성</button>
                </div>
            </form>
        </div>
    </div>
</div>
