<?php
    $authotiry = $this->session->userdata('authority');
?>
<!doctype html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no, address=no, email=no">
    <title>EMS</title>
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/slick/slick.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/css/main.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/css/main-asos.css">
    <script src="<?=HOME_DIR?>/js/jquery-3.4.1.min.js"></script>
    <script src="<?=HOME_DIR?>/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?=HOME_DIR?>/js/formcheck.js?v=<?=time()?>"></script>
    
    
    <script type="text/javascript">

        function move(type, url) {
            window.location = url;
        }

        function privacyPop() {
            $('#modalPrivacy').modal();
        }

        function termPop() {
        	$('#modalAgreement').modal();
        }

    </script>
    
</head>

<?php
    $param = array(
        'left'  =>  $left,
        'customer_name' =>  $customer_name,
        'symposium_left'    => $symposium_left
    );
   

    $member_name = $this->session->userdata('member_name');

    $authority = $this->session->userdata('authority');
    ?>
            
<body id="body" class="body body-main menu-symposium">
<!-- popup -->
<div id="pop_layer_0"></div>
<div id="pop_layer_1"></div>
<div id="pop_inquire"></div>
<div id="pop_layer2"></div>
<div id="pop_layer2_2"></div>
<div id="pop_layer3"></div>
<div id="pop_layer4"></div>
<div id="pop_layer5"></div>
<div id="pop_terms">
	<div class="modal fade" id="modalPrivacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <div>
	                    <h5 class="modal-title" id="exampleModalCenterTitle">개인정보 처리방침</h5>
	                </div>
	
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body small">
	                <?=$private_desc ?>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="button" data-dismiss="modal">창닫기</button>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="modal fade" id="modalAgreement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <div>
	                    <h5 class="modal-title" id="exampleModalCenterTitle">사이트 이용약관</h5>
	                </div>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body small">
	                <?=$term_desc ?>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="button" data-dismiss="modal">창닫기</button>
	            </div>
	        </div>
	    </div>
	</div>	
</div>
<div id="page" class="page">
    <nav class="navbar fixed-top">
        <button type="button" class="sidebar-toggle"><span></span></button>
        <div class="navbar-brand">
            <a href="<?=HOME_DIR?>/"><img src="<?=HOME_DIR?>/images/logo/logo-default.png" alt="EMS"></a>
        </div>
        <div class="navbar-info">
            <span class="d-none d-md-inline-block">안녕하세요. <?=$member_name?><?=strtoupper($authority)?>님</span>
            <a href="<?=HOME_DIR?>/member/mypage" class="button button-setting">setting</a>
        </div>
    </nav>
    
    <div class="wrapper">
    <?php  $this->load->view('left', $param); ?>
