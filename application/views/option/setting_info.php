<?php
?>

<script type="application/javascript">

    function settingUpdate() {
        var customer_name = $('#customer_name').val();
        var symposium_desc = $('#symposium_desc').val();
        var symposium_left = $('#symposium_left').val();
        var dashboard_num1 = $('#dashboard_num1').val();
        var term_desc = $('#term_desc').val();
        var private_desc = $('#private_desc').val();



        var param = {
            customer_name: customer_name,
            symposium_desc: symposium_desc,
            symposium_left: symposium_left,
            dashboard_num1: dashboard_num1,
            private_desc: private_desc,
            term_desc: term_desc,
            setting: 1
        };

        $.post('<?=HOME_DIR?>/settings/settingUpdate', param, function(data) {
            alert('수정되었습니다.');
            window.location.reload();
        });
    }

</script>



<main class="main menu-address">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">설정</h4>
	    </div>
	</nav>
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	                <div class="div-tab-warp">
	                	<ul>
	                		<li class="item active"><a class="link" href="javascript:move('setting', '<?=HOME_DIR?>/settings/setting_info');">사이트정보</a></li>
                        	<li class="item"><a class="link" href="javascript:move('setting', '<?=HOME_DIR?>/settings/setting_function');">기능 허용</a></li>
                        </ul>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-0 mt-sm-3">
	            <div class="col-12">
	                <form action="">
	                    <div class="box-wrap p-3">
	                        <div class="form-group">
	                            <label for="s1">사이트명</label>
	                            <input type="text" class="form-control" name="customer_name" id="customer_name" value="<?=$row['customer_name']?>" placeholder="웨비나스">
	                        </div>
							<div class="form-group">
							    <label for="private_desc">개인정보 처리방침(사이트)</label>
							    <textarea name="private_desc" id="private_desc" class="form-control" style="height: 150px"><?=$row['private_desc']?></textarea>
							</div>
							<div class="form-group">
							    <label for="term_desc">사이트 이용약관</label>
							    <textarea name="term_desc" id="term_desc" class="form-control" style="height: 150px"><?=$row['term_desc']?></textarea>
							</div>
							<div class="form-group">
							    <label for="symposium_desc">로그인 – 솔루션 사무국 정보</label>
							    <textarea name="symposium_desc" id="symposium_desc" class="form-control" style="height: 150px" placeholder="<p>심포지엄 사무국</p>
<div>
<p>사용과 관련하여 문의 사항은 심포지엄사무국으로 메일 및 연락을 주시기바랍니다.</p>
<dl>
<dt>이메일 : </dt>
<dd>support@webinars.co.kr</dd>
</dl>
<dl>
<dt>연락처 : </dt>
<dd>02-6342-6842</dd>
</dl>
</div>"><?=$row['symposium_desc']?></textarea>
							</div>
							<div class="form-group">
							    <label for="symposium_left">메인메뉴(좌측) – 솔루션 사무국 정보</label>
							    <textarea name="symposium_left" id="symposium_left" class="form-control" style="height: 150px" placeholder="<p class='tit'>심포지엄 사무국</p>
<p class='txt'>행사 문의 및 기술 지원은 아래 번호로 연락주세요.</p>
<p>- 등록 관련 : 02-6342-6830</p>
<p>- 접속 관련 : 02-6342-6831</p>"><?=$row['symposium_left']?></textarea>
	                        </div>
	                        <div class="form-group">
	                            <label for="dashboard_num1">대시보드 솔루션 사무국(사용관련 문의)</label>
	                            <input type="text" class="form-control" name="dashboard_num1" id="dashboard_num1" value="<?=$row['dashboard_num1']?>" placeholder="02-6342-6830">
	                        </div>
	                    </div>
	                    <div class="row mb-3">
	                        <div class="col-12 text-center">
	                            <button type="button" class="button" onclick ="settingUpdate()">저장하기</button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</section>

