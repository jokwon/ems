<?php
?>

<script type="application/javascript">

    function settingUpdate() {
        var btn1Yn = $(":input:checkbox[name=btn1Yn]:checked").val();
        var btn3Yn = $(":input:checkbox[name=btn3Yn]:checked").val();

        if(btn1Yn != 'Y') {
            btn1Yn = 'N'
        }
        if(btn3Yn != 'Y') {
            btn3Yn = 'N'
        }
        var param = {
            btn1Yn: btn1Yn,
            btn3Yn: btn3Yn,
            setting: 3
        };

        $.post('<?=HOME_DIR?>/settings/settingUpdate', param, function(data) {
            alert('수정하였습니다.');
            window.location.reload();
        });
    }

</script>

<main class="main menu-address">
	<nav aria-label="breadcrumb">
	    <div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">설정</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	                <div class="div-tab-warp">
	                	<ul>
	                		<li class="item"><a class="link" href="javascript:move('setting', '<?=HOME_DIR?>/settings/setting_info');">사이트정보</a></li>
                        	<li class="item active"><a class="link" href="javascript:move('setting', '<?=HOME_DIR?>/settings/setting_function');">기능 허용</a></li>
                        </ul>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-0 mt-sm-3">
	            <div class="col-12 col-md-6">
	                <div class="card">
	                    <div class="card-header">
	                        <h6 class="card-title">등록기능 활성화</h6>
	                    </div>
	                    <div class="card-body">
	                        <table class="table text-center mb-0">
	                            <colgroup>
	                                <col style="width: 50%">
	                                <col style="width: 50%">
	                            </colgroup>
	                            <tbody>
	                                <tr>
	                                    <td>개별등록</td>
	                                    <td>
	                                        <label class="switch">
	                                            <input type="checkbox" id="btn1Yn" name="btn1Yn" <?if($row['btn1Yn'] == 'Y') {?> checked <?}?> value="Y">
	                                            <span class="slider"></span>
	                                        </label>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td>방문등록</td>
	                                    <td>
	                                        <label class="switch">
	                                            <input type="checkbox" id="btn3Yn" name="btn3Yn" <?if($row['btn3Yn'] == 'Y') {?> checked <?}?> value="Y">
	                                            <span class="slider"></span>
	                                        </label>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-12 text-center">
	                <button type="button" class="button" onclick="javascript:settingUpdate();">저장하기</button>
	            </div>
	        </div>
	    </div>
	</section>
	
