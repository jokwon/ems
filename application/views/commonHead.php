<?php

?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no, address=no, email=no">
    <title>EMS</title>
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/vendor/slick/slick.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/css/main.css">
    <link rel="stylesheet" href="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.css">
    <script src="<?=HOME_DIR?>/js/jquery-3.4.1.min.js"></script>
    <script src="<?=HOME_DIR?>/js/formcheck.js?v=<?=time()?>"></script>
    <style>
        html,
        body {
            background-color: #1a5dd7;
        }
    </style>
</head>

<script type="text/javascript">

function privacyPop() {
    $('#modalPrivacy').modal();
}

function termPop() {
	$('#modalAgreement').modal();
}
</script>

<body id="body" class="body body-login">
<div id="pop_terms">
	<div class="modal fade" id="modalPrivacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <div>
	                    <h5 class="modal-title" id="exampleModalCenterTitle">개인정보 처리방침</h5>
	                </div>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body small">
	                <?=$private_desc ?>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="button" data-dismiss="modal">창닫기</button>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="modal fade" id="modalAgreement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <div>
	                    <h5 class="modal-title" id="exampleModalCenterTitle">사이트 이용약관</h5>
	                </div>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body small">
	                <?=$term_desc ?>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="button" data-dismiss="modal">창닫기</button>
	            </div>
	        </div>
	    </div>
	</div>	
</div>
    <div id="page" class="page">

