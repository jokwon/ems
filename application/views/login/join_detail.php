<?php
?>



<script type="application/javascript">
	function pwCheck(obj) {
	
	    var pw = $('#pw1').val();
	    var pwchk = $(obj).val();

		if(pwchk != "" && pwchk != null)
		{
		    if(pw != pwchk) {
		    	$('.confirm').show();
		        $('.confirm').html('비밀번호가 일치하지 않습니다.');
		        $('.confirm').css('color','#fb5151');
		    } else {
		    	$('.confirm').show();
		        $('.confirm').html('비밀번호가 일치합니다.');
		        $('.confirm').css('color','#337ab7');
		    }
		}
	}
	
	function memberRegist() {
	    if(!check_null("member_hp", "모바일을")) return;
	    if(!isMobile("member_hp")) return;
	
	    var sympoagree = $('input[name=sympoagree]').is(':checked');
	    var privateagree = $('input[name=privateagree]').is(':checked');
	
	    var pw1 = $('#pw1').val();
	    var member_pass = $('#member_pass').val();
	
	    if(pw1 == '') {
	        alert('비밀번호를 입력하여 주세요');
	        return;
	    }
	    if(pw1 != member_pass) {
	        alert('비밀번호확인이 틀립니다.');
	        return;
	    } else {
	        if(sympoagree && privateagree) {
	            $('#actfrm').submit();
	        } else {
	            if(!sympoagree) {
	                alert('사이트 이용약관을 읽고 동의 체크를 해야 합니다.');
	                return;
	            }
	
	            if(!privateagree) {
	                alert('개인정보 취급방침을 읽고 동의 체크를 해야 합니다.');
	                return;
	            }
	        }
	    }
	}
	
	function allAgreeCheck(obj) {
	    if($(obj).is(':checked')) {
	        $('#sympoagree').prop('checked', true);
	        $('#privateagree').prop('checked', true);
	    } else {
	        $('#sympoagree').prop('checked', false);
	        $('#privateagree').prop('checked', false);
	    }
	}
	
	function sympoAgree() {
	    var sympoagree = $('#sympoagree').is(':checked');
	    var privateagree = $('#privateagree').is(':checked');
	
	    if(sympoagree && privateagree) {
	        $('#allagree').prop('checked', true);
	    } else {
	        $('#allagree').prop('checked', false);
	    }
	}
	
	function privateAgree() {
	    var sympoagree = $('#sympoagree').is(':checked');
	    var privateagree = $('#privateagree').is(':checked');
	
	    if(sympoagree && privateagree) {
	        $('#allagree').prop('checked', true);
	    } else {
	        $('#allagree').prop('checked', false);
	    }
	}
</script>

<section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="site-brand text-center">
                                    <img src="<?=HOME_DIR?>/images/logo/logo-white.svg" alt="ASOS">
                                </h1>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-12">
                                <p class="text-center mb-5">
                                    하단의 기본정보를 확인 후 추가 정보를를 작성하세요.<br>
                                    정보수정이 필요시 심포지엄 사무국으로 연락바랍니다.
                                </p>
                                <form name="actfrm" id="actfrm" method="post" action="<?=HOME_DIR?>/member/member_regist">
    								<input type="hidden" name="member_cd" value="<?=$member['member_cd']?>" />
                                    <h5>기본정보</h5>
                                    <hr>
                                    <div class="form-group">
                                        <label for="member_email">이메일 주소</label>
                                        <input type="email" class="form-control" name="member_email" id="member_email" value="<?=$member['member_email'] ?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="member_name">성명</label>
                                        <input type="text" class="form-control" name="member_name" id="member_name" value="<?=$member['member_name']?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="member_org">회사명</label>
                                        <input type="text" class="form-control" name="member_org" id="member_org" value="<?=$member['member_org']?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="member_dep">부서명</label>
                                        <input type="text" class="form-control" name="member_dep" id="member_dep" value="<?=$member['member_dep']?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="member_team">팀명</label>
                                        <input type="text" class="form-control" name="member_team" id="member_team" value="<?=$member['member_team']?>" readonly>
                                    </div>
                                    <h5 class="mt-5">추가정보</h5>
                                    <hr>
                                    <div class="form-group">
                                        <label for="pw1">비밀번호</label>
                                        <input type="password" class="form-control" name="pw1" id="pw1" placeholder="비밀번호를 입력하세요.">
                                    </div>
                                    <div class="form-group">
                                        <label for="member_pass">비밀번호 확인</label><small><span class="confirm" style="display: none;"></span></small>
                                        <input type="password" class="form-control" name="member_pass" id="member_pass" placeholder="비밀번호를 다시 입력하세요." onkeyup="pwCheck(this);">
                                    </div>
                                    <div class="form-group">
                                        <label for="member_hp">모바일</label>
                                        <input type="tel" class="form-control" name="member_hp" id="member_hp" placeholder="e.g. 01012345678" onkeydown="return numberChk(event);">
                                    </div>
									<div class="form-group mb-1">
                                        <label for="t1">사이트 이용약관</label>
                                        <a href="javascript:termPop();" class="form-group-right-link-white">전체보기</a>
                                        <div class="has-scroll" style="height: 145px; overflow-y:scroll;"><?=$term_desc ?></div>
                                    </div>
                                    <div class="form-group">
                                        <input type="checkbox" class="custom-checkbox" name="sympoagree" id="sympoagree" onclick="sympoAgree();">
                                        <label for="sympoagree">사이트 이용약관을 읽고 이에 동의합니다.</label>
                                    </div>
									<div class="form-group mb-1">
                                        <label for="t1">개인정보 처리방침</label>
                                        <a href="javascript:privacyPop();" class="form-group-right-link-white">전체보기</a>
                                        <div class="has-scroll" style="height: 145px; overflow-y:scroll;"><?=$private_desc ?></div>
									</div>
                                    <div class="form-group">
                                        <input type="checkbox" class="custom-checkbox" name="privateagree" id="privateagree" onclick="privateAgree()">
                                        <label for="privateagree">개인정보 처리방침을 읽고 이에 동의합니다.</label>
                                    </div>

                                    <div class="form-group">
                                        <input type="checkbox" class="custom-checkbox" name="allagree" id="allagree" onclick="allAgreeCheck(this);">
                                        <label for="allagree">사이트 이용약관, 개인정보 처리방침을 읽고 모두 동의합니다.</label>
                                    </div>

                                    <div class="form-group mt-3 mb-5">
                                        <button type="button" class="button button-lg w-100" onclick="javascript:memberRegist();">계정 활성화</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h6>솔루션 사무국</h6>
                                <p class="small opacity-50">
                                    사용과 관련된 문의 사항은 하단의 메일 및 전화번호로 연락 주시기 바랍니다.
                                </p>
                                <dl class="dl-list">
                                    <dt>이메일:</dt>
                                    <dd>
                                        <a href="mailto:support@webinars.co.kr" class="opacity-50">support@webinars.co.kr</a>
                                    </dd>
                                    <dt>연락처:</dt>
                                    <dd>
                                        <a href="tel:02-6342-6830" class="opacity-50"><?=$dashboard_num1?></a>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    <script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>

</html>