<?php
    $idSave_id = get_cookie('myprefix_webicookie');
?>

<script type="text/javascript">
    $(document).ready(function() {
    
        var idSave_id = '<?=$idSave_id?>';
        if(idSave_id != null && idSave_id != '') {
            $('#member_id').val(idSave_id);
            $("input:checkbox[id='idSave']").prop("checked", true);
        }

        $("#member_id").keydown(function (key) {
            if(key.keyCode == 13){
                if($("#member_pass").val() != null && $("#member_pass").val() != '') {
                    sendit();
                }
            }
        });
        $("#member_pass").keydown(function (key) {
            if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
                if($("#member_id").val() != null && $("#member_id").val() != '') {
                    sendit();
                }
            }
        });
        $("#companySelectBox").on("change", function(){
        	
        	var selectBoxOpt	=	"";
        	var companyId		=	$(this).val();
        	var param = {
                companyId: companyId,
            };
			$.ajax({
					type: 'POST',
					url:'<?=HOME_DIR?>/login/getBrand/',
					data: param,
					success:function(data){
						
						if(JSON.parse(data).length == 0){
    								selectBoxOpt	+=	"<option value='-1'>해당하는 브랜드가 없습니다.</option>";
						}else{
    						$.each(JSON.parse(data), function(key, value){
        							selectBoxOpt	+=	"<option value='"+value.seq+"'>"+value.name+"</option>";
    						});
						}
						$("#brandSelectBox").html(selectBoxOpt);
					}	
			});
        });
    });

    function sendit() {
        if(!check_null("user_id","아이디를")) return;
        if(!check_null("user_pass","비밀번호을")) return;

        $("#loginfrm").submit();
    }

    function emailCheck(obj) {
    	$('#emailCheckVal').val('Y');
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
        if (!regExp.test($('#member_email').val())) {
            $('#emailConfirm').html('이메일 형식이 잘못 되었습니다.');
            $('#emailCheckVal').val('N');
            return ;
        }

        var member_email = $(obj).val();


        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/member/emailCheck",
            dataType: "json",
            data: {
                member_email: member_email
            },
            success: function (data) {
                if(member_email != null && member_email != '') {
                    if(data.result > 0) {
                        $('#emailConfirm').html('이미 가입된 이메일 주소입니다.');
                        $('#emailCheckVal').val('N');
                        
                    } else {
                        $('#emailConfirm').html('사용 가능한 이메일입니다.');
                        $('#emailCheckVal').val('Y');
                    }
                } else {
                    $('#emailConfirm').html('');
                    $('#emailCheckVal').val('N');
                }
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function registMem(){

        var bool = true;

        if(!check_null("member_email","이메일을")) {
            bool = false;
            return;
        }
        if(!check_null("member_name","성명을")) {
            bool = false;
            return;
        }
        if(!check_null("member_org","회사명을")) {
            bool = false;
            return;
        }
        if(!check_null("member_dep","부서명을")) {
            bool = false;
            return;
        }
        if(!check_null("member_team","팀명을")) {
            bool = false;
            return;
        }

        if(!check_null("member_brand","담당브랜드명을")) {
            bool = false;
            return;
        }
        var emailCheckVal = $('#emailCheckVal').val();

        if(!$('input[name=agree]').is(':checked')) {
            alert("사이트 이용약관에 동의 하셔야 합니다.");
            bool = false;
            return;
        }

        if(!$('input[name=agree2]').is(':checked')) {
            alert("개인정보 처리방침에 동의 하셔야 합니다.");
            bool = false;
            return;
        }

        if(emailCheckVal == 'N') {
            alert("이메일 입력란을 확인해주세요.");
            bool = false;
            return;
        }

        if(!isEmail("member_email")) return;

        if(bool) {
            $("#joinfrm").submit();
        }

    }

    function termdesc() {
        toggleLayer($('#pop_layer2'), 'on');
    }

    function privacydesc() {
        toggleLayer($('#pop_layer3'), 'on');
    }
</script>								
		<section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="site-brand text-center">
                                    <img src="<?=HOME_DIR?>/images/logo/logo-white.svg" alt="EMS for enrollment">
                                </h1>
								<div class="tabList">
                                    <ul>
                                        <li class="item">
                                            <a href="#" class="link">로그인</a>
                                        </li>
                                        <li class="item">
                                            <a href="#" class="link">가입요청</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tabView">
                                    <ul>
                                        <li>
											<form name="loginfrm" id="loginfrm" method="post" action="<?=HOME_DIR?>/login/login_exec">
                                                <div class="form-group">
                                                    <input type="email" class="form-control form-control-lg" name="member_id" id="member_id" placeholder="이메일을 입력하세요.">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control form-control-lg" name="member_pass" id="member_pass" placeholder="비밀번호를 입력하세요.">
                                                </div>
                                                <div class="form-group mb-1">
                                                    <input type="checkbox" class="custom-checkbox" id="idSave" name="idSave">
                                                    <label for="idSave">이메일 계정 기억하기</label>
                                                </div>
                                                <div class="form-group mb-1">
                                                    <input type="checkbox" class="custom-checkbox" id="isWebinar" name="isWebinar" value="Y">
                                                    <label for="isWebinar">웨비나스 소속</label>
                                                </div>
                                                <div class="form-group mt-3">
                                                    <button type="button" class="button button-lg w-100" onclick="sendit();">입장하기</button>
                                                </div>
                                            </form>
                                            <p class="mb-5 text-center">
                                                <a href="<?=HOME_DIR?>/member/idSearch">가입 이메일 계정 / 비밀번호 찾기</a>
                                            </p>
                                        </li>
                                        <li>
						                    <p class="lead text-center">
						                        하단의 가입신청 양식을 작성해주시면,<br>
						                        계정활성화 메일을 보내드리겠습니다.
						                    </p>
						                    <form name="joinfrm" id="joinfrm" method="post" action="<?=HOME_DIR?>/member/member_request">
						                        <div class="form-group">
						                            <label for="member_email">이메일 주소</label>
						                            <input type="email" class="form-control" id="member_email" name="member_email" placeholder="e.g. support@webinars.co.kr" onkeyup="emailCheck(this, event);">
						                            <span id="emailConfirm" class="confirm"></span><input type="hidden" id="emailCheckVal" value="N"/>
						                        </div>
						                        <div class="form-group">
						                            <label for="s2">성명</label>
						                            <input type="text" class="form-control" id="member_name" name="member_name" placeholder="e.g. 홍길동">
						                        </div>
						                        <div class="form-group">
						                            <label for="s3">회사명</label>
													<select class="form-control" id="companySelectBox" name="company_seq">
														<option value="">선택하세요.</option>
														<?php if(count($companyList) > 0) {
														       foreach ($companyList as $sympo_key => $row) { ?>
																<option value="<?= $row['seq']?>"><?= $row['name']?></option>
																
																<?php  }
														} else { ?>
														
														
														<?php }?>
													</select>				
						                        </div>
						                        <div class="form-group">
						                            <label for="s4">부서명</label>
						                            <input type="text" class="form-control" id="member_dep" name="member_dep" placeholder="e.g. 영업기획부">
						                        </div>
						
						                        <div class="form-group">
						                            <label for="s5">팀명</label>
						                            <input type="text" class="form-control" id="member_team" name="member_team" placeholder="e.g. 영업1팀">
						                        </div>
						                        <!--<div class="form-group">
						                            <label for="s6">담당 브랜드명</label>
						                            <input type="text" class="form-control" id="member_brand" name="member_brand" placeholder="e.g. Brand">
						                        </div>-->
						                        <div class="form-group">
						                            <label for="s6">담당 브랜드명</label>
						                            <select class="form-control" id="brandSelectBox" name="brand_seq">
						                            
						                            </select>
						                        </div>
						
						                        <div class="form-group">
						                            <input type="checkbox" class="custom-checkbox" name="agree" id="agree">
						                            <label for="agree"><a href="#" role="button" data-toggle="modal" data-target="#modalAgreement" onclick="termdesc();">사이트 이용약관</a>에
						                                대한 안내를 읽고 이에 동의합니다.</label>
						                        </div>
						                        <div class="form-group">
						                            <input type="checkbox" class="custom-checkbox" name="agree2" id="agree2">
						                            <label for="agree2"><a href="#" role="button" data-toggle="modal" data-target="#modalPrivacy" onclick="privacydesc();">개인정보 처리방침</a>에
						                                대한 안내를 읽고 이에 동의합니다.</label>
						                        </div>
						                        <div class="form-group mt-3 mb-5">
						                            <button type="button" class="button button-lg w-100" onclick="registMem();">작성완료</button>
						                        </div>
						                    </form>
						                </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h6>솔루션 사무국</h6>
                                <p class="small opacity-50">
                                    사용과 관련된 문의 사항은 하단의 메일 및 전화번호로 연락 주시기 바랍니다.
                                </p>
                                <dl class="dl-list">
                                    <dt>이메일:</dt>
                                    <dd>
                                        <a href="mailto:support@webinars.co.kr" class="opacity-50">support@webinars.co.kr</a>
                                    </dd>
                                    <dt>연락처:</dt>
                                    <dd>
                                        <a href="tel:02-6342-6830" class="opacity-50"><?=$dashboard_num1?></a>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    <script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>

</html>