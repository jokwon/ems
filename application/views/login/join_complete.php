<?php
?>



<script type="application/javascript">

</script>


<section class="section">
	    <div class="container">
	        <div class="row justify-content-center">
	            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
	                <div class="row">
	                    <div class="col-12">
	                        <h1 class="site-brand text-center">
	                            <img src="<?=HOME_DIR?>/images/logo/logo-white.svg" alt="ASOS">
	                        </h1>
	                    </div>
	                </div>
	                <div class="row py-5">
	                    <div class="col-12 text-center">
	                        <div class="login-say white">
	                            <h5 class="title"><?=$member['member_email'] ?> <span>님</span></h5>
	                            <p class="desc">계정 활성화 단계가 완성되었습니다.<br><strong><?=strtoupper($member['authority'])?></strong> 권한으로 사이트 이용이 가능합니다.</p>
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-12">
	                        <button type="button" class="button button-lg w-100" onclick="location.href='<?=HOME_DIR?>/login'">로그인 화면 바로가기</button>
	                    </div>
	                </div>
	                <div class="row mt-5">
	                    <div class="col-12">
	                        <h6>솔루션 사무국</h6>
	                        <p class="small opacity-50">
	                            사용과 관련된 문의 사항은 하단의 메일 및 전화번호로 연락 주시기 바랍니다.
	                        </p>
	                        <dl class="dl-list">
	                            <dt>이메일:</dt>
	                            <dd>
	                                <a href="mailto:support@webinars.co.kr" class="opacity-50">support@webinars.co.kr</a>
	                            </dd>
	                            <dt>연락처:</dt>
	                            <dd>
	                                <a href="tel:02-6342-6830" class="opacity-50"><?=$dashboard_num1?></a>
	                            </dd>
	                        </dl>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	</div>
    <script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>

</html>