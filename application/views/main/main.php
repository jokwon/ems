
<script type="application/javascript">

    var symPage = 1;

    function selectType(sympoCd, eventStatus, type, privateDesc, symposiumInfo) {
    	if(privateDesc == 'N'){
			alert('관리자가 개인정보 활용 동의서 설정 작업을 완료해야 사전등록 진행이 가능합니다.');return;
        }
        if(symposiumInfo == 'N'){
        	alert('관리자가 신청서 설정 작업을 완료해야 사전등록 진행이 가능합니다.');return;            
        }
        if(eventStatus == 'Y') {
            alert('사전등록 대기중에는 신청서 작성이 불가능 합니다.');return;
        } else if(eventStatus == 'EI') {
            alert('심포지엄 진행중에는 신청서 작성이 불가능 합니다.');return;
        } else if(eventStatus == 'EI2' || eventStatus == 'EI3') {
            alert('심포지엄 대기중에는 신청서 작성이 불가능 합니다.');return;
    	} else if(eventStatus == 'EN') {
            alert('심포지엄이 종료되어 신청서 작성이 불가능 합니다.');return;
        } else {
            var param = {
                sympoCd: sympoCd,
                type: type
            };

            $.ajax({
                type: 'POST',
                url: '<?=HOME_DIR?>/popup/selectType/',
                data: param,
                async: false,
                success: function(data) {
                     if(data != null) {
                    	 $("#pop_layer2").html(data);
                         $("#modalSelect").modal();
                     }
                }
			});
        }
    }

    function moreList() {
        move('symposium', '<?=HOME_DIR?>/symposium/');
    }

    function noticeView(notice_cd) {
        var param = {
            notice_cd: notice_cd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/noticeView/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $("#modalNotice").modal();
                 }
            }
		});
    }
</script>

<main class="main">
    <nav aria-label="breadcrumb">
    	<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">대시보드</h4>
	    <div class="div">
    </nav>
    <section class="section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">공지사항</h5>
                            <div class="card-info">
                                <a href="<?=HOME_DIR?>/notice" class="more">more</a>
                            </div>
                        </div>
                        <div class="card-body">
                        	<?php if(count($notice_list) == 2) { ?>
                            <dl class="dl-list board brand">
                            	<?php foreach ($notice_list as $notice_key => $row): ?>
                                <dt>
                                	<?php
	                    			$groupNamesArr = explode(",", $row['groupNames']);
	                    			foreach ($groupNamesArr as $groupName) {
	                                    if($groupName != null && $groupName != '') { ?>
	                                    <button type="button" class="tag tag-brand"><?=$groupName?></button>
	                                    <?php }
	                    			} ?>
                                </dt>
                                <dd>
                                    <a href=javascript:; onclick="noticeView('<?=$row['noticeCd']?>')"><em>(<?=$row['noticeRegdate']?>)</em>  <?=$row['noticeTitle']?></a>
                                </dd>
                                <?php endforeach; ?>
                            </dl>
                            <?php } else if(count($notice_list) == 1) { ?>
                            	<?php foreach ($notice_list as $notice_key => $row): ?>
								<dt>
                                    <?php
	                    			$groupNamesArr = explode(",", $row['groupNames']);
	                    			foreach ($groupNamesArr as $groupName) {
	                                    if($groupName != null && $groupName != '') { ?>
	                                    <button type="button" class="tag tag-brand"><?=$groupName?></button>
	                                    <?php }
	                    			} ?>
                                </dt>
                                <dd>
                                    <a href=javascript:; onclick="noticeView('<?=$row['noticeCd']?>')"><em>(<?=$row['noticeRegdate']?>)</em>  <?=$row['noticeTitle']?></a>
                                </dd>
                                <?php endforeach; ?>
                            <?php } else { ?>
                            	<div class="card-body-align-center">
                            	현재 등록된 공지사항이 없습니다.
                            	</div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">솔루션 사무국</h5>
                        </div>
                        <div class="card-body">
                            <p>사이트 사용 문의 및 기술 지원은 아래 번호로 연락 주세요.</p>

                            <ul class="ul-inline mt-4">
                                <li>
                                    <h6>사용관련 문의</h6>
                                    <h5><?=$dashboard_num1?></h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h4 class="line-title">사전등록 진행중 심포지엄</h4>
                </div>
            </div>
            <?php if(count($sympo_list) > 0) { ?>
            <?php foreach ($sympo_list as $sympo_key => $row): ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tag">
                            	<?php
                    			$groupNamesArr = explode(",", $row['groupNames']);
                    			foreach ($groupNamesArr as $groupName) {
                                    if($groupName != null && $groupName != '') { ?>
                                    <button type="button" class="tag tag-brand"><?=$groupName?></button>
                                    <?php }
                    			} ?>
                            </div>
                            <div class="card-info">
                                <?php if($row['sympoType'] == 'LIVE') {?>
	                            	<button type="button" id="<?=$row['sympoCd']?>_sympoType" class="tag tag-live"><?=$row['sympoType']?></button>
	                            <?php } else {?>
	                            	<button type="button" id="<?=$row['sympoCd']?>_sympoType" class="tag tag-demand"><?=$row['sympoType']?></button>
	                            <?php }?>
                                <button type="button" class="tag tag-count d-none d-md-inline-block">심포지엄 D-<?php if($row['dDay'] != null && $row['dDay'] != '') { ?><?=$row['dDay']?><? } else { ?>day<?php }?></button>
                                <button type="button" class="tag tag-count d-inline-block d-md-none">D-<?php if($row['dDay'] != null && $row['dDay'] != '') { ?><?=$row['dDay']?><? } else { ?>day<?php }?></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php if($row['eventStatus'] == 'EN') {?>
	                            <div class="tag tag-info end" id="<?=$row['sympoCd']?>_eventPreTime">사전등록 기간 | <?=$row['eventPreRegStartDateTime']?>(<?=$row['eventPreRegStartWeek']?>) 00:00 ~ <?=$row['eventPreRegEndDateTime']?>(<?=$row['eventPreRegEndWeek']?>) 23:59</div>
	                        <?php } else {?>
	                            <div class="tag tag-info ing" id="<?=$row['sympoCd']?>_eventPreTime">사전등록 기간 | <?=$row['eventPreRegStartDateTime']?>(<?=$row['eventPreRegStartWeek']?>) 00:00 ~ <?=$row['eventPreRegEndDateTime']?>(<?=$row['eventPreRegEndWeek']?>) 23:59</div>
	                        <?php } ?>
                            <h4 class="card-body-title"><a href="<?=HOME_DIR?>/symposium/sympo_detail/<?=$row['sympoCd']?>"><?php if($row['gubun'] == 'N') {?><button type="button" class="tag tag-end">OFF</button><?php }?>  <?=$row['eventName']?></a></h4>
                            <small class="small-date"><?=$row['eventStartDate']?>(<?=$row['eventStartWeek']?>) <?=$row['eventStartTime']?> ~ <?=$row['eventEndDate']?>(<?=$row['eventEndWeek']?>) <?=$row['eventEndTime']?><em>|</em> <?=$row['presenter']?></small>
                            <ul class="ul-icon">
                                <li>
                                    <a href="<?=HOME_DIR?>/symposium/sympo_enrollment/<?=$row['sympoCd']?>">
                                        <dl>
                                            <dt>
                                                <figure>
                                                    <i class="demo-icon icon-calendar-check-o"></i>
                                                </figure>
                                                사전등록완료
                                            </dt>
                                            <dd><?=$row['applicantCount']?></dd>
                                        </dl>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=HOME_DIR?>/symposium/sympo_attend_complete/<?=$row['sympoCd']?>">
                                        <dl>
                                            <dt>
                                                <figure>
                                                    <i class="demo-icon icon-envelope-open"></i>
                                                </figure>
                                                패스코드 발급완료
                                            </dt>
                                            <dd><?=$row['passcodeYcount']?></dd>
                                        </dl>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <div>
                            	<?php if($row['btn3Yn'] == 'Y') {?>
                                	<button type="button" class="button" onclick="javascript:selectType('<?=$row['sympoCd']?>', '<?=$row['eventStatus']?>','hcp','<?php if($row['privateDesc']==null || $row['privateDesc']==''){?>N<?php }else{?>Y<?php }?>','<?php if($row['symposiumInfo']==null || $row['symposiumInfo']==''){?>N<?php }else{?>Y<?php }?>');">방문등록</button>
                                <?php } ?>
                                <?php if($row['btn1Yn'] == 'Y') {?>
                                	<button type="button" class="button" onclick="javascript:selectType('<?=$row['sympoCd']?>', '<?=$row['eventStatus']?>','psr', '<?php if($row['privateDesc']==null || $row['privateDesc']==''){?>N<?php }else{?>Y<?php }?>' ,'<?php if($row['symposiumInfo']==null || $row['symposiumInfo']==''){?>N<?php }else{?>Y<?php }?>');">예비등록</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php } else { ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-body-align-center">
                                현재 등록된 심포지엄이 없습니다.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <? if(!empty($sympo_list)){?>
            <div class="row">
                <div class="col-12 text-center">
                    <button type="button" class="button gray" onclick="javascript:moreList()">리스트 추가 보기</button>
                </div>
            </div>
            <?}?>
        </div>
    </section>