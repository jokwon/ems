		<footer id="colophon" class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h6>심포지엄 준비 사무국</h6>
                    </div>
                    <div class="col-12">
                        <p class="m-0"><?=$setting['agency_info']?></p>
                    </div>
                </div>
            </div>
        </footer>
        
        <script>
		var dom = $(document);
		dom.ready(function() {
		
		    $('.slick-wrap').slick({
		        arrows: false,
		        dots: true,
		        infinite: true,
		        fade: false,
		        rows: 0,
		        autoplay: true,
		        swipeToSlide: true,
		        speed: 500,
		        autoplaySpeed: 7000,
		    });
		});
		
		const $window = $(window);
		const $dom = $(document);
		const $body = $('#body');
		const $page = $('#page');
		const $masthead = $('#masthead');
		const $menuBg = $('#bg-over');
		const $menuToggle = $('.menu-toggle');
		const $siteHeader = $('.site-header');
		
		const offsetTopNum = 50;
		const offsetHeader = 300;
		
		function init() {
		    $('.href-empty').on('click', function(e) {
		        e.preventDefault();
		        return false;
		    })
		}
		
		function toggleTrigger() {
		    $menuToggle.trigger('click');
		}
		
		function initToggle() {
		    $menuToggle.on('click', function() {
		        if ($body.attr('aria-expanded') === 'true') {
		            $body.attr('aria-expanded', 'false');
		            $page.attr('aria-expanded', 'false');
		            $masthead.attr('aria-expanded', 'false');
		        } else {
		            $body.attr('aria-expanded', 'true');
		            $page.attr('aria-expanded', 'true');
		            $masthead.attr('aria-expanded', 'true');
		        }
		    });
		    $menuBg.on('click', function() {
		        toggleTrigger();
		    });
		}
		
		function initMenu() {
		    $dom.on('click', '#primary-top li a.quick-go', function(e) {
		        e.preventDefault();
		        var id = e.currentTarget.getAttribute('href').replace('#', '');
		        var position = document.getElementById(id);
		        $('html, body').animate({
		            'scrollTop': position.offsetTop - offsetTopNum
		        }, 800);
		        toggleTrigger();
		    });
		}
		
		function initHeader() {
			var scrollPos = window.scrollY || window.pageYOffset;
			$siteHeader[((scrollPos > offsetHeader) ? 'add' : 'remove') + 'Class']('nav-up');
		}
		
		function initScroll() {
		
		    $window.scroll(function(e) {
		        initHeader();
		    });
		}

		$dom.ready(function() {
		    init();
		    initToggle();
		    initMenu();
		    if (!$body.hasClass('is-header')) {
		        console.log('initheader');
		        initScroll();
		        initHeader();
		    }
		});
	</script>
    </div>
    <div id="pop_terms">
		<div class="modal fade privacy" id="modalPrivacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered modal-lg">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <div>
	                        <h2 class="modal-title">개인정보의 수집 및 이용에 대한 동의</h2>
	                    </div>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body">
	                    <section class="privacy">
	                        <h3>개인정보 수집 및 활용 동의서</h3>
	                        <p class="mb-5"><?=$setting['private_desc']?></p>
	                        <h4>1. 개인정보의 수집 및 이용에 대한 동의</h4>
	                        <h5>① 수집 및 이용목적:</h5>
	                        <div class="indent">
	                            <p><?=$setting['private_collection']?></p>
	                            <h6>■ 필수 : </h6>
	                            <p><?=$setting['private_collection_nc']?></p>
	                            <h6>■ 선택 : </h6>
	                            <p class="mb-4"><?=$setting['private_collection_se']?></p>
	                        </div>
	
	                        <h5>② 수집항목:</h5>
	                        <div class="indent">
	                            <h6>■ 필수 :</h6>
	                            <p><?=$setting['private_item_nc']?></p>
	                            <h6>■ 선택 :</h6>
	                            <p class="mb-4"><?=$setting['private_item_se']?></p>
	                        </div>
	                        <h5 class="underline">③ 보유 및 이용기간 :</h5>
	                        <p class="underline mb-4"><?=$setting['private_period']?></p>
	                        <h5>④ 거부권 및 거부에 따른 불이익:</h5>
	                        <p><?=$setting['private_disadvantage']?></p>
	                    </section>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="button button-black-outline" data-dismiss="modal" aria-label="Close">닫기</button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="modal fade privacy" id="modalAgreement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-centered modal-lg">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <div>
	                        <h2 class="modal-title">개인정보의 수집 및 고유식별정보의 제 3자 제공에 대한 동의</h2>
	                    </div>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                </div>
	                <div class="modal-body">
	                    <section class="privacy">
	                        <figure class="mb-0">
	                            <img src="<?=HOME_DIR?><?=$setting['private_img']?>" alt="개인정보 수집 동의">
	                        </figure>
	                    </section>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="button button-black-outline" data-dismiss="modal" aria-label="Close">닫기</button>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/vendor/swiper/js/swiper.min.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>
</html>
