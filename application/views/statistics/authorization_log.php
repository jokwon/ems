<?php
?>

<script type="application/javascript">

    $(document).ready(function() {
    	// date picker
    	$('.input-date').datepicker({
    		maxDate: "0",
	        showOn: 'button',
	        showButtonPanel: false,
	        buttonImage: '<?=HOME_DIR?>/images/icon/calendar.svg',
	        buttonImageOnly: true,
	        showMonthAfterYear: true,
	        dateFormat: 'yy-mm-dd',
	    });

    	var startdate= '<?=$startdate?>';
        var enddate= '<?=$enddate?>';
        if(startdate != null && startdate != '' && startdate != '0000.00.00') {
            $("#startdate").val(startdate);
        }
        if(enddate != null && enddate != '' && enddate != '0000.00.00') {
            $("#enddate").val(enddate);
        }

	    $("#searchText").keydown(function (key) {
            if(key.keyCode == 13){
                search();
            }
        });
        
        $("#searchText").keydown(function (key) {
            if(key.keyCode == 13){
                search();
            }
        });

    });

	function dateCheck(inThis){
        
    	if($("#startdate").val()!="" && $("#enddate").val()!=""){
	        if($("#startdate").val() > $("#enddate").val()){
	            alert("검색 시작 날짜는 마지막날짜 보다 작아야 합니다.");
	            inThis.value="";
	            inThis.focus();
	        }
	    }
	}

    var page = 1;
    function moreBtn() {
        page += 1;

        var html = '';
        var authType = $(":input:radio[name=authType]:checked").val();
        var searchText = $('#searchText').val();
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/statistics/AuthorizationLogAjax",
            dataType: "json",
            data: {
                page: page,
                authType: authType,
                searchText: searchText,
                startdate: startdate,
                enddate: enddate,
                sortWhere: sortWhere,
                sortType: sortType
            },
            success: function (data) {
                if(data.data.length != 0) {
                    for (var key in data.data) {
                    	html += '<tr>';
                        html += '<td>'+data.data[key]['log_date']+'</td>';
                        html += '<td>'+data.data[key]['log_ip']+'</td>';
                        html += '<td>'+data.data[key]['to_authority'].toUpperCase()+' '+data.data[key]['to_member_name']+' '+data.data[key]['log_content']+'</td>';
                        html += '</tr>';
                    }

                    $('#tbody').append(html);

                    if(data.data.length < 10) {
                        $('#more').hide();
                    }
                } else {
                    alert('데이터가 없습니다.');
                }
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function sort(sortWhere, sortType) {
        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);
        search();
    }

    function search() {
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();

        if(startdate == '' && enddate == '') {

        } else {
            if(startdate == '' || enddate == '') {
                if(startdate == '') {
                    alert('시작일을 입력하세요');
                    $('#startdate').focus();
                    return;
                }
                if(enddate == '') {
                    alert('종료일을 입력하세요');
                    $('#enddate').focus();
                    return;
                }
            }
        }

        $('#memberForm').attr('method', 'get');
        $('#memberForm').attr('action', '<?=HOME_DIR?>/statistics/authorization_log');
        $('#memberForm').submit();
    }

</script>

<main class="main setting">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">접속 / 통계</h4>
	    </div>
	</nav>
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
	                <div class="div-tab-warp">
	                    <ul>
	                        <li class="item"><a class="link" href="javascript:;" onclick="move('option3', '<?=HOME_DIR?>/statistics/login_log');">로그인 로그</a></li>
	                        <li class="item active"><a class="link" href="javascript:;" onclick="move('option4', '<?=HOME_DIR?>/statistics/authorization_log');">권한부여 로그</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
	        <div class="row mt-3">
			    <div class="col-12">
			        <form id="memberForm" name="memberForm" method="post">
			            <input type="hidden" id="sortType" name="sortType" value="<?=$sortType?>"/>
			            <input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere?>"/>
			            <div class="filter-wrap filter-type">
			                <div class="drawer-wrap">
			                    <div class="box-round">
			                        <h5 class="box-title">권한</h5>
			                        <label class="radio"><input type="radio" name="authType" value="all" <?if($authType == 'all') {?> checked <?}?>><span></span>ALL</label>
	                                <label class="radio"><input type="radio" name="authType" value="op" <?if($authType == 'op') {?> checked <?}?>><span></span>OP</label>
	                                <label class="radio"><input type="radio" name="authType" value="mcm" <?if($authType == 'mcm') {?> checked <?}?>><span></span>MCM</label>
	                                <label class="radio"><input type="radio" name="authType" value="pm" <?if($authType == 'pm') {?> checked <?}?>><span></span>PM</label>
	                                <label class="radio"><input type="radio" name="authType" value="mr" <?if($authType == 'mr') {?> checked <?}?>><span></span>MR</label>
	                                <label class="radio"><input type="radio" name="authType" value="ag" <?if($authType == 'ag') {?> checked <?}?>><span></span>AG</label>
			                    </div>
			                    <em></em>
			                    <div class="box-round ml-0 ml-xl-1">
			                        <h5 class="box-title">기간조회</h5>
			                        <div class="div-date" id="dateStart">
			                            <input type="text" id="startdate" name="startdate" class="input-date" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$startdate?>">
			                        </div>
			                        <div class="div-date mx-1" id="blockAnd">~</div>
			                        <div class="div-date" id="dateEnd">
			                            <input type="text" id="enddate" name="enddate" class="input-date" placeholder="yyyy-mm-dd" onchange="dateCheck(this)" value="<?=$enddate?>">
			                        </div>
			                    </div>
			                </div>
			                <div class="search-wrap">
			                    <div class="input-group">
			                        <input type="text" class="form-control" id="searchText" name="searchText" value="<?=$searchText?>" placeholder="성명을 입력하세요." aria-label="성명을 입력하세요." aria-describedby="button-addon-search">
			                        <div class="input-group-append">
			                            <button class="btn" type="button" id="button-addon-search" onclick="search();">검색</button>
			                        </div>
			                    </div>
			                    <button type="button" class="button button-filter"></button>
			                </div>
			            </div>
			        </form>
			    </div>
			</div>
	        <div class="row mt-3">
	            <div class="col-12">
	                <table class="table text-center">
	                    <colgroup>
	                        <col style="width: 20%;">
	                        <col style="width: 20%;">
	                        <col style="width: 60%;">
	                    </colgroup>
	                    <thead>
	                        <tr>
	                            <?php if($sortWhere == 'log_date' && $sortType == 'desc') { ?>
	                            	<th><a href="javascript:sort('log_date', 'asc');">접속일<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
	                            <?php } else { ?>
	                            	<th><a href="javascript:sort('log_date', 'desc');">접속일<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
	                            <?php } ?>
	                            <th>접속IP</th>
	                            <th>로그 내용 </th>
	                        </tr>
	                    </thead>
	                    <tbody id="tbody">
	                        <?php if(count($log_list) > 0) {
	                			foreach ($log_list as $log_key => $row) { ?>
	                        <tr>
	                            <td><?=$row['log_date']?></td>
	                            <td><?=$row['log_ip']?></td>
	                            <td><?=strtoupper($row['to_authority'])?> <?=$row['to_member_name']?> <?=$row['log_content']?></td>
	                        </tr>
	                        <?php  }
	                        } else {?>
	                            <tr><td colspan="3">조회된 데이터가 없습니다.</td></tr>
	                        <?php }?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <?php if(count($log_list) >= 10) {?>
	        <div class="row">
	            <div class="col-12 text-center">
	                <a href="javascript:moreBtn()" id="more" class="button gray">리스트 추가 보기</a>
	            </div>
	        </div>
	        <?php }?>
	    </div>
	</section>