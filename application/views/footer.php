				<footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <a href="javascript:privacyPop();" class="mr-3">개인정보 처리방침</a>
                                <a href="javascript:termPop();" >이용약관</a>
                            </div>
                            <div class="col-12">
                                <p class="m-0">Copyright © 2019 webinars. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </main>
        </div>
    </div>
    <script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/vendor/swiper/js/swiper.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>
</html>

