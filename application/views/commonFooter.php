						<div class="row">
                            <div class="col-12">
                                <h6>심포지엄 사무국</h6>
                                <p class="small opacity-50">
                                    사용과 관련된 문의 사항은 심포지엄 사무국으로 메일 및 연락을 주시기 바랍니다.
                                </p>
                                <dl class="dl-list">
                                    <dt>이메일:</dt>
                                    <dd>
                                        <a href="mailto:support@webinars.co.kr" class="opacity-50">support@webinars.co.kr</a>
                                    </dd>
                                    <dt>연락처:</dt>
                                    <dd>
                                        <a href="tel:02-6342-6830" class="opacity-50">02-6342-6830</a>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    <script src="<?=HOME_DIR?>/vendor/enquire/enquire.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=HOME_DIR?>/vendor/slick/slick.min.js"></script>
    <script src="<?=HOME_DIR?>/js/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?=HOME_DIR?>/js/jquery.ui.monthpicker.js"></script>
    <script src="<?=HOME_DIR?>/js/custom.js"></script>
</body>

</html>