<?php

?>

<script type="application/javascript">

	var checkBoxes =""

	var dom = $(document);
	dom.ready(function() {
	    checkBoxes = document.querySelectorAll("input[class=cv]");
	});

	function allChk() {
        if($("#all").prop("checked")){
            $("input[name=c]").prop("checked",true);
            checkboxCount()
        }else{
            $("input[name=c]").prop("checked",false);
            checkboxCount()
        }
    }

	function sort(sortWhere, sortType) {
        $('#sortWhere').val(sortWhere);
        $('#sortType').val(sortType);
        $('#noticeFrm').submit();
    }
    
    function checkboxCount()
    {
    	var cnt = 0;
        var target = document.querySelector('.div-table-footer');
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].checked === true) {
                cnt++;
            }
        }
        (cnt > 0) ? target.classList.add('active'): target.classList.remove('active');
    }
	

    var page = 1;
    function moreBtn() {
        page += 1;

        var html = '';
        var authotiry = '<?=$authority?>';
        var searchType = $('#searchType').val();
        var searchText = $('#searchText').val();
        var sortWhere = $('#sortWhere').val();
        var sortType = $('#sortType').val();

        $.ajax({
            type: "post",
            url: "<?=HOME_DIR?>/notice/notice_listAjax",
            dataType: "json",
            data: {
                page: page,
                searchType: searchType,
                searchText: searchText,
                sortWhere: sortWhere,
                sortType: sortType,
            },
            success: function (data) {
                if(data.data.length != 0) {
                    for (var key in data.data) {
                    	html += '<tr>';
                    	if(authotiry == 'op' || authotiry == 'mcm' || authotiry == 'pm') {
                        	html += '<td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="'+data.data[key]['noticeCd']+'" onchange="checkboxCount();"><span></span></label></td>';
                    	}
                        html += '<td onclick="javascript:noticeView(\''+data.data[key]['noticeCd']+'\');" class="cursor-pointer">'+data.data[key]['groupNames']+'</td>';
                        html += '<td onclick="javascript:noticeView(\''+data.data[key]['noticeCd']+'\');" class="cursor-pointer overflow-ellipsis text-left">'+data.data[key]['noticeTitle']+'</a></td>';
                        html += '<td onclick="javascript:noticeView(\''+data.data[key]['noticeCd']+'\');" class="cursor-pointer hidden-mobile">'+data.data[key]['noticeRegdate']+'<small class="small-date">'+data.data[key]['updatedate']+'</small></td>';
                        if(authotiry == 'op' || authotiry == 'mcm' || authotiry == 'pm') {
                        	html += '<td>';
                        	html += '<button type="button" class="tag tag-icon modify mobile-sm" onclick="noticeEdit(\''+data.data[key]['noticeCd']+'\')"><span><img src="<?=HOME_DIR?>/images/icon/edit.svg" alt="수정"></span><em>수정</em></button>';
                        	html += '</td>';
                        }
                        html += '</tr>';
                    }

                    $('#tbody').append(html);

                    if(data.data.length < 10) {
                        $('#more').hide();
                    }
                } else {
                    alert('데이터가 없습니다.');
                    if(data.data.length < 10) {
                        $('#more').hide();
                    }
                }

                checkBoxes = document.querySelectorAll("input[class=cv]");
                
            }, error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    function noticeDels() {

        var noticeCds = '';
        $("input[name=c]:checked").each(function() {
            noticeCds += $(this).val() + ';';
        });

        
        $('#noticeCds').val(noticeCds);

        if(noticeCds == null || noticeCds == '') {
            alert('공지사항을 선택하셔야 합니다.');
        } else {
            if(confirm('선택한 공지사항을 정말로 삭제하시겠습니까?') == true) {
            	$('#noticeFrm').attr('method', 'post');
                $('#noticeFrm').attr('action', '<?=HOME_DIR?>/notice/deletes_exec');
                $('#noticeFrm').submit();
            }
        }
    }

    function search() {

        $('#noticeFrm').attr('method', 'post');
        $('#noticeFrm').attr('action', '<?=HOME_DIR?>/notice/notice_list');
        $('#noticeFrm').submit();
    }

    function noticeWrite() {
    	$.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/noticeWrite',
            data: null,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $('#modalAdd').modal();
                 }
            }
		});
    }

    function noticeView(notice_cd) {
        var param = {
            notice_cd: notice_cd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/noticeView/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $("#modalNotice").modal();
                 }
            }
		});
    }

    function noticeEdit(notice_cd) {
        var param = {
        		notice_cd: notice_cd
        };

        $.ajax({
            type: 'POST',
            url: '<?=HOME_DIR?>/popup/noticeEdit/',
            data: param,
            async: false,
            success: function(data) {
                 if(data != null) {
                	 $("#pop_layer2").html(data);
                     $('#modalEdit').modal();
                 }
            }
		});
    }
    
</script>

<main class="main menu-address">
	<nav aria-label="breadcrumb">
		<div class="div">
	    	<button type="button" class="sidebar-toggle"><span></span></button>
	    	<h4 class="title">공지사항</h4>
	    </div>
	</nav>
	
	<section class="section">
	    <div class="container-fluid p-0">
	        <div class="row">
	            <div class="col-12">
                    <div class="filter-wrap md-column">
                        <div class="d-inline-flex align-items-center">
                            <h5 class="title m-0 d-inline-flex mr-3">총 <?=$notice_count['cnt']?>건</h5>
                            <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
                            <button type="button" class="button button-responsive" onclick="javascript:noticeWrite()">공지사항 등록</button>
                            <?php } ?>
                        </div>
                        <div class="d-inline-block d-md-none position-relative mt-0">
                            <button type="button" class="button button-search position-absolute toggle-search" data-target="search-zone" style="bottom: 0; right: 0;"></button>
                        </div>
                        <div class="search-zone md-hidden">
                            <form id="noticeFrm" name="noticeFrm" method="post">
                            	<input type="hidden" id="sortWhere" name="sortWhere" value="<?=$sortWhere ?>"/>
		                        <input type="hidden" id="sortType" name="sortType" value="<?=$sortType ?>"/>
                				<input type="hidden" id="noticeCds" name="noticeCds" value=""/>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <select name="searchType" id="searchType" class="custom-select" style="width: auto;">
                                            <option value="notice_title" <?php if($searchType == 'notice_title') { ?>  selected <?php } ?>>제목</option>
                                            <option value="brand_group_name" <?php if($searchType == 'brand_group_name') { ?>  selected <?php } ?>>브랜드계정</option>
                                        </select>
                                    </div>
                                    <input type="text" class="form-control" id="searchText" name="searchText" placeholder="검색어를 입력하세요." aria-label="검색어를 입력하세요." aria-describedby="button-addon-search" value="<?=$searchText?>">
                                    <div class="input-group-append">
                                        <button class="btn" type="button" id="button-addon-search" onclick="javascript:search();">검색</button>
                                    </div>
                                </div>
                            </form>    
                        </div>
                    </div>
	            </div>
	        </div>
	        <div class="row mt-3">
	            <div class="col-12">
	                <table class="table text-center">
	                    <colgroup>
	                    	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                        <col style="width: 5%;">
	                        <col style="width: 25%;">
	                        <col style="width: 35%;">
	                        <col style="width: 25%;" class="hidden-mobile">
	                        <col style="width: 10%;">
	                        <?php } else {?>
	                        <col style="width: 30%;">
	                        <col style="width: 40%;">
	                        <col style="width: 30%;" class="hidden-mobile">
	                        <?php } ?>
	                    </colgroup>
	                    <thead>
	                        <tr>
	                        	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                            <th><label class="checkbox"><input type="checkbox" name="all" id="all" class="check-all cv" onclick="allChk()"><span></span></label></th>
	                            <?php } ?>
	                            <th>브랜드계정</th>
	                            <?php if($sortWhere == 'notice_title' && $sortType == 'desc') { ?>
						        	<th><a href="javascript:sort('notice_title', 'asc');">제목<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
						        <?php } else { ?>
						        	<th><a href="javascript:sort('notice_title', 'desc');">제목<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
						        <?php } ?>
	                            <?php if($sortWhere == 'createdate' && $sortType == 'desc') { ?>
						        	<th class="hidden-mobile"><a href="javascript:sort('createdate', 'asc');">등록일/수정일<img src="<?=HOME_DIR?>/images/svg/arrow-down.svg" class="arrow" alt="down"></a></th>
						        <?php } else { ?>
						        	<th class="hidden-mobile"><a href="javascript:sort('createdate', 'desc');">등록일/수정일<img src="<?=HOME_DIR?>/images/svg/arrow-up.svg" class="arrow" alt="up"></a></th>
						        <?php } ?>
	                            <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                            <th>관리</th>
	                            <?php } ?>
	                        </tr>
	                    </thead>
	                    <tbody id="tbody">
	                    	<?php if(count($notice_list) > 0) {
	                    		foreach ($notice_list as $notice_key => $notice) { ?>
	                            <tr>
	                            	<?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                                <td><label class="checkbox"><input type="checkbox" class="cv" name="c" value="<?=$notice['noticeCd']?>" onchange="checkboxCount();"><span></span></label></td>
	                                <?php } ?>
	                                <td onclick="javascript:noticeView('<?=$notice['noticeCd']?>');" class="cursor-pointer"><?=$notice['groupNames']?></td>
	                                <td onclick="javascript:noticeView('<?=$notice['noticeCd']?>');" class="cursor-pointer overflow-ellipsis text-left"><?=$notice['noticeTitle']?></td>
	                                <td onclick="javascript:noticeView('<?=$notice['noticeCd']?>');" class="cursor-pointer hidden-mobile"><?=$notice['noticeRegdate']?><small class="small-date"><?=$notice['updatedate']?></small></td>
	                                <?php if($authority == 'op' || $authority == 'mcm' || $authority == 'pm') {?>
	                                <td><button type="button" class="tag tag-icon modify mobile-sm" onclick="noticeEdit('<?=$notice['noticeCd']?>')"><span><img src="<?=HOME_DIR?>/images/icon/edit.svg" alt="수정"></span><em>수정</em></button></td>
	                                <?php } ?>
	                            </tr>
	                        	<?php }?>
	    					<?php } else {?>
								<tr><td colspan="10">등록 리스트가 존재하지 않습니다.</td></tr>
							<?php }?>   
	                    </tbody>
	                </table>
	                <div class="div-table-footer">
	                    <button type="button" class="tag tag-icon delete" onclick="javascript:noticeDels();"><span><img src="<?=HOME_DIR?>/images/icon/delete.svg" alt="삭제"></span>삭제</button>
	                </div>
	                <?php if(count($notice_list) >= 10) {?>
	                <div class="text-center">
	                    <a href="javascript:;" class="button gray" id="more" onclick="moreBtn()">리스트 추가 보기</a>
	                </div>
	                <?php }?>
	            </div>
	        </div>
	    </div>
	</section>