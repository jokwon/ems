<?php
    $authotiry = $this->session->userdata('authority');
?>

<script type="application/javascript">
    $(function(){
        var left = '<?=$left?>';
        $('#'+left).attr('class', 'item active');
    });

    
    function inquirePopup() {
        var param = {
            customer_name: '<?=$customer_name?>'
        };
        $.post('<?=HOME_DIR?>/popup/inquireAdd/', param, function(data) {
        	$("#pop_inquire").html(data);
            $('#modalInquire').modal();
        });
    }

    function returnAdress(){
        alert('현재 제공되지 않는 기능입니다.');
    }
</script>

<div class="sidebar">
    <ul class="sidebar-nav">
        <li id="dashboard" class="item">
            <a href="javascript:move('dashboard', '<?=HOME_DIR?>/main/');" class="link"><img src="<?=HOME_DIR?>/images/icon/dashboard.svg" alt="대시보드">대시보드</a>
        </li>
        <li id="symposium" class="item ">
            <a href="javascript:move('symposium', '<?=HOME_DIR?>/symposium/');" class="link"><img src="<?=HOME_DIR?>/images/icon/symposium.svg" alt="심포지엄">심포지엄</a>
        </li>
        <li id="notice" class="item ">
            <a href="javascript:move('notice', '<?=HOME_DIR?>/notice/');" class="link"><img src="<?=HOME_DIR?>/images/icon/notice.svg" alt="공지사항">공지사항</a>
        </li>
        <?php if($authotiry == 'op' || $authotiry == 'mcm' || $authotiry == 'pm') {?>
        <li id="member" class="item ">
            <a href="javascript:move('member', '<?=HOME_DIR?>/member/');" class="link"><img src="<?=HOME_DIR?>/images/icon/user.svg" alt="사용자 관리">사용자 관리</a>
        </li>
        <?php }?>
		<?php if($authotiry == 'op') {?>
        <li id="setting" class="item ">
            <a href="javascript:move('setting', '<?=HOME_DIR?>/settings/');" class="link"><img src="<?=HOME_DIR?>/images/icon/setting.svg" alt="설정">설정</a>
        </li>
        <?php }?>
		<?php if($authotiry == 'op' || $authotiry == 'mcm') {?>
        <li id="statistics" class="item ">
            <a href="javascript:move('statistics', '<?=HOME_DIR?>/statistics/');" class="link"><img src="<?=HOME_DIR?>/images/icon/stats.svg" alt="접속 / 통계">접속 / 통계</a>
        </li>
        <?php }?>
        <li id="login" class="item ">
            <a href="<?=HOME_DIR?>/login/logout" class="link"><img src="<?=HOME_DIR?>/images/icon/logout.svg" alt="로그아웃">로그아웃</a>
        </li>
        <li>
            <button type="button" class="button" onclick="inquirePopup()">문 의 하 기</button>
        </li>
        <li>
            <div class="side-contact">
            	<?=$symposium_left?>
            </div>
        </li>
    </ul>
</div>
 
            
 