<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function getSympo_AuthToken(){
    $_API_URL  =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_symposium_proc";
    $_API_URL .=   "/requestAuthToken";
    $arr_query_data    =   array(
        'cus_id'       =>  "test"
    ,   'cus_pw'       =>  "1234"
    );
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL              , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER           , 0);
    curl_setopt ($ch, CURLOPT_POST             , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS       , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT          , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER   , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function getSympo_info011($authToken,$key){
    $arr_query_data    =   array(
        'cus_id'           =>  "test"
        ,'authToken'        =>  $authToken
        ,'event_id'        =>  $key
    );

    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_symposium_proc";
    $_API_URL .=   "/getSymposium_info_type_011";

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_POST           , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS     , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function getSympo_info021($authToken,$key){
    $arr_query_data    =   array(
        'cus_id'           =>  "test"
        ,'authToken'        =>  $authToken
        ,'event_id'        =>  $key
    );

    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_symposium_proc";
    $_API_URL .=   "/getSymposium_info_type_021";

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_POST           , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS     , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);


    return $result;
}

function getSympo_info031($authToken,$domain,$key){
    $arr_query_data    =   array(
        'cus_id'           =>  "test"
        ,'authToken'       =>  $authToken
        ,'domain'          =>  $domain
        ,'event_id'        =>  $key
    );

    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_symposium_proc";
    $_API_URL .=   "/getSymposium_info_type_031";

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_POST           , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS     , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

//전체회사 리스트 가져오기
function getCompanyList(){
    $_API_URL =   "http://192.168.124.111:8080/api/register/company";
    
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);
    
    return $result;
}

//전체회사 카운트 수 가져오기
function getCompanyTotalCnt(){
    $_API_URL =   "http://192.168.124.111:8080/api/register/companyCount";
    
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);
    
    return $result;
}
//해당컴퍼니에 속한 회사리스트 가져오기
function getBrandListByCompany($companyId){
    $_API_URL =   "http://192.168.124.111:8080/api/register/brand/$companyId";
    
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);
    
    return $result;
}
//웨비나스 직원 로그인
function getAdminMember($loginId, $loginPw){
    $_API_URL =   "http://192.168.124.111:8080/api/register/admin/$loginId";
    
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);
      
    return $result;
}


function sms_AuthToken_get(){
    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_sms_proc";
    $_API_URL .=   "/requestAuthToken";
    $arr_query_data    =   array(
     'cus_id'       =>  'testdev'
    ,'cus_pw'      =>  'test1357'
    );
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL              , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER           , 0);
    curl_setopt ($ch, CURLOPT_POST             , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS       , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT          , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER   , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function sms_send_msg($authToken, $to, $msg, $subject){
    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_sms_proc";
    $_API_URL .=   "/put_send_msg_data";

    $arr_query_data    =   array(
         'cus_id'       =>  'testdev'
        ,'authToken'    =>  $authToken
        ,'CALL_FROM'    =>  '0263426830'
        ,'CALL_TO'      =>  $to
        ,'SMS_TXT'      =>  $msg
        ,'SUBJECT'      =>  ''
    );

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL              , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER           , 0);
    curl_setopt ($ch, CURLOPT_POST             , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS       , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT          , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER   , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function sms_send_result($authToken,$date,$page){
    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_sms_proc";
    $_API_URL .=   "/api_msgSendResultLogByDate";

    $arr_query_data    =   array(
         'cus_id'        =>  'testdev'
        ,'authToken'     =>  $authToken
        ,'TargetDate'    =>  $date
        ,'NowPage'       =>  $page
    );
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL              , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER           , 0);
    curl_setopt ($ch, CURLOPT_POST             , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS       , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT          , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER   , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function kakao_AuthToken_get(){
    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_kakao_proc";
    $_API_URL .=   "/requestAuthToken";
    $arr_query_data    =   array(
        'cus_id'       =>  'testmingyu'
        ,'cus_pw'      =>  'test1234'
    );
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL              , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER           , 0);
    curl_setopt ($ch, CURLOPT_POST             , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS       , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT          , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER   , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function kakao_send_msg($authToken, $to, $msg, $template_code, $subject, $domain){
    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_kakao_proc";
    $_API_URL .=   "/put_send_kakao_data";

    $arr_query_data    =   array(
        'cus_id'            =>  'testmingyu'
        ,'authToken'        =>  $authToken
        ,'date_client_req'  =>  date('Y-m-d H:i:s')
        ,'subject'          =>  ''
        ,'content'          =>  $msg
        ,'callback'         =>  ''
        ,'msg_status'       =>  '1'
        ,'recipient_num'    =>  $to
        ,'msg_type'         =>  '1008'
        ,'sender_key'       =>  'BIZTALK SANDER KEY'
        ,'template_code'    =>  $template_code
//            ,'kko_btn_type'     =>  '1'
//            ,'kko_btn_info'     =>  '심포지엄 바로가기^WL^http://demo.medinar.kr^http://demo.medinar.kr|문의하기^BK'


    );

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL              , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER           , 0);
    curl_setopt ($ch, CURLOPT_POST             , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS       , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT          , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER   , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function getGroup($authToken){
    $arr_query_data    =   array(
        'cus_id'           =>  "test"
        ,'authToken'       =>  $authToken
    );

    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_group_proc";
    $_API_URL .=   "/getGroup";

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_POST           , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS     , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

function getSite($authToken, $group_id){
    $arr_query_data    =   array(
        'cus_id'           =>  "test"
        ,'authToken'       =>  $authToken
        ,'group_id'        =>  $group_id
    );

    $_API_URL =   "http://api.medinar.co.kr/CI/index.php";
    $_API_URL .=   "/Api_group_proc";
    $_API_URL .=   "/getSite";

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL            , $_API_URL);
    curl_setopt ($ch, CURLOPT_HEADER        , 0);
    curl_setopt ($ch, CURLOPT_POST           , 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS     , http_build_query($arr_query_data) );
    curl_setopt ($ch, CURLOPT_TIMEOUT        , 30);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    $result = curl_exec ($ch);
    curl_close ($ch);

    return $result;
}

?>