<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function alert($msg='', $url='')
{
    $CI =& get_instance();

    if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>alert('".$msg."');";
    if ($url)
        echo "location.replace('".$url."');";
    else
        echo "history.go(-1);";
    echo "</script>";
    exit;
}

function select_alert($msg='', $url='')
{
    $CI =& get_instance();

    if ($msg)
    {
        echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
        echo "<script type='text/javascript'>alert('".$msg."');";
    }
    else
    {
        echo "<script type='text/javascript'>";
    }

    if ($url)
        echo "location.replace('".$url."');";
    else
        echo "history.go(-1);";
    echo "</script>";
    exit;
}

function confirm_alert($msg='', $url='')
{
    $CI =& get_instance();

    if ($msg)
    {
        echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
        echo "<script type='text/javascript'> if ( confirm('".$msg."') == true ) {";
        if ($url)
            echo "location.replace('".$url."');";
        else
            echo "history.go(-1);";
        echo " } 
					else {
						history.go(-1);
					}
					</script>";
    }
    else
    {
        echo "<script type='text/javascript'>";
        if ($url)
            echo "location.replace('".$url."');";
        else
            echo "history.go(-1);";
        echo "</script>";
    }


    exit;
}

function alert_close($msg)
{
    $CI =& get_instance();

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'> alert('".$msg."'); window.close(); </script>";
    exit;
}

function alert_only($msg)
{
    $CI =& get_instance();

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'> alert('".$msg."'); </script>";
    exit;
}


function parent_alert($msg,$url){

    $CI =& get_instance();

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>alert('".$msg."');";
    echo "parent.location.replace('".$url."');";
    echo "</script>";
    exit;

}

function layer_alert($msg,$url){

    $CI =& get_instance();

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script src=\"/js/jquery-3.2.1.min.js\"></script>";
    echo "<script type='text/javascript'>";
    echo "$('.layer').html('".$msg."');";
    echo "$('.layer').show();";
    echo "location.replace('".$url."');";
    echo "</script>";
    exit;

}

function layer_alert_only($msg){

    $CI =& get_instance();

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>$('.layer').show();";
    echo "$('.layer').html('test');";
    echo "</script>";
    exit;

}

function parent_function($funcname,$returnval, $no_upload = 0){

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>";
    echo "eval(\"parent.".$funcname."('".$returnval."', '".$no_upload."')\");";
    echo "</script>";
    exit;

}

function alert_opener_reload($msg = '', $url = '', $close = false){

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>alert('".$msg."');";
    echo "opener.location.reload();";

    if ($url)
        echo "location.replace('".$url."');";
    else
        echo "history.go(-1);";

    if( $close === true )
        echo "window.close();";

    echo "</script>";
    exit;

}

function alert_opener_reload_close($msg = ''){

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>alert('".$msg."');";
    echo "opener.location.reload();";
    echo "window.close();";
    echo "</script>";
    exit;

}

function confirm_alert_main($msg='', $url='')
{
    $CI =& get_instance();

    if ($msg)
    {
        echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
        echo "<script type='text/javascript'> if ( confirm('".$msg."') == true ) {";
        echo "parent.location.replace('".$url."');";
        echo " } 
					else {
						parent.location.replace('/');
					}
					</script>";
    }

    exit;
}

function login_redirect($type='')
{
    $CI =& get_instance();

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>";

    echo 'location.replace("'.( $type == 'admin' ? '/admin' : '' ).'/login?redirect="+encodeURIComponent(location.href))';

    echo "</script>";
    exit;
}

?>