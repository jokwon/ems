<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function print_pre($obj = '', $is_exit = false)
{

    echo '<pre>';
    print_r($obj);

    if( $is_exit === false )
        echo '</pre>';
    else
        exit('</pre>');

}

function segment_explode($seg)
{
    $len = strlen($seg);
    if(substr($seg, 0, 2) == '/')
    {
        $seg = substr($seg, 0, $len);
    }
    $len = strlen($seg);

    if(substr($seg, -1) == '/')
    {
        $seg = substr($seg, 0, $len-1);
    }
    $seg_exp = explode("/", $seg);

    return $seg_exp;
}

function url_explode($url, $key)
{
    $cnt = count($url);
    for($i=0; $cnt>$i; $i++ )
    {
        if($url[$i] == $key)
        {
            $k = $i+1;
            return @$url[$k];
        }
    }
}

function get_url_value($key)
{
    $CI =& get_instance();

    $uri_array = segment_explode($CI->uri->uri_string());
    $value = url_explode($uri_array, $key);

    return $value;
}

function upload($path, $file_name = 'file1') {

    $CI =& get_instance();

    $uploadconfig['encrypt_name'] = true;
    $uploadconfig['max_size'] = '2048';
    $uploadconfig['upload_path'] = $_SERVER['DOCUMENT_ROOT'].$path;
    $uploadconfig['allowed_types']  = 'xls|xlsx|csv';

    $set_path = $_SERVER['DOCUMENT_ROOT'];

    foreach(explode('/', $path) as $check_path) {
        if( empty($check_path) )
            continue;
        $set_path .= '/'.$check_path;

        if (!is_dir($set_path)) {
            mkdir($set_path, 0777, TRUE);
            chmod($set_path, 0777);
        }
    }
    $CI->load->library('upload', $uploadconfig);
    if (!$CI->upload->do_upload($file_name)) {
        $data = array('result' => 'err', 'error' => $CI->upload->display_errors());
    } else {
        $data = array('result' => 'ok', 'upload_data' => $CI->upload->data());
    }

    return $data;
}

function all_upload($path, $file_name = 'file1') {
	
	$CI =& get_instance();
	
	$uploadconfig['encrypt_name'] = true;
	$uploadconfig['max_size'] = '16384';
	$uploadconfig['upload_path'] = $_SERVER['DOCUMENT_ROOT'].$path;
	$uploadconfig['allowed_types']  = 'png|jpg|jpeg|gif|pdf';
	
	$CI->load->library('upload', $uploadconfig);
	if (!$CI->upload->do_upload($file_name)) {
		$data = array('result' => 'err', 'error' => $CI->upload->display_errors());
	} else {
		$data = array('result' => 'ok', 'upload_data' => $CI->upload->data());
	}
	
	return $data;
}

function img_upload($path, $file_name = 'file1') {

    $CI =& get_instance();

    $uploadconfig['encrypt_name'] = true;
    $uploadconfig['max_size'] = '2048';
    $uploadconfig['upload_path'] = $_SERVER['DOCUMENT_ROOT'].$path;
    $uploadconfig['allowed_types']  = 'gif|jpg|jpeg|png|zip|psd|xls|xlsx|ppt|pptx|doc|docx|tiff|tif';

    $set_path = $_SERVER['DOCUMENT_ROOT'];

    foreach(explode('/', $path) as $check_path) {
        if( empty($check_path) )
            continue;
        $set_path .= '/'.$check_path;

        if (!is_dir($set_path)) {
            mkdir($set_path, 0777, TRUE);
            chmod($set_path, 0777);
        }
    }
    $CI->load->library('upload', $uploadconfig);
    if (!$CI->upload->do_upload($file_name)) {
        $data = array('result' => 'err', 'error' => $CI->upload->display_errors());
    } else {
        $data = array('result' => 'ok', 'upload_data' => $CI->upload->data());
    }

    return $data;
}

function file_upload($file_name = 'up_file', $set_config = array()) {
    $CI =& get_instance();
    $CI->load->library('upload');

    $base_upload = $_SERVER['DOCUMENT_ROOT'];

    $return = array(
        'status' => 'fail'
        ,'is_multi' => false
        ,'message' => '-'
        ,'files' => []
    );

    $upload_config = array(
        'encrypt_name' => true
        ,'max_size' => '2048'
        ,'upload_path' => $base_upload.'/uploads'
        ,'allowed_types' => 'gif|jpg|jpeg|png|zip|psd|xls|xlsx|ppt|pptx|doc|docx|tiff|tif'
    );

    if( is_array($set_config) ) {

        if( !empty($set_config['max_size']) )
            $upload_config['max_size'] = $set_config['max_size'];
        if( !empty($set_config['upload_path']) )
            $upload_config['upload_path'] = $base_upload.'/'.$set_config['upload_path'];
        if( !empty($set_config['allowed_types']) )
            $upload_config['allowed_types'] = $set_config['allowed_types'];

    }

    $set_path = '';
    foreach(explode('/', $upload_config['upload_path']) as $check_path) {
        if( empty($check_path) ) continue;

//        $set_path .= '/'.$check_path;
//
//        if( !is_dir($set_path) ) {
//            mkdir($set_path, 0777, TRUE);
//            chmod($set_path, 0777);
//        }
    }

    if( is_array($_FILES[$file_name]['name']) ) {
        $return['is_multi'] = true;

        $up_files = $_FILES;
        for($i=0; $i<COUNT($up_files[$file_name]['name']); $i++) {

            $_FILES = array();

            $_FILES[$file_name]['name']= $up_files[$file_name]['name'][$i];
            $_FILES[$file_name]['type']= $up_files[$file_name]['type'][$i];
            $_FILES[$file_name]['tmp_name']= $up_files[$file_name]['tmp_name'][$i];
            $_FILES[$file_name]['error']= $up_files[$file_name]['error'][$i];
            $_FILES[$file_name]['size']= $up_files[$file_name]['size'][$i];

            $CI->upload->initialize($upload_config);
            if ( $CI->upload->do_upload($file_name) ) {
                $do_result = $CI->upload->data();
                $do_result['uri_path'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $do_result['full_path']);
                $do_result['img_url'] = '//'.$_SERVER['HTTP_HOST'].$do_result['uri_path'];

                array_push($return['files'], $do_result);
            } else {
                array_push($return['files'], array('err' => strip_tags($CI->upload->display_errors())));
            }

        }

        $return['status'] = 'ok';
        $return['message'] = 'success';

    } else {
        $CI->upload->initialize($upload_config);
        if ($CI->upload->do_upload($file_name) ) {
            $return['status'] = 'ok';
            $return['message'] = 'success';

            $do_result = $CI->upload->data();
            $do_result['uri_path'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $do_result['full_path']);
            $do_result['img_url'] = '//'.$_SERVER['HTTP_HOST'].$do_result['uri_path'];
            array_push($return['files'], $do_result);

        } else {
            $return['message'] = $CI->upload->display_errors();
            $return['err'] = strip_tags($CI->upload->display_errors());
        }


    }

    if( count($return['files']) == '1' ) {
        $return['is_multi'] = false;
        $return['files'] = reset($return['files']);
    }

    return $return;

}

function GetUniqFileName($FN, $PN)
{
    $FileExt = substr(strrchr($FN, "."), 1); // 확장자 추출
    $FileName = substr($FN, 0, strlen($FN) - strlen($FileExt) - 1); // 화일명 추출

    $ret = "$FileName.$FileExt";
    if(file_exists($PN . $ret)){
        $FileCnt = 0;
    }

    while (file_exists($PN . $ret)) // 화일명이 중복되지 않을때 까지 반복
    {
        $FileCnt++;
        $ret = $FileName . "_" . $FileCnt . "." . $FileExt; // 화일명뒤에 (_1 ~ n)의 값을 붙여서....
    }

    return $ret; // 중복되지 않는 화일명 리턴


}

function multi_upload($field_name,$upload_dir, $multi_files=FALSE, $multi_index=FALSE) {
    $CI =& get_instance();

    $config['encrypt_name']     = true;
    $config['upload_path']      = realpath($upload_dir);
    $config['allowed_types']    = 'gif|jpg|jpeg|png|zip|psd|xls|xlsx|ppt|pptx|doc|docx|tiff|tif';
    $config['max_size']         = '2048';

    $CI->load->library('upload', $config);

    if($multi_index !== FALSE)
    {
        $_FILES[$field_name]['name']= $multi_files[$field_name]['name'][$multi_index];
        $_FILES[$field_name]['type']= $multi_files[$field_name]['type'][$multi_index];
        $_FILES[$field_name]['tmp_name']= $multi_files[$field_name]['tmp_name'][$multi_index];
        $_FILES[$field_name]['error']= $multi_files[$field_name]['error'][$multi_index];
        $_FILES[$field_name]['size']= $multi_files[$field_name]['size'][$multi_index];
    }

    if (!$CI->upload->do_upload($field_name))
    {
        $data = array('result' => 'err', 'error' => $CI->upload->display_errors());
    }
    else
    {
        $data = array('result' => 'ok', 'upload_data' => $CI->upload->data());
    }

    return $data;
}

function page_navi($start,$total,$page,$scale,$page_scale,$url){

    $page_html = "<ul>";

    if($total > $scale):

        $page_html .= "";


        //if($start > 0):
        //$page_html .= "<a href=\"".$url."&start=0\">처음</a>\n";
        //endif;


        if ($start + 1 > $scale * $page_scale):
            $pre_start  = $start - $scale * $page_scale ;
            $page_html .=  "<li class=\"prev\"><a href=\"".$url."&start=".$pre_start."\"><span>이전</span></a></li>\n";
        endif;

        for($vj = 0; $vj < $page_scale; $vj++):
            $ln = ($page * $page_scale + $vj) * $scale;
            $vk = $page * $page_scale + $vj + 1;
            if($ln < $total):
                if($ln != $start):
                    $page_html .= "<li><a href=\"".$url."&start=".$ln."\">$vk</a></li>\n";
                else:
                    $page_html .= "<li><a href=\"".$url."&start=".$ln."\" class='cur' style='color:#ff0000;font-weight:bold;'>$vk</a></li>\n";
                endif;
            endif;
        endfor;

        if($total > (($page + 1) * $scale * $page_scale)):
            $n_start = ($page + 1) * $scale * $page_scale;
            $page_html .= "<li class=\"next\"><a href=\"".$url."&start=".$n_start."\"><span>다음</span></a></li>\n";
        endif;


        //$maxstart = ( ceil($total / $scale) - 1) * $scale;
        //if($start < $maxstart):
        //	$page_html .= "<a href=\"".$url."&start=".$maxstart."\">끝</a>";
        //endif;


        $page_html .= "</ul>";

    endif;

    return $page_html;
}

function front_page_navi($start,$total,$page,$scale,$page_scale,$url,$type){

//    $page_html = "<nav>";
//    $page_html .= "<ul class='pagination'>";
//
//    if($total > $scale):
//
//        $page_html .= "";
//
//
//        if($start > 0):
//            $page_html .= "<li class='btn-first'><a href=\"".$url."&start=0\"><span class='sr-only'>처음</span></a></li>\n";
//        endif;
//
//
//        if ($start + 1 > $scale * $page_scale):
//            $pre_start  = $start - $scale * $page_scale ;
//            $page_html .=  "<li class='btn-prev'><a href=\"".$url."&start=".$pre_start."\"><span class='sr-only'>이전</span></a></li>\n";
//        endif;
//
//        for($vj = 0; $vj < $page_scale; $vj++):
//            $ln = ($page * $page_scale + $vj) * $scale;
//            $vk = $page * $page_scale + $vj + 1;
//            if($ln < $total):
//                if($ln != $start):
//                    $page_html .= "<li><a href=\"".$url."&start=".$ln."\">$vk</a></li>\n";
//                else:
//                    $page_html .= "<li class=\"active\"><a href=\"".$url."&start=".$ln."\">$vk</a></li>\n";
//                endif;
//            endif;
//        endfor;
//
//        if($total > (($page + 1) * $scale * $page_scale)):
//            $n_start = ($page + 1) * $scale * $page_scale;
//            $page_html .= "<li class='btn-next'><a href=\"".$url."&start=".$n_start."\"><span class='sr-only'>다음</span></a></li>\n";
//        endif;
//
//
//        $maxstart = ( ceil($total / $scale) - 1) * $scale;
//        if($start < $maxstart):
//            $page_html .= "<li class='btn-last'><a href=\"".$url."&start=".$maxstart."\"><span class='sr-only'>마지막</a>";
//        endif;
//
//
//        $page_html .= "</ul>";
//        $page_html .= "</nav>";
//
//    endif;

    $page_html = "<div class=\"paging\">";
    if($start > 0):
        $page_html .= "<a href=\"javascript:move('".$type."', '".$url."&start=0');\" class=\"first\"><i class=\"ico ico-angle-double-left\"><span class=\"blind\">처음</span></i></a>";
    endif;

    if ($start > 0):
        $pre_start  = $start - $scale ;
        $page_html .= "<a href=\"javascript:move('".$type."', '".$url."&start=".$pre_start."');\" class=\"prev\"><i class=\"ico ico-angle-left\"><span class=\"blind\">이전</span></i></a>";
    endif;

    for($vj = 0; $vj < $page_scale; $vj++):
        $ln = ($page * $page_scale + $vj) * $scale;
        $vk = $page * $page_scale + $vj + 1;
        if($ln < $total):
            if($ln != $start):
                $page_html .= "<a href=\"javascript:move('".$type."', '".$url."&start=".$ln."');\">$vk</a>";
            else:
                $page_html .= "<strong>$vk</strong>";
            endif;
        endif;
    endfor;

    if($total > ($start + $scale)):
        $n_start = ($start + $scale);
        $page_html .= "<a href=\"javascript:move('".$type."', '".$url."&start=".$n_start."');\" class=\"next\"><i class=\"ico ico-angle-right\"><span class=\"blind\">다음</span></i></a>";
    endif;

    $maxstart = ( ceil($total / $scale) - 1) * $scale;
    if($start < $maxstart):
        $page_html .= "<a href=\"javascript:move('".$type."', '".$url."&start=".$maxstart."');\" class=\"last\"><i class=\"ico ico-angle-double-right\"><span class=\"blind\">마지막</span></i></a>";
    endif;


    $page_html .= "</div>";



    return $page_html;
}

function page_navi_ajax($start,$total,$page,$scale,$page_scale,$funcname){

    $page_html = "<ul>";

    if($total > $scale):

        $page_html .= "";


        //if($start > 0):
        //$page_html .= "<a href=\"".$url."&start=0\">처음</a>\n";
        //endif;


        if ($start + 1 > $scale * $page_scale):
            $pre_start  = $start - $scale * $page_scale ;
            $page_html .=  "<li class=\"prev\"><a href=\"javascript:".$funcname."('".$pre_start."');\"><span>이전</span></a></li>\n";
        endif;

        for($vj = 0; $vj < $page_scale; $vj++):
            $ln = ($page * $page_scale + $vj) * $scale;
            $vk = $page * $page_scale + $vj + 1;
            if($ln < $total):
                if($ln != $start):
                    $page_html .= "<li><a href=\"javascript:".$funcname."('".$ln."');\">$vk</a></li>\n";
                else:
                    $page_html .= "<li class=\"cur\"><a href=\"javascript:".$funcname."('".$ln."');\">$vk</a></li>\n";
                endif;
            endif;
        endfor;

        if($total > (($page + 1) * $scale * $page_scale)):
            $n_start = ($page + 1) * $scale * $page_scale;
            $page_html .= "<li class=\"next\"><a href=\"javascript:".$funcname."('".$n_start."');\"><span>다음</span></a></li>\n";
        endif;


        //$maxstart = ( ceil($total / $scale) - 1) * $scale;
        //if($start < $maxstart):
        //	$page_html .= "<a href=\"".$url."&start=".$maxstart."\">끝</a>";
        //endif;


        $page_html .= "</ul>";

    endif;

    return $page_html;
}


function front_page_navi_ajax($start,$total,$page,$scale,$page_scale,$funcname){

    $page_html = "<nav>";
    $page_html .= "<ul class='pagination'>";

    if($total > $scale):

        $page_html .= "";


        if($start > 0):
            $page_html .= "<li class='btn-first'><a href=\"javascript:".$funcname."('0');\"><span class='sr-only'>처음</span></a></li>\n";
        endif;


        if ($start + 1 > $scale * $page_scale):
            $pre_start  = $start - $scale * $page_scale ;
            $page_html .=  "<li class='btn-prev'><a href=\"javascript:".$funcname."('".$pre_start."');\"><span class='sr-only'>이전</span></a></li>\n";
        endif;

        for($vj = 0; $vj < $page_scale; $vj++):
            $ln = ($page * $page_scale + $vj) * $scale;
            $vk = $page * $page_scale + $vj + 1;
            if($ln < $total):
                if($ln != $start):
                    $page_html .= "<li><a href=\"javascript:".$funcname."('".$ln."');\">$vk</a></li>\n";
                else:
                    $page_html .= "<li class=\"active\"><a href=\"javascript:".$funcname."('".$ln."');\">$vk</a></li>\n";
                endif;
            endif;
        endfor;

        if($total > (($page + 1) * $scale * $page_scale)):
            $n_start = ($page + 1) * $scale * $page_scale;
            $page_html .= "<li class='btn-next'><a href=\"javascript:".$funcname."('".$n_start."');\"><span class='sr-only'>다음</span></a></li>\n";
        endif;


        $maxstart = ( ceil($total / $scale) - 1) * $scale;
        if($start < $maxstart):
            $page_html .= "<li class='btn-last'><a href=\"javascript:".$funcname."('".$maxstart."');\"><span class='sr-only'>마지막</a>";
        endif;


        $page_html .= "</ul>";
        $page_html .= "</nav>";

    endif;

    return $page_html;
}

function menuauth_explode($value)
{
    $menuauthArr = explode(",", $value);

    foreach ($menuauthArr as $menuauth):

        $tempArr1 = explode("=", $menuauth);
        $tempArr2 = explode("|", $tempArr1[1]);

        $data["l".$tempArr1[0]] = $tempArr2[0];
        $data["r".$tempArr1[0]] = $tempArr2[1];
        $data["c".$tempArr1[0]] = $tempArr2[2];
        $data["u".$tempArr1[0]] = $tempArr2[3];
        $data["d".$tempArr1[0]] = $tempArr2[4];
        $data["a".$tempArr1[0]] = $tempArr2[5];
        $data["b".$tempArr1[0]] = $tempArr2[6];
        $data["z".$tempArr1[0]] = $tempArr2[7];


    endforeach;

    return $data;
}

function authcheck($page,$type,$auth)
{
    $CI =& get_instance();

    $CI->load->model('authModel');
    if($type == "l") $type = "1";
    if($type == "r") $type = "2";
    if($type == "c") $type = "3";
    if($type == "u") $type = "4";
    if($type == "d") $type = "5";

    $count = $CI->authModel->getAuthCheck($page,$type,$auth);

    if($count > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}



function getQueryString($params = array()){
    $tmp_arr = array();
    if(!empty($params)){
        foreach(explode('&', $_SERVER['QUERY_STRING']) as $q) {
            list($key, $value) = explode('=', $q);

            if( in_array($key, array('idx','start')) )
                continue;

            if( !empty($key) && strcmp('start', $key) && !in_array($key, $tmp_arr) ) {
                array_push($tmp_arr, $key);
                array_push($params, $key.'='.$value);
            }
        }
    }


    return implode('&', $params);

}

function sendmail($tomail,$subject,$message,$member_idx = '') {
    $member_idx = preg_replace('/[^0-9]/', '', $member_idx);

    $CI =& get_instance();

    $mailconfig['mailtype'] = "html";
    $mailconfig['charset'] = "utf-8";

    $CI->load->library('email', $mailconfig);

    if( !empty($member_idx) ) {
        $CI->load->model('/admin/M_member');
        $member_row = $CI->M_member->getMemberRow($member_idx);
        if( $tomail === null )
            $tomail = $member_row['member_email'];

        $message = str_replace('{USER_ID}', $member_row['member_id'], $message);
        $message = str_replace('{USER_NAME}', $member_row['member_name'], $message);
    }

    $CI->email->set_newline("\r\n");
    $CI->email->clear();
    $CI->email->from('no-reply@eweddingfair.co.kr', "다이렉트컴즈");
    $CI->email->to($tomail);
    $CI->email->subject('[다이렉트웨딩 온라인박람회] '.$subject);
    $CI->email->message($message);
    if (!$CI->email->send())
    {
        return false;
    }
    else
    {
        return true;
    }
}

function sendmail_smtp($tomail,$subject,$message,$member_idx = '') {
    $member_idx = preg_replace('/[^0-9]/', '', $member_idx);

    $CI =& get_instance();

    $mailconfig['mailtype'] = "html";
    $mailconfig['charset'] = "utf-8";
    $mailconfig['protocol'] = "smtp";
    $mailconfig['smtp_host'] = "ssl://smtp.gmail.com";
    $mailconfig['smtp_port'] = 465;
    $mailconfig['smtp_user'] = "enroll01@webinars.co.kr";
    $mailconfig['smtp_pass'] = "enroll5785";
    $mailconfig['smtp_timeout'] = 10;




    $CI->load->library('email', $mailconfig);
    $CI->email->set_newline("\r\n");
    $CI->email->clear();
    $CI->email->from('enroll01@webinars.co.kr', "웨비나스");
    $CI->email->to($tomail);
    $CI->email->subject($subject);
    $CI->email->message($message);
    if (!$CI->email->send())
    {
        error_log("/////////////////////////////////////////");
        error_log($CI->email->print_debugger());
        error_log("/////////////////////////////////////////");
        return false;
    }
    else
    {
        return true;
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}



function Encrypt($str, $secret_key='direct_comz_onfair', $secret_iv='#@$%^&*()_+=-') {
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 32);

    return str_replace("=", "", base64_encode(
            openssl_encrypt($str, "AES-256-CBC", $key, 0, $iv))
    );
}

function Decrypt($str, $secret_key='direct_comz_onfair', $secret_iv='#@$%^&*()_+=-') {
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 32);

    return openssl_decrypt(
        base64_decode($str), "AES-256-CBC", $key, 0, $iv
    );
}



?>