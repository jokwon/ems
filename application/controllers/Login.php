<?php
//if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Login
 *
 * @property M_f_member M_f_member
 */
class Login extends CI_Controller {
 
    private $start = 0;
    private $scale = 15;
    private $page_scale = 10;

    private $head = array();

    private $data = array();

    private $footer = array();

    function __construct() {
        parent::__construct();
        $this->load->model(array('settings_model', 'member_model', 'common_model','main_model'));

        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);

        $row = $this->settings_model->getSettings('');
        $this->head['customer_name'] = $row['customer_name'];
        $this->data['symposium_desc'] = $row['symposium_desc'];
        $this->data['term_desc'] = $row['term_desc'];
        $this->data['private_desc'] = $row['private_desc'];
        $this->data['dashboard_num1'] = $row['dashboard_num1'];

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];
        
    }

    function _remap($method){
        if( MEMBER_IDX && in_array($method, array('index', 'login_exec')))
            select_alert(null, HOME_DIR.'/');

        $this->{$method}();
    }

    function index() {
        $redirect = $this->input->get_post('redirect_url', true);

        $this->login_page($redirect);
        $this->session->sess_destroy();
    }

    function emailConfirm() {
        $emailCert    = $this->input->get("emailCert");

        $row = $this->member_model->getEmailCertRow($emailCert);

        if($row['member_cd'] != null && $row['member_cd'] != '') {
            $this->member_model->updateEmailCert($emailCert);

            $newdata = array(
                'member_cd'     => $row['member_cd'],
                'member_id'     => $row['member_id'],
                'member_name'   => $row['member_name'],
                'member_email'  => $row['member_email'],
                'member_hp'     => $row['member_hp'],
                'member_org'    => $row['member_org'],
                'member_dep'    => $row['member_dep'],
                'member_team'   => $row['member_team'],
                'authority'     => $row['authority']
            );

            $this->session->set_userdata($newdata);




            $dataArr = array (
                'log_type'        =>  'login',
                'log_content'     =>  ' 님이 로그인 하였습니다.',
                'log_to'          =>  $row['member_id'],
                'log_ip'          =>  $_SERVER['REMOTE_ADDR'],
                'search_auth'     =>  $row['authority']
            );

            $res = $this->common_model->getSequences("log");
            $dataArr['log_cd'] = str_pad($res['currval'], 20, "LOG_0000000000000000", STR_PAD_LEFT);
            $this->common_model->logInsert($dataArr);


            select_alert('이메일 인증이 되었습니다.',empty($redirect) ? HOME_DIR.'/main' : $redirect);

        } else {
            alert('등록된 아이디가 없습니다.', HOME_DIR.'/login');
        }


    }

    function login_exec() {
        $member_id    = $this->input->post("member_id");
        $member_pass  = $this->input->post("member_pass");
        $idSave       = $this->input->post("idSave");
        $redirect     = $this->input->post("redirect");
        $isWebinar    = $this->input->post("isWebinar");
        
        if($isWebinar == "" || $isWebinar == null){
            $row = $this->member_model->getMemberRow(array('member_id'=>$member_id,'member_pass'=>$member_pass));
    
            if($row['member_cd'] == '') alert('아이디 패스가 일치하지 않습니다.',HOME_DIR.'/login');
    
            if($row['emailConfirm'] == 'false') {
                select_alert("가입 승인이 되지 않았습니다.",empty($redirect) ? HOME_DIR.'/login' : $redirect);
            } else {
                $newdata = array(
                    'member_cd'     => $row['member_cd'],
                    'member_id'     => $row['member_id'],
                    'member_name'   => $row['member_name'],
                    'member_email'  => $row['member_email'],
                    'member_hp'     => $row['member_hp'],
                    'member_org'    => $row['member_org'],
                    'member_dep'    => $row['member_dep'],
                    'member_team'   => $row['member_team'],
                    'authority'     => $row['authority']
                );
                
                if($idSave != null && $idSave != '')
                {
                	$cookie = array(
                			'name'   => 'webicookie',
                			'value'  => $row['member_id'],
                			'expire' => '2592000',
                			'domain' => '.ems.medinar.co.kr',
                			'path'   => '/',
                			'prefix' => 'myprefix_',
                			'secure '=> false,
                			'httponly'=> false
                	);
                	set_cookie($cookie);
                }
    
                $this->session->set_userdata($newdata);
    
                $dataArr = array (
                    'log_type'        =>  'login',
                    'log_content'     =>  ' 님이 <strong class="point1">로그인</strong> 하였습니다.',
                    'log_to'          =>  $row['member_id'],
                    'log_ip'          =>  $_SERVER['REMOTE_ADDR'],
                    'search_auth'     =>  $row['authority']
                );
    
                $res = $this->common_model->getSequences("log");
                $dataArr['log_cd'] = str_pad($res['currval'], 20, "LOG_0000000000000000", STR_PAD_LEFT);
                $this->common_model->logInsert($dataArr);
    
    
                select_alert(null,empty($redirect) ? HOME_DIR.'/main' : $redirect);
            }
        }else{
            $adminMemberInfo    =   getAdminMember($member_id,  $member_pass);
            
            $result1 = json_decode($adminMemberInfo, true);
            
            if ( ! password_verify($member_pass, $result1['password'])) {
                alert('아이디 패스가 일치하지 않습니다.',HOME_DIR.'/login');
            }else{
                $newdata = array(
                    'member_cd'     => $result1['memberCd'],
                    'member_id'     => $result1['userId'],
                    'member_name'   => $result1['name'],
                    'member_email'  => $result1['email'],
                    'member_hp'     => $result1['hp'],
                    'member_org'    => $result1['organization'],
                    'member_dep'    => $result1['department'],
                    'member_team'   => $result1['team'],
                    'authority'     => $result1['authority']
                );
                
                
                if($idSave != null && $idSave != '')
                {
                    $cookie = array(
                        'name'   => 'webicookie',
                        'value'  => $result1['userId'],
                        'expire' => '2592000',
                        'domain' => '.ems.medinar.co.kr',
                        'path'   => '/',
                        'prefix' => 'myprefix_',
                        'secure '=> false,
                        'httponly'=> false
                    );
                    set_cookie($cookie);
                }
                
                $this->session->set_userdata($newdata);
                
                $dataArr = array (
                    'log_type'        =>  'login',
                    'log_content'     =>  ' 님이 <strong class="point1">로그인</strong> 하였습니다.',
                    'log_to'          =>  $result1['userId'],
                    'log_ip'          =>  $_SERVER['REMOTE_ADDR'],
                    'search_auth'     =>  $result1['authority']
                );
                
                $res = $this->common_model->getSequences("log");
                $dataArr['log_cd'] = str_pad($res['currval'], 20, "LOG_0000000000000000", STR_PAD_LEFT);
                $this->common_model->logInsert($dataArr);
                
                
                select_alert(null,empty($redirect) ? HOME_DIR.'/main' : $redirect);
            }
            
        }
    }


    function emailReSend() {
        $member_id    = $this->input->post("member_id");
        $member_pass  = $this->input->post("member_pass");


        $row = $this->member_model->getMemberRow(array('member_id'=>$member_id,'member_pass'=>$member_pass));

        if($row['emailConfirm'] == 'false') {
            $this->email_certification($row['emailCert'], $row['member_email']);
        }

        echo json_encode(array("result"=>"SUCCESS"));
    }


    function email_certification($certification_cd, $member_email) {

        $mail_subject = "[".$this->head['customer_name']."]EMS 가입신청 확인 인증메일입니다.";

        $mail_msg = '<!DOCTYPE html>';
        $mail_msg .= '<html lang="ko">';
        $mail_msg .= '<head>';
        $mail_msg .= '<title>EMS for Enrollment</title>';
        $mail_msg .= '<meta charset="utf-8">';
        $mail_msg .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $mail_msg .= '<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">';
        $mail_msg .= '</head>';
        $mail_msg .= '<body style="width:100%;height:100%;margin:0;padding:0;box-sizing:border-box;">';
        $mail_msg .= '<div style="max-width:700px;padding:30px;margin:0;border:1px solid #ccc;box-sizing:border-box;border-radius:10px;">';
        $mail_msg .= '<p style="padding:0 0 10px 0;margin:0;line-height:120%;font-size:16px;font-weight:bold;border-bottom:2px solid #347ab6;font-family:돋움, Dotum, Arial, sans-serif;">이메일 인증</p>';
        $mail_msg .= '<p style="margin:50px 0;padding:0;text-align:center;"><img style="margin:0;padding:0;" src="'.FULL_URL.'/images/visu_mail.png" alt="" /></p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;text-align:center;font-weight:bold;">가입 완료를 위한 이메일 인증을 해주세요.</p>';
        $mail_msg .= '<p style="margin:0 0 25px 0;padding:0;text-align:center;font-weight:bold;">이메일 인증을 하지 않았을 경우, 가입완료가 되지 않습니다.</p>';
        $mail_msg .= '<p style="margin:0 auto;padding:8px 0;max-width:400px;border:1px solid #ccc;border-radius:5px;text-align:center;font-size:13px">';
        $mail_msg .= '<a href="'.FULL_URL.'/login/emailConfirm/?emailCert='.$certification_cd.'" style="display:block;margin:0;padding:0 5px;text-decoration:none;font-size:13px;color:#0072c1;overflow: hidden;text-overflow:ellipsis;white-space:nowrap;box-sizing:border-box;">인증하러가기</a>';
        $mail_msg .= '</p>';
        $mail_msg .= '<div style="margin:25px auto 0;padding:0;max-width:430px;">';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:13px;font-weight:bold;">심포지엄 사무국</p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:12px;line-height:140%;">사용과 관련하여 문의사항은 심포지엄 사무국으로 메일 및 연락을 주시기 바랍니다.</p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:12px;line-height:140%;"><b>이메일</b> : support@webinars.co.kr</p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:12px;line-height:140%;"><b>연락처</b> : 02-6342-6830</p>';
        $mail_msg .= '</div>';
        $mail_msg .= '</div>';
        $mail_msg .= '</body>';
        $mail_msg .= '</html>';

        sendmail_smtp($member_email, $mail_subject, $mail_msg);

    }


    function logout()
    {


        $member_id = $this->session->userdata('member_id');
        $member_cd = $this->session->userdata('member_cd');
        $member_name = $this->session->userdata('member_name');
        $member_email = $this->session->userdata('member_email');
        $member_hp = $this->session->userdata('member_hp');
        $member_org = $this->session->userdata('member_org');
        $member_dep = $this->session->userdata('member_dep');
        $member_team = $this->session->userdata('member_team');
        $authority = $this->session->userdata('authority');

        $dataArr = array(
            'log_type' => 'login',
            'log_content' => ' 님이 <strong class="point2">로그아웃</strong> 하였습니다.',
            'log_to' => $member_id,
            'log_ip' => $_SERVER['REMOTE_ADDR'],
            'search_auth'     =>  $authority
        );

        if ($member_id != null && $member_id != '') {
            $res = $this->common_model->getSequences("log");
            $dataArr['log_cd'] = str_pad($res['currval'], 20, "LOG_0000000000000000", STR_PAD_LEFT);
            $this->common_model->logInsert($dataArr);
        }

        $this->session->unset_userdata('member_cd');
        $this->session->unset_userdata('member_id');
        $this->session->unset_userdata('member_name');
        $this->session->unset_userdata('member_email');
        $this->session->unset_userdata('member_hp');
        $this->session->unset_userdata('member_org');
        $this->session->unset_userdata('member_dep');
        $this->session->unset_userdata('member_team');
        $this->session->unset_userdata('authority');
        
        alert("로그아웃 되었습니다.",HOME_DIR.'/login');
    }

    function idsearch_ajax() {
        $member_name      = $this->input->post("member_name");
        $member_mobile1   = $this->input->post("member_mobile1");
        $member_mobile2   = $this->input->post("member_mobile2");
        $member_mobile3   = $this->input->post("member_mobile3");

        $params = array(
            'member_name'  		=> $member_name,
            'member_mobile1'  	=> $member_mobile1,
            'member_mobile2'  	=> $member_mobile2,
            'member_mobile3'  	=> $member_mobile3
        );

        $getSearchId = $this->member_model->getSearchId($params);

        $searchid = "";
        if($getSearchId["member_id"] != "") $searchid = substr($getSearchId["member_id"],0,-3) . '***';

        echo $searchid;
        exit;
    }

    function pwsearch_ajax() {
        $member_id        = $this->input->post("member_id");
        $member_name      = $this->input->post("member_name");
        $member_mobile1   = $this->input->post("member_mobile1");
        $member_mobile2   = $this->input->post("member_mobile2");
        $member_mobile3   = $this->input->post("member_mobile3");

        $params = array(
            'member_id'  		=> $member_id,
            'member_name'  		=> $member_name,
            'member_mobile1'  	=> $member_mobile1,
            'member_mobile2'  	=> $member_mobile2,
            'member_mobile3'  	=> $member_mobile3
        );

        $getSearchPw = $this->member_model->getSearchPw($params);

        if($getSearchPw["member_id"] != "") {
            $newpass = generateRandomString();

            $params = array(
                'member_passwd'            => $newpass
            );

            $saveData = $this->member_model->updateMemberData($getSearchPw["idx"],$params);

            $mail_subject = " 임시비밀번호가 발급 되었습니다.";

            $this->data["user_name"]    = $member_name;
            $this->data["tmp_pass"]     = $newpass;

            $mail_msg = $this->load->view('/mailform/mem_temp_pw',$this->data, true);

            sendmail($getSearchPw["member_email"],$mail_subject,$mail_msg);

            echo $getSearchPw["member_email"];
        } else {
            echo "";
        }

        exit;
    }

    function join_detail() {
        $emailCert         = $this->input->get_post('emailCert');
        $member = $this->member_model->getEmailCertRow($emailCert);

        if($member != null && $member['member_cd'] != null && $member['member_cd'] != '') {
            if($member['emailConfirm'] == 'true') {
                alert_close("이미 가입이 완료된 계정입니다.");
            } else {
                if ($member['session'] == 'Y') {
                    alert_close("세션이 만료되었습니다. 계속 진행을 원하실 경우 심포지엄 사무국에 문의 바랍니다.");
                } else {
                	$member = $this->member_model->getMemberDetail($member['member_cd']);
                    $this->data["member"] = $member;

                    $this->load->view('commonHead', $this->head);
                    $this->load->view('login/join_detail', $this->data);
                }
            }
        } else {
            alert_close("해당권한이 없습니다1.");
        }
    }

    function login() {

        $redirect = $this->input->get_post('redirect_url', true);

        $lType        = $this->input->get_post('lType');

        if($lType == 'login_page') {
        	$this->login_page($redirect);
        } else {
        	$this->login_page($redirect);
        }
    }

    function login_page($redirect) {
        $companyList =   getCompanyList();
        $companyTotalCnt =   getCompanyTotalCnt();
        
        $companyList = json_decode($companyList, true);
        
        
        
        $this->data['redirect'] = $redirect;
        $this->data['companyList'] = $companyList;
        $this->data['companyTotalCnt'] = $companyTotalCnt;

        $this->load->view('commonHead',$this->head);
        $this->load->view('login/login_page',$this->data);
    }
    function getBrand() {
        $companyId          =   $this->input->post('companyId');
        $brandList          =   getBrandListByCompany($companyId);
        
        echo($brandList);
    }
    

}
