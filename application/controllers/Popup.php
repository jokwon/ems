<?php
/**
 * Created by PhpStorm.
 * User: complete
 * Date: 2018. 8. 9.
 * Time: PM 5:17
 */

class Popup extends CI_Controller {

    private $start = 0;
    private $scale = 8;
    private $page_scale = 10;

    private $base_uri = '';

    private $head = array();

    private $footer = array();

    private $data = array();

    private $personDataArr = array();

    private $return_json = false;
    private $return_array = array(
        'status' => '500'
        ,'msg' => '잘못된 접근입니다.'
        ,'url' => ''
        ,'reload' => 'off'
        ,'arr' => array()
    );



    function __construct() {
        parent::__construct();
        $this->head['left'] = "popup";

        $this->load->model(array('settings_model', 'member_model', 'symposium_model', 'common_model', 'notice_model','main_model'));

        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);


        $this->base_uri = '/'.strtolower(__CLASS__);
        $this->data['start'] = $this->start;
        $this->data['scale'] = $this->scale;

        $this->head['base_uri'] = $this->base_uri;
        $this->data['base_uri'] = $this->base_uri;
        
        $authority_now = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        $authority_db = $this->main_model->getAuthority($member_cd);
        if($authority_now != null && $authority_now != '')
        {
        	if($authority_now != $authority_db['authority'])
        	{
        		alert("권한이 변경되었습니다 다시 로그인 해주세요",HOME_DIR."/login/logout");
        	}
        }
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {

    }
    
    // 방문등록,일괄등록 시 어떻게 추가할것인지 선택하는 폼 열기
    function selectType(){
    	
    	$sympoCd          = $this->input->post('sympoCd');
    	$type          = $this->input->post('type');
    	$this->data['type'] = $type;
    	$this->data['sympoCd'] = $sympoCd;
    	
    	$this->load->view('popup/selectType',$this->data);
    }
    
    // 서명 캔버스 열기
    function signApplication(){
    	
    	$this->load->view('popup/signApplication');
    }
    
    // 개별등록
    function applyPsr(){
        $sympoCd          	= $this->input->post('sympoCd');
        
        $this->data['sympoCd'] = $sympoCd;
        $this->load->view('popup/applyPsr',$this->data);
    }

    // 개별등록 완료
    function applyPsrComplete() {
        $sympoCd            = $this->input->post('sympoCd');
        $applicant_name     = $this->input->post('applicant_name');
        $applicant_email    = $this->input->post('applicant_email');
        $applicant_hp       = $this->input->post('applicant_hp');
        $applicant_hospital = $this->input->post('applicant_hospital');
        $applicant_dep      = $this->input->post('applicant_dep');
        $member_id          = $this->session->userdata('member_id');

        $dataArr = Array (
            'applicant_name'	    =>	$applicant_name,
            'applicant_email'	    =>	$applicant_email,
            'applicant_hp'	        =>	str_replace("-", "", $applicant_hp),
            'applicant_hospital'	=>	$applicant_hospital,
            'applicant_dep'	        =>	$applicant_dep,
            'regi_complete'         =>  'N',
            'creator'	            =>	$member_id,
            'updater'	            =>	$member_id,
            'sympo_cd'	            =>	$sympoCd,
            'regist_type'           =>  'PSR',
        );
        
        $res = $this->common_model->getSequences("app");
        $dataArr['applicant_cd'] = str_pad($res['currval'], 20, "APP_0000000000000000", STR_PAD_LEFT);
        $saveData = $this->db->insert('applicant', $dataArr);

        $this->data['sympoCd'] = $sympoCd;

        $this->load->view('popup/applyPsrComplete',$this->data);
    }

    // 문의사항 접수
    function inquireAdd(){
        $customer_name    = $this->input->post('customer_name');
        $this->data['customer_name']          = $customer_name;
        $this->load->view('popup/inquireAdd', $this->data);
    }
    
    // 문의사항 접수 완료
    function inquireComplete() {
        $subject          = $this->input->post('subject');
        $content          = $this->input->post('content');
        $customer_name    = $this->input->post('customer_name');
        
        $admin_email      = "jiyoung.hong@webinars.co.kr";

        $member_name = $this->session->userdata('member_name');
        $member_email = $this->session->userdata('member_email');
        $member_org = $this->session->userdata('member_org');
        $member_hp = $this->session->userdata('member_hp');
        $member_dep = $this->session->userdata('member_dep');
        $member_team = $this->session->userdata('member_team');

        $mail_subject = "EMS 문의 메일입니다.";
        $mail_msg = '<!DOCTYPE html>';
        $mail_msg .= '<html lang="ko">';
        $mail_msg .= '<head>';
        $mail_msg .= '<title>웨비나스 문의하기</title>';
        $mail_msg .= '<meta charset="utf-8">';
        $mail_msg .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $mail_msg .= '<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">';
        $mail_msg .= '</head>';
        $mail_msg .= '<body style="width:100%;height:100%;margin:0;padding:0;box-sizing:border-box;">';
	    $mail_msg .= '<div style="max-width:700px;padding:30px;margin:0;border:1px solid #ccc;box-sizing:border-box;border-radius:10px;">';
		$mail_msg .= '<p style="padding:0 0 10px 0;margin:0;line-height:120%;font-size:16px;font-weight:bold;border-bottom:2px solid #347ab6;font-family:돋움, Dotum, Arial, sans-serif;">EMS for Enrollment 문의하기</p>';
		$mail_msg .= '<div style="margin:15px 0 0 0;padding:0;">';
        $mail_msg .= '<p style="margin:0 0 10px 0;padding:0;font-size:14px;font-weight:bold;font-family:\'Malgun Gothic\', \'맑은 고딕\', sans-serif;">고객정보</p>';
        $mail_msg .= '<div style="margin:0;padding:0;border-top:2px solid #ccc;border-bottom:2px solid #ccc;">';
        $mail_msg .= '<table style="border-spacing:0;border-collapse:collapse;word-break:break-all;table-layout:fixed;width:100%;">';
        $mail_msg .= '<caption style="line-height:0;font-size:1px;overflow:hidden;">고객정보</caption>';
        $mail_msg .= '<colgroup>';
        $mail_msg .= '<col style="width:15%;"/>';
        $mail_msg .= '<col />';
        $mail_msg .= '<col style="width:15%;"/>';
        $mail_msg .= '<col />';
        $mail_msg .= '</colgroup>';
        $mail_msg .= '<tbody>';
        $mail_msg .= '<tr>';
        $mail_msg .= '<th scope="row" style="padding:10px 0;font-size:13px;background:#f5f5f5;">이름</th>';
        $mail_msg .= '<td style="padding:10px;font-size:13px;">'.$member_name.'</td>';
        $mail_msg .= '<th scope="row" style="padding:10px 0;font-size:13px;background:#f5f5f5;">이메일</th>';
        $mail_msg .= '<td style="padding:10px;font-size:13px;">'.$member_email.'</td>';
        $mail_msg .= '</tr>';
        $mail_msg .= '<tr>';
        $mail_msg .= '<th scope="row" style="padding:10px 0;font-size:13px;background:#f5f5f5;border-top:1px solid #ccc;">회사명</th>';
        $mail_msg .= '<td style="padding:10px;font-size:13px;border-top:1px solid #ccc;">'.$member_org.'</td>';
        $mail_msg .= '<th scope="row" style="padding:10px 0;font-size:13px;background:#f5f5f5;border-top:1px solid #ccc;">모바일</th>';
        $mail_msg .= '<td style="padding:10px;font-size:13px;border-top:1px solid #ccc;">'.$member_hp.'</td>';
        $mail_msg .= '</tr>';
        $mail_msg .= '<tr>';
        $mail_msg .= '<th scope="row" style="padding:10px 0;font-size:13px;background:#f5f5f5;border-top:1px solid #ccc;">부서명</th>';
        $mail_msg .= '<td style="padding:10px;font-size:13px;border-top:1px solid #ccc;">'.$member_dep.'</td>';
        $mail_msg .= '<th scope="row" style="padding:10px 0;font-size:13px;background:#f5f5f5;border-top:1px solid #ccc;">팀명</th>';
        $mail_msg .= '<td style="padding:10px;font-size:13px;border-top:1px solid #ccc;">'.$member_team.'</td>';
        $mail_msg .= '</tr>';
        $mail_msg .= '</tbody>';
        $mail_msg .= '</table>';
        $mail_msg .= '</div>';
        $mail_msg .= '<p style="margin:30px 0 10px 0;padding:0;font-size:14px;font-weight:bold;font-family:\'Malgun Gothic\', \'맑은 고딕\', sans-serif;">문의 내용</p>';
        $mail_msg .= '<div style="margin:0;padding:10px;border:1px solid #ccc;">';
        $mail_msg .= '<dl style="margin:0;padding:0 0 0 40px;overflow:hidden;position:relative;">';
        $mail_msg .= '<dt style="margin:0;padding:0;font-size:13px;font-weight:bold;position:absolute;top:0;left:0;">제목 : </dt>';
        $mail_msg .= '<dd style="margin:0;padding:0;float:left;font-size:13px;">'.$subject.'</dd>';
        $mail_msg .= '</dl>';
        $mail_msg .= '<dl style="margin:10px 0 0 0;padding:0 0 0 40px;overflow:hidden;position:relative;">';
        $mail_msg .= '<dt style="margin:0;padding:0;font-size:13px;font-weight:bold;position:absolute;top:0;left:0;">내용 : </dt>';
        $mail_msg .= '<dd style="margin:0;padding:0;float:left;font-size:13px;">'.$content.'</dd>';
        $mail_msg .= '</dl>';
        $mail_msg .= '</div>';
		$mail_msg .= '</div>';
		$mail_msg .= '<p style="max-width:300px;margin:20px auto;padding:8px 0;border:1px solid #ccc;border-radius:5px;text-align:center;">';
        $mail_msg .= '<a href="http://ems.medinar.co.kr" target="_blank" style="display:block;margin:0;padding:0;text-decoration:none;font-size:13px;color:#0072c1;">EMS for Enrollment 사이트로 바로가기</a>';
		$mail_msg .= '</p>';
    	$mail_msg .= '</div>';
        $mail_msg .= '</body>';
        $mail_msg .= '</html>';

        if(sendmail_smtp($admin_email, $mail_subject, $mail_msg)) {
            $result = "ok";
        }else{
            $result = "no";
        }

        //echo json_encode(array("result"=>$result));

    }

    // 등록정보 상세보기
    function applicantDetail() {
    	
    	$applicantCd = $this->input->post('applicantCd');
    	
    	$applicant = $this->symposium_model->getApplicantOnly($applicantCd);
    	$symposium = $this->symposium_model->getSymposium($applicant['sympo_cd'], '');
    	
    	$this->data['applicant'] = $applicant;
    	$this->data['symposium'] = $symposium;
    	
    	$this->load->view('popup/applicantDetail', $this->data);
    }
    
   
    // 등록 관리자 정보 상세보기
    function applicantManagerDetail() {
    	
    	$applicantCd = $this->input->post('applicantCd');
    	$rowType = $this->input->post('rowType');
    	
    	$applicant = $this->symposium_model->getApplicantManager($applicantCd);
    	
    	$this->data['applicant'] = $applicant;
    	$this->data['rowType'] = $rowType;
    	
    	
    	$this->load->view('popup/applicantManagerDetail', $this->data);
    }
    
    // 등록정보 수정
    function applicantDetailEdit() {
    	
    	$applicantCd = $this->input->post('applicantCd');
    	
    	$applicant = $this->symposium_model->getApplicantOnly($applicantCd);
    	
    	$this->data['applicant'] = $applicant;
    	
    	$this->load->view('popup/applicantDetailEdit', $this->data);
    }
    
    // 개인정보 추가동의
    function agreeStep1(){
    	$applicant_cd          = $this->input->post('applicant_cd');
    	$applicant = $this->symposium_model->getApplicantOnly($applicant_cd);
    	$setting = $this->settings_model->getSympoSettings($applicant['sympo_cd']);
    	
    	$this->data['applicant_cd'] = $applicant_cd;
    	$this->data['setting'] = $setting;
    	
    	$this->load->view('popup/agreeStep1', $this->data);
    }
    
    // 개인정보 추가동의
    function agreeStep2(){
    	$applicant_cd          = $this->input->post('applicant_cd');
    	$applicant = $this->symposium_model->getApplicantOnly($applicant_cd);
    	$setting = $this->settings_model->getSympoSettings($applicant['sympo_cd']);
    	
    	$this->data['applicant_cd'] = $applicant_cd;
    	$this->data['setting'] = $setting;
    	
    	$this->load->view('popup/agreeStep2', $this->data);
    }
    
    // 개인정보 추가동의
    function agreeStep3(){
    	$applicant_cd          = $this->input->post('applicant_cd');
    	$canvas_img = $this->input->post("canvas_img");
    	$applicant = $this->symposium_model->getApplicantOnly($applicant_cd);
    	
    	$canvas_img = substr($canvas_img,strpos($canvas_img, ",") + 1);
    	$canvas_img = base64_decode($canvas_img);
    	$fp = fopen('./uploads/signiture/'.$applicant['applicantName'].'_'.time().'.png', 'wb');
    	fwrite($fp, $canvas_img);
    	fclose($fp);
    	
    	$filename = $applicant['applicantName'].'_'.time();
    	$fileext = '.png';
    	$member_id = $this->session->userdata('member_id');
    	
    	$dataArr = array(
    			'filename' 			=> $filename,
    			'fileext' 			=> $fileext,
    			'regi_complete' 	=> 'Y',
    			'addsign_manager' 	=> $member_id,
    			'addsigndate'     	=> 'now()',
    			'updater'           => $member_id,
    			'updatedate'        => 'now()'
    	);
    	
    	$this->symposium_model->signUpdate($applicant_cd,$dataArr);
    	
    	echo json_encode(array("result"=>'success'));
    	
    }
    
    function success($status) {
        $arr = array('result' => $status);
        header('Content-Type: application/json');

        echo json_encode( $arr );
    }

    // 심포지엄 정보 수정
   function symposiumModify() {

        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op' && $authority != 'pm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }
        $sympoCd          = $this->input->post('sympoCd');

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $this->data['symposium'] = $symposium;

        $sympo_to_brand = $this->symposium_model->getSympoToBrand($sympoCd);
        $this->data['sympo_to_brand'] = $sympo_to_brand;

        $brand_group_list = $this->member_model->getBrandGroupAllList("", "", "brand_group_name", "asc");
        $this->data['brandGroupList'] = $brand_group_list;

        $this->load->view('popup/symposiumModify', $this->data);
    }

    // passcode 일괄 완료
    function passcodeAllComplete() {
        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op' && $authority != 'pm' && $authority != 'ag') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $sympoCd          = $this->input->post('sympoCd');

        $this->data['sympoCd'] = $sympoCd;

        $this->load->view('popup/passcodeAllComplete', $this->data);
    }
    
    // passcode 일괄 복원
    function passcodeAllRestore() {
    	$authority = $this->session->userdata('authority');
    	if($authority != 'mcm' && $authority != 'op' && $authority != 'pm' && $authority != 'ag') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$sympoCd          = $this->input->post('sympoCd');
    	
    	$this->data['sympoCd'] = $sympoCd;
    	
    	$this->load->view('popup/passcodeAllRestore', $this->data);
    }

    // passcode 완료
    function passcodeComplete() {
        $sympoCd          = $this->input->post('sympoCd');
        $applicantCds       = $this->input->post('applicantCds');
        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op' && $authority != 'pm' && $authority != 'ag') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $this->data['sympoCd'] = $sympoCd;
        $this->data['applicantCds'] = $applicantCds;

        $this->load->view('popup/passcodeComplete', $this->data);
    }
    
    // passcode 취소
    function passcodeCancel() {
    	$sympoCd          = $this->input->post('sympoCd');
    	$applicantCds       = $this->input->post('applicantCds');
    	$authority = $this->session->userdata('authority');
    	if($authority != 'mcm' && $authority != 'op' && $authority != 'pm' && $authority != 'ag') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$this->data['sympoCd'] = $sympoCd;
    	$this->data['applicantCds'] = $applicantCds;
    	
    	$this->load->view('popup/passcodeCancel', $this->data);
    }
    
    //passcode 삭제
    function passcodeDelete() {
    	$sympoCd          = $this->input->post('sympoCd');
    	$applicantCds       = $this->input->post('applicantCds');
    	$authority = $this->session->userdata('authority');
    	if($authority != 'mcm' && $authority != 'op' && $authority != 'pm' && $authority != 'ag') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$this->data['sympoCd'] = $sympoCd;
    	$this->data['applicantCds'] = $applicantCds;
    	
    	$this->load->view('popup/passcodeDelete', $this->data);
    }
    
    //passcode 복원
    function passcodeRestore() {
    	$sympoCd          = $this->input->post('sympoCd');
    	$applicantCds       = $this->input->post('applicantCds');
    	$authority = $this->session->userdata('authority');
    	if($authority != 'mcm' && $authority != 'op' && $authority != 'pm' && $authority != 'ag') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$this->data['sympoCd'] = $sympoCd;
    	$this->data['applicantCds'] = $applicantCds;
    	
    	$this->load->view('popup/passcodeRestore', $this->data);
    }
    
    //사용자 대량등록 팝업
    function memberExcelUpload(){
        $this->load->view('popup/memberExcelUpload',$this->data);
    }

    //사용자 정보 수정 팝업
    function memberModify(){
        $member_cd          = $this->input->post('member_cd');
        $member = $this->member_model->getMemberDetail($member_cd);
        $this->data['member'] = $member;

        $this->load->view('popup/memberModify',$this->data);
    }

    // 사용자 삭제
    function memberDel() {
        $member_cd          = $this->input->post('member_cd');
        $this->member_model->getMemberDel($member_cd);
    }
    
    // 사용자 정보 상세보기
    function memberDetail() {
    	$memberCd = $this->input->post('memberCd');
    	
    	$member = $this->member_model->getMembertOnly($memberCd);
    	
    	$this->data['member'] = $member;
    	$this->load->view('popup/memberDetail', $this->data);
    }
    
    // 사용자 정보 수정
    function memberDetailEdit() {
    	
    	$member_cd          	= $this->input->post('memberCd');
    	$member 				= $this->member_model->getMemberDetail($member_cd);
    	$brandGroupList 		= $this->member_model->getMemberRelGroup($member_cd);
    	$office_list 			= $this->member_model->getOfficeAllList('');
    	$brand_group_All_list = $this->member_model->memberToBrands($member_cd);
    	
    	$this->data['member'] = $member;
    	$this->data['brandGroupList'] = $brandGroupList;
    	$this->data['brand_group_All_list'] = $brand_group_All_list;
    	$this->data['office_list'] = $office_list;
    	
    	$this->load->view('popup/memberDetailEdit', $this->data);
    }
    
    // 사용자 사이트 이용정보 상세보기
    function memberManagerDetail() {
    	
    	$memberCd = $this->input->post('memberCd');
    	
    	$member = $this->member_model->getMembertOnly($memberCd);
    	
    	$this->data['member'] = $member;
    	
    	$this->load->view('popup/memberManagerDetail', $this->data);
    }

    // 공지사항 보기
    function noticeView() {
        $notice_cd = $this->input->post('notice_cd');

        if($notice_cd != ""){
        	$notice = $this->notice_model->getNoticeDetail($notice_cd);
        }else{
            alert("존재하지않는 게시물입니다.");
        }

        $this->data['notice'] = $notice;

        $this->load->view('popup/noticeView', $this->data);
    }
    
    // 공지사항 작성
    function noticeWrite(){
    	
    	$authority = $this->session->userdata('authority');
    	if($authority != 'mcm' && $authority != 'op' && $authority != 'pm') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$member_id		= $this->session->userdata('member_id');
    	$member_cd		= $this->session->userdata('member_cd');
    	
    	$brandList = $this->member_model->memberToBrands($member_cd);
    	
    	$this->data['brandList'] = $brandList;
    	$this->data['member_id'] = $member_id;
    	
    	$this->load->view('popup/noticeWrite',$this->data);
    }
    
    // 사용자별 브랜드 추가
    function memeberGroupList() {
    	$member_cd		= $this->session->userdata('member_cd');
    	
    	$brand_group_list = $this->member_model->memberToBrands($member_cd);
    	
    	echo json_encode($brand_group_list);
    }
    
    // 공지사항 수정
    function noticeEdit() {
    	$notice_cd = $this->input->post('notice_cd');
    	$member_cd		= $this->session->userdata('member_cd');
    	
    	$notice = $this->notice_model->getNoticeDetail($notice_cd);
    	$brandGroupList = $this->notice_model->getNoticeRelGroup($notice_cd);
    	$brand_group_All_list = $this->member_model->memberToBrands($member_cd);
    	
    	$this->data['brandGroupList'] = $brandGroupList;
    	$this->data['brand_group_All_list'] = $brand_group_All_list;
    	$this->data['notice'] = $notice;
    	
    	$this->load->view('popup/noticeEdit', $this->data);
    }

    // 브랜드 추가
    function brandAdd() {

        //api로 브랜드 전체를 불러온다.
        $sympo_token = getSympo_AuthToken();
        $json = json_decode($sympo_token, true);
        $apiGroupList= getGroup($json['authToken']);
        $resultGroup = json_decode($apiGroupList, true);

        $apiSiteList= getSite($json['authToken'], 'all');
        $resultSite = json_decode($apiSiteList, true);



        $this->data['apiGroupList'] = $resultGroup['Data'];
        $this->data['apiSiteList'] = $resultSite['Data'];

        $this->load->view('popup/brandAdd', $this->data);
    }

    // 브랜드에 사용자 추가 폼 열기
    function brandInclude() {

        $brand_group_cd             = $this->input->post('brand_group_cd');

        $this->data['brand_group_cd']            = $brand_group_cd;

        $this->load->view('popup/brandInclude', $this->data);
    }

    // 브랜드 그룹에 추가할 사용자 검색
    function brandIncludeMemSearch() {

        $brand_group_cd             = $this->input->post('brand_group_cd');
        $searchType                 = $this->input->post('searchType');
        $searchText                 = $this->input->post('searchText');

        $memberList = $this->member_model->getBrandMemberAllList($brand_group_cd, $searchType, $searchText);
        echo json_encode($memberList);
    }

    // 브랜드 그룹에 사용자 추가
    function brandIncludeMemSelect() {
        $member_id 			= $this->session->userdata('member_id');
        $brand_group_cd     = $this->input->post('brand_group_cd');
        $memberCds          = $this->input->post('memberCds');

        $memberCdsArr = explode(";", $memberCds);

        foreach ($memberCdsArr as $memberCd):
            if($memberCd != null && $memberCd != '') {
                $dataArr = array(
                    'brand_group_cd' => $brand_group_cd,
                    'member_cd' => $memberCd,
                    'creator' => $member_id,
                    'updater' => $member_id
                );

                $res = $this->common_model->getSequences("bgm");
                $dataArr['brand_group_member_cd'] = str_pad($res['currval'], 20, "BGM_0000000000000000", STR_PAD_LEFT);
                $this->db->insert('brand_group_member', $dataArr);
                
                $dataArr2 = Array (
                		'updatedate'        =>  date("Y-m-d H:i:s",strtotime("+9 hours"))
                		);
                $this->db->where('brand_group_cd', $brand_group_cd);
                $updateData = $this->db->update('brand_group',$dataArr2);
            }
        endforeach;
    }

    
    // 사용자 추가
    function memberAdd() {

        $brand_group_list 	= $this->member_model->getBrandGroupAllList("", "", "brand_group_name", "asc");
        $office_list 		= $this->member_model->getOfficeAllList('');
        $this->data['brandGroupList'] = $brand_group_list;
        $this->data['office_list'] = $office_list;

        $this->load->view('popup/memberAdd', $this->data);
    }

    
    // 사용자 정보 수정
    function memberEdit() {

        $member_cd          = $this->input->post('member_cd');
        $member = $this->member_model->getMemberDetail($member_cd);
        $brandGroupList = $this->member_model->getMemberRelGroup($member_cd);
        $office_list 		= $this->member_model->getOfficeAllList('');
        $brand_group_All_list = $this->member_model->memberToBrands($member_cd);

        $this->data['member'] = $member;
        $this->data['brandGroupList'] = $brandGroupList;
        $this->data['brand_group_All_list'] = $brand_group_All_list;
        $this->data['office_list'] = $office_list;

        $this->load->view('popup/memberEdit', $this->data);
    }
    
    function brand_group_regist() {

        $member_id          = $this->session->userdata('member_id');

        $org_id       = $this->input->post('org_id');
        $org_name     = $this->input->post('org_name');
        $brand_id     = $this->input->post('brand_id');
        $brand_name   = $this->input->post('brand_name');


        $dataArr = Array(
            'org_id'            => $org_id,
            'brand_group_id'    => $brand_id,
            'org_name'          => $org_name,
            'brand_group_name'  => $brand_name,
            'creator'           => $member_id,
            'updater'           => $member_id
        );

        $res = $this->common_model->getSequences("brg");
        $dataArr['brand_group_cd'] = str_pad($res['currval'], 20, "BRG_0000000000000000", STR_PAD_LEFT);
        $this->db->insert('brand_group', $dataArr);
        $this->success('success');
    }

    function groupSearch() {
        $searchText                 = $this->input->post('searchText');
        $member_cd                  = $this->input->post('member_cd');

        $groupList = $this->member_model->getBrandGroupAllList1($member_cd, 'brand_group_name', $searchText, 'updatedate', 'desc');
        echo json_encode($groupList);
    }
    
    // 선택된 사용자 권한 가져오기
    function getAuthority() {
    	$member_cd                  = $this->input->post('member_cd');
    	
    	$authority = $this->member_model->getAuthority($member_cd);
    	
    	echo json_encode($authority);
    }

    // 사용자별 브랜드 추가
    function BrandGroupAllList() {
        $searchType = "";
        $searchText = "";
        $sortWhere = "brand_group_name";
        $sortType = "asc";

        $brand_group_list = $this->member_model->getBrandGroupAllList($searchType, $searchText, $sortWhere, $sortType);

        echo json_encode($brand_group_list);
    }

    function orgTobrand() {
        $group_id                  = $this->input->post('group_id');

        $sympo_token = getSympo_AuthToken();
        $json = json_decode($sympo_token, true);

        $apiSiteList= getSite($json['authToken'], $group_id);
        $resultSite = json_decode($apiSiteList, true);

        echo json_encode($resultSite['Data']);

    }
}