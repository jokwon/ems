<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller{
    private $start = 0;
    private $scale = 10;
    private $page_scale = 10;

    private $base_uri = '';

    private $head = array();

    private $footer = array();

    private $data = array();
    
    private $return_json = false;
    private $return_array = array(
        'status' => '500'
        ,'msg' => '잘못된 접근입니다.'
        ,'url' => ''
        ,'reload' => 'off'
        ,'arr' => array()
    );


    function __construct() {
        parent::__construct();

        $this->load->model(array('settings_model', 'member_model','common_model','main_model'));

        $this->head['left'] = "member";
        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);

        $this->base_uri = '/'.strtolower(__CLASS__);
        $this->data['base_uri'] = $this->base_uri;

        $row = $this->settings_model->getSettings('');
        $this->head['customer_name'] = $row['customer_name'];
        $this->head['symposium_left'] = $row['symposium_left'];
        $this->footer['symposium_desc'] = $row['symposium_desc'];
        $this->data['dashboard_num1'] = $row['dashboard_num1'];

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];

        $this->data['authority']  =$authotiry = $this->session->userdata('authority');
        
        $authority_now = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        $authority_db = $this->main_model->getAuthority($member_cd);
        if($authority_now != null && $authority_now != '') {
        	if($authority_now != $authority_db['authority']) {
        		alert("권한이 변경되었습니다 다시 로그인 해주세요",HOME_DIR."/login/logout");
        	}
        }
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {
        $this->user_all();
    }

    // 전체회원
    function user_all($ErrorData = null, $saveData = "") {
        $authority = $this->session->userdata('authority');
        $member_id = $this->session->userdata('member_id');
        $member_cd = $this->session->userdata('member_cd');
        if($authority != 'op' && $authority != 'mcm' && $authority != 'pm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $authType = $this->input->post('authType');
        $requestType = $this->input->post('requestType');
        $searchType = $this->input->post('searchType');
        $searchText = $this->input->post('searchText');
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");

        if($authType == null || $authType == '') {
            $authType = 'All';
        }
        if($requestType == null || $requestType == '') {
            $requestType = 'All';
        }
        if($searchType == null || $searchType == '') {
            $searchType = 'name';
        }
        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        $memberList = $this->member_model->getMemberList(1, $authType, $requestType, $searchType, $searchText, $sortWhere, $sortType);
        $memberListCount = $this->member_model->getMemberCount($authType, $requestType, $searchType, $searchText, $sortWhere, $sortType);
        $brand_group_list = $this->member_model->getBrandGroupAllList("", "", "brand_group_name", "asc");

        $this->data['brandGroupList'] = $brand_group_list;
        $this->data['memberList'] = $memberList;
        $this->data['memberListCount'] = $memberListCount;
        $this->data['authType']     = $authType;
        $this->data['requestType']  = $requestType;
        $this->data['searchType']   = $searchType;
        $this->data['searchText']   = $searchText;
        $this->data['sortWhere']    = $sortWhere;
        $this->data['sortType']     = $sortType;
        $this->data['excel'] = $ErrorData;
        $this->data['saveData'] = $saveData;
        $this->data['authority'] = $authority;
        $this->data['member_id'] = $member_id;
        $this->data['member_cd'] = $member_cd;

        $this->load->view('head',$this->head);
        $this->load->view('member/user_all',$this->data);
        $this->load->view('footer', $this->footer);
    }
    
    // 전체회원 더보기
    function member_listAjax() {
    	$authority = $this->session->userdata('authority');
    	if($authority != 'admin' && $authority != 'op' && $authority != 'mcm' && $authority != 'pm') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$page           = $this->input->post("page");
    	$authType       = $this->input->post("authType");
    	$searchType     = $this->input->post("searchType");
    	$searchText     = $this->input->post('searchText', true);
    	$sortWhere     = $this->input->post("sortWhere");
    	$sortType     = $this->input->post("sortType");
    	$requestType     = $this->input->post("requestType");
    	
    	if($authType == null || $authType == '') {
    		$authType = 'All';
    	}
    	if($requestType == null || $requestType == '') {
    		$requestType = 'All';
    	}
    	if($searchType == null || $searchType == '') {
    		$searchType = 'name';
    	}
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	
    	$memberList = $this->member_model->getMemberList($page, $authType, $requestType, $searchType, $searchText, $sortWhere, $sortType);
    	$brandGroupList = $this->member_model->getBrandGroupAllList("", "", "brand_group_name", "asc");
    	
    	echo json_encode(array("brandGroupList"=>$brandGroupList, "memberList"=>$memberList));
    }

    // 브랜드 리스트
    function user_brand() {

        $searchType = $this->input->post('searchType');
        $searchText = $this->input->post('searchText');
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");

        if($searchType == null || $searchType == '') {
            $searchType = 'name';
        }
        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        $brandGroupList = $this->member_model->getBrandGroupList(1, $searchType, $searchText, $sortWhere, $sortType);
        $brandGroupCount = $this->member_model->getBrandGroupCount($searchType, $searchText);

        $this->data['brandGroupList'] = $brandGroupList;
        $this->data['brandGroupCount'] = $brandGroupCount;
        $this->data['searchType']   = $searchType;
        $this->data['searchText']   = $searchText;
        $this->data['sortWhere']    = $sortWhere;
        $this->data['sortType']     = $sortType;

        $this->load->view('head',$this->head);
        $this->load->view('member/user_brand',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 브랜드 리스트 더보기
    function user_brandAjax() {
        $authority = $this->session->userdata('authority');
        if($authority != 'admin' && $authority != 'op' && $authority != 'mcm' && $authority != 'pm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $page           = $this->input->post("page");
        $searchType     = $this->input->post("searchType");
        $searchText     = $this->input->post('searchText', true);
        $sortWhere     = $this->input->post("sortWhere");
        $sortType     = $this->input->post("sortType");

        if($searchType == null || $searchType == '') {
            $searchType = 'name';
        }
        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }
        $brandGroupList = $this->member_model->getBrandGroupList($page, $searchType, $searchText, $sortWhere, $sortType);

        echo json_encode($brandGroupList);
    }

    // 브랜드 리스트 상세
    function user_brand_detail() {
        $brand_group_cd = $this->input->get_post('brand_group_cd');
        $searchType = $this->input->post('searchType');
        $searchText = $this->input->post('searchText');
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $authType = $this->input->post('authType');

        if($authType == null || $authType == '') {
            $authType = 'All';
        }
        if($searchType == null || $searchType == '') {
            $searchType = 'name';
        }
        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }
        $brandGroup = $this->member_model->getBrandGroup($brand_group_cd);
        $brandGroupPeopleList = $this->member_model->getBrandGroupPeopleList(1, $brand_group_cd, $authType, $searchType, $searchText, $sortWhere, $sortType);
        $brandGroupPeopleCount = $this->member_model->getBrandGroupPeopleCount($brand_group_cd, $authType, $searchType, $searchText);
        $brand_group_list = $this->member_model->getBrandGroupAllList("", "", "brand_group_name", "asc");
        
        $this->data['brandGroupList'] = $brand_group_list;
        $this->data['brandGroup'] = $brandGroup;
        $this->data['brandGroupPeopleList'] = $brandGroupPeopleList;
        $this->data['brandGroupPeopleCount'] = $brandGroupPeopleCount;
        $this->data['authType']     = $authType;
        $this->data['searchType']   = $searchType;
        $this->data['searchText']   = $searchText;
        $this->data['sortWhere']    = $sortWhere;
        $this->data['sortType']     = $sortType;

        $this->load->view('head',$this->head);
        $this->load->view('member/user_brand_detail',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 브랜드 리스트 상세 더보기
    function user_brand_detailAjax() {
        $authority = $this->session->userdata('authority');
        if($authority != 'admin' && $authority != 'op' && $authority != 'mcm' && $authority != 'pm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $page           = $this->input->post("page");
        $brand_group_cd = $this->input->get_post('brand_group_cd');
        $searchType = $this->input->post('searchType');
        $searchText = $this->input->post('searchText');
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $authType = $this->input->post('authType');


        if($authType == null || $authType == '') {
            $authType = 'All';
        }
        if($searchType == null || $searchType == '') {
            $searchType = 'name';
        }
        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }
        $memberList = $this->member_model->getBrandGroupPeopleList($page, $brand_group_cd, $authType, $searchType, $searchText, $sortWhere, $sortType);
        $brandGroupList = $this->member_model->getBrandGroupAllList("", "", "brand_group_name", "asc");

        echo json_encode(array("brandGroupList"=>$brandGroupList, "memberList"=>$memberList));
    }

    // 회원가입요청 완료
    function member_complete() {
        $this->load->view('commonHead',$this->head);
        $this->load->view('member/member_complete',$this->data);
    }

    // 마이페이지
    function mypage() {
        $member_cd = $this->session->userdata('member_cd');
        $member_id = $this->session->userdata('member_id');
        $member_name = $this->session->userdata('member_name');
        $member_email = $this->session->userdata('member_email');
        $member_hp = $this->session->userdata('member_hp');
        $member_org = $this->session->userdata('member_org');
        $member_dep = $this->session->userdata('member_dep');
        $member_team = $this->session->userdata('member_team');

        $row = $this->member_model->getMemberDetail($member_cd);

        $this->data['row'] = $row;
        $this->data['member_cd'] = $member_cd;
        $this->data['member_id'] = $member_id;
        $this->data['member_name'] = $member_name;
        $this->data['member_email'] = $member_email;
        $this->data['member_hp'] = $member_hp;
        $this->data['member_org'] = $member_org;
        $this->data['member_dep'] = $member_dep;
        $this->data['member_team'] = $member_team;

        $this->load->view('head',$this->head);
        $this->load->view('member/mypage',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 아이디 존재여부 확인
    function idCheck() {
        $member_id = $this->input->post('member_id');
        $memCount = $this->member_model->getMemberCountById($member_id);
        echo json_encode(array("result"=>$memCount));
    }

    // 이메일 존재여부 확인
    function emailCheck() {
       $member_email = $this->input->post('member_email');
       $memCount = $this->member_model->getMemberCountByEmail($member_email);
       echo json_encode(array("result"=>$memCount));
    }

    // 회원가입페이지에서 회원가입
    function member_request() {
        //$member_brand     = $this->input->post('member_brand');
        //$member_org       = $this->input->post('member_org');
        $member_email     = $this->input->post('member_email');
        $member_name      = $this->input->post('member_name');
        $member_dep       = $this->input->post('member_dep');
        $member_team      = $this->input->post('member_team');
        $brand_seq        = $this->input->post('brand_seq');
        $company_seq      = $this->input->post('company_seq');
        
        $agree            = $this->input->post('agree');

        $certification_cd = generateRandomString(10);
        $memCount = $this->member_model->getMemberCountByEmailReJoin($member_email);
        if($memCount > 0) {
        	$dataArr = array(
        			'member_pass'		=> null,
        			'member_name'       => $member_name,
        			'member_hp'			=> null,
        	        'brand_seq'         => $brand_seq,
        			'member_dep'        => $member_dep,
        			'member_team'       => $member_team,
        	        'company_seq'       => $company_seq,
        			'member_regdate'	=> 'now()',
        			'createdate'		=> 'now()',
        			'updatedate'		=> 'now()',
        			'emailCert'         => $certification_cd,
        			'agree'             => 'Y',
        			'emailConfirm'      => 'false',
        			'creator'           => 'system',
        			'updater'           => 'system',
        			'authority'         => 'mr',
        			'deleteCheck'       => 'N',
        			'session'       	=> 'N'
        			
        	);
        	
        	$saveData = $this->member_model->insertMemberDataRejoin($dataArr,$member_email);
        	
        } else {
        	$dataArr = array(
        			'member_id'       => $member_email,
        			'member_name'       => $member_name,
        			'member_email'      => $member_email,
        	        'company_seq'        => $company_seq,
        			'member_dep'        => $member_dep,
        			'member_team'       => $member_team,
        	        'brand_seq'      => $brand_seq,
        			'emailCert'         => $certification_cd,
        			'agree'             => 'Y',
        			'emailConfirm'      => 'false',
        			'creator'           => 'system',
        			'updater'           => 'system',
        			'authority'         => 'mr'
        	);
        	
        	$res = $this->common_model->getSequences("mem");
        	$dataArr['member_cd'] = str_pad($res['currval'], 20, "MEM_0000000000000000", STR_PAD_LEFT);
        	$saveData = $this->member_model->insertMemberData($dataArr);
        }

        $this->load->view('commonHead',$this->head);
        $this->load->view('member/member_complete',$this->data);
    }

    // 사용자 추가 관리자 페이지에서 회원가입
    function member_request1() {
        $member_id              = $this->session->userdata('member_id');

        $member_email     = $this->input->post('member_email');
        $member_name      = $this->input->post('member_name');
        $member_org       = $this->input->post('member_org');
        $member_dep       = $this->input->post('member_dep');
        $member_team      = $this->input->post('member_team');
        $member_hp      = $this->input->post('member_hp');
        $authority        = $this->input->post('authority');
        $office_name        = $this->input->post('office_name');
        $brand_group_cds  = $this->input->post('brand_group_cds');

        $certification_cd = generateRandomString(10);
        
        $memCount = $this->member_model->getMemberCountByEmailReJoin($member_email);
        $createMember = $member_id = $this->session->userdata('member_id');
        
        if($authority == null || $authority == '') {
        	$authority = 'mr';
        }
        
        // 다시 회원가입한 경우
        if($memCount > 0) {
        	$dataArr = array(
        			'member_pass'		=> null,
        			'member_name'       => $member_name,
        			'member_hp'			=> $member_hp,
        			'member_org'        => $member_org,
        			'member_dep'        => $member_dep,
        			'member_team'       => $member_team,
        			'office_name'       => $office_name,
        			'member_brand'      => null,
        			'member_regdate'	=> 'now()',
        			'createdate'		=> 'now()',
        			'updatedate'		=> 'now()',
        			'emailCert'         => $certification_cd,
        			'agree'             => null,
        			'emailConfirm'      => 'add',
        			'creator'           => $createMember,
        			'authority'         => $authority,
        			'updater'           => $createMember,
        			'deleteCheck'       => 'N',
        			'session'       	=> 'N'
        			
        	);
        	
        	$saveData = $this->member_model->insertMemberDataRejoin($dataArr,$member_email);
        	$member_cd = $this->member_model->getMemberCd($member_email);
        	$dataArr['member_cd'] = $member_cd['member_cd'];
        	
        }else { // 처음 회원가입한 경우
	        $dataArr = array(
	            'member_id'         => $member_email,
	            'member_name'       => $member_name,
	            'member_email'      => $member_email,
	            'member_org'        => $member_org,
	            'member_dep'        => $member_dep,
	            'member_team'       => $member_team,
	            'member_hp'         => $member_hp,
	        	'office_name'       => $office_name,
	            'emailCert'         => $certification_cd,
	            'emailConfirm'      => 'add',
	        	'creator'           => $createMember,
	        	'updater'           => $createMember,
	            'authority'         => $authority
	        );
	
	        $res = $this->common_model->getSequences("mem");
	
	        $dataArr['member_cd'] = str_pad($res['currval'], 20, "MEM_0000000000000000", STR_PAD_LEFT);
	
	        $saveData = $this->member_model->insertMemberData($dataArr);
        }


        $brand_group_cdsArr = explode(";", $brand_group_cds);

        // 브랜드 그룹 설정
        foreach ($brand_group_cdsArr as $brand_group_cd):
	        if($brand_group_cd != null && $brand_group_cd != "") {
	        	$memberGroupCount = $this->member_model->getCheckMemberGroup($dataArr['member_cd'],$brand_group_cd);
	        	
	        	if($memberGroupCount == 0) {
		            $dataArr1 = array(
		                'brand_group_cd' => $brand_group_cd,
		                'member_cd' => $dataArr['member_cd'],
		                'creator' => $member_id,
		                'updater' => $member_id
		            );
		
		            $res1 = $this->common_model->getSequences("bgm");
		            $dataArr1['brand_group_member_cd'] = str_pad($res1['currval'], 20, "BGM_0000000000000000", STR_PAD_LEFT);
		            $this->db->insert('brand_group_member', $dataArr1);
		        }
	        }
        endforeach;
    }

    // 활성화 메일 진행완료
    function member_regist() {
        $member_cd          = $this->input->post('member_cd');
        $member_pass        = $this->input->post('member_pass');
        $member_hp          = $this->input->post('member_hp');

        $dataArr = array (
            'newPw1'      		=> $member_pass,
            'member_hp'        	=> $member_hp,
        	'member_regdate'    => date("Y-m-d H:i:s",strtotime("+9 hours")),
        	'updatedate'    	=> date("Y-m-d H:i:s",strtotime("+9 hours")),
            'emailConfirm'      => 'true',
        	'agree'				=> 'Y',
        	'session'			=> 'Y'
        );
        
        $this->member_model->updateMemberData($member_cd, $dataArr);

        $member = $this->member_model->getMemberDetail($member_cd);
        $this->data["member"]     = $member;

        $this->load->view('commonHead');
        $this->load->view('login/join_complete',$this->data);

    }

    // 마이페이지에서 사용자 정보 업데이트
    function mypageUpdate() {
        $member_cd          = $this->input->post('member_cd');
        $member_id          = $this->input->post('member_id');
        $member_pass        = $this->input->post('member_pass');
        $member_hp          = $this->input->post('member_hp');
        $member_org         = $this->input->post('member_org');
        $member_dep         = $this->input->post('member_dep');
        $member_team        = $this->input->post('member_team');
        $newPw1             = $this->input->post('newPw1');

        $params = array(
            'member_id'         => $member_id,
            'member_pass'       => $member_pass,
            'member_hp'         => $member_hp,
            'member_org'        => $member_org,
            'member_dep'        => $member_dep,
            'member_team'       => $member_team,
            'newPw1'            => $newPw1
        );

        $row = $this->member_model->getMemberRow($params);

        if($row['member_cd'] == '') {
            alert('패스워드가 일치하지 않습니다.',HOME_DIR.'/member/mypage');
        } else {
            $saveData = $this->member_model->updateMemberData($member_cd, $params);
            $params['authority'] = $row['authority'];
            $this->session->set_userdata($params);
            alert("수정 되었습니다.",HOME_DIR.'/member/mypage');
        }
    }

    // 아이디 찾기 페이지 이동
    function idSearch() {
        $this->load->view('commonHead',$this->head);
        $this->load->view('member/id_search',$this->data);
    }

    // 임시 비밀번호 발급 이메일
    function idSearchEmail() {
    	$member_name = $this->input->post("member_name");
    	$member_email = $this->input->post("member_email");
    	$certification_cd = generateRandomString(10);
    	
    	$params = array(
    			'member_name'  		=> $member_name,
    			'member_email'     	=> $member_email
    	);
    	
    	$getSearch = $this->member_model->getSearchEmailId($params);
    	
    	if($getSearch != null && $getSearch['member_id'] != '' && $getSearch['member_id'] != '') {
    		$this->member_model->setPasswdChangeData($member_email, $certification_cd);
    		$row = $this->settings_model->getSettings('');
    		
    		$mail_subject = "[ASOS] 임시 비밀번호 입니다.";
    		
    		$mail_msg = '<!DOCTYPE html>';
    		$mail_msg .= '<html lang="ko">';
    		$mail_msg .= '<head>';
    		$mail_msg .= '<meta charset="UTF-8">';
    		$mail_msg .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">';
    		$mail_msg .= '<meta http-equiv="X-UA-Compatible" content="ie=edge">';
    		$mail_msg .= '<title>[ASOS] 임시 비밀번호 입니다.</title>';
    		$mail_msg .= '</head>';
    		$mail_msg .= '<body style="margin: 0; padding: 0; width: 100%; height: 100%; box-sizing: border-box;">';
    		$mail_msg .= '<div style="max-width:500px; padding:30px; margin:0 auto; box-sizing:border-box; text-align: center;">';
    		$mail_msg .= '<h1 style="font-weight: 700; font-size: 3.25rem; margin-bottom: 0; line-height: 1;">ASOS</h1>';
    		$mail_msg .= '<p style="font-size: 1.125rem; margin-top: 0;">Astellas Symposium Operation System</p>';
    		$mail_msg .= '<div style="padding: 1rem 0 2.5rem;">';
    		$mail_msg .= '<h2 style="font-weight: 300; font-size: 1.025rem;">요청하신 임시 비밀번호는<br><strong style="font-weight: 500; color: #a40b2e;">['.$certification_cd.']</strong> 입니다.</h2>';
    		$mail_msg .= '</div>';
    		$mail_msg .= '<div style=" margin-bottom: 3rem">';
    		$mail_msg .= '<a href="ems.medinar.co.kr" style="background-color: #7e001d; height: 3.75rem; white-space: nowrap; word-break: keep-all; display: block; height: 60px; line-height: 60px; width: calc(100% - 40px); font-weight: 500; letter-spacing: normal; color: white; text-align: center; padding: 0 1.25rem; border-radius: 3px; background-clip: padding-box; text-decoration: none;">사이트 바로가기</a>';
    		$mail_msg .= '</div>';
    		$mail_msg .= '<div style="text-align: left;">';
    		$mail_msg .= '<h6 style="font-size: 1rem; font-weight: 500; line-height: 1.2; margin-bottom: 0.5rem;">솔루션 사무국</h6>';
    		$mail_msg .= '<p style="font-size: 0.875rem; opacity: 0.5; margin-top: 0;">사용과 관련된 문의 사항은 하단의 메일 및 전화번호로 연락 주시기 바랍니다.</p>';
    		$mail_msg .= '<p style="font-size: 0.875rem; font-weight: 500; margin-bottom: 0.25rem;">이메일: <a href="mailto:support@webinars.co.kr" style="font-weight: 400; text-decoration: none; color: black;">support@webinars.co.kr</a></p>';
    		$mail_msg .= '<p style="font-size: 0.875rem; font-weight: 500; margin-top: 0;">전화번호: <span style="font-weight: 400; text-decoration: none; color: black;">'.$row['dashboard_num1'].'</span></p>';
    		$mail_msg .= '</div>';
    		$mail_msg .= '</div>';
    		$mail_msg .= '</body>';
    		$mail_msg .= '</html>';
    		
    		if (sendmail_smtp($member_email, $mail_subject, $mail_msg)) {
    			$result = "ok";
    		} else {
    			$result = "no";
    		}
    	} else {
    		$result = "not";
    	}
    	
    	echo json_encode(array("result"=>$result));
    }
    
    // 아이디 찾기 페이지에서 아이디 찾기 눌렀을때
    function idSearch_ajax() {
        $member_hp   = $this->input->post('member_hp');
        $member_name    = $this->input->post('member_name');
        $params = array(
            'member_name'  		=> $member_name,
            'member_hp'      	=> $member_hp
        );
        $getSearch = $this->member_model->getSearchId($params);
        echo json_encode(array('list'=>$getSearch));

    }

    // 사용자 삭제
    function memberDel() {
        $memberCds = $this->input->post('memberCds');

        $memberCdsArr = explode(";", $memberCds);

        foreach ($memberCdsArr as $memberCd):
        
        	$memberId = $this->member_model->getMemberId($memberCd);
         	$this->member_model->getBrandGroupMemberDelMemberOnly($memberCd);
            $this->member_model->getMemberDel($memberCd);
        endforeach;

        alert("삭제되었습니다.",HOME_DIR."/member");
    }

    // 사용자를 특정 브랜드 그룹에서 제거
    function brandGroupMemberDel() {
        $brand_group_cd = $this->input->post('brand_group_cd');
        $memberCds = $this->input->post('memberCds');

        $memberCdsArr = explode(";", $memberCds);

        foreach ($memberCdsArr as $memberCd):
            $this->member_model->getBrandGroupMemberDel($brand_group_cd, $memberCd);
        
            $dataArr2 = Array (
            		'updatedate'        =>  date("Y-m-d H:i:s",strtotime("+9 hours"))
            		);
            $this->db->where('brand_group_cd', $brand_group_cd);
            $updateData = $this->db->update('brand_group',$dataArr2);
            
        endforeach;

        alert("삭제되었습니다.",HOME_DIR."/member/user_brand_detail?brand_group_cd=".$brand_group_cd);
    }

    // 브랜드 그룹 삭제
    function brandGroupDel() {
        $brandGroupCds = $this->input->post('brandGroupCds');

        $brandGroupCdsArr = explode(";", $brandGroupCds);

        foreach ($brandGroupCdsArr as $brandGroupCd):
            $this->member_model->getBrandGroupDel($brandGroupCd);
            $this->member_model->getBrandGroupMemberDelGroupOnly($brandGroupCd);
        endforeach;

        alert("삭제되었습니다.",HOME_DIR."/member/user_brand");
    }
    
    // 사용자 대량 업로드
    function excel_upload_exec() {

        $saveData = 'no';
        $saveData2 = 'no';

        if($_FILES["file1"]["name"]) {
            $resultArr = upload(HOME_DIR."/uploads/excel","file1");
            
            if($resultArr["result"] != "ok") {
            	$errorCode = json_encode("양식에 맞는 엑셀 파일만 업로드 가능합니다.", JSON_UNESCAPED_UNICODE);
            	echo json_encode(array("errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
            	return;
            } else {
                $file_ext 	= $resultArr["upload_data"]["file_ext"];
                $full_path 	= $resultArr["upload_data"]["full_path"];

                if($file_ext != '.cvs' && $file_ext != ".xls" && $file_ext != ".xlsx") {
                	$errorCode = json_encode("엑셀파일만 업로드가능합니다.", JSON_UNESCAPED_UNICODE);
                	echo json_encode(array("errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
                	return;
                }

                $this->load->library("PHPExcel");
                $objPHPExcel = new PHPExcel();
                $objPHPExcel = PHPExcel_IOFactory::load($full_path);
                $sheetsCount = $objPHPExcel->getSheetCount();

                $ErrorData = array();
                
                // a열에 데이터가 들어가있는지 확인
                for($i = 0; $i < $sheetsCount; $i++) {
                	$objPHPExcel->setActiveSheetIndex($i);
                	$sheet          = $objPHPExcel->getActiveSheet();
                	$highestRow     = $sheet->getHighestRow();
                	$highestColumn  = $sheet->getHighestColumn();
	                for($check_row = 1; $check_row <= $highestRow; $check_row++) {
	                	$rowCheck = $sheet->rangeToArray('A' . $check_row . ':' . $highestColumn . $check_row, NULL, TRUE, FALSE);
	                	if($rowCheck[$check_row-1][0] != "" || $rowCheck[$check_row-1][0] != null) {
	                		$errorCode = json_encode("양식에 맞는 엑셀 파일만 업로드 가능합니다.", JSON_UNESCAPED_UNICODE);
	                		echo json_encode(array("errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
	                		return;
	                	}
	                }
                }

                for($i = 0; $i < $sheetsCount; $i++) {
                    $objPHPExcel->setActiveSheetIndex($i);
                    $sheet          = $objPHPExcel->getActiveSheet();
                    $highestRow     = $sheet->getHighestRow();
                    $highestColumn  = $sheet->getHighestColumn();
                    
                    for ($row = 6; $row <= $highestRow; $row++) {
                        $rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $member_id		    = $rowData[$row-1][1];
                        $member_name		= $rowData[$row-1][0];
                        $member_email	    = $rowData[$row-1][1];
                        $member_org	        = $rowData[$row-1][2];
                        $member_dep	        = $rowData[$row-1][3];
                        $member_team	    = $rowData[$row-1][4];
                        $member_auth	    = strtolower($rowData[$row-1][5]);
                        
                        // 이름이 없을경우 넘김
                        if ($member_name == "" || $member_name == null) {
                        	continue;
                        }
                        
                        // 권한이 없을 경우 넘김
                        if($member_auth == "" || $member_auth == null) {
                        	$errorCode = json_encode("양식에 맞는 엑셀 파일만 업로드 가능합니다.", JSON_UNESCAPED_UNICODE);
                        	echo json_encode(array("errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
                        	return;
                        }

                        $check_email = preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $member_email);

                        $unique = $this->member_model->getMemberUnique($member_id, $member_email);
             
                        if($member_id != null && $member_id != '' && count($unique) <= 0 && $check_email 
                        		&& !($member_auth !=null && $member_auth != '' && $member_auth != "ag" && $member_auth != "mr" && $member_auth != "pm" && $member_auth != "mcm" && $member_auth != "op")) 
                        {
                            $certification_cd = generateRandomString(10);

                            $memCount = $this->member_model->getMemberCountByEmailReJoin($member_email);
                            $createMember = $this->session->userdata('member_id');;
                            
                            if($memCount > 0) {
                            	$dataArr = array(
                            			'member_pass'		=> null,
                            			'member_name'       => $member_name,
                            			'member_hp'			=> null,
                            			'member_org'        => $member_org,
                            			'member_dep'        => $member_dep,
                            			'member_team'       => $member_team,
                            			'member_brand'      => null,
                            			'member_regdate'	=> 'now()',
                            			'createdate'		=> 'now()',
                            			'updatedate'		=> 'now()',
                            			'emailCert'         => $certification_cd,
                            			'agree'             => null,
                            			'emailConfirm'      => 'add',
                            			'creator'           => $createMember,
                            			'authority'         => $member_auth,
                            			'updater'           => $createMember,
                            			'deleteCheck'       => 'N',
                            			'session'       	=> 'N'
                            			
                            	);
                            	
                            	$saveData = $this->member_model->insertMemberDataRejoin($dataArr,$member_email);
                            	
                            } else {
                            	
	                            $res = $this->common_model->getSequences("mem");
	                            $dataArr = array (
	                                'member_id'        => $member_id,
	                                'member_name'      => $member_name,
	                                'member_email'     => $member_email,
	                                'emailCert'        => $certification_cd,
	                                'member_org'       => $member_org,
	                                'member_dep'       => $member_dep,
	                                'member_team'      => $member_team,
	                                'emailConfirm'     => 'add',
	                                'authority'        => $member_auth,
	                            	'member_hp'        => null,
	                            	'creator'          => $createMember,
	                            	'updater'          => $createMember
	                            );
	
	                            $dataArr['member_cd'] = str_pad($res['currval'], 20, "MEM_0000000000000000", STR_PAD_LEFT);
	                            $saveData = $this->member_model->insertMemberData($dataArr);
                            }
                        } else {

							// 중복 및 오류데이터 확인
							$duple_error = null;
							$auth_error = null;
							$email_error = null;
							$saveData2 = "Error";
							if($member_email != null && $member_email != '' && count($unique) > 0) {
								$duple_error = "중복";
							}
							if($member_auth !=null && $member_auth != '' && $member_auth != "ag" && $member_auth != "mr" && $member_auth != "pm" && $member_auth != "mcm" && $member_auth != "op") {
								$auth_error = "오류";
							}
							if($check_email == false) {
								$email_error = "오류";
							}
							$data = $member_name.";".$member_email.";".$member_org.";".$member_dep.";".$member_team.";".$member_auth.";".$duple_error.";".$auth_error.";".$email_error;
							array_push($ErrorData, $data);
                        }
                    }
                    
                }
            }
            
            // 모든리스트가 정상적으로 들어간 경우
            if($saveData == "ok" && $saveData2 == "Error") {
            	$saveData = json_encode($saveData, JSON_UNESCAPED_UNICODE);
            	$ErrorData = json_encode($ErrorData, JSON_UNESCAPED_UNICODE);
            	$errorCode = json_encode('에러', JSON_UNESCAPED_UNICODE);
            	echo json_encode(array("ErrorData"=>$ErrorData, "saveData"=>$saveData,"errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
            } else if($saveData2 == "Error" && $saveData != "ok") {
            	$saveData = json_encode($saveData, JSON_UNESCAPED_UNICODE);
            	$ErrorData = json_encode($ErrorData, JSON_UNESCAPED_UNICODE);
            	$errorCode = json_encode('에러', JSON_UNESCAPED_UNICODE);
            	echo json_encode(array("ErrorData"=>$ErrorData, "saveData"=>$saveData,"errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
            } else if($saveData2 != "Error" && $saveData == "ok") {
            	$saveData = json_encode($saveData, JSON_UNESCAPED_UNICODE);
            	$ErrorData = json_encode($ErrorData, JSON_UNESCAPED_UNICODE);
            	$errorCode = json_encode('에러', JSON_UNESCAPED_UNICODE);
            	echo json_encode(array("ErrorData"=>$ErrorData, "saveData"=>$saveData,"errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
            } else {
            	$errorCode = json_encode("파일업로드에 장애가 발생했습니다.", JSON_UNESCAPED_UNICODE);
            	echo json_encode(array("errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
            	return;
            }
        } else {
        	$errorCode = json_encode("엑셀 파일을 등록해주세요.", JSON_UNESCAPED_UNICODE);
        	echo json_encode(array("errorCode"=>$errorCode), JSON_UNESCAPED_UNICODE);
        	return;
        }
    }

    // 대량등록 업로드 리스트 중 중복되거나 오류 데이터 다운로드 
    function excelDupleDown() {
        $data  = $this->input->post("data");

        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('워크시트');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', '성명');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', '이메일');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', '회사명');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', '부서명');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', '팀명');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', '등급');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', '중복메일확인');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', '등급오류확인');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', '이메일오류확인');

        $dataArr = explode(',', $data);

        foreach ($dataArr as $key => $item) {
            $itemArr = explode(';', $item);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . ($key + 2), $itemArr[0], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . ($key + 2), $itemArr[1], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . ($key + 2), $itemArr[2], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . ($key + 2), $itemArr[3], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . ($key + 2), $itemArr[4], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . ($key + 2), $itemArr[5], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . ($key + 2), $itemArr[6], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . ($key + 2), $itemArr[7], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . ($key + 2), $itemArr[8], PHPExcel_Cell_DataType::TYPE_STRING);
        }

        $filename = iconv("UTF-8", "EUC-KR", "ErrorCheck_Members.xls"); // 엑셀 파일 이름
        header('Content-Type: application/vnd.ms-excel'); //mime 타입
        header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
        
    }
   
    // 활성화 메일 전송
    function memberActivationMailSend() {

        $creator = $this->session->userdata('member_id');
        $memberCds  = $this->input->get_post("memberCds");
        $memberCdArr = explode(';', $memberCds);

        foreach ($memberCdArr as $key => $item) {
            if ($item != null && $item != '') {
                $member = $this->member_model->getMemberDetail($item);

                $this->emsActivationEmail($member['member_cd'], $member['emailCert'], $member['member_email']);

                $dataArr = array(
                    'member_cd' => $member['member_cd'],
                    'creator' => $creator,
                    'updater' => $creator
                );

                $this->member_model->activityMailDelete($member['member_cd']);

                $res = $this->common_model->getSequences("amm");
                $dataArr['activity_member_mail_cd'] = str_pad($res['currval'], 20, "AMM_0000000000000000", STR_PAD_LEFT);
                $saveData = $this->member_model->activityMailInsert($dataArr);

                $dataArr1 = array(
                    'emailConfirm' => 'mail',
                );

                $this->member_model->updateMemberData($member['member_cd'], $dataArr1);
            }
        }
    }

    // 계정활성화 메일 전송
    function emsActivationEmail($member_cd, $emailCert, $member_email) {
    	
    	$row = $this->settings_model->getSettings('');

    	$mail_subject = "한국아스텔라스 심포지엄 온라인등록시스템 가입계정 활성화";
    	$mail_msg = '<!DOCTYPE html>';
    	$mail_msg .= '<html lang="ko">';
    	$mail_msg .= '<head>';
    	$mail_msg .= '<meta charset="UTF-8">';
    	$mail_msg .= '<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">';
    	$mail_msg .= '<meta http-equiv="X-UA-Compatible" content="ie=edge">';
    	$mail_msg .= '<title>한국아스텔라스 심포지엄 온라인등록시스템 가입계정 활성화</title>';
    	$mail_msg .= '</head>';
    	$mail_msg .= '<body style="margin: 0; padding: 0; width: 100%; height: 100%; box-sizing: border-box;">';
    	$mail_msg .= '<div style="max-width:500px; padding:30px; margin:0 auto; box-sizing:border-box; text-align: center;">';
    	$mail_msg .= '<h1 style="font-weight: 700; font-size: 3.25rem; margin-bottom: 0; line-height: 1;">ASOS</h1>';
    	$mail_msg .= '<p style="font-size: 1.125rem; margin-top: 0;">Astellas Symposium Operation System</p>';
    	$mail_msg .= '<div style="padding: 1rem 0 2.5rem;">';
    	$mail_msg .= '<h2 style="font-weight: 500; font-size: 1.25rem; color: #a40b2e;">'.$member_email.'</h2>';
    	$mail_msg .= '<p style="font-size: 1.025rem; font-weight: 300;">이메일 계정 활성화 버튼을 클릭 후<br>회원 가입 단계를 진행하세요.</p>';
    	$mail_msg .= '</div>';
    	$mail_msg .= '<div style="margin-bottom: 3rem">';
    	$mail_msg .= '<a href="'. FULL_URL .'/login/join_detail?emailCert='.$emailCert.'" style="background-color: #7e001d; height: 3.75rem; white-space: nowrap; word-break: keep-all; display: block; height: 60px; line-height: 60px; width: calc(100% - 40px); font-weight: 500; letter-spacing: normal; color: white; text-align: center; padding: 0 1.25rem; border-radius: 3px; background-clip: padding-box; text-decoration: none;">이메일 계정 활성화</a>';
    	$mail_msg .= '</div>';
    	$mail_msg .= '<div style="text-align: left;">';
    	$mail_msg .= '<h6 style="font-size: 1rem; font-weight: 500; line-height: 1.2; margin-bottom: 0.5rem;">솔루션 사무국</h6>';
    	$mail_msg .= '<p style="font-size: 0.875rem; opacity: 0.5; margin-top: 0;">사용과 관련된 문의 사항은 하단의 메일 및 전화번호로 연락 주시기 바랍니다.</p>';
    	$mail_msg .= '<p style="font-size: 0.875rem; font-weight: 500; margin-bottom: 0.25rem;">이메일: <a href="mailto:support@webinars.co.kr" style="font-weight: 400; text-decoration: none; color: black;">support@webinars.co.kr</a></p>';
    	$mail_msg .= '<p style="font-size: 0.875rem; font-weight: 500; margin-top: 0;">전화번호: <span style="font-weight: 400; text-decoration: none; color: black;">'.$row['dashboard_num1'].'</span></p>';
    	$mail_msg .= '</div>';
    	$mail_msg .= '</div>';
    	$mail_msg .= '</body>';
    	$mail_msg .= '</html>';

        if(sendmail_smtp($member_email, $mail_subject, $mail_msg)){
            $result = "ok";
            //메일 활성화 디비 입력
        } else {
            $result = "no";
        }

        echo json_encode(array("result"=>$result));
    }

	// 전체 사용자 엑셀 다운로드
    function memberExcelAjax() {
        $fileType = $this->input->post("fileType");
        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $authType = $this->input->post("authType");
        $requestType = $this->input->post("requestType");

        echo HOME_DIR."/member/memberExcel/?searchText=".$searchText."&searchType=".$searchType."&sortWhere=".$sortWhere."&sortType=".$sortType."&authType=".$authType."&requestType=".$requestType."&fileType=".$fileType;
    }

    // 전체 사용자 엑셀 다운로드
    function memberExcel() {
        $fileType = $this->input->get("fileType");
        $searchType = $this->input->get("searchType");
        $searchText = $this->input->get("searchText");
        $sortWhere = $this->input->get("sortWhere");
        $sortType = $this->input->get("sortType");
        $authType = $this->input->get("authType");
        $requestType = $this->input->get("requestType");

        $memberList = $this->member_model->getMemberAllList($authType, $requestType, $searchType, $searchText, $sortWhere, $sortType);

        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('워크시트');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '성명');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', '이메일');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', '모바일');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', '연결 브랜드계정');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', '회사명');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', '부서명');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', '팀명');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', '담당영업소');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', '권한');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', '계정상태');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', '가입 승인 담당자');
        

        if(count($memberList) > 0) {
            foreach ($memberList as $key => $item) {
                $status = "";
                if($item['emailConfirm'] == 'true') {
                    $status = '활성화';
                } else if($item['emailConfirm'] == 'add') {
                    $status = '계정등록';
                } else if($item['emailConfirm'] == 'mail') {
                    $status = '비활성화';
                } else if($item['emailConfirm'] == 'false') {
                    $status = '가입요청';
                }
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . (($key + 1) + 1), $item['member_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . (($key + 1) + 1), $item['member_email'], PHPExcel_Cell_DataType::TYPE_STRING);
                if($item['member_hp'] != null && $item['member_hp'] != '') {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . (($key + 1) + 1), $item['member_hp'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . (($key + 1) + 1), '-', PHPExcel_Cell_DataType::TYPE_STRING);
                }
                if($item['brand_names'] != null && $item['brand_names'] != '') {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . (($key + 1) + 1), $item['brand_names'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . (($key + 1) + 1), '-', PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . (($key + 1) + 1), $item['member_org'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . (($key + 1) + 1), $item['member_dep'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . (($key + 1) + 1), $item['member_team'], PHPExcel_Cell_DataType::TYPE_STRING);
                if($item['office_name'] != null && $item['office_name'] != '') {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . (($key + 1) + 1), $item['office_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . (($key + 1) + 1), '-', PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . (($key + 1) + 1), strtoupper($item['authority']), PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . (($key + 1) + 1), $status, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . (($key + 1) + 1), $item['appr_name'].' '.strtoupper($item['appr_authority']), PHPExcel_Cell_DataType::TYPE_STRING);
                
            }
        }

        if($fileType == 'pdf') {
            $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];
            $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];
            $mpdf = new \Mpdf\Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/custom/font/directory',
                ]),
                'fontdata' => $fontData + [
                        'frutiger' => [
                            'R' => 'UnBatang_0613.ttf',
                        ]
                    ],
                'default_font' => 'frutiger'
            ]);
            $mpdf->useAdobeCJK = true;

            $html = '<table style="width:100%; table-layout: fixed; border-top: 1px solid #000; border-bottom: 1px solid #000; border-spacing: 0; border-collapse: collapse; word-break: break-all; display: table; border-color: grey;">
                <caption style="font-size: 25px;">회원_전체사용자</caption>
                <colgroup style="display: table-column-group;">
                    <col style="width:20%">
                    <col style="width:10%">
                    <col style="width:10%">
                    <col style="width:20%">
                    <col style="width:20%">
                    <col style="width:20%">
                </colgroup>
                <thead style="border-top: 1px solid #000;">
                <tr style="display: table-row; vertical-align:inherit; border-color: inherit;">
                    <th scope="row" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">성명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">이메일</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">브랜드계정</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">회사명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">부서명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">팀명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">모바일</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">계정상태</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">등급</th>
                </tr>
                </thead>
                <tbody>';
            if(count($memberList) > 0) {
                foreach ($memberList as $key => $item) {
                    $status = "";
                    if($item['emailConfirm'] == 'true') {
                        $status = '활성화';
                    } else if($item['emailConfirm'] == 'add') {
                        $status = '계정등록';
                    } else if($item['emailConfirm'] == 'mail') {
                        $status = '비활성화';
                    } else if($item['emailConfirm'] == 'false') {
                        $status = '가입요청';
                    }

                    $html .= '<tr style="background: #ececec;">
                        <td style="width:10%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_name'].'</td>
                        <td style="width:15%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_email'].'</td>
                        <td style="width:15%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_brand_cd'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_org'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_dep'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_team'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_hp'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$status.'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['authority'].'</td>
                    </tr>';

                }
            }
            $html .= '</tbody>
            </table>';

            $mpdf->WriteHTML($html);
            $mpdf->Output(iconv('utf-8', 'euc-kr', '전체사용자 리스트.pdf'), 'D');
        } else if($fileType == 'csv') {
            $filename = '전체사용자 리스트.csv'; // 엑셀 파일 이름
            header('Content-Type: application/pdf'); //mime 타입
            header('Content-Disposition: attachment;filename="' . iconv('utf-8', 'euc-kr', $filename) . '"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        } else {
            $filename = '전체사용자 리스트.xls'; // 엑셀 파일 이름
            header('Content-Type: application/vnd.ms-excel'); //mime 타입
            header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        }

        $objWriter->save('php://output');
    }

    // 브랜드 그룹 엑셀 다운로드
    function brandGroupExcelAjax() {
        $fileType = $this->input->post("fileType");
        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");


        echo HOME_DIR."/member/brandGroupExcel/?searchText=".$searchText."&searchType=".$searchType."&sortWhere=".$sortWhere."&sortType=".$sortType."&fileType=".$fileType;
    }

    // 브랜드 그룹 엑셀 다운로드
    function brandGroupExcel() {
        $fileType = $this->input->get("fileType");
        $searchType = $this->input->get("searchType");
        $searchText = $this->input->get("searchText");
        $sortWhere = $this->input->get("sortWhere");
        $sortType = $this->input->get("sortType");

        $brandGroupList = $this->member_model->getBrandGroupAllList($searchType, $searchText, $sortWhere, $sortType);
        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('워크시트');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '브랜드계정명');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', '사용자수');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', '회사계정명');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', '등록일');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', '수정일');

        if(count($brandGroupList) > 0) {
            foreach ($brandGroupList as $key => $item) {
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . (($key + 1) + 1), $item['brand_group_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . (($key + 1) + 1), $item['pCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . (($key + 1) + 1), $item['org_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . (($key + 1) + 1), $item['createdate'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . (($key + 1) + 1), $item['updatedate'], PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }

        if($fileType == 'pdf') {
            $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];
            $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];
            $mpdf = new \Mpdf\Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/custom/font/directory',
                ]),
                'fontdata' => $fontData + [
                        'frutiger' => [
                            'R' => 'UnBatang_0613.ttf',
                        ]
                    ],
                'default_font' => 'frutiger'
            ]);
            $mpdf->useAdobeCJK = true;

            $html = '<table style="width:100%; table-layout: fixed; border-top: 1px solid #000; border-bottom: 1px solid #000; border-spacing: 0; border-collapse: collapse; word-break: break-all; display: table; border-color: grey;">
                <caption style="font-size: 25px;">브랜드계정</caption>
                <colgroup style="display: table-column-group;">
                    <col style="width:20%">
                    <col style="width:10%">
                    <col style="width:10%">
                    <col style="width:20%">
                    <col style="width:20%">
                    <col style="width:20%">
                </colgroup>
                <thead style="border-top: 1px solid #000;">
                <tr style="display: table-row; vertical-align:inherit; border-color: inherit;">
                    <th scope="row" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">브랜드계정명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">참여자수</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">회사명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">등록일</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">수정일</th>
                </tr>
                </thead>
                <tbody>';
            if(count($brandGroupList) > 0) {
                foreach ($brandGroupList as $key => $item) {
                    $html .= '<tr style="background: #ececec;">
                        <td style="width:10%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['brand_group_name'].'</td>
                        <td style="width:15%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['pCount'].'</td>
                        <td style="width:15%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['org_name'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['createdate'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['updatedate'].'</td>
                    </tr>';

                }
            }
            $html .= '</tbody>
            </table>';

            $mpdf->WriteHTML($html);
            $mpdf->Output(iconv('utf-8', 'euc-kr', '브랜드계정.pdf'), 'D');
        } else if($fileType == 'csv') {
            $filename = '브랜드계정.csv'; // 엑셀 파일 이름
            header('Content-Type: application/pdf'); //mime 타입
            header('Content-Disposition: attachment;filename="' . iconv('utf-8', 'euc-kr', $filename) . '"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        } else {
            $filename = '브랜드계정.xls'; // 엑셀 파일 이름
            header('Content-Type: application/vnd.ms-excel'); //mime 타입
            header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        }

        $objWriter->save('php://output');
    }

    // 브랜드 그룹 상세보기 엑셀 다운로드
    function brandGroupPeopleExcelAjax() {
        $brand_group_cd = $this->input->post("brand_group_cd");
        $fileType = $this->input->post("fileType");
        $authType = $this->input->post("authType");
        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");

        echo HOME_DIR."/member/brandGroupPeopleExcel/?brand_group_cd=".$brand_group_cd."&searchText=".$searchText."&searchType=".$searchType."&sortWhere=".$sortWhere."&sortType=".$sortType."&authType=".$authType."&fileType=".$fileType;
    }

    // 브랜드 그룹 상세보기 엑셀 다운로드
    function brandGroupPeopleExcel() {
        $brand_group_cd = $this->input->get("brand_group_cd");
        $fileType = $this->input->get("fileType");
        $authType = $this->input->get("authType");
        $searchType = $this->input->get("searchType");
        $searchText = $this->input->get("searchText");
        $sortWhere = $this->input->get("sortWhere");
        $sortType = $this->input->get("sortType");

        $brandGroup = $this->member_model->getBrandGroup($brand_group_cd);
        $brandGroupPeopleList = $this->member_model->getAllBrandGroupPeopleList($brand_group_cd, $authType, $searchType, $searchText, $sortWhere, $sortType);

        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('워크시트');

        $objPHPExcel->getActiveSheet()->setCellValue('A1', '회사계정');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', '브랜드계정');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', '성명');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', '이메일');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', '모바일');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', '연결 브랜드계정');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', '회사명');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', '부서명');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', '팀명');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', '담당영업소');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', '권한');
        $objPHPExcel->getActiveSheet()->setCellValue('L1', '계정상태');
        $objPHPExcel->getActiveSheet()->setCellValue('M1', '가입 승인 담당자');
        

        if(count($brandGroupPeopleList) > 0) {
            foreach ($brandGroupPeopleList as $key => $item) {
                $status = "";

                if($item['emailConfirm'] == 'true') {
                    $status = '활성화';
                } else if($item['emailConfirm'] == 'add') {
                    $status = '계정등록';
                } else if($item['emailConfirm'] == 'mail') {
                    $status = '비활성화';
                } else if($item['emailConfirm'] == 'false') {
                    $status = '가입요청';
                }

                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . (($key + 1) + 1), $brandGroup['org_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . (($key + 1) + 1), $brandGroup['brand_group_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . (($key + 1) + 1), $item['member_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . (($key + 1) + 1), $item['member_email'], PHPExcel_Cell_DataType::TYPE_STRING);
                if($item['member_hp'] != null && $item['member_hp'] != '') {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . (($key + 1) + 1), $item['member_hp'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . (($key + 1) + 1), '-', PHPExcel_Cell_DataType::TYPE_STRING);
                }
                if($item['brand_names'] != null && $item['brand_names'] != '') {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . (($key + 1) + 1), $item['brand_names'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . (($key + 1) + 1), '-', PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . (($key + 1) + 1), $item['member_org'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . (($key + 1) + 1), $item['member_dep'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . (($key + 1) + 1), $item['member_team'], PHPExcel_Cell_DataType::TYPE_STRING);
                if($item['office_name'] != null && $item['office_name'] != '') {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . (($key + 1) + 1), $item['office_name'], PHPExcel_Cell_DataType::TYPE_STRING);
                } else {
                	$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . (($key + 1) + 1), '-', PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . (($key + 1) + 1), strtoupper($item['authority']), PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . (($key + 1) + 1), $status, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . (($key + 1) + 1), $item['appr_name'].' '.strtoupper($item['appr_authority']), PHPExcel_Cell_DataType::TYPE_STRING);

            }
        }

        if($fileType == 'pdf') {
            $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $mpdf = new \Mpdf\Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/custom/font/directory',
                ]),
                'fontdata' => $fontData + [
                        'frutiger' => [
                            'R' => 'UnBatang_0613.ttf',
                        ]
                    ],
                'default_font' => 'frutiger'
            ]);
            $mpdf->useAdobeCJK = true;

            $html = '<table style="width:100%; table-layout: fixed; border-top: 1px solid #000; border-bottom: 1px solid #000; border-spacing: 0; border-collapse: collapse; word-break: break-all; display: table; border-color: grey;">
                <caption style="font-size: 25px;">회원_전체사용자</caption>
                <colgroup style="display: table-column-group;">
                    <col style="width:20%">
                    <col style="width:10%">
                    <col style="width:10%">
                    <col style="width:20%">
                    <col style="width:20%">
                    <col style="width:20%">
                </colgroup>
                <thead style="border-top: 1px solid #000;">
                <tr style="display: table-row; vertical-align:inherit; border-color: inherit;">
                <th scope="row" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">회사계정</th>
                <th scope="row" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">브랜드계정</th>
                    <th scope="row" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">성명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">이메일</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">연결브랜드계정</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">회사명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">부서명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">팀명</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">모바일</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">계정상태</th>
                    <th scope="col" style="padding: 10px 5px; border: 1px solid #ccc; border-top: none; font-size: 13px; text-align: center; line-height: 120%;">등급</th>
                </tr>
                </thead>
                <tbody>';
            if(count($brandGroupPeopleList) > 0) {
                foreach ($brandGroupPeopleList as $key => $item) {

                    $status = "";

                    if($item['emailConfirm'] == 'true') {
                        $status = '활성화';
                    } else if($item['emailConfirm'] == 'add') {
                        $status = '계정등록';
                    } else if($item['emailConfirm'] == 'mail') {
                        $status = '비활성화';
                    } else if($item['emailConfirm'] == 'false') {
                        $status = '가입요청';
                    }

                    $html .= '<tr style="background: #ececec;">
						<td style="width:10%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$brandGroup['org_name'].'</td>
						<td style="width:10%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$brandGroup['brand_group_name'].'</td>
                        <td style="width:10%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_name'].'</td>
                        <td style="width:15%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_email'].'</td>
                        <td style="width:15%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$brandGroup['brand_group_name'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_org'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_dep'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_team'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['member_hp'].'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$status.'</td>
                        <td style="width:20%; border-right: 1px solid #ccc; border-left: 1px solid #ccc; padding: 7px 5px; font-size: 13px; text-align: center; line-height: 120%;">'.$item['authority'].'</td>
                    </tr>';

                }
            }
            $html .= '</tbody>
            </table>';

            $mpdf->WriteHTML($html);
            $mpdf->Output(iconv('utf-8', 'euc-kr', $brandGroup['brand_group_name'].'사용자 리스트.pdf'), 'D');
        } else if($fileType == 'csv') {
            $filename = $brandGroup['brand_group_name'].'사용자 리스트.csv'; // 엑셀 파일 이름
            header('Content-Type: application/pdf'); //mime 타입
            header('Content-Disposition: attachment;filename="' . iconv('utf-8', 'euc-kr', $filename) . '"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        } else {
            $filename = $brandGroup['brand_group_name'].'사용자 리스트.xls'; // 엑셀 파일 이름
            header('Content-Type: application/vnd.ms-excel'); //mime 타입
            header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        }

        $objWriter->save('php://output');
    }

	// 사용자 정보 수정
    function memberEdit() {
        $member_id          = $this->session->userdata('member_id');
        $member_cd          = $this->input->post('member_cd');
        $member_name        = $this->input->post('member_name');
        $member_hp          = $this->input->post('member_hp');
        $member_org         = $this->input->post('member_org');
        $member_dep         = $this->input->post('member_dep');
        $member_team        = $this->input->post('member_team');
        $authority          = $this->input->post('authority');
        $office_name        = $this->input->post('office_name');
        $brand_group_cds    = $this->input->post('brand_group_cds');

        $row = $this->member_model->getMemberDetail($member_cd);
        
        if($authority != $row['authority']) {
	        $dataArr = array(
	        		'log_type' => 'auth',
	        		'log_content' => ' 님이 <strong>'.strtoupper($row['authority']).' '.$member_name.'</strong>님을 <span class="font-color-deep-red">'.strtoupper($authority).'권한으로 변경</span> 하였습니다.',
	        		'log_from'=> $row['member_id'],
	        		'log_to' => $member_id,
	        		'log_ip' => $_SERVER['REMOTE_ADDR'],
	        		'search_auth'     =>  $authority
	        );
	        
	        $res = $this->common_model->getSequences("log");
	        $dataArr['log_cd'] = str_pad($res['currval'], 20, "LOG_0000000000000000", STR_PAD_LEFT);
	        $this->common_model->logInsert($dataArr);
        }

        $params = array(
            'member_name'       => $member_name,
            'member_org'        => $member_org,
            'member_dep'        => $member_dep,
            'member_team'       => $member_team,
            'member_hp'         => $member_hp,
            'authority'         => $authority,
        	'office_name'		=> $office_name
        );


        $saveData = $this->member_model->updateMemberData($member_cd, $params);

        $authority_user = $this->session->userdata('authority');
        if($authority_user != 'pm') {
	        $current_brand_group_cdsArr = $this->member_model->currentMemberGroups($member_cd);
	        
	        $brand_group_cdsArr = explode(";", $brand_group_cds);
	        
	        foreach ($current_brand_group_cdsArr as $key => $item) {
	        
	        	$flag = true;
	        	
	        	foreach ($brand_group_cdsArr as $brand_group_cd) {
	        		
	        		if($brand_group_cd == $item['brand_group_cd']) {
	        			$flag = false;
	        			break;
	        		}
	        	}
	        	
	        	if($flag){
	        		$this->member_model->getBrandGroupMemberDel($item['brand_group_cd'],$member_cd);
	        		
	        		$dataArr2 = Array (
	        				'updatedate'        =>  date("Y-m-d H:i:s",strtotime("+9 hours"))
	        				);
	        		$this->db->where('brand_group_cd', $item['brand_group_cd']);
	        		$updateData = $this->db->update('brand_group',$dataArr2);
	        	}
	        }
	
	        foreach ($brand_group_cdsArr as $brand_group_cd):
		        if($brand_group_cd != null && $brand_group_cd != "") {
		        	$memberGroupCount = $this->member_model->getCheckMemberGroup($member_cd,$brand_group_cd);
		        	
		        	if($memberGroupCount == 0) {
			            $dataArr = array(
			                'brand_group_cd' => $brand_group_cd,
			                'member_cd' => $member_cd,
			                'creator' => $member_id,
			                'updater' => $member_id
			            );
			
			            if($brand_group_cd != null && $brand_group_cd != '' && $member_cd != null && $member_cd != '') {
			                $res = $this->common_model->getSequences("bgm");
			                $dataArr['brand_group_member_cd'] = str_pad($res['currval'], 20, "BGM_0000000000000000", STR_PAD_LEFT);
			                $this->db->insert('brand_group_member', $dataArr);
			            }
			            
			            $dataArr2 = Array (
			            		'updatedate'        =>  date("Y-m-d H:i:s",strtotime("+9 hours"))
			            		);
			            $this->db->where('brand_group_cd', $brand_group_cd);
			            $updateData = $this->db->update('brand_group',$dataArr2);
		        	}
		        }
	        endforeach;
        }

        echo json_encode($saveData);
    }
    
    function download_file() {
    	$filename   = $this->input->get_post('samplefile');
    	
    	$data = file_get_contents(FCPATH . "uploads/sample/".$filename); // Read the file's contents
    	
    	force_download($filename, $data);
    }
}