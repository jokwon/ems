<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Notice
 *
 * @property M_board M_board
 */
class Settings extends CI_Controller{
    private $start = 0;
    private $scale = 8;
    private $page_scale = 10;

    private $base_uri = '';

    private $board = [];
    private $board_id = 'notice';

    private $head = array(
        'gpage' => '0601'
    );

    private $footer = array(
        'js' => array()
    );

    private $data = array();

    private $return_json = false;
    private $return_array = array(
        'status' => '500'
    ,'msg' => '잘못된 접근입니다.'
    ,'url' => ''
    ,'reload' => 'off'
    ,'arr' => array()
    );


    function __construct() {
        parent::__construct();

        $this->load->model(array('member_model', 'settings_model','common_model','main_model'));

        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);

        $this->base_uri = '/'.strtolower(__CLASS__);
        $this->data['base_uri'] = $this->base_uri;

        $row = $this->settings_model->getSettings('');
        $this->head['customer_name'] = $row['customer_name'];
        $this->head['symposium_left'] = $row['symposium_left'];

        $this->data['application_img']  = $row['application_img'];
        $this->data['dashboard_num1']   = $row['dashboard_num1'];
        $this->data['term_desc']        = $row['term_desc'];
        $this->data['send_way']         = $row['send_way'];

        $this->head['application_img']  = $row['application_img'];
        $this->head['filename'] = $row['filename'];
        $this->head['fileext'] = $row['fileext'];

        $this->head['dashboard_num1']   = $row['dashboard_num1'];

        $this->head['btn4Yn']   = $row['btn4Yn'];
        $this->head['btn5Yn']   = $row['btn5Yn'];

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];

        $this->data['authority']  = $this->session->userdata('authority');
        
        $authority_now = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        $authority_db = $this->main_model->getAuthority($member_cd);
        if($authority_now != null && $authority_now != '')
        {
        	if($authority_now != $authority_db['authority'])
        	{
        		alert("권한이 변경되었습니다 다시 로그인 해주세요",HOME_DIR."/login/logout");
        	}
        	
        }
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {
    	$this->setting_info();
    }

    // 사이트정보
    function setting_info() {
        $this->head['left'] = "setting";

        $row = $this->settings_model->getSettings('');

        $this->data['row'] = $row;

        $this->load->view('head',$this->head);
        $this->load->view('option/setting_info',$this->data);
        $this->load->view('footer', $this->footer);
    }

    //사이트 기능허용
    function setting_function() {
        $this->head['left'] = "setting";

        $row = $this->settings_model->getSettings('');

        $this->data['row'] = $row;

        $this->load->view('head',$this->head);
        $this->load->view('option/setting_function',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 사이트 세팅 - 사이트정보,기능허용
    function settingUpdate() {
        $sympo_cd       = $this->input->post('sympo_cd');

        $setting = $this->input->post('setting');
        $member_id = $this->session->userdata('member_id');
        
        $dataArr = array(
        		'updater'           =>  $member_id,
        		'updatedate'        =>  'now()'
        );
        
        // 사이트정보
        if($setting == 1)
        {
        	$customer_name = $this->input->post('customer_name');
        	$symposium_desc = $this->input->post('symposium_desc');
        	$symposium_left = $this->input->post('symposium_left');
        	$dashboard_num1 = $this->input->post('dashboard_num1');
        	$term_desc = $this->input->post('term_desc');
        	$private_desc = $this->input->post('private_desc');
        	
        	$dataArr['customer_name'] = $customer_name;
        	$dataArr['symposium_desc'] = $symposium_desc;
        	$dataArr['symposium_left'] = $symposium_left;
        	$dataArr['dashboard_num1'] = $dashboard_num1;
        	$dataArr['term_desc'] = $term_desc;
        	$dataArr['private_desc'] = $private_desc;
        	
        // 기능허용
        } else if($setting == 3) {
        	$btn1Yn = $this->input->post('btn1Yn');
        	$btn3Yn = $this->input->post('btn3Yn');
        	
       		$dataArr['btn1Yn'] = $btn1Yn;
       		$dataArr['btn3Yn'] = $btn3Yn;
        }
        
        if($sympo_cd != null && $sympo_cd != '') {
            $row = $this->settings_model->getSympoSettings($sympo_cd);
            if($row != null && $row['sympo_cd'] != null && $row['sympo_cd'] != '' && $row['sympo_cd'] != 'system') {
                $this->settings_model->update($sympo_cd, $dataArr);
            } else {
                $dataArr['creator'] = $member_id;
                $dataArr['createdate'] = 'now()';
                $dataArr['sympo_cd'] = $sympo_cd;

                $res = $this->common_model->getSequences("OPT");
                $dataArr['setting_cd'] = str_pad($res['currval'], 20, "OPT_0000000000000000", STR_PAD_LEFT);
                $this->settings_model->insert($dataArr);
            }
        } else {
            $sympo_cd = 'system';
            $this->settings_model->update($sympo_cd, $dataArr);
        }
    }
    
    // 심포지엄 설정관리
    function settingSympoUpdate() {
    	$sympo_cd = $this->input->post('sympo_cd');
    	$btn1Yn = $this->input->post('btn1Yn');
    	$btn3Yn = $this->input->post('btn3Yn');
    	$member_id = $this->session->userdata('member_id');
    	
    	$dataArr = array(
    			'updater'        		=>  $member_id,
    			'updatedate'        	=>  'now()',
    			'btn1Yn'        		=>  $btn1Yn,
    			'btn3Yn'        		=>  $btn3Yn,
    		);
    	
    	$this->settings_model->update($sympo_cd, $dataArr);
    	
    }

    // 심포지엄 신청서관리-사전등록페이지
    function template_upload_exec() {
        $sympo_cd = $this->input->post('sympo_cd');
        $fileDel = $this->input->post("fileDel");
        $fileDelCF1 = $this->input->post('fileDelCF1');
        $fileDelCF2 = $this->input->post('fileDelCF2');
        $agency_info = $this->input->post('agency_info');
        $symposium_info = $this->input->post('symposium_info');
        $template_onoff = $this->input->post('template_onoff');
        $content_onoff1 = $this->input->post('content_onoff1');
        $content_subject1 = $this->input->post('content_subject1');
        $content_onoff2 = $this->input->post('content_onoff2');
        $content_subject2 = $this->input->post('content_subject2');
        
       	$list1 = array(
       			'fileDel' => $this->input->post("fileDel1"),
       			'sub_num' => $this->input->post("PreRegiSelect1"),
       			'setting_template_cd' => $this->input->post("temp_cd1"),
       			'onoff' => $this->input->post("onoff1"),
       	);
       	
       	$list2 = array(
       			'fileDel' => $this->input->post("fileDel2"),
       			'sub_num' => $this->input->post("PreRegiSelect2"),
       			'setting_template_cd' => $this->input->post("temp_cd2"),
       			'onoff' => $this->input->post("onoff2"),
       	);
       	
       	$list3 = array(
       			'fileDel' => $this->input->post("fileDel3"),
       			'sub_num' => $this->input->post("PreRegiSelect3"),
       			'setting_template_cd' => $this->input->post("temp_cd3"),
       			'onoff' => $this->input->post("onoff3"),
       	);
       	
       	$list4 = array(
       			'fileDel' => $this->input->post("fileDel4"),
       			'sub_num' => $this->input->post("PreRegiSelect4"),
       			'setting_template_cd' => $this->input->post("temp_cd4"),
       			'onoff' => $this->input->post("onoff4"),
       	);

        $allList = array();
        array_push($allList,$list1);
        array_push($allList,$list2);
        array_push($allList,$list3);
        array_push($allList,$list4);

        $member_id = $this->session->userdata('member_id');

        $saveData="ok";
        
        //사전등록 여러페이지
       	for($i=1;$i<5;$i++)
       	{	
			$filename       = "";
       		$filename_ext   = "";
       		
       		if($_FILES["file".$i]["name"]) {
       			$resultArr = file_upload('file'.$i, array('upload_path' => HOME_DIR.'/uploads/settings'));
       			if($resultArr["status"] != "ok") {
       				alert("파일업로드에 장애가 발생했습니다.");
       			} else {
       				$filename = $resultArr["files"]["raw_name"];
       				$filename_ext = $resultArr["files"]["file_ext"];
       				
       				if($filename_ext != '.jpg' && $filename_ext != ".png" && $filename_ext != ".jpeg" && $filename_ext != '.JPG' && $filename_ext != ".PNG" && $filename_ext != ".JPEG") {
       					alert("이미지 파일만 업로드가능합니다.");
       				}
       			}
       		}
       		
       		$val = HOME_DIR.'/uploads/settings/'.$filename.$filename_ext;
       		
       		$dataArr = array(
       				'sub_num'           	=>  $allList[$i-1]['sub_num'],
       				'setting_template_cd'   =>  $allList[$i-1]['setting_template_cd'],
       				'onoff'        			=>  $allList[$i-1]['onoff'],
       				'updater'           	=>  $member_id,
       				'updatedate'        	=>  'now()'
       		);
       		
       		if($allList[$i-1]['fileDel'] == 'Y') {
       			$dataArr['application_img'] = '';
       			$dataArr['filename'] = '';
       			$dataArr['fileext'] = '';
       		}
       		
       		if($filename != '') {
       			$dataArr['application_img'] =  $val;
       			$dataArr['filename'] = $filename;
       			$dataArr['fileext'] = $filename_ext;
       		}
       		
       		$saveData = $this->settings_model->updateTemplate($sympo_cd, $dataArr,$allList[$i-1]['setting_template_cd']);
       	
		}
		//사전등록 여러페이지 끝
		
		$dataArr2 = array(
				'agency_info' 		=>  $agency_info,
				'symposium_info'  	=>  $symposium_info,
				'template_onoff'  	=>  $template_onoff,
				'content_onoff1'  	=>  $content_onoff1,
				'content_subject1'  =>  $content_subject1,
				'content_onoff2'  	=>  $content_onoff2,
				'content_subject2'  =>  $content_subject2,
				'updater'           =>  $member_id,
				'updatedate'        =>  'now()'
		);

		//페이지 상단 배경 이미지
       	$filename       = "";
       	$filename_ext   = "";
       
		if($_FILES["file"]["name"]) {
			$resultArr = file_upload('file', array('upload_path' => HOME_DIR.'/uploads/settings'));
	       	
	       	if($resultArr["status"] != "ok") {
	       		alert("파일업로드에 장애가 발생했습니다.");
	       	} else {
	       		$filename = $resultArr["files"]["raw_name"];
	       		$filename_ext = $resultArr["files"]["file_ext"];
	       		
	       		if($filename_ext != '.jpg' && $filename_ext != ".png" && $filename_ext != ".jpeg" && $filename_ext != '.JPG' && $filename_ext != ".PNG" && $filename_ext != ".JPEG") {
	       			alert("이미지 파일만 업로드가능합니다.");
	       		}
	       	}
		}
		
		$application_img = HOME_DIR.'/uploads/settings/'.$filename.$filename_ext;
		if($fileDel == 'Y') {
			$dataArr2['application_img'] = '';
			$dataArr2['filename'] = '';
			$dataArr2['fileext'] = '';
		}
		if($filename != '') {
			$dataArr2['application_img'] =  $application_img;
			$dataArr2['filename'] = $filename;
			$dataArr2['fileext'] = $filename_ext;
		}
		
		//컨텐츠영역1
		$content_filename1  = "";
		$content_fileext1   = "";
		
		if($_FILES["content_file1"]["name"]) {
			$resultArr = file_upload('content_file1', array('upload_path' => HOME_DIR.'/uploads/settings'));
			
			if($resultArr["status"] != "ok") {
				alert("파일업로드에 장애가 발생했습니다.");
			} else {
				$content_filename1 = $resultArr["files"]["raw_name"];
				$content_fileext1 = $resultArr["files"]["file_ext"];
				
				if($content_fileext1 != '.jpg' && $content_fileext1 != ".png" && $content_fileext1 != ".jpeg" && $content_fileext1 != '.JPG' && $content_fileext1 != ".PNG" && $content_fileext1 != ".JPEG") {
					alert("이미지 파일만 업로드가능합니다.");
				}
			}
		}
		
		$content_img1 = HOME_DIR.'/uploads/settings/'.$content_filename1.$content_fileext1;
		
		if($fileDelCF1 == 'Y') {
			$dataArr2['content_img1'] = '';
			$dataArr2['content_filename1'] = '';
			$dataArr2['content_fileext1'] = '';
		}
		
		if($content_filename1 != '') {
			$dataArr2['content_img1'] =  $content_img1;
			$dataArr2['content_filename1'] = $content_filename1;
			$dataArr2['content_fileext1'] = $content_fileext1;
		}
		
		//컨텐츠영역2
		$content_filename2	= "";
		$content_fileext2   = "";
		
		if($_FILES["content_file2"]["name"]) {
			$resultArr = file_upload('content_file2', array('upload_path' => HOME_DIR.'/uploads/settings'));
			
			if($resultArr["status"] != "ok") {
				alert("파일업로드에 장애가 발생했습니다.");
			} else {
				$content_filename2 = $resultArr["files"]["raw_name"];
				$content_fileext2 = $resultArr["files"]["file_ext"];
				
				if($content_fileext2 != '.jpg' && $content_fileext2 != ".png" && $content_fileext2 != ".jpeg" && $content_fileext2 != '.JPG' && $content_fileext2 != ".PNG" && $content_fileext2 != ".JPEG") {
					alert("이미지 파일만 업로드가능합니다.");
				}
			}
		}
		
		$content_img2 = HOME_DIR.'/uploads/settings/'.$content_filename2.$content_fileext2;
		
        if($fileDelCF2 == 'Y') {
        	$dataArr2['content_img2'] = '';
        	$dataArr2['content_filename2'] = '';
        	$dataArr2['content_fileext2'] = '';
        }
        
        if($content_filename2 != '') {
        	$dataArr2['content_img2'] =  $content_img2;
        	$dataArr2['content_filename2'] = $content_filename2;
        	$dataArr2['content_fileext2'] = $content_fileext2;
        }
        
        //update구문
        
        $saveData2 = "ok";
        
        if($sympo_cd == null || $sympo_cd == '') {
            $sympo_cd = 'system';
        }
        
        $saveData2 = $this->settings_model->update($sympo_cd, $dataArr2);

        if($saveData == "ok" && $saveData2 == "ok"){
            alert("저장되었습니다.",HOME_DIR."/symposium/sympo_application/$sympo_cd");
        } else {
            alert("장애가 발생했습니다.",HOME_DIR."/symposium/sympo_application/$sympo_cd");
        }
    }
    
    // 심포지엄 신청서관리 - 개인정보 활용동의
    function template_upload_private() {
    	$sympo_cd = $this->input->post('sympo_cd');
    	$fileDel = $this->input->post("fileDel");
    	$private_desc = $this->input->post('private_desc');
    	$private_collection = $this->input->post('private_collection');
    	$private_collection_nc = $this->input->post('private_collection_nc');
    	$private_collection_se = $this->input->post('private_collection_se');
    	$private_item_nc = $this->input->post('private_item_nc');
    	$private_item_se = $this->input->post('private_item_se');
    	$private_period = $this->input->post('private_period');
    	$private_disadvantage = $this->input->post('private_disadvantage');
    	
    	$member_id = $this->session->userdata('member_id');
    	
    	$dataArr = array(
    			'private_desc' 			=>  $private_desc,
    			'private_collection'  	=>  $private_collection,
    			'private_collection_nc' =>  $private_collection_nc,
    			'private_collection_se' =>  $private_collection_se,
    			'private_item_nc'  		=>  $private_item_nc,
    			'private_item_se'  		=>  $private_item_se,
    			'private_period'  		=>  $private_period,
    			'private_disadvantage'  =>  $private_disadvantage,
    			'updater'           	=>  $member_id,
    			'updatedate'        	=>  'now()'
    	);
    	
    	//페이지 상단 배경 이미지
    	$private_filename       = "";
    	$private_fileext   = "";
    	
    	if($_FILES["file"]["name"]) {
    		$resultArr = file_upload('file', array('upload_path' => HOME_DIR.'/uploads/settings'));
    		
    		if($resultArr["status"] != "ok") {
    			alert("파일업로드에 장애가 발생했습니다.");
    		} else {
    			$private_filename = $resultArr["files"]["raw_name"];
    			$private_fileext = $resultArr["files"]["file_ext"];
    			
    			if($private_fileext != '.jpg' && $private_fileext != ".png" && $private_fileext != ".jpeg" && $private_fileext != '.JPG' && $private_fileext != ".PNG" && $private_fileext != ".JPEG") {
    				alert("이미지 파일만 업로드가능합니다.");
    			}
    		}
    	}
    	
    	$private_img = HOME_DIR.'/uploads/settings/'.$private_filename.$private_fileext;
    	
    	if($fileDel == 'Y') {
    		$dataArr['private_img'] = '';
    		$dataArr['private_filename'] = '';
    		$dataArr['fileext'] = '';
    	}
    	if($private_filename != '') {
    		$dataArr['private_img'] =  $private_img;
    		$dataArr['private_filename'] = $private_filename;
    		$dataArr['private_fileext'] = $private_fileext;
    	}
    	    	
    	//update구문
    	
    	$saveData = "ok";
    	
    	if($sympo_cd == null || $sympo_cd == '') {
    		$sympo_cd = 'system';
    	}
    	
    	$saveData = $this->settings_model->update($sympo_cd, $dataArr);
    	
    	if($saveData == "ok"){
    		alert("저장되었습니다.",HOME_DIR."/symposium/sympo_personalInfo/$sympo_cd");
    	} else {
    		alert("장애가 발생했습니다.",HOME_DIR."/symposium/sympo_personalInfo/$sympo_cd");
    	}
    }

    function settingImgView() {

        $row = $this->settings_model->getSettingImg();


        echo json_encode($row);
    }
}