<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Main
 *
 * @property Main
*/
class Main extends CI_Controller {

    private $head = array();

    private $data = array();

    private $footer = array();

    private $return_json = false;
    private $return_array = array(
         'status' => '500'
        ,'msg' => '잘못된 접근입니다.'
        ,'url' => ''
        ,'reload' => 'off'
        ,'arr' => array()
    );

    private $start = 0;
    private $scale = 4;

    private $total_sch_value = '';

    function __construct() {
        parent::__construct();

        $this->load->model(array('settings_model', 'main_model','notice_model','symposium_model'));
        $this->head['left'] = "dashboard";
        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);
        $this->total_sch_value = htmlspecialchars($this->input->post('total_sch_value', true));

        $row = $this->settings_model->getSettings('');
        $this->head['customer_name'] = $row['customer_name'];
        $this->head['dashboard_num1'] = $row['dashboard_num1'];
        $this->head['symposium_left'] = $row['symposium_left'];
        $this->data['dashboard_num1'] = $row['dashboard_num1'];

        $this->data['authority']  =$authotiry = $this->session->userdata('authority');

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];
        
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {
        $this->	main();
    }

    function main() {
        //공지사항 최근 2개
        $notice_list = $this->notice_model->getLimitNoticeList();
        $this->data['notice_list'] = $notice_list;

        //심포지엄 최근 5개
        $sympo_list = $this->symposium_model->getMainSympoList('');
        $this->data['sympo_list'] = $sympo_list;

        $report = $this->main_model->getReport('');
        $this->data['report'] = $report;


        $setting = $this->settings_model->getSettings('');
        $this->data['setting'] = $setting;
        

        $this->load->view('head',$this->head);
        $this->load->view('main/main',$this->data);
        $this->load->view('footer', $this->footer);

    }
}


?>