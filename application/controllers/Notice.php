<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Notice
 *
 * @property M_board M_board
 */
class Notice extends CI_Controller{
    private $start = 0;
    private $scale = 10;
    private $page_scale = 10;

    private $base_uri = '';

    private $head = array(
        'gpage' => '0601'
    );

    private $footer = array(
        'js' => array()
    );

    private $data = array();

    private $return_json = false;
    private $return_array = array(
        'status' => '500'
        ,'msg' => '잘못된 접근입니다.'
        ,'url' => ''
        ,'reload' => 'off'
        ,'arr' => array()
    );


    function __construct() {
        parent::__construct();
        $this->head['left'] = "notice";

        $this->load->model(array('settings_model', 'notice_model','common_model', 'member_model','main_model'));

        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);


        $this->base_uri = '/'.strtolower(__CLASS__);
        $this->data['start'] = $this->start;
        $this->data['scale'] = $this->scale;

        $this->head['base_uri'] = $this->base_uri;
        $this->data['base_uri'] = $this->base_uri;


        $row = $this->settings_model->getSettings('');
        $this->head['customer_name'] = $row['customer_name'];
        $this->head['symposium_left'] = $row['symposium_left'];

        $this->data['authority']  =$authotiry = $this->session->userdata('authority');

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];
        
        $authority_now = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        $authority_db = $this->main_model->getAuthority($member_cd);
        if($authority_now != null && $authority_now != '')
        {
        	if($authority_now != $authority_db['authority'])
        	{
        		alert("권한이 변경되었습니다 다시 로그인 해주세요",HOME_DIR."/login/logout");
        	}
        	
        }
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {
        $this->notice_list();
    }

    function notice_list(){

        $searchType = $this->input->post('searchType');
        $searchText = $this->input->post('searchText');
        $sortWhere = $this->input->post('sortWhere');
        $sortType = $this->input->post('sortType');
        
        if($sortWhere == null || $sortWhere == '') {
        	$sortWhere = 'updatedate';
        }
        
        if($sortType == null || $sortType == '') {
        	$sortType = 'desc';
        }

        $getResult = $this->notice_model->getnoticeList(1, $searchType, $searchText, $sortWhere, $sortType);
        $getCntResult = $this->notice_model->getNoticeCount($searchType, $searchText, $sortWhere, $sortType);

        $this->data['notice_list'] = $getResult;
        $this->data['notice_count'] = $getCntResult;
        $this->data['page'] = 1;

        $this->data['searchType'] = $searchType;
        $this->data['searchText'] = $searchText;
        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;

        $this->load->view('head',$this->head);
        $this->load->view('notice/notice_list',$this->data);
        $this->load->view('footer', $this->footer);
    }

    function notice_listAjax() {

        $page           = $this->input->post("page");
        $searchType = $this->input->post('searchType');
        $searchText = $this->input->post('searchText');
        $sortWhere = $this->input->post('sortWhere');
        $sortType = $this->input->post('sortType');

        if($page == null || $page == '') {
            $page = 1;
        }
        
        if($sortWhere == null || $sortWhere == '') {
        	$sortWhere = 'updatedate';
        }
        
        if($sortType == null || $sortType == '') {
        	$sortType = 'desc';
        }

        $getResult = $this->notice_model->getnoticeList($page, $searchType, $searchText, $sortWhere, $sortType);
        $getCntResult = $this->notice_model->getNoticeCount($searchType, $searchText, $sortWhere, $sortType);

        echo json_encode(array("data"=>$getResult, "page"=>$page, "cnt"=>$getCntResult));
    }

    function deletes_exec() {

        $noticeCds = $this->input->post('noticeCds');

        $noticeCdsArr = explode(";", $noticeCds);

        foreach ($noticeCdsArr as $noticeCd):
            $this->notice_model->delete($noticeCd);
            $this->notice_model->noticeGroupDelete($noticeCd,'');
        endforeach;

        alert("삭제 하였습니다.",HOME_DIR."/notice/notice_list");
    }

    function write_exec() {

        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op' && $authority != 'pm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $member_id		= $this->session->userdata('member_id');

        $notice_cd 		= $this->input->post('notice_cd');
        $title 			= $this->input->post('notice_title');
        $content 		= $this->input->post('notice_content', false);
        $attfDel        = $this->input->post('attfDel');
        $brand_group_cds = $this->input->post("brand_group_cds");
        $filename       = "";
        $filename_ext   = "";

       
        if($_FILES["file"]["name"]) {
        	$uploadResult = all_upload('/uploads/notice','file');
            
            if($uploadResult["result"] != "ok") {
                alert("첨부파일 업로드 제한이 있습니다. 16MB 이하의 gif, jpg, jpeg, png, pdf 확장자 파일만 업로드 가능합니다.",HOME_DIR."/notice");
            } else {
                $filename = $uploadResult["upload_data"]["raw_name"];
                $filename_ext = $uploadResult["upload_data"]["file_ext"];
            }
        }

        // 수정인경우
        if($notice_cd != "") {
			//공지사항 업데이트
            $notice = $this->notice_model->getNoticeDetail($notice_cd);
            if(!$notice){
                alert("존재하지 않은 게시물입니다..",HOME_DIR."/notice/notice_list");
            }

            $dataArr = Array (
                'notice_title'	    =>	$title,
                'notice_content'	=>	$content,
                'updater'           =>  $member_id,
            	'updatedate'        =>  date("Y-m-d H:i:s",strtotime("+9 hours"))
            );

            // 파일이 있거나 없을때 
            if($filename != "") {
                $dataArr["notice_filename"] = $filename;
                $dataArr["notice_file_ext"] = $filename_ext;
            } else {
                if($attfDel == 'Y') {
                    $dataArr["notice_filename"] = '';
                    $dataArr["notice_file_ext"] = '';
                }
            }
	
            // 공지사항 db update
            $this->db->where('notice_cd', $notice_cd);
            $updateData = $this->db->update('notice',$dataArr);
            
	        if($authority != 'pm') {
	            
	            // 현재 공지사항이 속해있는 브랜드 리스트
	            $current_brand_group_cdsArr = $this->notice_model->currentNoticeGroups($notice_cd);
	            
	            // 새로운 공지사항의 브랜드 리스트
	            $brand_group_cdsArr = explode(";", $brand_group_cds);
	            
	            foreach ($current_brand_group_cdsArr as $key => $item) {
	            	
	            	$flag = true;
	            	
	            	// 현재 있는 공지사항 브랜드 인지 체크
	            	foreach ($brand_group_cdsArr as $brand_group_cd) {
	            		
	            		if($brand_group_cd == $item['brand_group_cd']) {
	            			$flag = false;
	            			break;
	            		}
	            	}
	            	
	            	// 있다면 삭제
	            	if($flag){
	            		$this->notice_model->noticeGroupDelete($notice_cd,$item['brand_group_cd']);
	            	}
	            }
	            
	            foreach ($brand_group_cdsArr as $brand_group_cd):
		            if($brand_group_cd != null && $brand_group_cd != ""){
		            	$noticeGroupCount = $this->notice_model->getCheckNoticeGroup($notice_cd,$brand_group_cd);
		            	
		            	if($noticeGroupCount == 0){
		            		$dataArr1 = array(
		            				'brand_group_cd' => $brand_group_cd,
		            				'notice_cd' => $notice_cd,
		            				'creator' => $member_id,
		            				'updater' => $member_id
		            		);
		            		
		            		$res1 = $this->common_model->getSequences("bgn");
		            		$dataArr1['brand_group_notice_cd'] = str_pad($res1['currval'], 20, "BGN_0000000000000000", STR_PAD_LEFT);
		            		$this->db->insert('brand_group_notice', $dataArr1);
		            	}
		            }
	            endforeach;
            }

            if($updateData == "ok"){
                alert("수정되었습니다.",HOME_DIR."/notice/notice_list");
            } elseif($updateData == "err"){
                alert("장애가 발생했습니다.",HOME_DIR."/notice/notice_list");
            }
        } else {//신규인경우
            if($filename != null && $filename != '') {
                $dataArr = Array (
                    'notice_title'	    =>	$title,
                    'notice_content'	=>	$content,
                    'notice_filename'	=>	$filename,
                    'notice_file_ext'	=>	$filename_ext,
                    'creator'           =>  $member_id,
                    'updater'           =>  $member_id
                );
            } else {
                $dataArr = Array (
                    'notice_title'	    =>	$title,
                    'notice_content'	=>	$content,
                    'creator'           =>  $member_id,
                    'updater'           =>  $member_id
                );
            }

            $res = $this->common_model->getSequences("noe");
            $dataArr['notice_cd'] = str_pad($res['currval'], 20, "NOE_0000000000000000", STR_PAD_LEFT);
            $saveData = $this->db->insert('notice',$dataArr);
            
            $brand_group_cdsArr = explode(";", $brand_group_cds);
            
            foreach ($brand_group_cdsArr as $brand_group_cd):
	            if($brand_group_cd != null && $brand_group_cd != '') {
	            	
	            	$noticeGroupCount = $this->notice_model->getCheckNoticeGroup($notice_cd,$brand_group_cd);
	            	
	            	if($noticeGroupCount == 0){
		            	$dataArr1 = array(
		            			'brand_group_cd' => $brand_group_cd,
		            			'notice_cd' => $dataArr['notice_cd'],
		            			'creator' => $member_id,
		            			'updater' => $member_id
		            	);
		            	
		            	$res1 = $this->common_model->getSequences("bgn");
		            	$dataArr1['brand_group_notice_cd'] = str_pad($res1['currval'], 20, "BGN_0000000000000000", STR_PAD_LEFT);
		            	$this->db->insert('brand_group_notice', $dataArr1);
	            	}
	            }
            endforeach;

            if($saveData == "ok"){
                alert("등록되었습니다.",HOME_DIR."/notice/notice_list");
            } elseif($saveData == "err"){
                alert("장애가 발생했습니다.",HOME_DIR."/notice/notice_list");
            }
        }
    }

    function download_file() {
        $filename       = $this->input->get_post('notice_filename');

        $data = file_get_contents(FCPATH ."/uploads/notice/".$filename); // Read the file's contents

        force_download(iconv('utf-8', 'euc-kr',$filename), $data);
    }

}