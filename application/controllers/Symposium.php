<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Notice
 *
 * @property M_board M_board
 */
class Symposium extends CI_Controller{
    private $start = 0;
    private $scale = 8;
    private $page_scale = 10;

    private $base_uri = '';

    private $board = [];
    private $board_id = 'notice';

    private $head = array(
        'gpage' => '0601'
    );
    
    private $footer = array(
        'js' => array()
    );

    private $data = array(); 

    private $return_json = false;
    private $return_array = array(
        'status' => '500'
    ,'msg' => '잘못된 접근입니다.'
    ,'url' => ''
    ,'reload' => 'off'
    ,'arr' => array()
    );


    function __construct() {
        parent::__construct();
        $this->head['left'] = "symposium";

        $this->load->model(array('settings_model', 'symposium_model', 'common_model','main_model'));


        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);


        $this->base_uri = '/'.strtolower(__CLASS__);
        $this->data['base_uri'] = $this->base_uri;

        $row = $this->settings_model->getSettings('');
        $this->head['customer_name']    = $row['customer_name'];
        $this->head['symposium_left']   = $row['symposium_left'];

        $this->head['application_img']  = $row['application_img'];
        $this->head['filename'] = $row['filename'];
        $this->head['fileext'] = $row['fileext'];

        $this->data['dashboard_num1']   = $row['dashboard_num1'];
        $this->data['term_desc']        = $row['term_desc'];
        $this->data['private_desc']     = $row['private_desc'];

        $this->data['send_way']         = $row['send_way'];

        $this->head['application_img']  = $row['application_img'];
        $this->head['dashboard_num1']   = $row['dashboard_num1'];

        $this->data['authority']  =$authotiry = $this->session->userdata('authority');

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];

        $authority_now = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        $authority_db = $this->main_model->getAuthority($member_cd);
        if($authority_now != null && $authority_now != '')
        {
        	if($authority_now != $authority_db['authority'])
        	{
        		alert("권한이 변경되었습니다 다시 로그인 해주세요",HOME_DIR."/login/logout");
        	}
        }
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {
        $this->sympo_list();
    }

    function key() {
        $key 		= $this->uri->segment(3);
        $keyType 	= $this->uri->segment(4);
        $domain 	= $this->input->get('domain');
        
        
        $this->sympoCreate($key, $keyType, $domain);
    }
    
    function sympoDelete() {
    	$sympo_cd = $this->uri->segment(3);
    	$result_row = $this->symposium_model->sympoDelete1($sympo_cd);
    	$result_row = $this->symposium_model->sympoDelete2($sympo_cd);
    	$result_row = $this->symposium_model->sympoDelete3($sympo_cd);
    	$result_row = $this->symposium_model->sympoDelete4($sympo_cd);
    	$result_row = $this->symposium_model->sympoDelete5($sympo_cd);
    	$result_row = $this->symposium_model->sympoDelete6($sympo_cd);
    	$this->success($result_row);
    }

    function sympoCreate($key, $keyType, $domain) {
        
        if($keyType == '1' || $keyType == '2' || $keyType == '3') {
            $sympo_token = getSympo_AuthToken();

            $json = json_decode($sympo_token, true);
            if($keyType == '' || $keyType == null) {
                $keyType = '1';
            }

            if($keyType == '1') {
                $sympo_info = getSympo_info011($json['authToken'], $key);
            } else if($keyType == '2') {
                $sympo_info = getSympo_info021($json['authToken'], $key);
            } else if($keyType == '3') {
                $sympo_info = getSympo_info031($json['authToken'], urldecode($domain), $key);
            }

            $result1 = json_decode($sympo_info, true);

            print_r($sympo_info);
            echo("\r\n");
            
            print_r($result1['result']);
            echo("\r\n");
            print_r(count($result1['Data']));

            $status = "success";

            if($result1['result'] == 'SUCCESS' && count($result1['Data']) != 0) {
                $evtId = $result1['Data'][0]['event_id'];
                $result_row = $this->symposium_model->dupleCheck($evtId, $keyType);

                $dataArr = array(
                    'event_name' => $result1['Data'][0]['event_name'],
                    'event_start_date' => $result1['Data'][0]['event_start_time'],
                    'event_end_date' => $result1['Data'][0]['event_end_time'],
                    'api_type' => $keyType,
                    'creator' => 'system',
                    'updater' => 'system'
                );

                if ($result1['Data'][0]['event_PreRegStartDateTime'] == null) {
                    $dataArr['event_PreRegStartDateTime'] = "0000-00-00 00:00:00";
                } else {
                    $dataArr['event_PreRegStartDateTime'] = $result1['Data'][0]['event_PreRegStartDateTime'];
                }
                if ($result1['Data'][0]['event_PreRegEndDateTime'] == null) {
                    $dataArr['event_PreRegEndDateTime'] = "0000-00-00 00:00:00";
                } else {
                    $dataArr['event_PreRegEndDateTime'] = $result1['Data'][0]['event_PreRegEndDateTime'];
                }


                if (isset($result1['Data'][0]['site_domain'])) {
                    $dataArr['domain'] = $result1['Data'][0]['site_domain'];
                }

                if (isset($result1['Data'][0]['event_type'])) {
                    if ($result1['Data'][0]['event_type'] == 'bclive' || $result1['Data'][0]['event_type'] == 'Live' || $result1['Data'][0]['event_type'] == 'live') {
                        $dataArr['sympo_type'] = 'LIVE';
                    } else if ($result1['Data'][0]['event_type'] == 'vod' || $result1['Data'][0]['event_type'] == 'bcarchive') {
                        $dataArr['sympo_type'] = 'ON DEMAND';
                    } else {
                        $dataArr['sympo_type'] = $result1['Data'][0]['event_type'];
                    }
                } else {
                    $dataArr['sympo_type'] = 'LIVE';
                }


                if (isset($result1['Data'][0]['path'])) {
                    $dataArr['path'] = $result1['Data'][0]['path'];
                }

                if ($result_row['count'] == 0) {
                    $res = $this->common_model->getSequences("syi");
                    $dataArr['event_id'] = $result1['Data'][0]['event_id'];
                    $dataArr['sympo_cd'] = str_pad($res['currval'], 20, "SYI_0000000000000000", STR_PAD_LEFT);
                    $saveData = $this->symposium_model->insert($dataArr);

                    if (isset($result1['Data'][0]['site_id'])) {
                        $dataArr['site_id'] = $result1['Data'][0]['site_id'];
                    } else {
                        $dataArr['site_id'] = '';
                    }

                    if (isset($result1['Data'][0]['site_name'])) {
                        $dataArr['site_name'] = $result1['Data'][0]['site_name'];
                    } else {
                        $dataArr['site_name'] = '';
                    }

                    if (isset($result1['Data'][0]['group_id'])) {
                        $dataArr['group_id'] = $result1['Data'][0]['group_id'];
                    } else {
                        $dataArr['group_id'] = '';
                    }

                    if (isset($result1['Data'][0]['group_name'])) {
                        $dataArr['group_name'] = $result1['Data'][0]['group_name'];
                    } else {
                        $dataArr['group_name'] = '';
                    }

                    //api 로 넘어온 심포지엄의 회사와 브랜드를 만든다.
                    //브랜드를 조회한다.
                    if ($dataArr['site_id'] != null && $dataArr['group_id'] != null && $dataArr['site_id'] != '' && $dataArr['group_id'] != '') {

                        $brandResult = $this->symposium_model->getBrand($dataArr['site_id'], $dataArr['group_id']);

                        if ($brandResult != null && $brandResult['brand_group_cd'] != null && $brandResult['brand_group_cd'] != '') {
                            print_r('brand_duple1');

                            //심포지엄과 브랜드를 연결한다.
                            $dataArr2 = array(
                                'brand_group_cd' => $brandResult['brand_group_cd'],
                                'sympo_cd' => $dataArr['sympo_cd'],
                                'default_brand_chk' => 'Y',
                                'creator' => 'system',
                                'updater' => 'system'
                            );

                            $res1 = $this->common_model->getSequences("bgs");
                            $dataArr2['brand_group_sympo_cd'] = str_pad($res1['currval'], 20, "BGS_0000000000000000", STR_PAD_LEFT);
                            $this->db->insert('brand_group_sympo', $dataArr2);
                            
                            for($i=0; $i<4; $i++)
                            {
                            	$dataArr3 = array(
                            			'creator' 	 => 'system',
                            			'updater' 	 => 'system',
                            			'sympo_cd'   => $dataArr['sympo_cd']
                            	);
                            	
                            	$res2 = $this->common_model->getSequences("STE");
                            	$dataArr3['setting_template_cd'] = str_pad($res2['currval'], 20, "STE_0000000000000000", STR_PAD_LEFT);
                            	$this->db->insert('setting_template', $dataArr3);
                            }
                            
                            $dataArr4 = array(
	                            'creator' 	 => 'system',
                            	'updatedate' =>  'now()',
	                            'updater' 	 => 'system',
                            	'updatedate' =>  'now()',
	                            'sympo_cd'   => $dataArr['sympo_cd']
                            );
                            
                            $res3 = $this->common_model->getSequences("OPT");
                            $dataArr4['setting_cd'] = str_pad($res3['currval'], 20, "OPT_0000000000000000", STR_PAD_LEFT);
                            $saveData = $this->settings_model->insert($dataArr4);


                        } else {
                            //브랜드를 조회해서 없으면 insert
                            $dataArr1 = Array(
                                'org_id' => $dataArr['group_id'],
                                'brand_group_id' => $dataArr['site_id'],
                                'org_name' => $dataArr['group_name'],
                                'brand_group_name' => $dataArr['site_name'],
                                'creator' => 'system',
                                'updater' => 'system'
                            );

                            $res = $this->common_model->getSequences("brg");
                            $dataArr1['brand_group_cd'] = str_pad($res['currval'], 20, "BRG_0000000000000000", STR_PAD_LEFT);
                            $this->db->insert('brand_group', $dataArr1);

                            //심포지엄과 브랜드를 연결한다.
                            $dataArr2 = array(
                                'brand_group_cd' => $dataArr1['brand_group_cd'],
                                'sympo_cd' => $dataArr['sympo_cd'],
                                'default_brand_chk' => 'Y',
                                'creator' => 'system',
                                'updater' => 'system'
                            );

                            $res1 = $this->common_model->getSequences("bgs");
                            $dataArr2['brand_group_sympo_cd'] = str_pad($res1['currval'], 20, "BGS_0000000000000000", STR_PAD_LEFT);
                            $this->db->insert('brand_group_sympo', $dataArr2);
                            
                            
                            for($i=0; $i<4; $i++)
                            {
	                            $dataArr3 = array(
	                            	'creator' 	 => 'system',
	                            	'updater' 	 => 'system',
	                            	'sympo_cd'   => $dataArr['sympo_cd']
	                            );
	                            
	                            $res2 = $this->common_model->getSequences("STE");
	                            $dataArr3['setting_template_cd'] = str_pad($res2['currval'], 20, "STE_0000000000000000", STR_PAD_LEFT);
	                            $this->db->insert('setting_template', $dataArr3);
                            }
                            
                            $dataArr4 = array(
                            		'creator' 	 => 'system',
                            		'updatedate' =>  'now()',
                            		'updater' 	 => 'system',
                            		'updatedate' =>  'now()',
                            		'sympo_cd'   => $dataArr['sympo_cd']
                            );
                            
                            $res3 = $this->common_model->getSequences("OPT");
                            $dataArr4['setting_cd'] = str_pad($res3['currval'], 20, "OPT_0000000000000000", STR_PAD_LEFT);
                            $saveData = $this->settings_model->insert($dataArr4);
                        }
                    }

                } else {
                    $saveData = $this->symposium_model->updateApi($result1['Data'][0]['event_id'], $keyType, $dataArr);
                }

            } else {
                $status = "fail";
            }
        } else {
            $status = "fail";
        }

        $this->success($status);
    }
    
    function download_file() {
    	$filename       = $this->input->get_post('template_filename');
    	
    	
    	$data = file_get_contents(FCPATH ."/uploads/settings/".$filename); // Read the file's contents
    	
    	force_download(iconv('utf-8', 'euc-kr',$filename), $data);
    }
    
    function download_file_sign() {
    	$filename       = $this->input->get_post('sign_filename');
    	
    	
    	$data = file_get_contents(FCPATH ."/uploads/signiture/".$filename); // Read the file's contents
    	
    	force_download(iconv('utf-8', 'euc-kr',$filename), $data);
    }

    function success($status) {
        $arr = array('result' => $status);
        header('Content-Type: application/json');

        echo json_encode( $arr );

    }
    
    function getInvitationCode() {
    	$res = $this->common_model->getSequences("inv");
    	$invitation = str_pad($res['currval'], 20, "INV_0000000000000000", STR_PAD_LEFT);
    	
    	echo json_encode( $invitation );
    }
    
    function getCheckInvitation() {
    	$invitation = $this->input->post("invitation");
    	$invitationCount = $this->symposium_model->checkInvitation($invitation);
    		
    	echo json_encode($invitationCount);
    }
    
    function hcp_detail(){
    	$sympo_cd = $this->input->post("sympo_cd");
    	$applicant_cd = $this->input->post("applicant_cd");
    	$invitation = $this->input->post("invitation");
    	
    	if($sympo_cd == null || $sympo_cd == ''){
    		alert_close('비정상적인 접근입니다.');
    	}
    	
    	$setting = $this->settings_model->getSympoSettings($sympo_cd);
    	$applicant = $this->symposium_model->getApplicantOnly($applicant_cd);
    	
    	$this->data['setting'] = $setting;
    	$this->data['applicant'] = $applicant;
    	$this->data['sympo_cd'] = $sympo_cd;
    	$this->data['applicant_cd'] = $applicant_cd;
    	$this->data['invitation'] = $invitation;
    	
    	$this->load->view('hcpHead');
    	$this->load->view('symposium/sympo_hcp_detail', $this->data);
    	$this->footer['setting'] = $setting;
    	$this->load->view('hcpFooter', $this->footer);
    }
    
    function hcp_home(){
    	$applicant_cd = $this->input->post("applicant_cd");
    	$type="";
    	$sympo_cd = $this->input->post("sympo_cd");
    	$invitation = $this->input->post("invitation");
    	
    	if($sympo_cd == null || $sympo_cd == ''){
    		alert_close('비정상적인 접근입니다.');
    	}
    	
    	$invitationCount = $this->symposium_model->checkInvitation($invitation);
    	
    	$symposium = $this->symposium_model->getSymposium($sympo_cd, '');
    	$setting = $this->settings_model->getSympoSettings($sympo_cd);
    	$templateList = $this->settings_model->getSympoSettingTemplate($sympo_cd);
    	
    	if($symposium['eventStatus'] == 'Y') {
    		alert_close('사전등록 대기중에는 신청서 작성이 불가능 합니다.');
    	} else if($symposium['eventStatus'] == 'EN') {
    		alert_close('심포지엄이 종료되어 신청서 작성이 불가능 합니다.');
    	}
    	
    	$this->data['symposium'] = $symposium;
    	$this->data['setting'] = $setting;
    	$this->data['templateList'] = $templateList;
    	$this->data['sympo_cd'] = $sympo_cd;
    	$this->data['applicant_cd'] = $applicant_cd;
    	$this->data['invitation'] = $invitation;
    	
    	$this->load->view('hcpHead');
    	$this->load->view('symposium/sympo_hcp_home', $this->data);
    	$this->footer['setting'] = $setting;
    	$this->load->view('hcpFooter', $this->footer);
    }
    
    function hcp_step1(){
    	
    	$sympo_cd = $this->input->post("sympo_cd");
    	$invitation = $this->input->post("invitation");
    	
    	if($sympo_cd == null || $sympo_cd == ''){
    		alert_close('비정상적인 접근입니다.');
    	}
    	
    	$invitationCount = $this->symposium_model->checkInvitation($invitation);
    	if($invitationCount != 0 ) {
    		alert_close('만료된 초대장입니다.');
    	}
    	
    	$applicant_name = $this->input->post("applicant_name");
    	$applicant_hospital = $this->input->post("applicant_hospital");
    	$applicant_dep = $this->input->post("applicant_dep");
    	$applicant_hp = $this->input->post("applicant_hp");
    	$applicant_email = $this->input->post("applicant_email");
    	
    	$symposium = $this->symposium_model->getSymposium($sympo_cd, '');
    	$setting = $this->settings_model->getSympoSettings($sympo_cd);
    	
    	if($symposium['eventStatus'] == 'Y') {
    		alert_close('사전등록 대기중에는 신청서 작성이 불가능 합니다.');
    	} else if($symposium['eventStatus'] == 'EN') {
    		alert_close('심포지엄이 종료되어 신청서 작성이 불가능 합니다.');
    	}
    	
    	if($applicant_name == null || $applicant_name == ''){
    		$this->data['applicant_name'] = '';
    		$this->data['applicant_hospital'] = '';
    		$this->data['applicant_dep'] = '';
    		$this->data['applicant_hp'] = '';
    		$this->data['applicant_email'] = '';
    	}else{
    		$this->data['applicant_name'] = $applicant_name;
    		$this->data['applicant_hospital'] = $applicant_hospital;
    		$this->data['applicant_dep'] = $applicant_dep;
    		$this->data['applicant_hp'] = $applicant_hp;
    		$this->data['applicant_email'] = $applicant_email;
    	}
    	
    	$this->data['sympo_cd'] = $sympo_cd;
    	$this->data['invitation'] = $invitation;
    	$this->footer['setting'] = $setting;
    	
    	$this->load->view('hcpHead');
    	$this->load->view('symposium/sympo_hcp_step1', $this->data);
    	$this->load->view('hcpFooter', $this->footer);
    }
    
    function hcp_step2(){
    	
    	$sympo_cd = $this->input->post("sympo_cd");
    	$applicant_name = $this->input->post("applicant_name");
    	$applicant_hospital = $this->input->post("applicant_hospital");
    	$applicant_dep = $this->input->post("applicant_dep");
    	$applicant_hp = $this->input->post("applicant_hp");
    	$applicant_email = $this->input->post("applicant_email");
    	$invitation = $this->input->post("invitation");
    	
    	$invitationCount = $this->symposium_model->checkInvitation($invitation);
    	if($invitationCount != 0 ) {
    		alert_close('만료된 초대장입니다.');
    	}
    	
    	$setting = $this->settings_model->getSympoSettings($sympo_cd);
    	
    	$this->data['sympo_cd'] = $sympo_cd;
    	$this->data['applicant_name'] = $applicant_name;
    	$this->data['applicant_hospital'] = $applicant_hospital;
    	$this->data['applicant_dep'] = $applicant_dep;
    	$this->data['applicant_hp'] = $applicant_hp;
    	$this->data['applicant_email'] = $applicant_email;
    	$this->data['invitation'] = $invitation;
    	$this->data['setting'] = $setting;
    	$this->footer['setting'] = $setting;
    	
    	$this->load->view('hcpHead');
    	$this->load->view('symposium/sympo_hcp_step2', $this->data);
    	$this->load->view('hcpFooter', $this->footer);
    }
    
    function hcp_step3(){
    	
    	$sympo_cd = $this->input->post("sympo_cd");
    	$applicant_name = $this->input->post("applicant_name");
    	$applicant_hospital = $this->input->post("applicant_hospital");
    	$applicant_dep = $this->input->post("applicant_dep");
    	$applicant_hp = $this->input->post("applicant_hp");
    	$applicant_email = $this->input->post("applicant_email");
    	$invitation = $this->input->post("invitation");
    	
    	$invitationCount = $this->symposium_model->checkInvitation($invitation);
    	if($invitationCount != 0 ) {
    		alert_close('만료된 초대장입니다.');
    	}
    	
    	$setting = $this->settings_model->getSympoSettings($sympo_cd);
    	
    	$this->data['sympo_cd'] = $sympo_cd;
    	$this->data['applicant_name'] = $applicant_name;
    	$this->data['applicant_hospital'] = $applicant_hospital;
    	$this->data['applicant_dep'] = $applicant_dep;
    	$this->data['applicant_hp'] = $applicant_hp;
    	$this->data['applicant_email'] = $applicant_email;
    	$this->data['invitation'] = $invitation;
    	$this->data['setting'] = $setting;
    	$this->footer['setting'] = $setting;
    	
    	$this->load->view('hcpHead');
    	$this->load->view('symposium/sympo_hcp_step3', $this->data);
    	$this->load->view('hcpFooter', $this->footer);
    }
    
    function hcp_step4(){
    	
     	$sympo_cd = $this->input->post("sympo_cd");
    	$applicant_name = $this->input->post("applicant_name");
    	$applicant_hospital = $this->input->post("applicant_hospital");
    	$applicant_dep = $this->input->post("applicant_dep");
    	$applicant_hp = $this->input->post("applicant_hp");
    	$applicant_email = $this->input->post("applicant_email");
     	$canvas_img = $this->input->post("canvas_img");
     	$invitation = $this->input->post("invitation");
     	
     	$invitationCount = $this->symposium_model->checkInvitation($invitation);
     	if($invitationCount != 0 ) {
     		alert_close('만료된 초대장입니다.');
     	}
    	
     	$canvas_img = substr($canvas_img,strpos($canvas_img, ",") + 1);
     	$canvas_img = base64_decode($canvas_img);
     	$fp = fopen('./uploads/signiture/'.$applicant_name.'_'.time().'.png', 'wb');
     	fwrite($fp, $canvas_img);
     	fclose($fp);
     	$filename = $applicant_name.'_'.time();
     	$fileext = '.png';
     	$member_id = $this->session->userdata('member_id');
     	
     	$dataArr = array(
     			'applicant_name' => $applicant_name,
     			'applicant_hospital' => $applicant_hospital,
     			'applicant_dep' => $applicant_dep,
     			'applicant_hp' => str_replace("-", "", $applicant_hp),
     			'applicant_email' => $applicant_email,
     			'filename' => $filename,
     			'fileext' => $fileext,
     			'regi_complete' => 'Y',
     			'sympo_cd' => $sympo_cd,
     			'invitation' => $invitation
     	);
     	
     	$comment = '';
     	//직접 HCP로
     	$dataArr['regist_type'] = 'HCP';
     	$dataArr['updater'] = $member_id;
     	$dataArr['creator'] = $member_id;
     	
     	$res = $this->common_model->getSequences("app");
     	$dataArr['applicant_cd'] = str_pad($res['currval'], 20, "APP_0000000000000000", STR_PAD_LEFT);
     	$this->symposium_model->applyHcpInsert($dataArr);
     	
     	echo json_encode(array("applicant_cd"=>$dataArr['applicant_cd'], "applicant_name"=>$applicant_name, "sympo_cd"=>$dataArr['sympo_cd']), JSON_UNESCAPED_UNICODE);
     	
    }
    
    function hcp_complete(){
    	
    	$sympo_cd = $this->input->post("sympo_cdL");
    	$applicant_cd = $this->input->post("applicant_cdL");
    	$applicant_name = $this->input->post("applicant_nameL");
    	$invitation = $this->input->post("invitation");
    	
    	$setting = $this->settings_model->getSympoSettings($sympo_cd);
    	$symposium = $this->symposium_model->getSymposium($sympo_cd, '');
    	
    	$this->data['setting'] = $setting;
    	$this->data['symposium'] = $symposium;
    	$this->data['applicant_cd'] = $applicant_cd;
    	$this->data['applicant_name'] = $applicant_name;
    	$this->data['sympo_cd'] = $sympo_cd;
    	$this->data['invitation'] = $invitation;
    	$this->footer['setting'] = $setting;
    	
    	$this->load->view('hcpHead');
    	$this->load->view('symposium/sympo_hcp_step4', $this->data);
    	$this->load->view('hcpFooter', $this->footer);
    	
    }
    
    // 사전등록 완료 이메일 발송
    function email_sendhcp($sympo_cd, $member_email) {

        $symposium = $this->symposium_model->getSymposium($sympo_cd, '');
        $eventName = $symposium['eventName'];

        $mail_subject = "[".$symposium['eventName']."] 사전등록 완료메일입니다.";

        $mail_msg = '<!DOCTYPE html>';
        $mail_msg .= '<html lang="ko">';
        $mail_msg .= '<head>';
        $mail_msg .= '<title>웨비나스 사전등록 시스템</title>';
        $mail_msg .= '<meta charset="utf-8">';
        $mail_msg .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        $mail_msg .= '<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">';
        $mail_msg .= '</head>';
        $mail_msg .= '<body style="width:100%;height:100%;margin:0;padding:0;box-sizing:border-box;">';
        $mail_msg .= '<div style="max-width:700px;padding:30px;margin:0;border:1px solid #ccc;box-sizing:border-box;border-radius:10px;">';
        $mail_msg .= '<p style="padding:0 0 10px 0;margin:0;line-height:120%;font-size:16px;font-weight:bold;border-bottom:2px solid #347ab6;font-family:돋움, Dotum, Arial, sans-serif;">사전 등록 완료</p>';
        $mail_msg .= '<p style="margin:50px 0;padding:0;text-align:center;"><img style="margin:0;padding:0;" src="'.FULL_URL.'/images/visu_complete.png" alt="" /></p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;text-align:center;font-weight:bold;">심포지엄 [ <span style="margin:0;padding:0;color:#0072c1;">'.$eventName.'</span> ]에 </p>';
        $mail_msg .= '<p style="margin:0 0 25px 0;padding:0;text-align:center;font-weight:bold;">정상적으로 사전등록이 완료되었습니다.</p>';
        $mail_msg .= '<div style="margin:25px auto 0;padding:0;max-width:430px;">';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:13px;font-weight:bold;">심포지엄 사무국</p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:12px;line-height:140%;">사용과 관련하여 문의사항은 심포지엄 사무국으로 메일 및 연락을 주시기 바랍니다.</p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:12px;line-height:140%;"><b>이메일</b> : support@webinars.co.kr</p>';
        $mail_msg .= '<p style="margin:0 0 5px 0;padding:0;font-size:12px;line-height:140%;"><b>연락처</b> : 02-6342-6830</p>';
        $mail_msg .= '</div>';
        $mail_msg .= '</div>';
        $mail_msg .= '</body>';
        $mail_msg .= '</html>';


        if(sendmail_smtp($member_email, $mail_subject, $mail_msg)) {
            $result = "ok";
        }else{
            $result = "no";
        }
    }

    //전체 심포지엄 리스트 보기
    function sympo_list() {

        $searchSortVal = $this->input->get("searchSortVal");
        $listSortVal = $this->input->get("listSortVal");
        $searchTypeVal = $this->input->get("searchTypeVal");
        $searchTextVal = $this->input->get("searchTextVal");
        $searchGubunVal = $this->input->get("searchGubunVal");

        if($searchSortVal == null || $searchSortVal == '') {
            $searchSortVal = 'all';
        }
        if($listSortVal == null || $listSortVal == '') {
            $listSortVal = 'all';
        }
        if($searchTextVal == null || $searchTextVal == '') {
            $searchTextVal = '';
        }
        if($searchTypeVal == null || $searchTypeVal == '') {
        	$searchTypeVal = '';
        }
        if($searchGubunVal == null || $searchGubunVal == '') {
            $searchGubunVal = 'A';
        }

        $sympo_list = $this->symposium_model->getLimitSympoList(1, $searchSortVal, $listSortVal, $searchTypeVal, $searchTextVal, $searchGubunVal);
        $sympoCount = $this->symposium_model->getTotalCount($searchSortVal, $searchTypeVal, $searchTextVal, $searchGubunVal);
        $this->data['sympo_list'] = $sympo_list;
        $this->data['sympoCount'] = $sympoCount;
        $this->data['searchSortVal'] = $searchSortVal;
        $this->data['listSortVal'] = $listSortVal;
        $this->data['searchTypeVal'] = $searchTypeVal;
        $this->data['searchTextVal'] = $searchTextVal;
        $this->data['searchGubunVal'] = $searchGubunVal;

        $setting = $this->settings_model->getSettings('');
        $this->data['setting'] = $setting;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_list',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 심포지엄 리스트 더보기
    function symposiumAjax() {

        $searchSortVal  = $this->input->post("searchSortVal");
        $listSortVal    = $this->input->post("listSortVal");
        $searchTextVal = $this->input->post("searchTextVal");
        $earchTypeVal = $this->input->post("searchTypeVal");
        $searchGubunVal = $this->input->post("searchGubunVal");
        $page           = $this->input->post("page");

        if($searchSortVal == null || $searchSortVal == '') {
            $searchSortVal = 'all';
        }
        if($listSortVal == null || $listSortVal == '') {
            $listSortVal = 'pre';
        }
        if($searchTextVal == null || $searchTextVal == '') {
            $searchTextVal = '';
            $searchTypeVal = '';
        }
        if($searchGubunVal == null || $searchGubunVal == '') {
            $searchGubunVal = 'A';
        }

        $sympo_list = $this->symposium_model->getLimitSympoList($page, $searchSortVal, $listSortVal, $searchTypeVal, $searchTextVal, $searchGubunVal);

        echo json_encode($sympo_list);
    }


    //심포지엄 상세정보
    function sympo_detail() {
        $sympoCd = $this->uri->segment(3);

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $this->data['symposium'] = $symposium;

        $setting = $this->settings_model->getSettings('');
        $this->data['setting'] = $setting;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_detail',$this->data);
        $this->load->view('footer', $this->footer);
    }

    //심포지엄 등록관리
    function sympo_enrollment() {
        $sympoCd = $this->uri->segment(3);

        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $passcode = $this->input->post("passcode");
        $regiComplete = $this->input->post("regiComplete");
        $dupleInspec = $this->input->post("dupleInspec");
        $dupleInspecType = $this->input->post("dupleInspecType");
        if($dupleInspec == null || $dupleInspec == '') {
            $dupleInspec = $this->input->get("dupleInspec");
            $dupleInspecType = $this->input->get("dupleInspecType");
        }

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $regilist = $this->symposium_model->getRegiList(1, $sympoCd, $passcode, $regiComplete, $searchType, $searchText, '', '', $sortWhere, $sortType, $dupleInspec, $dupleInspecType, 'N');
        $regilistcount = $this->symposium_model->getPasscodeListCount($sympoCd, $passcode, $regiComplete, $searchType, $searchText, '', '', 'N');
        $dupleHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, $passcode, $regiComplete, $searchType, $searchText, '', '','N');
        $dupleEmailCount = $this->symposium_model->getPasscodeListEmailDupleCount($sympoCd, $passcode, $regiComplete, $searchType, $searchText, '', '','N');
        $signCount = $this->symposium_model->getSignCount($sympoCd);

        $this->data['symposium'] = $symposium;
        $this->data['regilist'] = $regilist;
        $this->data['regilistcount'] = $regilistcount;
        $this->data['dupleHpCount'] = $dupleHpCount;
        $this->data['dupleEmailCount'] = $dupleEmailCount;
        $this->data['signCount'] = $signCount;
        $this->data['sympoCd'] = $sympoCd;
        $this->data['searchType'] = $searchType;
        $this->data['searchText'] = $searchText;
        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;
        $this->data['passcode'] = $passcode;
        $this->data['regiComplete'] = $regiComplete;
        $this->data['dupleInspec'] = $dupleInspec;
        $this->data['dupleInspecType'] = $dupleInspecType;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_enrollment',$this->data);
        $this->load->view('footer', $this->footer);
    }

	//심포지엄 등록관리 리스트 추가보기
    function sympo_enrollmentAjax() {
        $sympoCd = $this->input->post("sympoCd");
        $page = $this->input->post("page");
        $passcode = $this->input->post("passcode");
        $regiComplete = $this->input->post("regiComplete");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $dupleInspec = $this->input->post("dupleInspec");
        $dupleInspecType = $this->input->post("dupleInspecType");

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        $regilist = $this->symposium_model->getRegiList($page, $sympoCd, $passcode, $regiComplete, $searchType, $searchText, '', '',  $sortWhere, $sortType, $dupleInspec, $dupleInspecType, 'N');
        echo json_encode($regilist);
    }
    
    
    // 참가초대 메인
    function sympo_attend() {
        $sympoCd = $this->uri->segment(3);

        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $passcodeYcount = $this->symposium_model->getPasscodeListCount($sympoCd, 'Y', '', '', '', $scRegDtSt, $scRegDtEd,'N');
        $passcodeNcount = $this->symposium_model->getPasscodeListCount($sympoCd, 'N', '', '', '', $scRegDtSt, $scRegDtEd,'N');
        $passcodeCcount = $this->symposium_model->getPasscodeListCount($sympoCd, '', '', '', '', $scRegDtSt, $scRegDtEd,'Y');
        $dupleYHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, 'Y', '', '', '', $scRegDtSt, $scRegDtEd,'N');
        $dupleNHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, 'N', '', '', '', $scRegDtSt, $scRegDtEd,'N');
        $dupleCHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, '', '', '', '', $scRegDtSt, $scRegDtEd,'Y');

        $this->data['symposium'] = $symposium;
        $this->data['sympoCd'] = $sympoCd;
        $this->data['dupleYHpCount'] = $dupleYHpCount;
        $this->data['dupleNHpCount'] = $dupleNHpCount;
        $this->data['dupleCHpCount'] = $dupleCHpCount;
        $this->data['passcodeNcount'] = $passcodeNcount;
        $this->data['passcodeYcount'] = $passcodeYcount;
        $this->data['passcodeCcount'] = $passcodeCcount;
        $this->data['scRegDtSt'] = $scRegDtSt;
        $this->data['scRegDtEd'] = $scRegDtEd;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_attend',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // passcode 미정 리스트 보기
    function sympo_attend_ready() {
        $sympoCd = $this->uri->segment(3);

        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $dupleInspec = $this->input->post("dupleInspec");
        $dupleInspecType = $this->input->post("dupleInspecType");
        
        if($dupleInspec == null || $dupleInspec == '') {
            $dupleInspec = $this->input->get("dupleInspec");
            $dupleInspecType = $this->input->get("dupleInspecType");
        }
        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'updatedate';
        }
        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $attentList = $this->symposium_model->getRegiList(1, $sympoCd, 'N', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec,$dupleInspecType,'N');
        $attentcount = $this->symposium_model->getPasscodeListCount($sympoCd, 'N', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'N');
        $dupleHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, 'N', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'N');
        $dupleEmailCount = $this->symposium_model->getPasscodeListEmailDupleCount($sympoCd, 'N', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'N');

        $this->data['symposium'] = $symposium;
        $this->data['sympoCd'] = $sympoCd;
        $this->data['attentList'] = $attentList;
        $this->data['dupleHpCount'] = $dupleHpCount;
        $this->data['dupleEmailCount'] = $dupleEmailCount;
        $this->data['attentcount'] = $attentcount;
        $this->data['searchType'] = $searchType;
        $this->data['searchText'] = $searchText;
        $this->data['scRegDtSt'] = $scRegDtSt;
        $this->data['scRegDtEd'] = $scRegDtEd;
        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;
        $this->data['dupleInspec'] = $dupleInspec;
        $this->data['dupleInspecType'] = $dupleInspecType;
        
        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_attend_ready',$this->data);
        $this->load->view('footer', $this->footer);
    }
    
    // passcode 미정 리스트 더보기
    function sympo_attend_readyAjax() {
    	$sympoCd = $this->input->post("sympoCd");
    	$page = $this->input->post("page");
    	$sortWhere = $this->input->post("sortWhere");
    	$sortType = $this->input->post("sortType");
    	$scRegDtSt = $this->input->post("scRegDtSt");
    	$scRegDtEd = $this->input->post("scRegDtEd");
    	$searchType = $this->input->post("searchType");
    	$searchText = $this->input->post("searchText");
    	$dupleInspec = $this->input->post("dupleInspec");
    	$dupleInspecType = $this->input->post("dupleInspecType");
    	
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	
    	$attentList = $this->symposium_model->getRegiList($page, $sympoCd, 'N', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec, $dupleInspecType,'N');
    	echo json_encode($attentList);
    }
    
    // passcode 완료 리스트 보기
    function sympo_attend_complete() {
    	$sympoCd = $this->uri->segment(3);
    	$searchType = $this->input->post("searchType");
    	$searchText = $this->input->post("searchText");
    	$scRegDtSt = $this->input->post("scRegDtSt");
    	$scRegDtEd = $this->input->post("scRegDtEd");
    	$sortWhere = $this->input->post("sortWhere");
    	$sortType = $this->input->post("sortType");
    	$dupleInspec = $this->input->post("dupleInspec");
    	$dupleInspecType = $this->input->post("dupleInspecType");
    	
    	if($dupleInspec == null || $dupleInspec == '') {
    		$dupleInspec = $this->input->get("dupleInspec");
    		$dupleInspecType = $this->input->get("dupleInspecType");
    	}
    	
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	
    	$symposium = $this->symposium_model->getSymposium($sympoCd, '');
    	$attentList = $this->symposium_model->getRegiList(1, $sympoCd, 'Y', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec,$dupleInspecType,'N');
    	$attentcount = $this->symposium_model->getPasscodeListCount($sympoCd, 'Y', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'N');
    	$dupleHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, 'Y', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'N');
    	$dupleEmailCount = $this->symposium_model->getPasscodeListEmailDupleCount($sympoCd, 'Y', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'N');
    	
    	$this->data['symposium'] = $symposium;
    	$this->data['sympoCd'] = $sympoCd;
    	$this->data['attentList'] = $attentList;
    	$this->data['dupleHpCount'] = $dupleHpCount;
    	$this->data['dupleEmailCount'] = $dupleEmailCount;
    	$this->data['attentcount'] = $attentcount;
    	$this->data['searchType'] = $searchType;
    	$this->data['searchText'] = $searchText;
    	$this->data['scRegDtSt'] = $scRegDtSt;
    	$this->data['scRegDtEd'] = $scRegDtEd;
    	$this->data['sortWhere'] = $sortWhere;
    	$this->data['sortType'] = $sortType;
    	$this->data['dupleInspec'] = $dupleInspec;
    	$this->data['dupleInspecType'] = $dupleInspecType;
    	
    	$this->load->view('head',$this->head);
    	$this->load->view('symposium/sympo_attend_complete',$this->data);
    	$this->load->view('footer', $this->footer);
    }
    
    // Passcode 완료 리스트 더보기
    function sympo_attend_completeAjax() {
    	$sympoCd = $this->input->post("sympoCd");
    	$page = $this->input->post("page");
    	$sortWhere = $this->input->post("sortWhere");
    	$sortType = $this->input->post("sortType");
    	$scRegDtSt = $this->input->post("scRegDtSt");
    	$scRegDtEd = $this->input->post("scRegDtEd");
    	$searchType = $this->input->post("searchType");
    	$searchText = $this->input->post("searchText");
    	$dupleInspec = $this->input->post("dupleInspec");
    	$dupleInspecType = $this->input->post("dupleInspecType");
    	
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	
    	$attentList = $this->symposium_model->getRegiList($page, $sympoCd, 'Y', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec, $dupleInspecType,'N');
    	echo json_encode($attentList);
    }
    
    // Passcode 취소 리스트 보기
    function sympo_attend_cancel() {
    	$sympoCd = $this->uri->segment(3);
    	$searchType = $this->input->post("searchType");
    	$searchText = $this->input->post("searchText");
    	$scRegDtSt = $this->input->post("scRegDtSt");
    	$scRegDtEd = $this->input->post("scRegDtEd");
    	$sortWhere = $this->input->post("sortWhere");
    	$sortType = $this->input->post("sortType");
    	$dupleInspec = $this->input->post("dupleInspec");
    	$dupleInspecType = $this->input->post("dupleInspecType");
    	
    	if($dupleInspec == null || $dupleInspec == '') {
    		$dupleInspec = $this->input->get("dupleInspec");
    		$dupleInspecType = $this->input->get("dupleInspecType");
    	}
    	
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	
    	$symposium = $this->symposium_model->getSymposium($sympoCd, '');
    	$attentList = $this->symposium_model->getRegiList(1, $sympoCd, '', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec,$dupleInspecType,'Y');
    	$attentcount = $this->symposium_model->getPasscodeListCount($sympoCd, '', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'Y');
    	$dupleHpCount = $this->symposium_model->getPasscodeListHpDupleCount($sympoCd, '', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'Y');
    	$dupleEmailCount = $this->symposium_model->getPasscodeListEmailDupleCount($sympoCd, '', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd,'Y');
    	
    	$this->data['symposium'] = $symposium;
    	$this->data['sympoCd'] = $sympoCd;
    	$this->data['attentList'] = $attentList;
    	$this->data['dupleHpCount'] = $dupleHpCount;
    	$this->data['dupleEmailCount'] = $dupleEmailCount;
    	$this->data['attentcount'] = $attentcount;
    	$this->data['searchType'] = $searchType;
    	$this->data['searchText'] = $searchText;
    	$this->data['scRegDtSt'] = $scRegDtSt;
    	$this->data['scRegDtEd'] = $scRegDtEd;
    	$this->data['sortWhere'] = $sortWhere;
    	$this->data['sortType'] = $sortType;
    	$this->data['dupleInspec'] = $dupleInspec;
    	$this->data['dupleInspecType'] = $dupleInspecType;
    	
    	$this->load->view('head',$this->head);
    	$this->load->view('symposium/sympo_attend_cancel',$this->data);
    	$this->load->view('footer', $this->footer);
    }
    
    // Passcode 취소 리스트 더보기
    function sympo_attend_cancelAjax() {
    	$sympoCd = $this->input->post("sympoCd");
    	$page = $this->input->post("page");
    	$sortWhere = $this->input->post("sortWhere");
    	$sortType = $this->input->post("sortType");
    	$scRegDtSt = $this->input->post("scRegDtSt");
    	$scRegDtEd = $this->input->post("scRegDtEd");
    	$searchType = $this->input->post("searchType");
    	$searchText = $this->input->post("searchText");
    	$dupleInspec = $this->input->post("dupleInspec");
    	$dupleInspecType = $this->input->post("dupleInspecType");
    	
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	
    	$attentList = $this->symposium_model->getRegiList($page, $sympoCd, '', '', $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec, $dupleInspecType,'Y');
    	echo json_encode($attentList);
    }
    
    // ????????????????
    function attentApplyDupleAjax() {
        $sympoCd = $this->input->post("sympoCd");
        $applicantCds = $this->input->post("applicantCds");

        $cnt = $this->symposium_model->getAttentApplyDuple($sympoCd, 'Y', $applicantCds);

        echo json_encode($cnt['cnt']);
    }

    // 참가 완료
    function sympo_attend_insertApply() {
        $sympo_cd           = $this->input->post("sympo_cd");
        $applicantCds       = $this->input->post('applicantCds');
        $applicantCdsArr 	= explode(";", $applicantCds);

        foreach ($applicantCdsArr as $applicantCd):
        	if($applicantCd != null && $applicantCd != '') {
        		$this->symposium_model->passcodeUpdate($applicantCd);
        	}
        endforeach;

        alert("패스코드 발급이 완료되었습니다.. ",HOME_DIR."/symposium/sympo_attend_complete/".$sympo_cd);
    }
    
    // 참가 취소
    function sympo_attend_insertCancel() {
    	$sympo_cd           = $this->input->post("sympo_cd");
    	$applicantCds       = $this->input->post('applicantCds');
    	
    	$applicantCdsArr = explode(";", $applicantCds);
    	
    	foreach ($applicantCdsArr as $applicantCd):
    	$this->symposium_model->passcodeCancel($applicantCd);
    	endforeach;
    	
    	alert("참가취소로 변경되었습니다.",HOME_DIR."/symposium/sympo_attend_cancel/".$sympo_cd);
    }
    
    // 참가자 삭제
    function sympo_attend_Del() {
    	$sympo_cd           = $this->input->post("sympo_cd");
    	$applicantCds       = $this->input->post('applicantCds');
    	
    	$applicantCdsArr = explode(";", $applicantCds);
    	
    	foreach ($applicantCdsArr as $applicantCd):
    		$this->symposium_model->attentDelete($applicantCd);
    	endforeach;
    	
    	alert("삭제하였습니다.",HOME_DIR."/symposium/sympo_attend/".$sympo_cd);
    }
    
    // 참가 복원
    function sympo_attend_Restore() {
    	$sympo_cd           = $this->input->post("sympo_cd");
    	$applicantCds       = $this->input->post('applicantCds');
    	
    	$applicantCdsArr = explode(";", $applicantCds);
    	
    	foreach ($applicantCdsArr as $applicantCd):
    	$this->symposium_model->passcodeRestore($applicantCd);
    	endforeach;
    	
    	alert("참가자를 패스코드 미발급 단계로 모두 복원하였습니다.",HOME_DIR."/symposium/sympo_attend/".$sympo_cd);
    }

    // 일괄 passcode완료
    function sympo_attend_AllApply() {
        $sympo_cd           = $this->input->post("sympo_cd");
        $attentList = $this->symposium_model->getAttentAllList($sympo_cd, 'N','N');

        foreach ($attentList as $attent):
        $this->symposium_model->passcodeUpdate($attent['applicantCd']);
        endforeach;

        alert("패스코드 발급이 완료되었습니다.",HOME_DIR."/symposium/sympo_attend_complete/".$sympo_cd);
    }
    
    // 일괄 복원
    function sympo_attend_AllRestore() {
    	$sympo_cd           = $this->input->post("sympo_cd");
    	$attentList = $this->symposium_model->getAttentAllList($sympo_cd, '','Y');
    	
    	foreach ($attentList as $attent):
    	$this->symposium_model->passcodeRestore($attent['applicantCd']);
    	endforeach;
    	
    	alert("참가자를 Passcode 미발급 단계로 모두 복원하였습니다.",HOME_DIR."/symposium/sympo_attend/".$sympo_cd);
    }
    
    //전체 리포트
    function sympo_reportAll() {
        $sympoCd = $this->uri->segment(3);

        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $report = $this->symposium_model->getReport($sympoCd, $scRegDtSt, $scRegDtEd, '');
        $reportDays = $this->symposium_model->getReportDays($sympoCd, $scRegDtSt, $scRegDtEd, '');

        $this->data['symposium'] = $symposium;
        $this->data['report'] = $report;
        $this->data['reportDays'] = $reportDays;
        $this->data['scRegDtSt'] = $scRegDtSt;
        $this->data['scRegDtEd'] = $scRegDtEd;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_reportAll',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 담당영업소 별 보고서
    function sympo_reportOffice() {
        $sympoCd = $this->uri->segment(3);

        $authority = $this->session->userdata('authority');

        $searchText = $this->input->post("searchText");
        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $sortWhere      = $this->input->post("sortWhere");
        $sortType       = $this->input->post("sortType");

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'regiCount';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'asc';
        }

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $officeReport = $this->symposium_model->getOfficeReport($sympoCd, $sortWhere, $sortType, $searchText, $scRegDtSt, $scRegDtEd);

        $this->data['symposium'] = $symposium;
        $this->data['officeReport'] = $officeReport;
        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;
        $this->data['searchText'] = $searchText;
        $this->data['scRegDtSt'] = $scRegDtSt;
        $this->data['scRegDtEd'] = $scRegDtEd;
        $this->data['authority'] = $authority;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_reportOffice',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 날짜별 보고서 보기
    function sympo_reportDate() {
        $sympoCd = $this->uri->segment(3);

        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $sortWhere      = $this->input->post("sortWhere");
        $sortType       = $this->input->post("sortType");

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'date';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'asc';
        }

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $daysReport = $this->symposium_model->getDaysReport($sympoCd, $sortWhere, $sortType, $scRegDtSt, $scRegDtEd, '');

        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;
        $this->data['symposium'] = $symposium;
        $this->data['daysReport'] = $daysReport;
        $this->data['scRegDtSt'] = $scRegDtSt;
        $this->data['scRegDtEd'] = $scRegDtEd;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_reportDate',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 심포지엄 신청서
    function sympo_application() {

        $authority = $this->session->userdata('authority');
        if($authority == 'mr') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $sympoCd = $this->uri->segment(3);

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $this->data['symposium'] = $symposium;

        $setting = $this->settings_model->getSympoSettings($sympoCd);
        $templateList = $this->settings_model->getSympoSettingTemplate($sympoCd);
        
        $this->data['setting'] = $setting;
        $this->data['templateList'] = $templateList;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_application',$this->data);
        $this->load->view('footer', $this->footer);
    }
    
    // 개인정보 동의 설정
    function sympo_personalInfo() {
    	
    	$authority = $this->session->userdata('authority');
    	if($authority == 'mr') {
    		alert('권한이 없습니다.', HOME_DIR.'/main');
    	}
    	
    	$sympoCd = $this->uri->segment(3);
    	
    	$symposium = $this->symposium_model->getSymposium($sympoCd, '');
    	$setting = $this->settings_model->getSympoSettings($sympoCd);
    	
    	$this->data['symposium'] = $symposium;
    	$this->data['setting'] = $setting;
    	
    	$this->load->view('head',$this->head);
    	$this->load->view('symposium/sympo_personalInfo',$this->data);
    	$this->load->view('footer', $this->footer);
    }

    // 심포지엄 설정
    function sympo_setting() {

        $authority = $this->session->userdata('authority');
        if($authority != 'op' && $authority != 'mcm' && $authority != 'pm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $sympoCd = $this->uri->segment(3);

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $this->data['symposium'] = $symposium;

        $row = $this->settings_model->getSympoSettings($sympoCd);

        $this->data['row'] = $row;

        $this->load->view('head',$this->head);
        $this->load->view('symposium/sympo_setting',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 참가자정보 수정
    function applicantEdit() {
        $applicantCd = $this->input->post("applicantCd");
        $applicant_name = $this->input->post("applicantName");
        $applicant_email = $this->input->post("applicantEmail");
        $applicant_hp = $this->input->post("applicantHp");
        $applicant_hospital = $this->input->post("applicantHospital");
        $applicant_dep = $this->input->post("applicantDep");
        $comment = $this->input->post("comment");
        
        $member_id = $this->session->userdata('member_id');

        $dataArr = array (
            'applicant_name'        => $applicant_name,
            'applicant_email'       => $applicant_email,
            'applicant_hp'          => str_replace("-", "", $applicant_hp),
            'applicant_hospital'    => $applicant_hospital,
            'applicant_dep'         => $applicant_dep,
            'updater'               => $member_id,
        	'comment'               => $comment,
            'updatedate'            => 'now()',
        );
        
        $saveData = $this->symposium_model->applicantDetailUpdate($applicantCd, $dataArr);

        if($saveData == "ok"){
        	alert("수정하였습니다.",$_SERVER['HTTP_REFERER']);
        } elseif($saveData == "err"){
        	alert("장애가 발생했습니다.",$_SERVER['HTTP_REFERER']);
        }
    }

    // 날짜별 보고서 엑셀 링크
    function daysReportExcelAjax() {
        $sympoCd = $this->input->post("sympoCd");
        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $fileType = $this->input->post("fileType");

        echo HOME_DIR."/symposium/daysReportExcel/?sympoCd=".$sympoCd."&sortWhere=".$sortWhere."&sortType=".$sortType."&scRegDtSt=".$scRegDtSt."&scRegDtEd=".$scRegDtEd."&fileType=".$fileType;
    }

    // 날짜별 보고서 엑셀 생성
    function daysReportExcel() {
        $sympoCd = $this->input->get("sympoCd");
        $scRegDtSt = $this->input->get("scRegDtSt");
        $scRegDtEd = $this->input->get("scRegDtEd");
        $sortWhere = $this->input->get("sortWhere");
        $sortType = $this->input->get("sortType");
        $fileType = $this->input->get("fileType");

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $daysReport = $this->symposium_model->getDaysReport($sympoCd, $sortWhere, $sortType, $scRegDtSt, $scRegDtEd, '');

        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('워크시트');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '등록일');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', '예비등록');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', '방문등록');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', '사전등록완료');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', '패스코드 발급완료');

        if(count($daysReport) > 0) {
            foreach ($daysReport as $key => $item) {
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . ($key + 2), $item['date'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . ($key + 2), $item['regiPSRCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . ($key + 2), $item['regiHCPCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . ($key + 2), $item['regiCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . ($key + 2), $item['passcodeYCount'], PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }

        if($fileType == 'csv') {
            $filename = $symposium['eventName'] . '_등록일별_보고서.csv'; // 엑셀 파일 이름
            header('Content-Type: application/pdf'); //mime 타입
            header('Content-Disposition: attachment;filename="' . iconv('utf-8', 'euc-kr', $filename) . '"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        } else {
            $filename = $symposium['eventName'].'_등록일별_보고서.xls'; // 엑셀 파일 이름
            header('Content-Type: application/vnd.ms-excel'); //mime 타입
            header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        }

        $objWriter->save('php://output');
    }

    // 담당 영업소별 보고서 엑셀 링크
    function officeReportExcelAjax() {
        $sympoCd = $this->input->post("sympoCd");
        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $searchText = $this->input->post("searchText");
        $fileType = $this->input->post("fileType");

        echo HOME_DIR."/symposium/officeReportExcel/?sympoCd=".$sympoCd."&scRegDtSt=".$scRegDtSt."&scRegDtEd=".$scRegDtEd."&searchText=".$searchText."&sortWhere=".$sortWhere."&sortType=".$sortType."&fileType=".$fileType;
    }

    // 담당 영업소별 보고서 엑셀 생성
    function officeReportExcel() {

        $sympoCd = $this->input->get("sympoCd");
        $scRegDtSt = $this->input->get("scRegDtSt");
        $scRegDtEd = $this->input->get("scRegDtEd");
        $sortWhere = $this->input->get("sortWhere");
        $sortType = $this->input->get("sortType");
        $searchText = $this->input->get("searchText");
        $fileType = $this->input->get("fileType");

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'regiCount';
        }

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');
        $psrReport = $this->symposium_model->getOfficeReport($sympoCd, $sortWhere, $sortType, $searchText, $scRegDtSt, $scRegDtEd);

        $this->load->library("PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('워크시트');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '담당영업소');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', '예비등록');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', '방문등록');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', '사전등록완료');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', '패스코드 발급완료');

        if(count($psrReport) > 0) {
            foreach ($psrReport as $key => $item) {
            	if($item['office_name'] != null && $item['office_name'] != ''){
            		$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . ($key + 2), $item['office_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            	} else {
            		$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . ($key + 2), '-', PHPExcel_Cell_DataType::TYPE_STRING);
            	}
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . ($key + 2), $item['regiPSRCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . ($key + 2), $item['regiHCPCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . ($key + 2), $item['regiCount'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . ($key + 2), $item['passcodeYCount'], PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }

        if($fileType == 'csv') {
            $filename = $symposium['eventName'] . '_담당_영업소별_보고서.csv'; // 엑셀 파일 이름
            header('Content-Type: application/pdf'); //mime 타입
            header('Content-Disposition: attachment;filename="' . iconv('utf-8', 'euc-kr', $filename) . '"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        } else {
            $filename = $symposium['eventName'].'_담당_영업소별_보고서.xls'; // 엑셀 파일 이름
            header('Content-Type: application/vnd.ms-excel'); //mime 타입
            header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
            header('Cache-Control: max-age=0'); //no cache

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        }

        $objWriter->save('php://output');
    }

    // 등록관리 엑셀 링크
    function applicantExcelAjax() {
        $sympoCd = $this->input->post("sympoCd");
        $searchType = $this->input->post("searchType");
        $searchText = $this->input->post("searchText");
        $fileType = $this->input->post("fileType");
        $sortWhere = $this->input->post("sortWhere");
        $sortType = $this->input->post("sortType");
        $passcode = $this->input->post("passcode");
        $dupleInspec = $this->input->post("dupleInspec");
        $dupleInspecType = $this->input->post("dupleInspecType");
        $scRegDtSt = $this->input->post("scRegDtSt");
        $scRegDtEd = $this->input->post("scRegDtEd");
        $cancel = $this->input->post("cancel");
        $passcodeType = $this->input->post("passcodeType");

        echo HOME_DIR."/symposium/applicantExcel/?sympoCd=".$sympoCd
        ."&searchType=".$searchType
        ."&searchText=".$searchText
        ."&fileType=".$fileType
        ."&sortWhere=".$sortWhere
        ."&sortType=".$sortType
        ."&passcode=".$passcode
        ."&dupleInspec=".$dupleInspec
        ."&dupleInspecType=".$dupleInspecType
        ."&scRegDtSt=".$scRegDtSt
        ."&scRegDtEd=".$scRegDtEd
        ."&cancel=".$cancel
        ."&passcodeType=".$passcodeType;
    }
    
    // 등록관리 엑셀생성
    function applicantExcel() {
    	$sympoCd        	= $this->input->get("sympoCd");
    	$fileType       	= $this->input->get("fileType");
    	$searchType     	= $this->input->get("searchType");
    	$searchText     	= $this->input->get("searchText");
    	$sortWhere      	= $this->input->get("sortWhere");
    	$sortType       	= $this->input->get("sortType");
    	$passcode     		= $this->input->get("passcode");
    	$dupleInspec    	= $this->input->get("dupleInspec");
    	$dupleInspecType 	= $this->input->get("dupleInspecType");
    	$scRegDtSt 			= $this->input->get("scRegDtSt");
    	$scRegDtEd 			= $this->input->get("scRegDtEd");
    	$cancel 			= $this->input->get("cancel");
    	$passcodeType 			= $this->input->get("passcodeType");
    	
    	if($dupleInspec == null || $dupleInspec == '') {
    		$dupleInspec = $this->input->get("dupleInspec");
    		$dupleInspecType = $this->input->get("dupleInspecType");
    	}
    	if($sortWhere == null || $sortWhere == '') {
    		$sortWhere = 'updatedate';
    	}
    	if($sortType == null || $sortType == '') {
    		$sortType = 'desc';
    	}
    	$symposium = $this->symposium_model->getSymposium($sympoCd, '');
    	$setting = $this->settings_model->getSympoSettings($sympoCd);
    	$regiList = $this->symposium_model->getRegiListExcel($sympoCd, $passcode, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec, $dupleInspecType, $cancel);
    	
    	$sheet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
    	$this->load->library("PHPExcel");
    	$objPHPExcel = new PHPExcel();
    	$objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->setTitle('워크시트');
    	$cnt = 0;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '순번'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '담당영업소'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '담당자'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '담당자 연락처'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '성명'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '병원명'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '진료과'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '모바일'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '이메일'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '개인정보 서명'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '메모'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '사전등록방식'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '사전등록 완료일'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '사전등록 최종 수정일'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '마지막 수정 담당자'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '패스코드 발급 여부'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '패스코드 발급일'); $cnt++;
    	$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '패스코드 발급 담당자'); $cnt++;
    	if($cancel == 'Y'){
    		$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '참가취소일'); $cnt++;
    		$objPHPExcel->getActiveSheet()->setCellValue($sheet[$cnt].'1', '참가 취소 담당자'); $cnt++;
    	}
    	
    	if(count($regiList) > 0) {
    		foreach ($regiList as $key => $item) {
    			$cnt = 0;
    			$applicant = $this->symposium_model->getApplicantOnly($item['applicantCd']);
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $key+1, PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			if($item['officeName'] != null && $item['officeName'] != '') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['officeName'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			} else {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '-', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['creatorName']. " ".strtoupper($item['creatorAuthority']), PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), str_replace("-", "", $item['creatorHp']), PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['applicantName'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['applicantHospital'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['applicantDep'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), str_replace("-", "", $item['applicantHp']), PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			if($item['applicantEmail'] != null && $item['applicantEmail'] != '') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['applicantEmail'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			} else {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '-', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['regiComplete'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			if($item['comment'] != null && $item['comment'] != '') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['comment'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			} else {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '-', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    			if($item['registType'] == 'PSR') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '예비등록', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			} else if($item['registType'] == 'HCP') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '방문등록', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['createdate'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['updatedate'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['updaterName']. " ".strtoupper($item['updaterAuthority']), PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			if($applicant['passcode'] == 'Y') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '발급', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			} else {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '미발급', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    			if($applicant['passcode'] == 'Y') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['passcodedate'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['passcodeManagerName']. " ".strtoupper($item['passcodeManagerAuthority']), PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			} else {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '-', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), '-', PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    			if($cancel == 'Y') {
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['canceldate'], PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    				$objPHPExcel->getActiveSheet()->setCellValueExplicit($sheet[$cnt] . ($key + 2), $item['cancelManagerName']. " ".strtoupper($item['cancelManagerAuthority']), PHPExcel_Cell_DataType::TYPE_STRING);$cnt++;
    			}
    		}
    	}
    	
    	$title = '';
    	if($passcodeType == 'enrollment') {
    		$title = '등록 관리';
    	} else if($passcodeType == 'ready') {
    		$title = '패스코드 미발급';
    	} else if($passcodeType == 'complete') {
    		$title = '패스코드 발급';
    	} else if($passcodeType == 'cancel') {
    		$title = '참가취소';
    	}
    	
    	if($fileType == 'csv') {
    		$filename = $symposium['eventName'].'_'.$title.'.csv'; // 엑셀 파일 이름
    		header('Content-Type: application/vnd.ms-excel;'); //mime 타입
    		header('Content-Disposition: attachment;filename="' .iconv('utf-8', 'euc-kr', $filename). '"'); // 브라우저에서 받을 파일 이름
    		header('Cache-Control: max-age=0'); //no cache
    		
    		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    	} else {
    		$filename = $symposium['eventName'].'_'.$title.'.xls'; // 엑셀 파일 이름
    		header('Content-Type: application/vnd.ms-excel;'); //mime 타입
    		header('Content-Disposition: attachment;filename="'.iconv('utf-8', 'euc-kr', $filename).'"'); // 브라우저에서 받을 파일 이름
    		header('Cache-Control: max-age=0'); //no cache
    		
    		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    	}
    	$objWriter->save('php://output');
    }
    
    // 서명 PDF 다운
    function pdfDownload(){
    	
    	$applicant_cd = $this->input->get_post('applicant_cd');
    	$applicant = $this->symposium_model->getApplicantOnly($applicant_cd);
    	$filename = $applicant['filename'].$applicant['fileext'];
    	$setting = $this->settings_model->getSympoSettings($applicant['sympo_cd']);
    	$symposium = $this->symposium_model->getSymposium($applicant['sympo_cd'], '');
    	$statistics_term  = date('Y년 m월 d일', strtotime($applicant['applicantRegdate']));
    	
    	$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    	$fontDirs = $defaultConfig['fontDir'];
    	$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    	$fontData = $defaultFontConfig['fontdata'];
    	
    	$mpdf = new \Mpdf\Mpdf([
    			'fontDir' => array_merge($fontDirs, [
    					__DIR__ . '/font/notokr/NotoKR-Regular',
    			]),
    			'fontdata' => $fontData + [
    					'notokr' => [
    							'R' => 'NanumSquareR.ttf',
    							'B' => 'NanumSquareB.ttf',
    					]
    			],
    			'default_font' => 'notokr'
    	]);
    	$mpdf->useAdobeCJK = true;
    	$mpdf->showImageErrors = true;
    	
   	
    	$html = '
<html>
<head>
</head>
<body>
    <div>
        <div style="background-color: #555555; height: 60px;">
            <h1 style="color: #ffffff; font-size: 2rem; font-weight: bold; line-height:60px; padding-top:15px; padding-left:10px;">개인정보 수집 및 활용 동의서</h1>
        </div>
        <div>
            <div>
                <h1 style="font-size: 2rem; font-weight: bold; letter-spacing: normal;">'.$symposium['eventName'].'</h1>
            </div>
            <div>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4rem;">'.$setting['private_desc'].'</p>
            </div>
            <div>
                <p style="font-size: 1.125rem; font-weight: bold;">1. 개인정보의 수집 및 이용에 대한 동의</p>
                <p style="font-size: 1rem; font-weight: bold;">① 수집 및 이용목적:</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 필수 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection_nc'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 선택 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection_se'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">② 수집항목:</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 필수 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_item_nc'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 선택 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_item_se'].'</p>
			</div>
			<div style="margin-top:20px;">
                <p style="font-size: 1.25rem; font-weight: bold; text-decoration: underline;">③ 보유 및 이용기간 :</p>
                <p style="font-size: 1.25rem; text-align: justify; line-height: 1.4; text-decoration: underline;">'.$setting['private_period'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">④ 거부권 및 거부에 따른 불이익:</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_disadvantage'].'</p>
            </div>
            <div style="background-color: #fafafa; border: 1px solid #d5d7db; padding: 1rem; box-sizing: border-box;">
                <div style="padding-bottom: 0.3rem;">
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">필수적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</span>
                </div>
                <div>
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">선택적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</span>
                </div>
            </div>
            <div>
                <section>
                    <h4 style="font-size: 1.125rem; font-weight: bold;">2. 개인정보 및 고유식별정보의 제 3자 제공에 대한 동의</h4>
                </section>
                <section>
                    <figure style="text-align: center;">
                        <img src="'.$setting['private_img'].'" alt="개인정보 수집 동의" style="width: 400px;">
                    </figure>
                </section>
                <p style="font-size: 1rem; font-weight: bold; text-align: justify; line-height: 1.4; padding-top: 1.5rem;">귀하는 위와 같은 개인정보 및 고유식별정보의 제3자 제공을 거부할 수 있습니다. 다만, 개인정보 및 고유식별정보의 제3자 제공에 동의하지 않을 경우 당사의 자사 제품설명회의 참석 및 정보제공, 여행자보험 가입이 제한될 수 있습니다.</p>
            </div>
            <div style="background-color: #fafafa; border: 1px solid #d5d7db; padding: 1rem; margin-top: 1rem; margin-bottom: 2rem; box-sizing: border-box;">
                <div style="padding-bottom: 0.3rem;">
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">제 3자 제공</strong>에 대한 설명을 모두 이해하고, 이에 동의하십니까?</span>
                </div>
            </div>
            <div style="text-align: center; margin-bottom: 1.25rem;">
                <h3 style="font-size: 1.25rem; font-weight: bold;">'.$statistics_term.'</h3>
            </div>
            <div style="padding-top: 1rem;">
                <div style="width: 45%; float: left;">
                    <div style="height: 70px;">
                        <div style="width: 20%; float:left;"><span style="font-size: 1rem; font-weight: bold;">성명 : </span></div>
						<div style="width: 80%; float:right; border-bottom: 1px solid #d5d7db;"><span style="font-size: 1rem;">'.$applicant['applicantName'].'</span></div>
                    </div>
                    <div style="height: 70px;">
						<div style="width: 20%; float:left;"><span style="font-size: 1rem; font-weight: bold;">병원명 : </span></div>
						<div style="width: 80%; float:right; border-bottom: 1px solid #d5d7db;"><span style="font-size: 1rem;">'.$applicant['applicantHospital'].'</span></div>
                    </div>
                </div>
                <div style="width: 50%; float: right;">
                    <div style="width: 20%; float: left;"><span style="font-size: 1rem; font-weight: bold;">서명 :</span></div>
                    <figure style="width: 60%; border: 1px solid #d5d7db; float:left; box-sizing:border-box;">
                        <img src="/uploads/signiture/'.$applicant['filename'].$applicant['fileext'].'" style=" align-items: center; height:120px;">
                    </figure>
                </div>
            </div>
        </div>
    </div>
</body>
</html>';
    	
     	$mpdf->WriteHTML($html);
    	
    	$mpdf->Output(iconv('utf-8', 'euc-kr', $symposium['eventName'].'_'.$applicant['applicantName'].'_'.$applicant['applicantHospital'].'.pdf'), 'D');
    }
    
    // 한개 pdf에 전부 저장
    function signAll(){
    	$sympoCd = $this->input->get_post('sympo_cd');
    	
    	$applicantAllArr = $this->symposium_model->getRegiListPdf($sympoCd);
    	$setting = $this->settings_model->getSympoSettings($sympoCd);
    	$symposium = $this->symposium_model->getSymposium($sympoCd, '');
    	
    	$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    	$fontDirs = $defaultConfig['fontDir'];
    	$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    	$fontData = $defaultFontConfig['fontdata'];
    	$mpdf = new \Mpdf\Mpdf([
    			'fontDir' => array_merge($fontDirs, [
    					__DIR__ . '/font/notokr/NotoKR-Regular',
    			]),
    			'fontdata' => $fontData + [
    					'notokr' => [
    							'R' => 'NanumSquareR.ttf',
    							'B' => 'NanumSquareB.ttf',
    					]
    			],
    			'default_font' => 'notokr'
    	]);
    	$mpdf->useAdobeCJK = true;
    	$mpdf->showImageErrors = true;
    	
    	$html = '<body>';
    	
    	foreach ($applicantAllArr as $applicantAll):
	    	$applicant = $this->symposium_model->getApplicantOnly($applicantAll['applicant_cd']);
	    	$filename = $applicant['filename'].$applicant['fileext'];
	    	$statistics_term  = date('Y년 m월 d일', strtotime($applicant['applicantRegdate']));
	    	
	    	$html .= '
    <div>
        <div style="background-color: #555555; height: 60px;">
            <h1 style="color: #ffffff; font-size: 2rem; font-weight: bold; line-height:60px; padding-top:15px; padding-left:10px;">개인정보 수집 및 활용 동의서</h1>
        </div>
        <div>
            <div>
                <h1 style="font-size: 2rem; font-weight: bold; letter-spacing: normal;">'.$symposium['eventName'].'</h1>
            </div>
            <div>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4rem;">'.$setting['private_desc'].'</p>
            </div>
            <div>
                <p style="font-size: 1.125rem; font-weight: bold;">1. 개인정보의 수집 및 이용에 대한 동의</p>
                <p style="font-size: 1rem; font-weight: bold;">① 수집 및 이용목적:</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 필수 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection_nc'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 선택 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection_se'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">② 수집항목:</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 필수 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_item_nc'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 선택 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_item_se'].'</p>
			</div>
			<div style="margin-top:20px;">
                <p style="font-size: 1.25rem; font-weight: bold; text-decoration: underline;">③ 보유 및 이용기간 :</p>
                <p style="font-size: 1.25rem; text-align: justify; line-height: 1.4; text-decoration: underline;">'.$setting['private_period'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">④ 거부권 및 거부에 따른 불이익:</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_disadvantage'].'</p>
            </div>
            <div style="background-color: #fafafa; border: 1px solid #d5d7db; padding: 1rem; box-sizing: border-box;">
                <div style="padding-bottom: 0.3rem;">
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">필수적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</span>
                </div>
                <div>
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">선택적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</span>
                </div>
            </div>
            <div>
                <section>
                    <h4 style="font-size: 1.125rem; font-weight: bold;">2. 개인정보 및 고유식별정보의 제 3자 제공에 대한 동의</h4>
                </section>
                <section>
                    <figure style="text-align: center;">
                        <img src="'.$setting['private_img'].'" alt="개인정보 수집 동의" style="width: 400px;">
                    </figure>
                </section>
                <p style="font-size: 1rem; font-weight: bold; text-align: justify; line-height: 1.4; padding-top: 1.5rem;">귀하는 위와 같은 개인정보 및 고유식별정보의 제3자 제공을 거부할 수 있습니다. 다만, 개인정보 및 고유식별정보의 제3자 제공에 동의하지 않을 경우 당사의 자사 제품설명회의 참석 및 정보제공, 여행자보험 가입이 제한될 수 있습니다.</p>
            </div>
            <div style="background-color: #fafafa; border: 1px solid #d5d7db; padding: 1rem; margin-top: 1rem; margin-bottom: 2rem; box-sizing: border-box;">
                <div style="padding-bottom: 0.3rem;">
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">제 3자 제공</strong>에 대한 설명을 모두 이해하고, 이에 동의하십니까?</span>
                </div>
            </div>
            <div style="text-align: center; margin-bottom: 1.25rem;">
                <h3 style="font-size: 1.25rem; font-weight: bold;">'.$statistics_term.'</h3>
            </div>
            <div style="padding-top: 1rem;">
                <div style="width: 45%; float: left;">
                    <div style="height: 70px;">
                        <div style="width: 20%; float:left;"><span style="font-size: 1rem; font-weight: bold;">성명 : </span></div>
						<div style="width: 80%; float:right; border-bottom: 1px solid #d5d7db;"><span style="font-size: 1rem;">'.$applicant['applicantName'].'</span></div>
                    </div>
                    <div style="height: 70px;">
						<div style="width: 20%; float:left;"><span style="font-size: 1rem; font-weight: bold;">병원명 : </span></div>
						<div style="width: 80%; float:right; border-bottom: 1px solid #d5d7db;"><span style="font-size: 1rem;">'.$applicant['applicantHospital'].'</span></div>
                    </div>
                </div>
                <div style="width: 50%; float: right;">
                    <div style="width: 20%; float: left;"><span style="font-size: 1rem; font-weight: bold;">서명 :</span></div>
                    <figure style="width: 60%; border: 1px solid #d5d7db; float:left; box-sizing:border-box;">
                        <img src="/uploads/signiture/'.$applicant['filename'].$applicant['fileext'].'" style=" align-items: center; height:120px;">
                    </figure>
                </div>
            </div>
        </div>
    </div>
<pagebreak>';
    	endforeach;
    	
    	$html .= '</body>';
    	$mpdf->WriteHTML($html);
    	$mpdf->Output(iconv('utf-8', 'euc-kr', $symposium['eventName'].'.pdf'), 'D');
    }
    
    // 각각 pdf 압축파일화
    function signAll2(){
    	
    	$sympoCd = $this->input->get_post('sympo_cd');
    	
    	$applicantAllArr = $this->symposium_model->getRegiListPdf($sympoCd);
    	$setting = $this->settings_model->getSympoSettings($sympoCd);
    	$symposium = $this->symposium_model->getSymposium($sympoCd, '');
    	
    	$member_id = $this->session->userdata('member_id');
    	$member_name = $this->session->userdata('member_name');
    	$member_authority = $this->session->userdata('authority');
    	$dir_path = './uploads/'.$sympoCd.'_'.$member_id.'/';
    	mkdir($dir_path,0777);
    	$num = 1;
    	
    	foreach ($applicantAllArr as $applicantAll):
    	$applicant = $this->symposium_model->getApplicantOnly($applicantAll['applicant_cd']);
    	$filename = $applicant['filename'].$applicant['fileext'];
    	$statistics_term  = date('Y년 m월 d일', strtotime($applicant['applicantRegdate']));
    	
    	$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    	$fontDirs = $defaultConfig['fontDir'];
    	$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    	$fontData = $defaultFontConfig['fontdata'];
    	
    	$mpdf = new \Mpdf\Mpdf([
    			'fontDir' => array_merge($fontDirs, [
    					__DIR__ . '/font/notokr/NotoKR-Regular',
    			]),
    			'fontdata' => $fontData + [
    					'notokr' => [
    							'R' => 'NanumSquareR.ttf',
    							'B' => 'NanumSquareB.ttf',
    					]
    			],
    			'default_font' => 'notokr'
    	]);
    	$mpdf->useAdobeCJK = true;
    	$mpdf->showImageErrors = true;
    	
    	
    	$html = '
<body>
    <div>
        <div style="background-color: #555555; height: 60px;">
            <h1 style="color: #ffffff; font-size: 2rem; font-weight: bold; line-height:60px; padding-top:15px; padding-left:10px;">개인정보 수집 및 활용 동의서</h1>
        </div>
        <div>
            <div>
                <h1 style="font-size: 2rem; font-weight: bold; letter-spacing: normal;">'.$symposium['eventName'].'</h1>
            </div>
            <div>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4rem;">'.$setting['private_desc'].'</p>
            </div>
            <div>
                <p style="font-size: 1.125rem; font-weight: bold;">1. 개인정보의 수집 및 이용에 대한 동의</p>
                <p style="font-size: 1rem; font-weight: bold;">① 수집 및 이용목적:</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 필수 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection_nc'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 선택 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_collection_se'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">② 수집항목:</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 필수 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_item_nc'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">■ 선택 :</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_item_se'].'</p>
			</div>
			<div style="margin-top:20px;">
                <p style="font-size: 1.25rem; font-weight: bold; text-decoration: underline;">③ 보유 및 이용기간 :</p>
                <p style="font-size: 1.25rem; text-align: justify; line-height: 1.4; text-decoration: underline;">'.$setting['private_period'].'</p>
                <p style="font-size: 1rem; font-weight: bold;">④ 거부권 및 거부에 따른 불이익:</p>
                <p style="font-size: 1rem; text-align: justify; line-height: 1.4;">'.$setting['private_disadvantage'].'</p>
            </div>
            <div style="background-color: #fafafa; border: 1px solid #d5d7db; padding: 1rem; box-sizing: border-box;">
                <div style="padding-bottom: 0.3rem;">
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">필수적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</span>
                </div>
                <div>
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">선택적 수집</strong> 및 이용에 관한 설명을 모두 이해하고 이에 동의합니다.</span>
                </div>
            </div>
            <div>
                <section>
                    <h4 style="font-size: 1.125rem; font-weight: bold;">2. 개인정보 및 고유식별정보의 제 3자 제공에 대한 동의</h4>
                </section>
                <section>
                    <figure style="text-align: center;">
                        <img src="'.$setting['private_img'].'" alt="개인정보 수집 동의" style="width: 400px;">
                    </figure>
                </section>
                <p style="font-size: 1rem; font-weight: bold; text-align: justify; line-height: 1.4; padding-top: 1.5rem;">귀하는 위와 같은 개인정보 및 고유식별정보의 제3자 제공을 거부할 수 있습니다. 다만, 개인정보 및 고유식별정보의 제3자 제공에 동의하지 않을 경우 당사의 자사 제품설명회의 참석 및 정보제공, 여행자보험 가입이 제한될 수 있습니다.</p>
            </div>
            <div style="background-color: #fafafa; border: 1px solid #d5d7db; padding: 1rem; margin-top: 1rem; margin-bottom: 2rem; box-sizing: border-box;">
                <div style="padding-bottom: 0.3rem;">
                    <img src="/images/checked.png" style="vertical-align: middle; padding-right: 5px;"/><span>개인정보의 <strong style="font-weight: bold;">제 3자 제공</strong>에 대한 설명을 모두 이해하고, 이에 동의하십니까?</span>
                </div>
            </div>
            <div style="text-align: center; margin-bottom: 1.25rem;">
                <h3 style="font-size: 1.25rem; font-weight: bold;">'.$statistics_term.'</h3>
            </div>
            <div style="padding-top: 1rem;">
                <div style="width: 45%; float: left;">
                    <div style="height: 70px;">
                        <div style="width: 20%; float:left;"><span style="font-size: 1rem; font-weight: bold;">성명 : </span></div>
						<div style="width: 80%; float:right; border-bottom: 1px solid #d5d7db;"><span style="font-size: 1rem;">'.$applicant['applicantName'].'</span></div>
                    </div>
                    <div style="height: 70px;">
						<div style="width: 20%; float:left;"><span style="font-size: 1rem; font-weight: bold;">병원명 : </span></div>
						<div style="width: 80%; float:right; border-bottom: 1px solid #d5d7db;"><span style="font-size: 1rem;">'.$applicant['applicantHospital'].'</span></div>
                    </div>
                </div>
                <div style="width: 50%; float: right;">
                    <div style="width: 20%; float: left;"><span style="font-size: 1rem; font-weight: bold;">서명 :</span></div>
                    <figure style="width: 60%; border: 1px solid #d5d7db; float:left; box-sizing:border-box;">
                        <img src="/uploads/signiture/'.$applicant['filename'].$applicant['fileext'].'" style=" align-items: center; height:120px;">
                    </figure>
                </div>
            </div>
        </div>
    </div>
</body>';
    	
    	$mpdf->WriteHTML($html);
    	$mpdf->Output( $dir_path.$symposium['eventName'].'_'.$applicant['applicantName'].'_'.$applicant['applicantHospital'].'_'.$num.'.pdf', 'F');
    	$num++;
    	endforeach;
    	
    	$path = './uploads/';
    	$zip_name = $symposium['eventName'].'_'.$member_name.'_'.$member_authority.'.zip';
    	fopen($path.$zip_name , 'w+');
    	chmod($path.$zip_name , 0777);
    	
    	$zip = new ZipArchive;
    	if($zip->open($path.$zip_name, ZipArchive::CREATE || ZipArchive::OVERWRITE)===TRUE){
    		$dir = opendir($dir_path);
    		
    		while($file = readdir($dir)) {
    			if(is_file($dir_path.$file)) {
    				$zip -> addFile($dir_path.$file,$file);
    			}
    		}
    		
    		$zip->close();
    		
    		header("content-type: application/attachment");
    		header("content-length: ".filesize("$path$zip_name"));
    		header("content-disposition: attachment; filename=\"$zip_name\"");
    		header("content-transfer-encoding: binary");
    		@readfile("$path$zip_name");
    	}
    	
    	$dir = opendir($dir_path);
    	while($file = readdir($dir)) {
    		if(is_file($dir_path.$file)) {
    			unlink($dir_path.$file);
    		}
    	}
    	rmdir($dir_path);
    	unlink($path.$zip_name);
    	
    }
    
    // 심포지엄 업데이트
    function symposiumUpdate() {

        $sympoCd                    = $this->input->post("sympoCd");
        $sympoType                  = $this->input->post("sympoType");
        $gubun                  	= $this->input->post("gubun");
        $eventPreRegStartDateTime   = $this->input->post("eventPreRegStartDateTime");
        $eventPreRegEndDateTime     = $this->input->post("eventPreRegEndDateTime");
        $presenter                  	= $this->input->post("presenter");
        $brand_group_cds            = $this->input->post("brand_group_cds");
        $member_id                  = $this->session->userdata('member_id');
        $authority 					= $this->session->userdata('authority');

        $dataArr = array(
        	'sympo_type'                 => $sympoType,
        	'gubun'						 => $gubun,
            'event_PreRegStartDateTime'  => $eventPreRegStartDateTime,
            'event_PreRegEndDateTime'    => $eventPreRegEndDateTime,
        	'presenter'                   => $presenter,
            'updater'                    => $member_id,
            'updatedate'                 => 'NOW()'
        );

        $saveData = $this->symposium_model->sympoUpdate($sympoCd, $dataArr);

        
        if($authority == 'mcm' || $authority == 'op') {
        	
        	$this->symposium_model->sympoGroupDelete($sympoCd);
		
	        $brand_group_cdsArr = explode(";", $brand_group_cds);
	
	        foreach ($brand_group_cdsArr as $brand_group_cd):
	            if($brand_group_cd != null && $brand_group_cd != '') {
	            	
	            	$sympoGroupCount = $this->symposium_model->getCheckSympoGroup($sympoCd,$brand_group_cd);
	            	
	            	if($sympoGroupCount == 0)
	            	{
		                $dataArr1 = array(
		                    'brand_group_cd' => $brand_group_cd,
		                    'sympo_cd' => $sympoCd,
		                    'creator' => $member_id,
		                    'updater' => $member_id
		                );
		
		                $res1 = $this->common_model->getSequences("bgs");
		                $dataArr1['brand_group_sympo_cd'] = str_pad($res1['currval'], 20, "BGS_0000000000000000", STR_PAD_LEFT);
		                $this->db->insert('brand_group_sympo', $dataArr1);
	            	}
	            }
	        endforeach;
		}

        $symposium = $this->symposium_model->getSymposium($sympoCd, '');

        $sympo_to_brand = $this->symposium_model->getSympoToBrand($sympoCd);

        $arr = array('result' => 'success', 'data' => $symposium, 'brand' => $sympo_to_brand);

        echo json_encode($arr);
    }
}