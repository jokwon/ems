<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: complete
 * Date: 2018. 8. 24.
 * Time: PM 12:09
 */

class Statistics extends CI_Controller {

    private $start = 0;
    private $scale = 8;
    private $page_scale = 10;

    private $base_uri = '';

    private $board = [];
    private $board_id = 'notice';

    private $head = array(
        'gpage' => '0601'
    );

    private $footer = array(
        'js' => array()
    );

    private $data = array();

    private $return_json = false;
    private $return_array = array(
        'status' => '500'
    ,'msg' => '잘못된 접근입니다.'
    ,'url' => ''
    ,'reload' => 'off'
    ,'arr' => array()
    );


    function __construct() {
        parent::__construct();

        $this->head['left'] = "statistics";

        $this->load->model(array('member_model', 'settings_model','common_model','main_model'));

        $start = preg_replace('/[^0-9]/', '', $this->input->get_post('start'));
        if( !empty($start) ) $this->start = intval($start);

        $scale = preg_replace('/[^0-9]/', '', $this->input->get_post('scale'));
        if( !empty($scale) ) $this->scale = intval($scale);

        $page_scale = preg_replace('/[^0-9]/', '', $this->input->get_post('page_scale'));
        if( !empty($page_scale) ) $this->page_scale = intval($page_scale);


        $this->base_uri = '/'.strtolower(__CLASS__);
        $this->data['base_uri'] = $this->base_uri;

        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $row = $this->settings_model->getSettings('');
        $this->head['customer_name'] = $row['customer_name'];
        $this->head['symposium_left'] = $row['symposium_left'];

        $this->head['term_desc'] = $row['term_desc'];
        $this->head['private_desc'] = $row['private_desc'];
        
        $authority_now = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        $authority_db = $this->main_model->getAuthority($member_cd);
        if($authority_now != null && $authority_now != '')
        {
        	if($authority_now != $authority_db['authority'])
        	{
        		alert("권한이 변경되었습니다 다시 로그인 해주세요",HOME_DIR."/login/logout");
        	}
        	
        }
    }

    function __destruct() {
        if( $this->return_json === true && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            echo json_encode($this->return_array);
    }

    function index() {
    	$this->login_log();
    }

	// 로그인 로그 리스트
    function login_log() {
        $authority = $this->session->userdata('authority');
        if($authority != 'op' && $authority != 'mcm') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $authType = $this->input->get('authType');
        $searchText = $this->input->get('searchText', true);
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $sortWhere = $this->input->get('sortWhere');
        $sortType = $this->input->get('sortType');

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'log_date';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        if($authType == null || $authType == '') {
            $authType = 'all';
        }


        $logType = 'login';

        $getResult = $this->settings_model->getLogList(1, $sortWhere, $sortType, $startdate, $enddate, $authType, $searchText, $logType);

        $this->data['log_list'] = $getResult['list'];
        $this->data['log_count'] = $getResult['count'];

        $this->data['authType'] = $authType;
        $this->data['searchText'] = $searchText;
        $this->data['startdate'] = $startdate;
        $this->data['enddate'] = $enddate;

        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;

        $this->load->view('head',$this->head);
        $this->load->view('statistics/login_log',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 로그인 로그 리스트 더보기
    function loginLogAjax() {

        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $page           = $this->input->post("page");
        $authType       = $this->input->post("authType");
        $searchText     = $this->input->post('searchText', true);
        $startdate      = $this->input->post('startdate');
        $enddate        = $this->input->post('enddate');
        $sortWhere      = $this->input->post('sortWhere');
        $sortType       = $this->input->post('sortType');

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'log_date';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        if($authType == null || $authType == '') {
            $authType = 'all';
        }

        $logType = 'login';

        $getResult = $this->settings_model->getLogList($page, $sortWhere, $sortType, $startdate, $enddate, $authType, $searchText, $logType);

        echo json_encode(array("data"=>$getResult['list'], "page"=>$page, "cnt"=>$getResult['count']));
    }

    // 권한 부여 로그 리스트
    function authorization_log() {

        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $authType = $this->input->get('authType');
        $searchText = $this->input->get('searchText', true);
        $startdate = $this->input->get('startdate');
        $enddate = $this->input->get('enddate');
        $sortWhere = $this->input->get('sortWhere');
        $sortType = $this->input->get('sortType');

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'log_date';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        if($authType == null || $authType == '') {
            $authType = 'all';
        }

        $logType = 'auth';

        $getResult = $this->settings_model->getLogList(1, $sortWhere, $sortType, $startdate, $enddate, $authType, $searchText, $logType);

        $this->data['log_list'] = $getResult['list'];
        $this->data['log_count'] = $getResult['count'];

        $this->data['authType'] = $authType;
        $this->data['searchText'] = $searchText;
        $this->data['startdate'] = $startdate;
        $this->data['enddate'] = $enddate;

        $this->data['sortWhere'] = $sortWhere;
        $this->data['sortType'] = $sortType;

        $this->load->view('head',$this->head);
        $this->load->view('statistics/authorization_log',$this->data);
        $this->load->view('footer', $this->footer);
    }

    // 권한부여 로그 리스트 더보기
    function AuthorizationLogAjax() {

        $authority = $this->session->userdata('authority');
        if($authority != 'mcm' && $authority != 'op') {
            alert('권한이 없습니다.', HOME_DIR.'/main');
        }

        $page           = $this->input->post("page");
        $authType       = $this->input->post("authType");
        $searchText     = $this->input->post('searchText', true);
        $startdate      = $this->input->post('startdate');
        $enddate        = $this->input->post('enddate');
        $sortWhere      = $this->input->post('sortWhere');
        $sortType       = $this->input->post('sortType');

        if($sortWhere == null || $sortWhere == '') {
            $sortWhere = 'log_date';
        }

        if($sortType == null || $sortType == '') {
            $sortType = 'desc';
        }

        if($authType == null || $authType == '') {
            $authType = 'all';
        }

        $logType = 'auth';

        $getResult = $this->settings_model->getLogList($page, $sortWhere, $sortType, $startdate, $enddate, $authType, $searchText, $logType);

        echo json_encode(array("data"=>$getResult['list'], "page"=>$page, "cnt"=>$getResult['count']));
    }
}