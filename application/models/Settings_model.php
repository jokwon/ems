<?php
/**
 * Created by PhpStorm.
 * User: 임창호
 * Date: 2017-09-11
 * Time: 오후 5:00
 */

class Settings_model extends CI_Model {
	
	private $where = array();
	
	function __construct() {
		parent::__construct();
		$this->db->query("SET time_zone='+9:00'");
	}
	
	
	function getSettings() {
		$set_query = ("
            SELECT
                setting_cd,
                send_way,
                customer_name,
                application_img,
                filename,
                fileext,
                symposium_desc,
                symposium_left,
				symposium_info,
				agency_info,
                dashboard_num1,
                term_desc,
                private_desc,
                btn1Yn,
                btn2Yn,
                btn3Yn,
                btn4Yn,
                btn5Yn,
                sympo_cd,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate
            FROM settings
            WHERE 1=1
            AND sympo_cd = 'system'
        ");
		
		
		$result = $this->db->query($set_query);
		$result_list = $result->row_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 단일심포지엄 전체 세팅 내역 가져오는 쿼리
	function getSympoSettings($sympoCd) {
		$set_query = ("
            SELECT
                setting_cd,
                send_way,
                customer_name,
                application_img,
                filename,
                fileext,
                symposium_desc,
                symposium_left,
                dashboard_num1,
                term_desc,
                agency_info,
                symposium_info,
                template_onoff,
                btn1Yn,
                btn2Yn,
                btn3Yn,
                btn4Yn,
                btn5Yn,
                sympo_cd,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate,
				content_subject1,
				content_img1,
				content_filename1,
				content_fileext1,
				content_onoff1,
				content_subject2,
				content_img2,
				content_filename2,
				content_fileext2,
				content_onoff2,
                private_desc,
				private_collection,
				private_collection_nc,
				private_collection_se,
				private_item_nc,
				private_item_se,
				private_period,
				private_disadvantage,
				private_img,
				private_filename,
				private_fileext
            FROM settings
            WHERE 1=1
            AND sympo_cd = ?
        ");
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->row_array();
		$result->free_result();
		
		if(count($result_list) > 0) {
			return $result_list;
		} else {
			$set_query = ("
            SELECT
                setting_cd,
                send_way,
                customer_name,
                application_img,
                filename,
                fileext,
                symposium_desc,
                symposium_left,
                dashboard_num1,
                term_desc,
                agency_info,
                symposium_info,
                template_onoff,
                btn1Yn,
                btn2Yn,
                btn3Yn,
                btn4Yn,
                btn5Yn,
                sympo_cd,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate,
				content_subject1,
				content_img1,
				content_filename1,
				content_fileext1,
				content_onoff1,
				content_subject2,
				content_img2,
				content_filename2,
				content_fileext2,
				content_onoff2,
                private_desc,
				private_collection,
				private_collection_nc,
				private_collection_se,
				private_item_nc,
				private_item_se,
				private_period,
				private_disadvantage,
				private_img,
				private_filename,
				private_fileext
            FROM settings
            WHERE 1=1
            AND sympo_cd = 'system'
        ");
			
			$result = $this->db->query($set_query, $sympoCd);
			$result_list = $result->row_array();
			$result->free_result();
			return $result_list;
		}
		
	}
	
	function getSympoSettingTemplate($sympoCd) {
		
		$set_query = ("
			SELECT
			    setting_template_cd		AS setting_template_cd
			    ,application_img		AS application_img
			    ,creator      			AS creator
			    ,createdate         	AS createdate
			    ,updater   				AS updater
			    ,updatedate        		AS updatedate
			    ,sympo_cd 				AS sympo_cd
			    ,sub_num 				AS sub_num
			    ,onoff             		AS onoff
			    ,filename          		AS filename
			    ,fileext            	AS fileext
			FROM setting_template
			WHERE 1=1
			AND sympo_cd = ?
		");
		
		$set_query .= ' ORDER BY onoff desc, sub_num asc';
		
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	
	function update($sympo_cd, $data = array()) {
		
		foreach($data as $key => $value) {
			if($key == 'updatedate' || $key == 'createdate') {
				$this->db->set($key, 'now()', false);
			} else {
				$this->db->set($key, $value);
			}
		}
		
		$this->db->where('sympo_cd', $sympo_cd);
		
		return $this->db->update('settings');
	}
	
	function updateTemplate($sympo_cd, $data = array(),$setting_template_cd) {
		
		foreach($data as $key => $value) {
			if($key == 'updatedate' || $key == 'createdate') {
				$this->db->set($key, 'now()', false);
			} else {
				$this->db->set($key, $value);
			}
		}
		
		$this->db->where('sympo_cd', $sympo_cd);
		$this->db->where('setting_template_cd', $setting_template_cd);
		
		return $this->db->update('setting_template');
	}
	
	function insert($data = array()) {
		foreach($data as $key => $value) {
			if($key == 'updatedate' || $key == 'createdate') {
				$this->db->set($key, 'now()', false);
			} else {
				$this->db->set($key, $value);
			}
		}
		$this->db->insert("settings");
		return $this->db->insert_id();
	}
	
	function getLogList($page, $sortWhere, $sortType, $startdate, $enddate, $authType, $searchText, $logType) {
		$this->where = array();
		
		$readCount = 10;
		$startCount = ($page-1) * $readCount;
		
		$set_query = ("
			SELECT
                A.log_type,
                A.log_content,
                A.log_date,
                A.log_from,
                A.log_to,
                A.log_ip,
                (SELECT authority FROM member WHERE member_id = A.log_to) AS to_authority,
                (SELECT member_name FROM member WHERE member_id = A.log_to) AS to_member_name,
                (SELECT authority FROM member WHERE member_id = A.log_from) AS from_authority,
                (SELECT member_name FROM member WHERE member_id = A.log_from) AS from_member_name
            FROM log A
            WHERE 1=1
    	");
		
		if($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
			$set_query .= ' AND DATE_FORMAT(A.log_date, \'%Y-%m-%d\') >= DATE_FORMAT(\''.$startdate.'\', \'%Y-%m-%d\')';
			$set_query .= ' AND DATE_FORMAT(A.log_date, \'%Y-%m-%d\') <= DATE_FORMAT(\''.$enddate.'\', \'%Y-%m-%d\')';
		}
		
		if($logType != null && $logType != '' ) {
			$set_query .= ' AND A.log_type = ?';
			array_push($this->where, $logType);
		}
		
		if($authType != '' && $authType != 'all' ) {
			$set_query .= ' AND search_auth = ?';
			array_push($this->where, $authType);
		}
		
		if($searchText !=  null && $searchText != '') {
			$set_query .= ' AND ((SELECT member_name FROM member WHERE member_id = A.log_to) LIKE CONCAT("%",?,"%") OR (SELECT member_name FROM member WHERE member_id = A.log_from) LIKE CONCAT("%",?,"%"))';
			array_push($this->where, $searchText);
			array_push($this->where, $searchText);
		}
		
		$getTotal = $this->getLogCount($startdate, $enddate, $authType, $searchText, $logType);
		
		$set_query .= ' ORDER BY A.'.$sortWhere.' '.$sortType.' ';
		
		$set_query .= ' LIMIT ' .$startCount.', '.$readCount;
		
		
		$result = $this->db->query($set_query, $this->where);
		$result_list = $result->result_array();
		$result->free_result();
		
		$tmp_arr = array();
		foreach($result_list as $row) {
			array_push($tmp_arr, $row);
		}
		
		return array(
				'count' => $getTotal['cnt'],
				'list' => $tmp_arr
		);
	}
	
	
	function getLogCount($startdate, $enddate, $authType, $searchText, $logType) {
		
		$where = array();
		
		$set_query = ("
			SELECT
				COUNT(*) AS cnt
			FROM log A
			WHERE 1 = 1
    	");
		
		if($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
			$set_query .= ' AND DATE_FORMAT(A.log_date, \'%Y-%m-%d\') >= DATE_FORMAT(\''.$startdate.'\', \'%Y-%m-%d\')';
			$set_query .= ' AND DATE_FORMAT(A.log_date, \'%Y-%m-%d\') <= DATE_FORMAT(\''.$enddate.'\', \'%Y-%m-%d\')';
		}
		
		if($logType != null && $logType != '' ) {
			$set_query .= ' AND A.log_type = ?';
			array_push($where, $logType);
		}
		
		if($authType != '' && $authType != 'all' ) {
			$set_query .= ' AND  (SELECT authority FROM member WHERE member_id = A.log_to) = ?';
			array_push($where, $authType);
		}
		
		if($searchText !=  null && $searchText != '') {
			$set_query .= ' AND (SELECT member_name FROM member WHERE member_id = A.log_to) LIKE CONCAT("%",?,"%")';
			array_push($where, $searchText);
		}
		
		$result = $this->db->query($set_query, $where);
		$result_row = $result->row_array();
		$result->free_result();
		
		return array(
				'cnt' => intval($result_row['cnt'])
		);
	}
	
	
	function getSettingsList($page) {
		
		$readCount = 10;
		$startCount = ($page-1) * $readCount;
		
		
		$set_query = ("
            SELECT
                setting_cd,
                send_way,
                customer_name,
                symposium_desc,
                symposium_left,
                dashboard_num1,
                term_desc,
                sympo_cd,
                (SELECT event_name FROM symposium_info WHERE sympo_cd = A.sympo_cd) AS event_name,
                (SELECT DATE_FORMAT(event_start_date, '%Y.%m.%d') FROM symposium_info WHERE sympo_cd = A.sympo_cd) AS event_start_date,
                (SELECT DATE_FORMAT(event_end_date, '%Y.%m.%d') FROM symposium_info WHERE sympo_cd = A.sympo_cd) AS event_end_date,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate
            FROM settings A
            WHERE 1=1
            AND sympo_cd != 'system'
            AND application_img is not null
        ");
		
		
		$getTotal = $this->getSettingsListCount();
		
		$set_query .= ' ORDER BY setting_cd DESC ';
		
		$set_query .= ' LIMIT ' .$startCount.', '.$readCount;
		
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		$tmp_arr = array();
		foreach($result_list as $row) {
			array_push($tmp_arr, $row);
		}
		
		return array(
				'count' => $getTotal['cnt']
				,'list' => $tmp_arr
		);
	}
	
	function getSettingsListCount($params = array()) {
		
		$set_query = ("
			SELECT
				COUNT(*) AS cnt
            FROM settings A
            WHERE 1=1
            AND sympo_cd != 'system'
            AND application_img is not null
    	");
		
		$result = $this->db->query($set_query, $this->where);
		$result_row = $result->row_array();
		$result->free_result();
		
		return array(
				'cnt' => intval($result_row['cnt'])
		);
	}
	
	function getSettingImg() {
		$set_query = ("
			SELECT
				application_img
            FROM settings A
            WHERE 1=1
            AND sympo_cd = 'system'
    	");
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	function getSendWay($sympoCd) {
		$set_query = ("
            SELECT
                setting_cd,
                send_way,
                customer_name,
                application_img,
                symposium_desc,
                symposium_left,
                dashboard_num1,
                term_desc,
                sympo_cd,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate
            FROM settings
            WHERE 1=1
            AND sympo_cd = ? OR sympo_cd = 'system'
            ORDER by setting_cd DESC
            LIMIT 1
        ");
		
		if($sympoCd == null || $sympoCd == '') {
			$sympoCd = 'system';
		}
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->row_array();
		$result->free_result();
		
		return $result_list;
	}
	
	
	
	
	
	function getSettingTemplateList($params = array()) {
		$this->where = array();
		
		$readCount = 10;
		$startCount = ($params['page']-1) * $readCount;
		
		$set_query = ("
            SELECT
                setting_cd,
                application_img,
                filename,
                fileext,
                sympo_cd,
                (SELECT event_name FROM symposium_info WHERE sympo_cd = A.sympo_cd) AS event_name,
                (SELECT DATE_FORMAT(event_start_date, '%Y.%m.%d') FROM symposium_info WHERE sympo_cd = A.sympo_cd) AS event_start_date,
                (SELECT DATE_FORMAT(event_end_date, '%Y.%m.%d') FROM symposium_info WHERE sympo_cd = A.sympo_cd) AS event_end_date,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate
            FROM settings A
            WHERE 1=1
            AND sympo_cd != 'system'
            AND application_img is not null
        ");
		
		if( !empty($params['searchText']) ) {
			if($params['searchType'] == 'application_title') {
				$set_query .= ' AND application_title LIKE CONCAT("%' . $params['searchText'] . '%")';
			} else if($params['searchType'] == 'event_name') {
				$set_query .= ' AND (SELECT event_name FROM symposium_info WHERE sympo_cd = A.sympo_cd) LIKE CONCAT("%' . $params['searchText'] . '%")';
			}
		}
		
		$getTotal = $this->getSettingsTemplateCount();
		
		if($params['sortWhere'] == 'application_title') {
			$set_query .= ' ORDER BY A.application_title '.$params['sortType'];
		} else if($params['sortWhere'] == 'event_start_date') {
			$set_query .= ' ORDER BY (SELECT DATE_FORMAT(event_start_date, \'%Y.%m.%d\') FROM symposium_info WHERE sympo_cd = A.sympo_cd) '.$params['sortType'];
		} else if($params['sortWhere'] == 'createdate') {
			$set_query .= ' ORDER BY createdate '.$params['sortType'];
		}
		
		$set_query .= ' LIMIT ' .$startCount.', '.$readCount;
		
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		$tmp_arr = array();
		foreach($result_list as $row) {
			array_push($tmp_arr, $row);
		}
		
		return array(
				'count' => $getTotal['cnt']
				,'list' => $tmp_arr
		);
	}
	
	function getSettingsTemplateCount($params = array()) {
		
		$set_query = ("
			SELECT
				COUNT(*) AS cnt
            FROM settings A
            WHERE 1=1
            AND sympo_cd != 'system'
            AND application_img is not null
    	");
		
		$result = $this->db->query($set_query, $this->where);
		$result_row = $result->row_array();
		$result->free_result();
		
		return array(
				'cnt' => intval($result_row['cnt'])
		);
	}
}