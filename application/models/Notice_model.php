<?php
/**
 * Created by PhpStorm.
 * User: 임창호
 * Date: 2017-09-11
 * Time: 오후 5:00
 */

class Notice_model extends CI_Model {

    private $where = array();

    function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+9:00'");
    }

    function delete($noticeCd = '') {
        $this->db->where('notice_cd', $noticeCd);
        return $this->db->delete("notice");
    }
    
    function noticeGroupDelete($notice_cd,$brand_group_cd) {
    	$this->db->where('notice_cd', $notice_cd);
    	if($brand_group_cd != null && $brand_group_cd != ''){
    		$this->db->where('brand_group_cd', $brand_group_cd);
    	}
    	return $this->db->delete("brand_group_notice");
    }

    // 공지사항 메인화면
    function getnoticeList($page, $searchType, $searchText, $sortWhere, $sortType) {
        $this->where = array();

        $smember_cd = $this->session->userdata('member_cd');
        $authority = $this->session->userdata('authority');

        $readCount = 10;
        $startCount = ($page-1) * $readCount;

        $set_query = ("
			SELECT 
                A.notice_cd AS noticeCd
				,(SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR ',') FROM brand_group_notice E where E.notice_cd = A.notice_cd) AS groupNames
			    , A.notice_title AS noticeTitle
			    , A.notice_content AS noticeContent
			    , A.notice_filename AS noticeFilename
			    , A.notice_file_ext AS noticeFileExt
			    , A.notice_regdate AS noticeRegdate
                , A.updatedate AS updatedate
			FROM notice A
            WHERE 1 = 1
    	");

        if($searchText != null && $searchText != '') {
        	
        	if($searchType == 'brand_group_name') {
        		$set_query .= ' AND (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR \',\') FROM brand_group_notice E where E.notice_cd = A.notice_cd) LIKE CONCAT("%","'.$searchText.'","%")';
        	}else {
        		$set_query .= ' AND notice_title LIKE CONCAT("%","'.$searchText.'","%")';
        	}
        }

        if($authority == 'pm' || $authority == 'mr' || $authority == 'ag') {
            $set_query .= ' AND notice_cd in (SELECT notice_cd FROM brand_group_notice WHERE brand_group_cd in (SELECT brand_group_cd FROM brand_group_member WHERE member_cd = \''.$smember_cd.'\'))';
        }

        $set_query .= ' ORDER BY';
        
        $set_query .= ' A.'.$sortWhere.' '.$sortType.'';

        $set_query .= ' LIMIT ' .$startCount.', '.$readCount;

        $result = $this->db->query($set_query, $this->where);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    // 공지사항 갯수
    function getNoticeCount($searchType, $searchText, $sortWhere, $sortType) {

        $smember_cd = $this->session->userdata('member_cd');
        $authority = $this->session->userdata('authority');

        $set_query = ("
			SELECT 
				COUNT(A.notice_cd) AS cnt
			FROM notice A
				WHERE 1 = 1 
    	");

        if($searchText != null && $searchText != '') {
        	
        	if($searchType == 'brand_group_name') {
        		$set_query .= ' AND (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR \',\') FROM brand_group_notice E where E.notice_cd = A.notice_cd) LIKE CONCAT("%","'.$searchText.'","%")';
        	}else {
        		$set_query .= ' AND notice_title LIKE CONCAT("%","'.$searchText.'","%")';
        	}
        }

        if($authority == 'pm' || $authority == 'mr' || $authority == 'ag') {
        	$set_query .= ' AND notice_cd in (SELECT notice_cd FROM brand_group_notice WHERE brand_group_cd in (SELECT brand_group_cd FROM brand_group_member WHERE member_cd = \''.$smember_cd.'\'))';
        }
        
        $set_query .= ' ORDER BY '.$sortWhere.' '.$sortType.' ';

        $result = $this->db->query($set_query);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    //공지사항 하나 상세정보
    function getNoticeDetail($notice_cd) {
        $this->where = array();

        $set_query = ("
			SELECT 
				A.notice_cd AS noticeCd
			    , A.notice_title AS noticeTitle
			    , A.notice_content AS noticeContent
				,(SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR ',') FROM brand_group_notice E where E.notice_cd = A.notice_cd) AS groupNames
			    , A.notice_filename AS noticeFilename
			    , A.notice_file_ext AS noticeFileExt
			    , A.notice_regdate AS noticeRegdate
                , A.updatedate AS updatedate
			FROM notice A 
			WHERE 1 = 1 
			AND A.notice_cd = ?
    	");

        $result = $this->db->query($set_query, $notice_cd);
        $result_row = $result->row_array();
        $result->free_result();

        if( empty($result_row) )
            return array();

        return $result_row;
    }

    // 메인화면 공지사항 2개
    function getLimitNoticeList() {
        $this->where = array();

        $smember_cd = $this->session->userdata('member_cd');
        $authority = $this->session->userdata('authority');

        $set_query = ("
			SELECT 
				A.notice_cd AS noticeCd
			    , notice_title AS noticeTitle
			    ,(SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR ',') FROM brand_group_notice E where E.notice_cd = A.notice_cd) AS groupNames
			    , DATE_FORMAT(A.notice_regdate, '%Y-%m-%d') AS noticeRegdate
			FROM notice A
			WHERE 1 = 1
			
    	");

        if($authority == 'pm' || $authority == 'mr' || $authority == 'ag') {
        	$set_query .= ' AND notice_cd in (SELECT notice_cd FROM brand_group_notice WHERE brand_group_cd in (SELECT brand_group_cd FROM brand_group_member WHERE member_cd = \''.$smember_cd.'\'))';
        }

        $set_query .= ' order by A.updatedate desc LIMIT 2';

        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }
    
    // 브랜드별 공지사항 갯수
    function getCheckNoticeGroup($notice_cd,$brand_group_cd) {
    	
    	$set_query = ("
			SELECT
				COUNT(*) AS cnt
			FROM brand_group_notice
			WHERE notice_cd = '".$notice_cd."'
			AND brand_group_cd = '".$brand_group_cd."'
    	");
    	
    	$result = $this->db->query($set_query);
    	$result_row = $result->row_array();
    	$result->free_result();
    	
    	return empty($result_row['cnt']) ? 0 : intval($result_row['cnt']);
    }
    
    // 현재 공지사항이 속해있는 브랜드의 그룹cd
    function currentNoticeGroups($notice_cd) {
    	
    	$set_query = ("
			SELECT
                brand_group_cd
            FROM brand_group_notice
            WHERE 1=1
            AND notice_cd = '".$notice_cd."'
    	");
    	
    	$result = $this->db->query($set_query);
    	$result_list = $result->result_array();
    	$result->free_result();
    	
    	return $result_list;
    }
    
    // 현재 공지사항이 속해있는 브랜드의 그룹cd + 브랜드 그룹이랑 join
    function getNoticeRelGroup($notice_cd) {
    	$set_query = ("
			SELECT
                B.brand_group_name as brand_group_name,
                A.brand_group_notice_cd as brand_group_notice_cd,
                A.brand_group_cd as brand_group_cd,
                A.notice_cd as notice_cd
            FROM brand_group_notice A, brand_group B
            WHERE 1=1
            AND A.brand_group_cd = B.brand_group_cd
            AND A.notice_cd = ?
    	");
    	
    	$result = $this->db->query($set_query, $notice_cd);
    	$result_list = $result->result_array();
    	$result->free_result();
    	
    	return $result_list;
    }
    
}