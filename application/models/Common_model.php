<?php
/**
 * Created by PhpStorm.
 * User: 임창호
 * Date: 2017-09-11
 * Time: 오후 5:00
 */

class Common_model extends CI_Model {

    private $where = array();

    function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+9:00'");
    }


    function getSequences($type){
        $qry_res    = $this->db->query("CALL nextval('{$type}')");
        $res       = $qry_res->row_array();

        $qry_res->next_result();
        $qry_res->free_result();

        return $res;
    }


    function logInsert($data = array()) {

        foreach($data as $key => $value) {
            $this->db->set($key, $value);
        }

        $this->db->insert("log");

        return $this->db->insert_id();
    }
}