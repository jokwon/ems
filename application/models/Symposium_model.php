<?php
/**
 * Created by PhpStorm.
 * User: 임창호
 * Date: 2017-09-11
 * Time: 오후 5:00
 */

class Symposium_model extends CI_Model {
	
	private $where = array();
	
	function __construct() {
		parent::__construct();
		$this->db->query("SET time_zone='+9:00'");
	}
	
	// 심포지엄 생성 시 심포지엄 있는지 비교
	function dupleCheck($evtId, $keyType) {
		$set_query = ("
				SELECT
				count(*) as count
				FROM symposium_info
				WHERE 1 = 1
				AND event_id = $evtId
				AND api_type = $keyType
				");
		
				$result = $this->db->query($set_query);
				$result_row = $result->row_array();
				
				return $result_row;
	}
	
	// 심포지엄 생성
	function insert($data = array()) {
		foreach($data as $key => $value) {
			$this->db->set($key, $value);
		}
		$this->db->insert("symposium_info");
		return $this->db->insert_id();
	}
	
	// 심포지엄 삭제
	function sympoDelete1($sympo_cd) {
		$this->db->where('sympo_cd', $sympo_cd);
		return $this->db->delete("setting_template");
	}
	function sympoDelete2($sympo_cd) {
		$this->db->where('sympo_cd', $sympo_cd);
		return $this->db->delete("settings");
	}
	function sympoDelete3($sympo_cd) {
		$this->db->where('sympo_cd', $sympo_cd);
		return $this->db->delete("applicant");
	}
	function sympoDelete4($sympo_cd) {
		$this->db->where('sympo_cd', $sympo_cd);
		return $this->db->delete("brand_group_sympo");
	}
	function sympoDelete5($sympo_cd) {
		$this->db->where('sympo_cd', $sympo_cd);
		return $this->db->delete("symposium_info");
	}
	
	function testupdate($applicantCd = '', $data = array()) {
		foreach($data as $key => $value) {
			if($key == 'updatedate') {
				$this->db->set($key, 'now()', false);
			} else {
				$this->db->set($key, $value);
			}
		}
		
		$this->db->where('applicant_cd', $applicantCd);
		return $this->db->update("applicant");
	}
	
	// 사전등록 페이지 완료시
	function applyHcpInsert($data = array()) {
		foreach($data as $key => $value) {
			$this->db->set($key, $value);
		}
		$this->db->insert("applicant");
		return $this->db->insert_id();
	}
	
	// passcode 정보 업데이트
	function passcodeUpdate($applicantCd) {
		$passcodedate = date("Y-m-d H:i:s",time()+32400);
		$member_id = $this->session->userdata('member_id');
		$this->db->set('passcode', 'Y');
		$this->db->set('passcodedate', $passcodedate);
		$this->db->set('passcode_manager', $member_id);
		$this->db->where('applicant_cd', $applicantCd);
		return $this->db->update("applicant");
	}
	
	// passcode 취소
	function passcodeCancel($applicantCd) {
		$updatedate = date("Y-m-d H:i:s",time()+32400);
		$member_id = $this->session->userdata('member_id');
		$this->db->set('cancel', 'Y');
		$this->db->set('cancel_manager', $member_id);
		$this->db->set('canceldate', $updatedate);
		$this->db->where('applicant_cd', $applicantCd);
		return $this->db->update("applicant");
	}
	
	// passcode 복원
	function passcodeRestore($applicantCd) {
		$updatedate = date("Y-m-d H:i:s",time()+32400);
		$this->db->set('cancel', 'N');
		$this->db->set('passcode', 'N');
		$this->db->set('passcodedate', null);
		$this->db->set('passcode_manager', null);
		$this->db->set('canceldate', null);
		$this->db->set('cancel_manager', null);
		$this->db->where('applicant_cd', $applicantCd);
		return $this->db->update("applicant");
	}
	
	// 심포지엄 업데이트
	function sympoUpdate($eventId = '', $data = array()) {
		foreach($data as $key => $value) {
			if($key == 'updatedate') {
				$this->db->set($key, $value, false);
			} else {
				$this->db->set($key, $value);
			}
		}
		$this->db->where('sympo_cd', $eventId);
		return $this->db->update("symposium_info");
	}
	
	// 서명 단일 업데이트
	function signUpdate($applicant_cd = '', $data = array()) {
		foreach($data as $key => $value) {
			if($key == 'updatedate' || $key == 'addsigndate' ) {
				$this->db->set($key, $value, false);
			} else {
				$this->db->set($key, $value);
			}
		}
		$this->db->where('applicant_cd', $applicant_cd);
		return $this->db->update("applicant");
	}
	
	function update($eventId = '', $data = array()) {
		foreach($data as $key => $value) {
			$this->db->set($key, $value);
		}
		$this->db->where('event_id', $eventId);
		return $this->db->update("symposium_info");
	}
	
	function updateApi($eventId = '', $keyType = 1, $data = array()) {
		foreach($data as $key => $value) {
			$this->db->set($key, $value);
		}
		$this->db->where('event_id', $eventId);
		$this->db->where('api_type', $keyType);
		return $this->db->update("symposium_info");
	}
	
	// 참가자 상세정보 수정
	function applicantDetailUpdate($applicant_cd = '', $data = array()) {
		foreach($data as $key => $value) {
			if($key == 'updatedate') {
				$this->db->set($key, $value, false);
			} else {
				$this->db->set($key, $value);
			}
		}
		
		$this->db->where('applicant_cd', $applicant_cd);
		
		return $this->db->update('applicant');
	}
	
	//심포지엄이 가지고 있는 브랜드의 갯수
	function getCheckSympoGroup($sympo_cd,$brand_group_cd) {
		
		$set_query = ("
			SELECT
				COUNT(*) AS cnt
			FROM brand_group_sympo
			WHERE sympo_cd = '".$sympo_cd."'
			AND brand_group_cd = '".$brand_group_cd."'
    	");
		
		$this->where = array($member_id);
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return empty($result_row['cnt']) ? 0 : intval($result_row['cnt']);
	}
	
	//사전등록 페이지 만료 여부 체크
	function checkInvitation($invitation) {
		
		$set_query = ("
			SELECT
				COUNT(*) AS cnt
			FROM applicant
			WHERE invitation = '".$invitation."'
    	");
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row['cnt'];
	}
	
	// 대쉬보드에서 심포지엄 리스트 - 굳이 나눌필요?
	function getMainSympoList($admin) {
		$this->where = array();
		
		
		$smember_id = $this->session->userdata('member_id');
		$smember_cd = $this->session->userdata('member_cd');
		$authority = $this->session->userdata('authority');
		
		$set_query = ("
			SELECT
                A.sympo_cd AS sympoCd,
                event_id AS eventId,
                event_name AS eventName,
                (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR ',') FROM brand_group_sympo E where E.sympo_cd = A.sympo_cd) AS groupNames,
                DATE_FORMAT(event_start_date, '%Y.%m.%d') AS eventStartDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_start_date), 1 ) AS eventStartWeek,
                DATE_FORMAT(event_end_date, '%Y.%m.%d') AS eventEndDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_end_date), 1 ) AS eventEndWeek,
                DATE_FORMAT(event_start_date, '%H:%i') AS eventStartTime,
                DATE_FORMAT(event_end_date, '%H:%i') AS eventEndTime,
                DATE_FORMAT(event_PreRegStartDateTime, '%Y.%m.%d') AS eventPreRegStartDateTime,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_PreRegStartDateTime), 1 ) AS eventPreRegStartWeek,
                DATE_FORMAT(event_PreRegEndDateTime, '%Y.%m.%d') AS eventPreRegEndDateTime,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_PreRegEndDateTime), 1 ) AS eventPreRegEndWeek,
                CASE WHEN DATEDIFF(DATE_FORMAT(event_start_date, '%Y.%m.%d'), now()) <= 0 THEN 0
                ELSE DATEDIFF(DATE_FORMAT(event_start_date, '%Y.%m.%d'), now()) END AS dDay,
                CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_start_date,'%Y.%m.%d %H.%i.%s'),now()) <= 0 THEN 'EI2'
                ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_end_date, '%Y.%m.%d %H.%i.%s'),now()) <= 0 THEN 'EI3'
				ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date,'%Y.%m.%d' )) < 0 THEN 'EI' ELSE 'EN' END
                END END	END
                ELSE 'Y' END AS eventStatus,
        ");
		
		$set_query .= "(SELECT COUNT(*) FROM applicant apt WHERE apt.sympo_cd = A.sympo_cd and apt.passcode = 'Y' and apt.cancel = 'N') AS passcodeYcount,";
		$set_query .= "(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd and cancel = 'N') AS applicantCount,";
		
		$set_query .= ("
                domain AS domain,
                gubun AS gubun,
				presenter AS presenter,
                path AS path,
				sympo_type AS sympoType,
                A.creator AS creator,
                A.createdate AS createdate,
                A.updater AS updater,
                A.updatedate AS updatedate,
                (CASE WHEN B.btn1Yn is null THEN (SELECT btn1Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn1Yn END) AS btn1Yn,
                (CASE WHEN B.btn2Yn is null THEN (SELECT btn2Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn2Yn END) AS btn2Yn,
                (CASE WHEN B.btn3Yn is null THEN (SELECT btn3Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn3Yn END) AS btn3Yn,
                (CASE WHEN B.btn4Yn is null THEN (SELECT btn4Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn4Yn END) AS btn4Yn,
                (CASE WHEN B.btn5Yn is null THEN (SELECT btn5Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn5Yn END) AS btn5Yn,
				(SELECT private_desc FROM settings WHERE sympo_cd = A.sympo_cd ) 	AS privateDesc,
				(SELECT symposium_info FROM settings WHERE sympo_cd = A.sympo_cd ) 	AS symposiumInfo
            FROM symposium_info A LEFT OUTER JOIN settings B ON A.sympo_cd = B.sympo_cd
            WHERE 1 = 1
    	");
		
		$set_query .= ' AND CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'I\'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_start_date,\'%Y.%m.%d %H.%i.%s\'),now()) <= 0 THEN \'EI2\'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_end_date, \'%Y.%m.%d %H.%i.%s\'),now()) <= 0 THEN \'EI3\'
				ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) < 0 THEN \'EI\'  ELSE \'EN\' END
                END END	END
                ELSE \'Y\' END = \'I\'';
		
		if($authority != 'mcm' && $authority != 'op') {
			if($authority == 'mr' || $authority == 'ag') {
				$set_query .= ' AND A.gubun = \'Y\'';
			}
			$set_query .= ' AND A.sympo_cd in (SELECT sympo_cd FROM brand_group_sympo WHERE brand_group_cd in (SELECT brand_group_cd FROM brand_group_member WHERE member_cd = \''.$smember_cd.'\'))';
		}
		
		$set_query .= 'order by';
		
		
		$set_query .= ' DATE_FORMAT(eventPreRegEndDateTime, "%Y.%m.%d") ASC, DATE_FORMAT(event_start_date, "%Y.%m.%d") ASC, ';
		
		$set_query .= ' CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'1\'
                ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE
                CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_end_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE \'4\' END
                END END
                ELSE \'3\' END = \'2\' desc';
		
		$set_query .= ', CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'1\'
                ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE
                CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_end_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE \'4\' END
                END END
                ELSE \'3\' END = \'1\' desc';
		
		$set_query .= ', DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\') is not null desc, DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\') != \'0000.00.00\' desc,
            DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) <= 0 desc,
            DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\') asc';
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	//전체심포지엄에서 심포지엄 5개 단위로 보여주는 쿼리
	function getLimitSympoList($page, $searchSortVal = '', $listSortVal = '', $searchTypeVal = '', $searchTextVal = '', $searchGubunVal = '') {
		$this->where = array();
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		$smember_cd = $this->session->userdata('member_cd');
		
		$readCount = 5;
		$startCount = ($page-1) * $readCount;
		
		$set_query = ("
			SELECT
                A.sympo_cd AS sympoCd,
                event_id AS eventId,
                event_name AS eventName,
                (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR ',') FROM brand_group_sympo E where E.sympo_cd = A.sympo_cd) AS groupNames,
                DATE_FORMAT(event_start_date, '%Y.%m.%d') AS eventStartDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_start_date), 1 ) AS eventStartWeek,
                DATE_FORMAT(event_end_date, '%Y.%m.%d') AS eventEndDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_end_date), 1 ) AS eventEndWeek,
                DATE_FORMAT(event_start_date, '%H:%i') AS eventStartTime,
                DATE_FORMAT(event_end_date, '%H:%i') AS eventEndTime,
                DATE_FORMAT(event_PreRegStartDateTime, '%Y.%m.%d') AS eventPreRegStartDateTime,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_PreRegStartDateTime), 1 ) AS eventPreRegStartWeek,
                DATE_FORMAT(event_PreRegEndDateTime, '%Y.%m.%d') AS eventPreRegEndDateTime,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_PreRegEndDateTime), 1 ) AS eventPreRegEndWeek,
                CASE WHEN DATEDIFF(DATE_FORMAT(event_start_date, '%Y.%m.%d'), now()) <= 0 THEN 0
                ELSE DATEDIFF(DATE_FORMAT(event_start_date, '%Y.%m.%d'), now()) END AS dDay,
                CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_start_date,'%Y.%m.%d %H.%i.%s'),now()) <= 0 THEN 'EI2'
                ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_end_date, '%Y.%m.%d %H.%i.%s'),now()) <= 0 THEN 'EI3'
				ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date,'%Y.%m.%d' )) < 0 THEN 'EI' ELSE 'EN' END
                END END	END
                ELSE 'Y' END AS eventStatus,
		");
		
		
		$set_query .= "(SELECT COUNT(*) FROM applicant apt WHERE apt.sympo_cd = A.sympo_cd and apt.passcode = 'Y' and apt.cancel = 'N') AS passcodeYcount,";
		$set_query .= "(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd and cancel = 'N') AS applicantCount,";
		
		
		$set_query .= ("
                gubun AS gubun,
                domain AS domain,
                path AS path,
				sympo_type AS sympoType,
				presenter AS presenter,
                A.creator AS creator,
                A.createdate AS createdate,
                A.updater AS updater,
                A.updatedate AS updatedate,
                (CASE WHEN B.btn1Yn is null THEN (SELECT btn1Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn1Yn END) AS btn1Yn,
                (CASE WHEN B.btn2Yn is null THEN (SELECT btn2Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn2Yn END) AS btn2Yn,
                (CASE WHEN B.btn3Yn is null THEN (SELECT btn3Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn3Yn END) AS btn3Yn,
                (CASE WHEN B.btn4Yn is null THEN (SELECT btn4Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn4Yn END) AS btn4Yn,
                (CASE WHEN B.btn5Yn is null THEN (SELECT btn5Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn5Yn END) AS btn5Yn,
				(SELECT private_desc FROM settings WHERE sympo_cd = A.sympo_cd ) 	AS privateDesc,
				(SELECT symposium_info FROM settings WHERE sympo_cd = A.sympo_cd ) 	AS symposiumInfo
            FROM symposium_info A LEFT OUTER JOIN settings B ON A.sympo_cd = B.sympo_cd
            WHERE 1 = 1
    	");
		
		if($searchGubunVal != null && $searchGubunVal != '' && $searchGubunVal != 'A') {
			$set_query .= ' AND gubun = "'.$searchGubunVal.'"';
		}
		
		if($searchTextVal != null && $searchTextVal != '') {
			
			if($searchTypeVal == 'brandName') {
				$set_query .= ' AND (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR \',\') FROM brand_group_sympo E where E.sympo_cd = A.sympo_cd) LIKE CONCAT("%","'.$searchTextVal.'","%")';
			}else {
				$set_query .= ' AND event_name LIKE CONCAT("%","'.$searchTextVal.'","%")';
			}
		}
		
		
		if($searchSortVal != null && $searchSortVal != '' && $searchSortVal != 'all') {
			$set_query .= ' AND CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'I\'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_start_date,\'%Y.%m.%d %H.%i.%s\'),now()) <= 0 THEN \'EI2\'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_end_date, \'%Y.%m.%d %H.%i.%s\'),now()) <= 0 THEN \'EI3\'
				ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) < 0 THEN \'EI\' ELSE \'EN\' END
                END END	END
                ELSE \'Y\' END = \''.$searchSortVal.'\'';
		}
		
		
		if($authority != 'mcm' && $authority != 'op') {
			if($authority == 'mr' || $authority == 'ag') {
				$set_query .= ' AND A.gubun = \'Y\'';
			}
			$set_query .= ' AND A.sympo_cd in (SELECT sympo_cd FROM brand_group_sympo WHERE brand_group_cd in (SELECT brand_group_cd FROM brand_group_member WHERE member_cd = \''.$smember_cd.'\'))';
		}
		
		$set_query .= 'order by';
		
		if($listSortVal != null && $listSortVal != '') {
			if($listSortVal == 'desc') {
				$set_query .= ' DATE_FORMAT(event_start_date, "%Y.%m.%d") DESC, ';
			} else if($listSortVal == 'asc') {
				$set_query .= ' DATE_FORMAT(event_start_date, "%Y.%m.%d") ASC, ';
			}
		}
		
		$set_query .= ' CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'1\'
                ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE
                CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_end_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE \'4\' END
                END END
                ELSE \'3\' END = \'2\' desc';
		
		$set_query .= ', CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'1\'
                ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE
                CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_end_date, \'%Y.%m.%d\')) <= 0 THEN \'2\' ELSE \'4\' END
                END END
                ELSE \'3\' END = \'1\' desc';
		
		$set_query .= ', DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\') is not null desc, DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\') != \'0000.00.00\' desc,
            DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) <= 0 desc,
            DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\') asc,';
		
		$set_query .= ' DATE_FORMAT(eventPreRegEndDateTime, "%Y.%m.%d") ASC, DATE_FORMAT(event_start_date, "%Y.%m.%d") ASC ';
		
		
		
		$set_query .= ' LIMIT ' .$startCount.', '.$readCount;
		
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	//검색 조건에 따른 전체 심포지엄 수
	function getTotalCount($searchSortVal = '', $searchTypeVal = '', $searchTextVal = '', $searchGubunVal = '') {
		
		$authority = $this->session->userdata('authority');
		$smember_cd = $this->session->userdata('member_cd');
		
		$set_query = ("
			SELECT
                COUNT(*) AS count
                FROM symposium_info A LEFT OUTER JOIN settings B ON A.sympo_cd = B.sympo_cd
            WHERE 1 = 1
        ");
		
		if($searchGubunVal != null && $searchGubunVal != '' && $searchGubunVal != 'A') {
			$set_query .= ' AND gubun = "'.$searchGubunVal.'"';
		}
		
		if($searchTextVal != null && $searchTextVal != '') {
			
			if($searchTypeVal != null && $searchTypeVal != '') {
				if($searchTypeVal == 'eventName'){
					$set_query .= ' AND event_name LIKE CONCAT("%","'.$searchTextVal.'","%")';
				}else{
					$set_query .= ' AND (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR \',\') FROM brand_group_sympo E where E.sympo_cd = A.sympo_cd) LIKE CONCAT("%","'.$searchTextVal.'","%")';
				}
			}
			
		}
		
		if($searchSortVal != null && $searchSortVal != '' && $searchSortVal != 'all') {
			$set_query .= ' AND CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, \'%Y.%m.%d\'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, \'%Y.%m.%d\'), now()) >= 0 THEN \'I\'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_start_date,\'%Y.%m.%d %H.%i.%s\'),now()) <= 0 THEN \'EI2\'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_end_date, \'%Y.%m.%d %H.%i.%s\'),now()) <= 0 THEN \'EI3\'
				ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date, \'%Y.%m.%d\')) < 0 THEN \'EI\' ELSE \'EN\' END
                END END	END
                ELSE \'Y\' END = \''.$searchSortVal.'\'';
		}
		
		if($authority != 'mcm' && $authority != 'op') {
			if($authority == 'mr' || $authority == 'ag') {
				$set_query .= ' AND A.gubun = \'Y\'';
			}
			$set_query .= ' AND A.sympo_cd in (SELECT sympo_cd FROM brand_group_sympo WHERE brand_group_cd in (SELECT brand_group_cd FROM brand_group_member WHERE member_cd = \''.$smember_cd.'\'))';
		}
		
		$result = $this->db->query($set_query);
		$result_list = $result->row_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 등록관리 심포지엄 하나 상세정보구함(쓰이는데 많음)
	function getSymposium($sympoCd, $admin) {
		
		$set_query = ("
            SELECT
                A.sympo_cd AS sympoCd,
                event_id AS eventId,
                event_name AS eventName,
                (SELECT GROUP_CONCAT((SELECT D.brand_group_name FROM brand_group D WHERE D.brand_group_cd = E.brand_group_cd) SEPARATOR ',') FROM brand_group_sympo E where E.sympo_cd = A.sympo_cd) AS groupNames,
                DATE_FORMAT(event_start_date, '%Y.%m.%d') AS eventStartDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_start_date), 1 ) AS eventStartWeek,
                DATE_FORMAT(event_end_date, '%Y.%m.%d') AS eventEndDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_end_date), 1 ) AS eventEndWeek,
                DATE_FORMAT(event_start_date, '%H:%i') AS eventStartTime,
                DATE_FORMAT(event_end_date, '%H:%i') AS eventEndTime,
                DATE_FORMAT(event_PreRegStartDateTime, '%Y.%m.%d') AS eventPreRegStartDateTime,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_PreRegStartDateTime), 1 ) AS eventPreRegStartWeek,
                DATE_FORMAT(event_PreRegEndDateTime, '%Y.%m.%d') AS eventPreRegEndDateTime,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_PreRegEndDateTime), 1 ) AS eventPreRegEndWeek,
                CASE WHEN DATEDIFF(DATE_FORMAT(event_start_date, '%Y.%m.%d'), now()) <= 0 THEN 0
                ELSE DATEDIFF(DATE_FORMAT(event_start_date, '%Y.%m.%d'), now()) END AS dDay,
                CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0
                THEN CASE WHEN DATEDIFF(DATE_FORMAT(event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I'
				ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_start_date,'%Y.%m.%d %H.%i.%s'),now()) <= 0 THEN 'EI2'
                ELSE CASE WHEN TIMESTAMPDIFF(SECOND, DATE_FORMAT(event_end_date, '%Y.%m.%d %H.%i.%s'),now()) <= 0 THEN 'EI3'
				ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(event_start_date,'%Y.%m.%d' )) < 0 THEN 'EI' ELSE 'EN' END
                END END	END
                ELSE 'Y' END AS eventStatus,
        ");
		
		$set_query .= "(SELECT COUNT(*) FROM applicant apt WHERE apt.sympo_cd = A.sympo_cd and apt.passcode = 'Y' and apt.cancel = 'N') AS passcodeYcount,";
		$set_query .= "(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd and cancel = 'N') AS applicantCount,";
		
		$set_query .= ("
                domain AS domain,
                path AS path,
				gubun AS gubun,
				presenter AS presenter,
				sympo_type AS sympoType,
                A.creator AS creator,
                A.createdate AS createdate,
                A.updater AS updater,
                A.updatedate AS updatedate,
                (CASE WHEN B.btn1Yn is null THEN (SELECT btn1Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn1Yn END) AS btn1Yn,
                (CASE WHEN B.btn2Yn is null THEN (SELECT btn2Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn2Yn END) AS btn2Yn,
                (CASE WHEN B.btn3Yn is null THEN (SELECT btn3Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn3Yn END) AS btn3Yn,
                (CASE WHEN B.btn4Yn is null THEN (SELECT btn4Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn4Yn END) AS btn4Yn,
                (CASE WHEN B.btn5Yn is null THEN (SELECT btn5Yn FROM settings WHERE sympo_cd = 'system') ELSE B.btn5Yn END) AS btn5Yn,
				(SELECT private_desc FROM settings WHERE sympo_cd = A.sympo_cd ) 	AS privateDesc,
				(SELECT symposium_info FROM settings WHERE sympo_cd = A.sympo_cd ) 	AS symposiumInfo
            FROM symposium_info A LEFT OUTER JOIN settings B ON A.sympo_cd = B.sympo_cd
            WHERE 1=1
            AND A.sympo_cd = ?
        ");
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->row_array();
		$result->free_result();
		
		return $result_list;
	}
	
	function getPersonCount($sympoCd) {
		$set_query = ("
			SELECT
			  COUNT(*) AS count
			FROM applicant
			WHERE 1=1
			AND sympo_cd = ?
    	");
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->row_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 보고서 정보 가져오기
	function getReport($sympoCd, $startdate, $enddate, $admin) {
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		
		if ($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
			$set_query = ("
					
			    SELECT
			        (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$startdate."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$enddate."' AND cancel = 'N')                  	AS regiCount,
			        (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$startdate."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$enddate."' AND cancel = 'N' AND passcode = 'Y' )   AS passcodeCount,
					(SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$startdate."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$enddate."' AND cancel = 'Y')      				AS cancelCount,
			        (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$startdate."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$enddate."' AND regist_type = 'PSR' AND cancel = 'N')	AS regiPSRCount,
			        (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$startdate."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$enddate."' AND regist_type = 'HCP' AND cancel = 'N')	AS regiHCPCount
			    FROM dual
			");
		} else {
			$set_query = ("
			     SELECT
			         (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND cancel = 'N')           AS regiCount,
			         (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND cancel = 'N' AND passcode = 'Y')	AS passcodeCount,
					 (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND cancel = 'Y')      		AS cancelCount,
			         (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND regist_type = 'PSR' AND cancel = 'N')    AS regiPSRCount,
			         (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = '".$sympoCd."' AND regist_type = 'HCP' AND cancel = 'N')    AS regiHCPCount
			     FROM dual
			 ");
		}
		
		$result = $this->db->query($set_query, $this->where);
		$result_list = $result->row_array();
		$result->free_result();
		
		return $result_list;
	}
	
	//일별 보고서 정보 가져오기
	function getReportDays($sympoCd, $startdate, $enddate, $admin) {
		
		
		if($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
			$set_query = ("
                SELECT
                    DATE_FORMAT(applicant_regdate, '%Y-%m-%d') AS date,
                    COUNT(DATE_FORMAT(applicant_regdate, '%y-%m-%d')) AS count
                FROM applicant
                WHERE 1=1
            ");
			
			if($sympoCd != null && $sympoCd != '') {
				$set_query .= "AND sympo_cd = '".$sympoCd."'";
			}
			
			if($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
				$set_query .= "AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$startdate."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$enddate."'";
			}
		} else {
			$set_query = ("
                SELECT
                    DATE_FORMAT(applicant_regdate, '%Y-%m-%d') AS date,
                    COUNT(DATE_FORMAT(applicant_regdate, '%y-%m-%d')) AS count
                FROM applicant
                WHERE 1=1
    	    ");
			
			if($sympoCd != null && $sympoCd != '') {
				$set_query .= "AND sympo_cd = '".$sympoCd."'";
			}
		}
		
		$set_query .= ' GROUP BY DATE_FORMAT(applicant_regdate, \'%Y-%m-%d\')';
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	function getAllPsrReport($sortVal, $member_id, $scRegDtSt, $scRegDtEd) {
		
		
		if($scRegDtSt != null && $scRegDtSt != '' && $scRegDtEd != null && $scRegDtEd != '') {
			$set_query = ("
                 SELECT
                      A1.member_cd,
                      A1.member_id,
                      A1.member_name,
                      A1.event_name,
                      A1.regiCount,
                      A1.regiPSRCount,
                      A1.regiHCPCount,
                 FROM (SELECT
                    A.member_cd,
                    A.member_id,
                    A.member_name,
                    B.event_name,
                    (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ?) AS regiCount,
                    (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ? AND regist_type = 'PSR') AS regiPSRCount,
                    (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ? AND regist_type = 'HCP') AS regiHCPCount,
                 FROM `member` A, symposium_info B) A1
                 WHERE 1=1
                 AND (A1.regiCount != 0 OR A1.regiPSRCount != 0 OR A1.regiHCPCount != 0)
            ");
			
			$this->where = array($scRegDtSt, $scRegDtEd, $scRegDtSt, $scRegDtEd, $scRegDtSt, $scRegDtEd, $scRegDtSt, $scRegDtEd, $scRegDtSt, $scRegDtEd, $scRegDtSt, $scRegDtEd, $scRegDtSt, $scRegDtEd);
		} else {
			$set_query = ("
                 SELECT
                      A1.member_cd,
                      A1.member_id,
                      A1.member_name,
                      A1.event_name,
                      A1.regiCount,
                      A1.regiPSRCount,
                      A1.regiHCPCount,
                 FROM (SELECT
                    A.member_cd,
                    A.member_id,
                    A.member_name,
                    B.event_name,
                    (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id) AS regiCount,
                    (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND regist_type = 'PSR') AS regiPSRCount,
                    (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND regist_type = 'HCP') AS regiHCPCount,
                 FROM `member` A, symposium_info B) A1
                 WHERE 1=1
                 AND (A1.regiCount != 0 OR A1.regiPSRCount != 0 OR A1.regiHCPCount != 0)
            ");
			$this->where = array();
		}
		
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		if($smember_id != null && $smember_id != '') {
			if($authority != 'admin' && $authority != 'op') {
				$set_query .= ' AND A1.member_id = "'.$smember_id.'"';
			}
		}
		
		
		if($member_id != null && $member_id != '') {
			$set_query .= ' AND A1.member_name LIKE CONCAT("%' . $member_id . '%")';
		}
		
		if($sortVal == 'regiCount') {
			$set_query .= ' ORDER BY A1.regiCount DESC ';
		} else if ($sortVal == 'local') {
			$set_query .= ' ORDER BY (A1.regiPSRCount+A1.regiHCPCount) DESC ';
		} else {
			$set_query .= ' ORDER BY A1.regiCount DESC ';
		}
		
		
		
		
		$result = $this->db->query($set_query, $this->where);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 담당영업소별 보고서
	function getOfficeReport($sympoCd, $sortWhere, $sortType, $searchText, $scRegDtSt, $scRegDtEd) {
		
		if($scRegDtSt != null && $scRegDtSt != '' && $scRegDtEd != null && $scRegDtEd != '') {
			$set_query = ("
		        SELECT
					A1.office_name 			AS office_name,
		            A1.sympo_cd				AS sympo_cd,
		            A1.event_name			AS event_name,
		            SUM(A1.regiCount)		AS regiCount,
		            SUM(A1.passcodeYCount)		AS passcodeYCount,
		            SUM(A1.regiPSRCount)	AS regiPSRCount,
		            SUM(A1.regiHCPCount)	AS regiHCPCount
		        FROM (SELECT
					A.office_name,
		            A.member_cd,
		            A.member_id,
		            A.member_name,
					A.authority as creatorAuthority,
		            B.sympo_cd,
		            B.event_name,
		            (SELECT COUNT(*) FROM applicant         WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$scRegDtSt."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$scRegDtEd."' AND cancel = 'N')                            	AS regiCount,
		            (SELECT COUNT(*) FROM applicant		 	WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$scRegDtSt."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$scRegDtEd."' AND passcode = 'Y' AND cancel = 'N')      			AS passcodeYCount,
		            (SELECT COUNT(*) FROM applicant         WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$scRegDtSt."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$scRegDtEd."' AND regist_type = 'PSR' AND cancel = 'N')    AS regiPSRCount,
		            (SELECT COUNT(*) FROM applicant         WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= '".$scRegDtSt."' AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= '".$scRegDtEd."' AND regist_type = 'HCP' AND cancel = 'N')    AS regiHCPCount
		        FROM `member` A, symposium_info B) A1
		        WHERE 1=1
		        AND A1.sympo_cd = '".$sympoCd."'
		        AND (A1.regiCount != 0 OR A1.passcodeYCount != 0 OR A1.regiPSRCount != 0 OR A1.regiHCPCount != 0)
		    ");
		} else {
			$set_query = ("
		        SELECT
					A1.office_name 			AS office_name,
		            A1.sympo_cd				AS sympo_cd,
		            A1.event_name			AS event_name,
		            SUM(A1.regiCount)		AS regiCount,
		            SUM(A1.passcodeYCount)	AS passcodeYCount,
		            SUM(A1.regiPSRCount)	AS regiPSRCount,
		            SUM(A1.regiHCPCount)	AS regiHCPCount
		        FROM (SELECT
					A.office_name,
		            A.member_cd,
		            A.member_id,
		            A.member_name,
					A.authority as creatorAuthority,
		            B.sympo_cd,
		            B.event_name,
		            (SELECT COUNT(*) FROM applicant         WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND cancel = 'N')                 AS regiCount,
		            (SELECT COUNT(*) FROM applicant		 	WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND passcode = 'Y' AND cancel = 'N')	AS passcodeYCount,
		            (SELECT COUNT(*) FROM applicant         WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND regist_type = 'PSR' AND cancel = 'N')  AS regiPSRCount,
		            (SELECT COUNT(*) FROM applicant         WHERE 1=1 AND sympo_cd = B.sympo_cd AND creator = A.member_id AND regist_type = 'HCP' AND cancel = 'N')  AS regiHCPCount
		        FROM `member` A, symposium_info B) A1
		        WHERE 1=1
		        AND A1.sympo_cd = '".$sympoCd."'
		        AND (A1.regiCount != 0 OR A1.passcodeYCount != 0 OR A1.regiPSRCount != 0 OR A1.regiHCPCount != 0)
		    ");
		}
		
		if($searchText != null && $searchText != '') {
			$set_query .= ' AND A1.office_name LIKE CONCAT("%' . $searchText . '%")';
		}
		
		$set_query .= 'GROUP BY office_name';
		
		if($sortWhere == 'regiCount') {
			$set_query .= ' ORDER BY A1.regiCount '.$sortType;
		} else if($sortWhere == 'passcodeYCount') {
			$set_query .= ' ORDER BY A1.passcodeYCount '.$sortType;
		} else if($sortWhere == 'office_name') {
			$set_query .= ' ORDER BY A1.office_name '.$sortType;
		}
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 날짜별 보고서 보기
	function getDaysReport($sympoCd, $sortWhere, $sortType, $scRegDtSt, $scRegDtEd, $admin) {
		
		$set_query = ("
		SELECT
		    A.date,
		    A.sympo_cd,
		    (SELECT event_name from symposium_info where sympo_cd = A.sympo_cd) as event_name,
		    (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') = A.date AND cancel = 'N') 							AS regiCount,
		    (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') = A.date AND passcode = 'Y' AND cancel = 'N')    	AS passcodeYCount,
		    (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') = A.date AND regist_type = 'PSR' AND cancel = 'N') 	AS regiPSRCount,
		    (SELECT COUNT(*) FROM applicant 		WHERE 1=1 AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') = A.date AND regist_type = 'HCP' AND cancel = 'N') 	AS regiHCPCount
		FROM (
		   SELECT
		    	sympo_cd,
		    	DATE_FORMAT(applicant_regdate, '%Y-%m-%d') AS date
		  	FROM applicant
		  	WHERE 1=1
		  	AND sympo_cd = '".$sympoCd."'
		  	GROUP BY DATE_FORMAT(applicant_regdate, '%Y-%m-%d'), sympo_cd
		) A
		WHERE 1=1
		");
		
		if($scRegDtSt != null && $scRegDtSt != '' && $scRegDtEd != null && $scRegDtEd != '') {
			$set_query .= ' AND A.date >= \''.$scRegDtSt.'\' AND A.date <= \''.$scRegDtEd.'\'';
		}
		
		
		if($sortWhere == 'date') {
			$set_query .= ' ORDER BY A.date '.$sortType.' ';
		} else if($sortWhere == 'regiCount') {
			$set_query .= ' ORDER BY (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, \'%Y-%m-%d\') = A.date) '.$sortType.' ';
		} else if($sortWhere == 'passcodeYCount') {
			$set_query .= ' ORDER BY (SELECT COUNT(*) FROM applicant WHERE 1=1 AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, \'%Y-%m-%d\') = A.date  AND passcode = \'Y\') '.$sortType.' ';
		}
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	function getRegiListPdf($sympoCd) {
		$set_query = ("
			SELECT applicant_cd
            FROM applicant 
            WHERE 1=1
			AND regi_complete = 'Y' AND sympo_cd = '".$sympoCd."' AND cancel = 'N'");
		
		$set_query .= ' ORDER BY applicant_name asc, applicant_cd asc';
		
		$result = $this->db->query($set_query);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 엑셀다운로드
	function getRegiListExcel($sympoCd, $passcode, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec, $dupleInspecType, $cancel) {
		
		$set_query = "";
		$registType2 = "";
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$passcode2 = "AND passcode =  '".$passcode."'";
		}
		else{
			$passcode2 = "";
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$cancel2 = " AND cancel =  '".$cancel."'";
		} else {
			$cancel2 = "";
		}
		
		$set_query = ("
		SELECT		A.applicant_cd          AS applicantCd "
				);
		
		$set_query .= ("
				,(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_hp = A.applicant_hp AND applicant_hp != '' ".$passcode2.$cancel2." GROUP BY applicant_hp) AS hpDupleCount
				,(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_email = A.applicant_email AND applicant_email != '' ".$passcode2.$cancel2." GROUP BY applicant_email) AS emailDupleCount
            	,(SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ', ') from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = (SELECT member_cd FROM `member` WHERE member_id = A.creator)) AS brand_names
           		,(SELECT member_org FROM `member` WHERE member_id = A.creator) AS creator_org
		        ,A.applicant_name       AS applicantName
		        ,A.applicant_email      AS applicantEmail
		        ,A.applicant_hp         AS applicantHp
		        ,A.applicant_hospital   AS applicantHospital
		        ,A.applicant_dep        AS applicantDep
		        ,A.applicant_regdate AS applicantRegdate
            	,DATE_FORMAT(A.applicant_regdate, '%Y-%m-%d') AS regdateDate
            	,DATE_FORMAT(A.applicant_regdate, '%H:%i:%s') AS regdateTime
				,A.creator              AS creator
				,A.createdate 			AS createdate
				,(select member_name from `member` where member_id = A.creator) AS creatorName
				,(select authority from `member` where member_id = A.creator) AS creatorAuthority
				,(select office_name from `member` where member_id = A.creator) AS officeName
				,(select member_hp from `member` where member_id = A.creator) AS creatorHp
		        ,A.updater 				AS updater
		        ,A.updatedate 			AS updatedate
				,(select member_name from `member` where member_id = A.updater) AS updaterName
				,(select authority from `member` where member_id = A.updater) AS updaterAuthority
				,A.cancel				AS cancel
				,A.canceldate			AS canceldate
				,A.cancel_manager		AS cancelManager
				,(select member_name from `member` where member_id = A.cancel_manager)	AS cancelManagerName
				,(select authority from `member` where member_id = A.cancel_manager) 	AS cancelManagerAuthority
				,A.passcode				AS passcode
				,A.passcodedate			AS passcodedate
				,A.passcode_manager		AS passcodeManager
				,(select member_name from `member` where member_id = A.passcode_manager) 	AS passcodeManagerName
				,(select authority from `member` where member_id = A.passcode_manager) 	AS passcodeManagerAuthority
		        ,A.sympo_cd             AS sympoCd
		        ,A.regist_type          AS registType
		        ,A.comment              AS comment
		        ,A.regi_complete        AS regiComplete
				,A.filename				AS filename
				,A.fileext				AS fileext
		    FROM applicant A
		    WHERE 1=1
		    AND A.sympo_cd = ?
		");
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$set_query .= ' AND A.passcode = "'.$passcode.'"';
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$set_query .= ' AND A.cancel = "'.$cancel.'"';
		}
		
		if($scRegDtSt != null && $scRegDtSt != '') {
			if($scRegDtEd != null && $scRegDtEd != '') {
				$set_query .= ' AND A.applicant_regdate between \''.$scRegDtSt.'\' and DATE_ADD(\''.$scRegDtEd.'\', INTERVAL 1 DAY) ';
			}
		}
		
		if($searchText != null && $searchText != '') {
			if($searchType == 'creator_name') {
				$set_query .= ' AND (select member_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			} else if($searchType == 'office_name'){
				$set_query .= ' AND (select office_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			}else{
				$set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%' . $searchText . '%")';
			}
		}
		
		$set_query .= ' ORDER BY';
		
		if($dupleInspec == 'Y') {
			if($dupleInspecType == 'applicant_hp')
			{
				$set_query .= ' (SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_hp = A.applicant_hp AND applicant_hp != "" '.$passcode2.$cancel2.' GROUP BY applicant_hp) DESC,  A.applicant_hp ASC, A.applicant_email, ';
			}else {
				$set_query .= ' (SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_email = A.applicant_email AND applicant_email != "" '.$passcode2.$cancel2.' GROUP BY applicant_email) DESC,  A.applicant_email ASC, A.applicant_hp, ';
			}
		}
		
		$set_query .= ' A.'.$sortWhere.' '.$sortType.'';
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	//조건에 따른 해당 심포지엄의 등록자 리스트
	function getRegiList($page, $sympoCd, $passcode, $regiComplete, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec, $dupleInspecType, $cancel) {
		$readCount = 10;
		$startCount = ($page-1) * $readCount;
		
		
		$set_query = "";
		$registType2 = "";
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$passcode2 = "AND passcode =  '".$passcode."'";
		} else {
			$passcode2 = "";
		}
		
		if($regiComplete != null && $regiComplete != '' && $regiComplete != 'All') {
			$regiComplete2 = "AND regi_complete =  '".$regiComplete."'";
		} else {
			$regiComplete2 = "";
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$cancel2 = " AND cancel =  '".$cancel."'";
		} else {
			$cancel2 = "";
		}
		
		$set_query = ("
		SELECT		A.applicant_cd          AS applicantCd "
				);
		
		$set_query .= ("
				,(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_hp = A.applicant_hp AND applicant_hp != '' ".$passcode2.$regiComplete2.$cancel2." GROUP BY applicant_hp) AS hpDupleCount
				,(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_email = A.applicant_email AND applicant_email != '' ".$passcode2.$regiComplete2.$cancel2." GROUP BY applicant_email) AS emailDupleCount
            	,(SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ', ') from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = (SELECT member_cd FROM `member` WHERE member_id = A.creator)) AS brand_names
           		,(SELECT member_org FROM `member` WHERE member_id = A.creator) AS creator_org
		        ,A.applicant_name       AS applicantName
		        ,A.applicant_email      AS applicantEmail
		        ,A.applicant_hp         AS applicantHp
		        ,A.applicant_hospital   AS applicantHospital
		        ,A.applicant_dep        AS applicantDep
		        ,A.applicant_regdate 	AS applicantRegdate
            	,DATE_FORMAT(A.applicant_regdate, '%Y-%m-%d') AS regdateDate
            	,DATE_FORMAT(A.applicant_regdate, '%H:%i:%s') AS regdateTime
		        ,A.creator              AS creator
				,(select member_name from `member` where member_id = A.creator) AS creatorName
				,(select authority from `member` where member_id = A.creator) AS creatorAuthority
				,(select office_name from `member` where member_id = A.creator) AS officeName
		        ,A.createdate 			AS createdate
		        ,A.updater 				AS updater
		        ,A.updatedate 			AS updatedate
		        ,A.sympo_cd             AS sympoCd
		        ,A.regist_type          AS registType
		        ,A.comment              AS comment
		        ,A.regi_complete        AS regiComplete
				,A.filename				AS filename
				,A.fileext				AS fileext
				,A.passcode				AS passcode
				,A.passcodedate				AS passcodedate
				,A.canceldate			AS canceldate
		    FROM applicant A
		    WHERE 1=1
		    AND A.sympo_cd = ?
		");
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$set_query .= ' AND A.passcode = "'.$passcode.'"';
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$set_query .= ' AND A.cancel = "'.$cancel.'"';
		}
		
		if($regiComplete != null && $regiComplete != '' && $regiComplete != 'All') {
			$set_query .= ' AND A.regi_complete = "'.$regiComplete.'"';
		}
		
		if($scRegDtSt != null && $scRegDtSt != '') {
			if($scRegDtEd != null && $scRegDtEd != '') {
				$set_query .= ' AND A.applicant_regdate between \''.$scRegDtSt.'\' and DATE_ADD(\''.$scRegDtEd.'\', INTERVAL 1 DAY) ';
			}
		}
		
		if($searchText != null && $searchText != '') {
			if($searchType == 'creator_name') {
				$set_query .= ' AND (select member_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			} else if($searchType == 'office_name'){
				$set_query .= ' AND (select office_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			}else{
				$set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%' . $searchText . '%")';
			}
		}
		
		$set_query .= ' ORDER BY';
		
		if($dupleInspec == 'Y') {
			if($dupleInspecType == 'applicant_hp')
			{
				$set_query .= ' (SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_hp = A.applicant_hp AND applicant_hp != "" '.$passcode2.$regiComplete2.$cancel2.' GROUP BY applicant_hp) DESC,  A.applicant_hp ASC, A.applicant_email, ';
			}else {
				$set_query .= ' (SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_email = A.applicant_email AND applicant_email != "" '.$passcode2.$regiComplete2.$cancel2.' GROUP BY applicant_email) DESC,  A.applicant_email ASC, A.applicant_hp, ';
			}
		}
		
		$set_query .= ' A.'.$sortWhere.' '.$sortType.'';
		
		$set_query .= ' LIMIT ' .$startCount.', '.$readCount;
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	function getApplicantOnly($applicantCd) {
		$set_query = ("
			SELECT
				A.applicant_cd 			AS applicantCd,
            	A.applicant_name 		AS applicantName,
				A.applicant_hp 			AS applicantHp,
            	A.applicant_email 		AS applicantEmail,
            	A.applicant_hospital 	AS applicantHospital,
            	A.applicant_dep 		AS applicantDep,
				A.applicant_regdate		AS applicantRegdate,
				A.regist_type			AS registType,
            	A.regi_complete 		AS regiComplete,
				A.comment 				AS comment,
				A.filename				AS filename,
				A.fileext				AS fileext,
				A.passcode				AS passcode,
				A.sympo_cd				AS sympo_cd
            FROM applicant A left join settings B on A.sympo_cd = B.sympo_cd
            WHERE 1=1
			AND applicant_cd = '".$applicantCd."'
    	");
		
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	function getApplicantManager($applicantCd)
	{
		$set_query = ("
			SELECT
		        A.applicant_cd          AS applicantCd
		        ,A.creator              AS creator
				,A.createdate 			AS createdate
				,(select member_name from `member` where member_id = A.creator) 	AS creatorName
				,(select authority from `member` where member_id = A.creator) 		AS creatorAuthority
				,(select member_hp from `member` where member_id = A.creator) 		AS creatorHp
				,(select member_email from `member` where member_id = A.creator) 	AS creatorEmail
				,(select office_name from `member` where member_id = A.creator) 	AS creatorOfficeName
		        ,A.updater 				AS updater
				,A.updatedate 			AS updatedate
				,(select member_name from `member` where member_id = A.updater) 	AS updaterName
				,(select member_hp from `member` where member_id = A.updater) 		AS updaterHp
				,(select authority from `member` where member_id = A.updater) 		AS updaterAuthority
				,(select office_name from `member` where member_id = A.updater) 	AS updaterOfficeName
				,A.addsign_manager 		AS addsignManager
				,A.addsigndate 			AS addsigndate
				,(select member_name from `member` where member_id = A.addsign_manager) 	AS addsignManagerName
				,(select member_hp from `member` where member_id = A.addsign_manager) 		AS addsignManagerHp
				,(select authority from `member` where member_id = A.addsign_manager) 		AS addsignManagerAuthority
				,(select office_name from `member` where member_id = A.addsign_manager) 	AS addsignManagerOfficeName
				,A.cancel				AS cancel
				,A.canceldate			AS canceldate
				,A.cancel_manager		AS cancelManager
				,(select member_name from `member` where member_id = A.cancel_manager)	AS cancelManagerName
				,(select member_hp from `member` where member_id = A.cancel_manager) 	AS cancelManagerHp
				,(select authority from `member` where member_id = A.cancel_manager) 	AS cancelManagerAuthority
				,(select office_name from `member` where member_id = A.cancel_manager) 	AS cancelManagerOfficeName
				,A.passcode					AS passcode
				,A.passcodedate				AS passcodedate
				,A.passcode_manager			AS passcodeManager
				,(select member_name from `member` where member_id = A.passcode_manager) 	AS passcodeManagerName
				,(select member_hp from `member` where member_id = A.passcode_manager) 	AS passcodeManagerHp
				,(select authority from `member` where member_id = A.passcode_manager) 	AS passcodeManagerAuthority
				,(select office_name from `member` where member_id = A.passcode_manager) 	AS passcodeManagerOfficeName
				,A.sympo_cd             AS sympoCd
		        ,A.regist_type          AS registType
		        ,A.comment              AS comment
		        ,A.regi_complete        AS regiComplete
		    FROM applicant A
			WHERE A.applicant_cd = '".$applicantCd."'
    	");
		
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	//passcode 엑셀다운로드 리스트
	function getPasscodeListTotal($sympoCd, $passcode, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $sortWhere, $sortType, $dupleInspec,$cancel) {
		
		$set_query = ("
			SELECT
                A.applicant_cd          AS applicantCd
                ,A.passcode
                ,(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_hp = A.applicant_hp AND passcode = '".$passcode."' GROUP BY applicant_hp) AS hpDupleCount
				,(SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_email = A.applicant_email AND passcode = '".$passcode."' GROUP BY applicant_email) AS emailDupleCount
                ,(SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = (SELECT member_cd FROM `member` WHERE member_id = A.creator)) AS brand_names
                ,(SELECT GROUP_CONCAT(B.org_name SEPARATOR ',') from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = (SELECT member_cd FROM `member` WHERE member_id = A.creator)) AS org_names
				,(SELECT member_org FROM `member` WHERE member_id = A.creator) AS creator_org
				,A.applicant_name       AS applicantName
                ,A.applicant_email      AS applicantEmail
                ,A.applicant_hp         AS applicantHp
                ,A.applicant_hospital   AS applicantHospital
                ,A.applicant_dep        AS applicantDep
                ,A.applicant_regdate AS applicantRegdate
                ,DATE_FORMAT(A.applicant_regdate, '%Y-%m-%d') AS regdateDate
                ,DATE_FORMAT(A.applicant_regdate, '%H:%i:%s') AS regdateTime
                ,(select member_name from `member` where member_id = A.creator) AS creatorName
                ,A.creator              AS creator
				,(select authority from `member` where member_id = A.creator) AS creatorAuthority
                ,A.createdate AS createdate
                ,A.updater AS updater
                ,A.updatedate AS updatedate
                ,A.sympo_cd             AS sympoCd
                ,A.regist_type          AS registType
                ,A.comment              AS comment
                ,A.regi_complete        AS regiComplete
				,A.filename				AS filename
				,A.fileext				AS fileext
            FROM applicant A
            WHERE 1=1
            and sympo_cd = ?
    	");
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$set_query .= ' AND A.passcode = \''.$passcode.'\'';
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$set_query .= ' AND A.cancel = \''.$cancel.'\'';
		}
		
		if($scRegDtSt != null && $scRegDtSt != '') {
			if($scRegDtEd != null && $scRegDtEd != '') {
				$set_query .= ' AND A.applicant_regdate between \''.$scRegDtSt.'\' and  DATE_ADD(\''.$scRegDtEd.'\', INTERVAL 1 DAY)';
			}
		}
		
		if($searchText != null && $searchText != '') {
			if($searchType != 'creatorName') {
				$set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%' . $searchText . '%")';
			} else {
				$set_query .= ' AND (select member_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
				
			}
		}
		
		$set_query .= ' ORDER BY';
		
		if($dupleInspec == 'Y') {
			$set_query .= ' (SELECT COUNT(*) FROM applicant WHERE sympo_cd = A.sympo_cd AND applicant_hp = A.applicant_hp AND passcode = \''.$passcode.'\' GROUP BY applicant_hp) DESC, A.applicant_email, ';
		}
		
		$set_query .= ' A.'.$sortWhere.' '.$sortType.'';
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	function getAttentAllList($sympoCd, $passcode, $cancel) {
		
		$set_query = ("
			SELECT
                A.applicant_cd          AS applicantCd
                ,A.applicant_name       AS applicantName
                ,A.applicant_email      AS applicantEmail
                ,A.applicant_hp         AS applicantHp
                ,A.applicant_hospital   AS applicantHospital
                ,A.applicant_dep        AS applicantDep
                ,A.applicant_regdate AS applicantRegdate
                ,DATE_FORMAT(A.applicant_regdate, '%Y-%m-%d') AS regdateDate
                ,DATE_FORMAT(A.applicant_regdate, '%H:%i:%s') AS regdateTime
                ,(select member_name from `member` where member_id = A.creator) AS creatorName
                ,A.creator              AS creator
                ,A.createdate AS createdate
                ,A.updater AS updater
                ,A.updatedate AS updatedate
                ,A.sympo_cd             AS sympoCd
                ,A.regist_type          AS registType
                ,A.comment              AS comment
                ,A.regi_complete        AS regiComplete
            FROM applicant A
            WHERE 1=1
            and sympo_cd = ?
    	");
		
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$set_query .= ' AND A.passcode = \''.$passcode.'\'';
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$set_query .= ' AND A.cancel = \''.$cancel.'\'';
		}
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	//조건에 따른 해당 심포지엄의 총 숫자
	function getPasscodeListCount($sympoCd, $passcode, $regiComplete, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $cancel) {
		
		$set_query = ("
			SELECT
                COUNT(*) as cnt
            from applicant A
            where sympo_cd = ?
    	");
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$set_query .= ' AND A.passcode = \''.$passcode.'\'';
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$set_query .= ' AND A.cancel = \''.$cancel.'\'';
		}
		
		if($regiComplete != null && $regiComplete != '' && $regiComplete != 'All') {
			$set_query .= "AND A.regi_complete =  '".$regiComplete."'";
		}
		
		
		if($scRegDtSt != null && $scRegDtSt != '') {
			if($scRegDtEd != null && $scRegDtEd != '') {
				$set_query .= ' AND A.applicant_regdate between \''.$scRegDtSt.'\' and  DATE_ADD(\''.$scRegDtEd.'\', INTERVAL 1 DAY)';
			}
		}
		
		if($searchText != null && $searchText != '') {
			if($searchType == 'creator_name') {
				$set_query .= ' AND (select member_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			} else if($searchType == 'office_name'){
				$set_query .= ' AND (select office_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			}else{
				$set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%' . $searchText . '%")';
			}
		}
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	// 조건에 따른 해당 심포지엄의 휴대폰번호 중복자 숫자
	function getPasscodeListHpDupleCount($sympoCd, $passcode, $regiComplete, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $cancel) {
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$passcode2 = " AND passcode =  '".$passcode."'";
		} else {
			$passcode2 = "";
		}
		
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$cancel2 = " AND cancel =  '".$cancel."'";
		} else {
			$cancel2 = "";
		}
		
		if($regiComplete != null && $regiComplete != '' && $regiComplete != 'All') {
			$regiComplete2 = "AND regi_complete =  '".$regiComplete."'";
		} else {
			$regiComplete2 = "";
		}
		
		
		$set_query = ("
				SELECT ifnull(SUM(A.CNT),0) as cnt
				FROM(SELECT *,COUNT(*) AS CNT FROM applicant WHERE 1=1 AND applicant_hp != '' ".$passcode2.$regiComplete2.$cancel2." GROUP BY applicant_hp,sympo_cd  HAVING COUNT(*)>=2)A
				WHERE A.sympo_cd = ?
		");
		
		if($scRegDtSt != null && $scRegDtSt != '') {
			if($scRegDtEd != null && $scRegDtEd != '') {
				$set_query .= ' AND A.applicant_regdate between \''.$scRegDtSt.'\' and  DATE_ADD(\''.$scRegDtEd.'\', INTERVAL 1 DAY)';
			}
		}
		
		if($searchText != null && $searchText != '') {
			if($searchType == 'creator_name') {
				$set_query .= ' AND (select member_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			} else if($searchType == 'office_name'){
				$set_query .= ' AND (select office_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			}else{
				$set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%' . $searchText . '%")';
			}
		}
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	// 조건에 따른 해당 심포지엄의 휴대폰번호 중복자 숫자
	function getSignCount($sympoCd) {
		
		$set_query = ("
				SELECT COUNT(*) as cnt
				FROM  applicant A
				WHERE A.sympo_cd = ? AND cancel = 'N' AND regi_complete='Y'
		");
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	//조건에 따른 해당 심포지엄의 이메일 중복자 숫자
	function getPasscodeListEmailDupleCount($sympoCd, $passcode, $regiComplete, $searchType, $searchText, $scRegDtSt, $scRegDtEd, $cancel) {
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$passcode2 = " AND passcode =  '".$passcode."'";
		} else {
			$passcode2 = "";
		}
		
		if($cancel != null && $cancel != '' && $cancel != 'All') {
			$cancel2 = " AND cancel =  '".$cancel."'";
		} else {
			$cancel2 = "";
		}
		
		if($regiComplete != null && $regiComplete != '' && $regiComplete != 'All') {
			$regiComplete2 = "AND regi_complete =  '".$regiComplete."'";
		} else {
			$regiComplete2 = "";
		}
		
		$set_query = ("
				SELECT ifnull(SUM(A.CNT),0) as cnt
				FROM(SELECT *,COUNT(*) AS CNT FROM applicant WHERE 1=1 AND applicant_email != ''".$passcode2.$regiComplete2.$cancel2." GROUP BY applicant_email,sympo_cd  HAVING COUNT(*)>=2)A
				WHERE A.sympo_cd = ?
		");
		
		
		if($scRegDtSt != null && $scRegDtSt != '') {
			if($scRegDtEd != null && $scRegDtEd != '') {
				$set_query .= ' AND A.applicant_regdate between \''.$scRegDtSt.'\' and  DATE_ADD(\''.$scRegDtEd.'\', INTERVAL 1 DAY)';
			}
		}
		
		if($searchText != null && $searchText != '') {
			if($searchType == 'creator_name') {
				$set_query .= ' AND (select member_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			} else if($searchType == 'office_name'){
				$set_query .= ' AND (select office_name from `member` where member_id = A.creator) LIKE CONCAT("%' . $searchText . '%")';
			}else{
				$set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%' . $searchText . '%")';
			}
		}
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	function getAttentApplyDuple($sympoCd, $passcode, $applicantCds) {
		
		$set_query = ("
			SELECT
                COUNT(*) as cnt
            FROM applicant
            WHERE 1=1
    	");
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		if($authority != 'admin' && $authority != 'op') {
			$set_query .= ' AND creator = "'.$smember_id.'"';
		}
		
		if($passcode != null && $passcode != '' && $passcode != 'All') {
			$set_query .= ' AND passcode = \''.$passcode.'\'';
		}
		
		if($applicantCds != null && $applicantCds != '') {
			$applicantCdsArr = explode(";", $applicantCds);
			$numItems = count($applicantCdsArr);
			$i = 0;
			$set_query .= ' AND applicant_hp in (SELECT b.applicant_hp FROM applicant b where b.applicant_cd in (';
			foreach ($applicantCdsArr as $applicantCd):
			
			$set_query .= '\''.$applicantCd.'\'';
			if(++$i !== $numItems) {
				$set_query .= ',';
			}
			endforeach;
			$set_query .= '))';
		}
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	// 참가자 삭제
	function attentDelete($applicantCd) {
		$this->db->where('applicant_cd', $applicantCd);
		return $this->db->delete("applicant");
	}
	
	function sympoReport($startdate, $enddate, $admin) {
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		
		if($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
			if($smember_id != null && $smember_id != '') {
				if($authority != 'admin' && $authority != 'op') {
					$set_query = ("
                        SELECT
                            event_name,
                            event_start_date,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'PSR' AND creator = '".$smember_id."' AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ?) AS psr,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'HCP' AND creator = '".$smember_id."' AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ?) AS hcp,
                        FROM symposium_info A
                        WHERE DATE_FORMAT(event_end_date, '%Y.%m.%d') > now()
                    ");
				} else {
					$set_query = ("
                        SELECT
                            event_name,
                            event_start_date,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'PSR' AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ?) AS psr,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'HCP' AND sympo_cd = A.sympo_cd AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') >= ? AND DATE_FORMAT(applicant_regdate, '%Y-%m-%d') <= ?) AS hcp,
                        FROM symposium_info A
                        WHERE DATE_FORMAT(event_end_date, '%Y.%m.%d') > now()
                    ");
				}
			}
			$set_query .= ' ORDER BY event_start_date';
			
			$this->where = array($startdate, $enddate, $startdate, $enddate, $startdate, $enddate, $startdate, $enddate);
			$result = $this->db->query($set_query, $this->where);
		} else {
			if($smember_id != null && $smember_id != '') {
				if($authority != 'admin' && $authority != 'op') {
					$set_query = ("
                        SELECT
                            event_name,
                            event_start_date,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'PSR' AND creator = '".$smember_id."' AND sympo_cd = A.sympo_cd) AS psr,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'HCP' AND creator = '".$smember_id."' AND sympo_cd = A.sympo_cd) AS hcp,
                        FROM symposium_info A
                        WHERE DATE_FORMAT(event_end_date, '%Y.%m.%d') > now()
                    ");
				} else {
					$set_query = ("
                        SELECT
                            event_name,
                            event_start_date,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'PSR' AND sympo_cd = A.sympo_cd) AS psr,
                            (SELECT count(*) FROM applicant WHERE regist_type = 'HCP' AND sympo_cd = A.sympo_cd) AS hcp,
                        FROM symposium_info A
                        WHERE DATE_FORMAT(event_end_date, '%Y.%m.%d') > now()
                    ");
				}
			}
			$set_query .= ' ORDER BY event_start_date';
			$result = $this->db->query($set_query);
		}
		
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	function sympoDayeport($startdate, $enddate, $admin) {
		$set_query = ("
            SELECT * FROM (
                SELECT
                    DATE_FORMAT(applicant_regdate, '%Y-%m-%d') AS date,
                    count(*) as cnt,
                    creator
                FROM applicant
                GROUP BY DATE_FORMAT(applicant_regdate, '%Y.%m.%d')
                ORDER BY DATE_FORMAT(applicant_regdate, '%Y.%m.%d') DESC) A
            WHERE DATE_FORMAT(A.date, '%Y.%m.%d') > DATE_FORMAT(date_sub(now(), interval 30 day), '%Y.%m.%d')
				
        ");
		
		
		$smember_id = $this->session->userdata('member_id');
		$authority = $this->session->userdata('authority');
		
		if($smember_id != null && $smember_id != '') {
			if($authority != 'admin' && $authority != 'op') {
				$set_query .= ' AND A.creator = "'.$smember_id.'"';
			}
		}
		
		if($startdate != null && $startdate != '' && $enddate != null && $enddate != '') {
			$set_query .= ' AND A.date >= ? AND A.date <= ?';
			$this->where = array($startdate, $enddate);
			
			$set_query .= ' ORDER BY A.date asc';
			$result = $this->db->query($set_query, $this->where);
			
		} else {
			$set_query .= ' ORDER BY A.date asc';
			$result = $this->db->query($set_query);
		}
		
		
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	
	// 심포지엄 브랜드 조회 쿼리
	function getSympoToBrand($sympoCd) {
		$set_query = ("
			SELECT
                B.brand_group_cd
                , B.brand_group_name
                , A.default_brand_chk
            FROM brand_group_sympo A INNER JOIN brand_group B ON A.brand_group_cd = B.brand_group_cd
            WHERE 1=1
			AND A.sympo_cd = ?
    	");
		
		
		$result = $this->db->query($set_query, $sympoCd);
		$result_list = $result->result_array();
		$result->free_result();
		
		return $result_list;
	}
	
	// 심포지엄 그룹 삭제
	function sympoGroupDelete($sympo_cd) {
		$this->db->where('sympo_cd', $sympo_cd);
		$this->db->where('default_brand_chk is null');
		return $this->db->delete("brand_group_sympo");
	}
	
	function getBrand($brand_group_id, $org_id) {
		$set_query = ("
			SELECT
               brand_group_cd
            FROM brand_group
            WHERE 1=1
			AND brand_group_id = '".$brand_group_id."'
			and org_id = '".$org_id."'
    	");
		
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
		
	}
	
	function getApiSymposium($event_id, $keyType) {
		$set_query = ("
			SELECT
               sympo_cd
            FROM symposium_info
            WHERE 1=1
			AND event_id = '".$event_id."'
			and api_type = '".$keyType."'
    	");
		
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	function getSymposiumOnce($sympo_cd) {
		
		$set_query = ("
			SELECT
                event_name,
				DATE_FORMAT(event_start_date, '%Y.%m.%d') AS eventStartDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_start_date), 1 ) AS eventStartWeek,
                DATE_FORMAT(event_end_date, '%Y.%m.%d') AS eventEndDate,
                SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(event_end_date), 1 ) AS eventEndWeek,
                DATE_FORMAT(event_start_date, '%H:%i') AS eventStartTime,
                DATE_FORMAT(event_end_date, '%H:%i') AS eventEndTime
            FROM symposium_info
            WHERE 1=1
			AND sympo_cd = '".$sympo_cd."'
    	");
		
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
	
	function checkPasscodeType($applicantCd) {
		$set_query = ("
			SELECT
                passcode
            FROM applicant
            WHERE 1=1
			AND applicant_cd = '".$applicantCd."'
    	");
		
		
		$result = $this->db->query($set_query);
		$result_row = $result->row_array();
		$result->free_result();
		
		return $result_row;
	}
}