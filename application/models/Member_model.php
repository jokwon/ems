<?php

class Member_model extends CI_Model {
    private $where = array();

    function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+9:00'");
    }



    function getBrandGroupPeopleList($page, $brand_group_cd, $authType, $searchType, $searchText, $sortWhere, $sortType) {
        $readCount = 10;
        $startCount = ($page-1) * $readCount;
        
        $set_query = ("
			SELECT
                A.brand_group_cd
                , (SELECT COUNT(*) from brand_group_member where member_cd = A.member_cd and brand_group_cd != '' and brand_group_cd is not null) AS brand_count
                , (SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_names
                , (SELECT GROUP_CONCAT(B.brand_group_cd SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_cds
                , B.member_cd
                , B.member_name
                , B.member_email
                , B.member_hp
                , B.member_org
                , B.member_dep
                , B.member_brand
				, B.office_name
                , B.agree
                , B.authority
                , B.emailConfirm
                , D.updatedate AS mail_date
                , B.member_team
                , B.member_regdate AS member_regdate
                , B.createdate AS createdate
                , B.member_id
                , (case B.creator when 'system' then '' else case B.creator when B.member_id then '' else (SELECT C.member_name +' '+ C.authority from `member` C where C.member_id = B.creator) end end) AS regi_manager
                , (SELECT B.member_name FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_name
                , (SELECT B.authority FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_authority
                , (case (SELECT COUNT(*) FROM activity_member_mail where member_cd = A.member_cd) when 0 then 'N' else 'Y' end) AS appr_status
                , (SELECT createdate FROM activity_member_mail where member_cd = A.member_cd) AS appr_date
            FROM brand_group_member A INNER JOIN `member` B ON A.member_cd = B.member_cd LEFT OUTER JOIN activity_member_mail D ON B.member_cd = D.member_cd
            WHERE 1=1
			AND B.deleteCheck = 'N'
            AND A.brand_group_cd = ?

    	");

        if($authType != null && $authType != 'All') {
            $set_query .= ' AND authority = \''.$authType.'\'';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $set_query .= ' ORDER BY B.'.$sortWhere.' '.$sortType.' ';
        $set_query .= ' LIMIT ' .$startCount.', '.$readCount;

        $result = $this->db->query($set_query, $brand_group_cd);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    function getAllBrandGroupPeopleList($brand_group_cd, $authType, $searchType, $searchText, $sortWhere, $sortType) {

        $set_query = ("
			SELECT
                A.brand_group_cd
                , (SELECT COUNT(*) from brand_group_member where member_cd = A.member_cd and brand_group_cd != '' and brand_group_cd is not null) AS brand_count
                , (SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_names
                , (SELECT GROUP_CONCAT(B.brand_group_cd SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_cds
                , B.member_cd
				, B.office_name
                , B.member_name
                , B.member_email
                , B.member_hp
                , B.member_org
                , B.member_dep
                , B.member_brand
                , B.agree
                , B.authority
                , B.emailConfirm
                , D.updatedate AS mail_date
                , B.member_team
                , B.member_regdate AS member_regdate
                , B.createdate AS createdate
                , B.member_id
                , (case B.creator when 'system' then '' else case B.creator when B.member_id then '' else (SELECT C.member_name +' '+ C.authority from `member` C where C.member_id = B.creator) end end) AS regi_manager
                , (SELECT B.member_name FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_name
                , (SELECT B.authority FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_authority
                , (case (SELECT COUNT(*) FROM activity_member_mail where member_cd = A.member_cd) when 0 then 'N' else 'Y' end) AS appr_status
                , (SELECT createdate FROM activity_member_mail where member_cd = A.member_cd) AS appr_date
            FROM brand_group_member A INNER JOIN `member` B ON A.member_cd = B.member_cd LEFT OUTER JOIN activity_member_mail D ON B.member_cd = D.member_cd
            WHERE 1=1
			AND B.deleteCheck = 'N'
            AND A.brand_group_cd = ?
    	");

        if($authType != null && $authType != 'All') {
            $set_query .= ' AND authority = \''.$authType.'\'';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $set_query .= ' ORDER BY B.'.$sortWhere.' '.$sortType.' ';

        $result = $this->db->query($set_query, $brand_group_cd);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    function getBrandGroupPeopleCount($brand_group_cd, $authType, $searchType, $searchText) {

        $set_query = ("
			SELECT
                COUNT(*) AS cnt
            FROM brand_group_member A INNER JOIN `member` B ON A.member_cd = B.member_cd
            WHERE 1=1
			AND B.deleteCheck = 'N'
            AND A.brand_group_cd = ?
    	");

        if($authType != null && $authType != 'All') {
            $set_query .= ' AND authority = \''.$authType.'\'';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $result = $this->db->query($set_query, $brand_group_cd);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }


    function getBrandGroup($brandGroupCd) {
        $set_query = ("
			SELECT
                brand_group_cd
                , brand_group_id
                , brand_group_name
                , org_id
                , org_name
                , (SELECT COUNT(*) FROM brand_group_member C INNER JOIN `member` B ON C.member_cd = B.member_cd WHERE C.brand_group_cd = A.brand_group_cd) AS pCount
                , org_name
                , creator
                , createdate AS createdate
                , updater
                , updatedate AS updatedate
            FROM brand_group A
            WHERE 1 = 1
            AND brand_group_cd = ?
    	");


        $result = $this->db->query($set_query, $brandGroupCd);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    //전체 브랜드 그룹 리스트
    function getBrandGroupAllList($searchType, $searchText, $sortWhere, $sortType) {

        $set_query = ("
			SELECT
                brand_group_cd
                , brand_group_id
                , brand_group_name
                , org_id
                , org_name
                , (SELECT COUNT(*) FROM brand_group_member C INNER JOIN `member` B ON C.member_cd = B.member_cd WHERE C.brand_group_cd = A.brand_group_cd) AS pCount
                , creator
                , createdate AS createdate
                , updater
                , updatedate AS updatedate
            FROM brand_group A
            WHERE 1 = 1
    	");

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }
        
        $authority = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        
        if($authority == 'pm') {
        	$set_query .= ' AND brand_group_cd in (SELECT brand_group_cd from brand_group_member where member_cd = \'' . $member_cd . '\') ';
        }

        $set_query .= ' ORDER BY '.$sortWhere.' '.$sortType.' ';

        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }
    
    //전체 브랜드 그룹 리스트
    function getOfficeAllList() {
    	
    	$set_query = ("
			SELECT
                office_num
                , office_name
                , sales_team
                , item
            FROM office
            WHERE 1 = 1
    	");
    	
    	$set_query .= ' ORDER BY office_num asc';
    	
    	$result = $this->db->query($set_query);
    	$result_list = $result->result_array();
    	$result->free_result();
    	
    	return $result_list;
    }

    function getBrandGroupAllList1($member_cd, $searchType, $searchText, $sortWhere, $sortType) {

        $set_query = ("
			SELECT
                brand_group_cd
                , brand_group_id
                , brand_group_name
                , org_id
                , org_name
                , (SELECT COUNT(*) FROM brand_group_member C INNER JOIN `member` B ON C.member_cd = B.member_cd WHERE C.brand_group_cd = A.brand_group_cd) AS pCount
                , creator
                , createdate AS createdate
                , updater
                , updatedate AS updatedate
            FROM brand_group A
            WHERE 1 = 1
    	");


        if($member_cd !=  null && $member_cd != '') {
            $set_query .= ' AND brand_group_cd not in (SELECT brand_group_cd from brand_group_member where member_cd = \'' . $member_cd . '\') ';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $set_query .= ' ORDER BY '.$sortWhere.' '.$sortType.' ';

        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    function getBrandGroupList($page, $searchType, $searchText, $sortWhere, $sortType) {
        $readCount = 10;
        $startCount = ($page-1) * $readCount;

        $set_query = ("
			SELECT
                brand_group_cd
                , brand_group_id
                , brand_group_name
                , org_id
                , org_name
                , (SELECT COUNT(*) FROM brand_group_member C INNER JOIN `member` B ON C.member_cd = B.member_cd WHERE C.brand_group_cd = A.brand_group_cd) AS pCount
                , org_name
                , creator
                , createdate AS createdate
                , updater
                , updatedate AS updatedate
            FROM brand_group A
            WHERE 1 = 1
    	");


        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }
        
        $authority = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        
        if($authority == 'pm') {
        	$set_query .= ' AND brand_group_cd in (SELECT brand_group_cd from brand_group_member where member_cd = \'' . $member_cd . '\') ';
        }

        $set_query .= ' ORDER BY '.$sortWhere.' '.$sortType.' ';
        $set_query .= ' LIMIT ' .$startCount.', '.$readCount;

        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    function getBrandGroupCount($searchType, $searchText) {

        $set_query = ("
			SELECT
                COUNT(*) AS cnt
            FROM brand_group
            WHERE 1 = 1
    	");

        $authority = $this->session->userdata('authority');
        $member_cd = $this->session->userdata('member_cd');
        
        if($authority == 'pm') {
        	$set_query .= ' AND brand_group_cd in (SELECT brand_group_cd from brand_group_member where member_cd = \'' . $member_cd . '\') ';
        }
        
        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $result = $this->db->query($set_query);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    // 전체회원 조회
    function getMemberList($page, $authType, $requestType, $searchType, $searchText, $sortWhere, $sortType) {
        $readCount = 10;
        $startCount = ($page-1) * $readCount;

        $set_query = ("
			SELECT
			    A.member_cd
                , A.member_name
                , A.member_email
                , A.member_hp
                , A.member_org
                , A.member_dep
                , A.member_team
                , A.member_brand
				, A.office_name
                , A.agree
                , A.authority
                , (SELECT COUNT(*) FROM brand_group WHERE brand_group_cd in (SELECT brand_group_cd from brand_group_member where member_cd = A.member_cd and brand_group_cd != '' and brand_group_cd is not null)) AS brand_count 
                , (SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_names
                , (SELECT GROUP_CONCAT(B.brand_group_cd SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_cds
                , A.emailConfirm
                , D.updatedate AS mail_date
                , A.createdate AS createdate
                , A.member_regdate AS member_regdate
                , member_id
                , (case A.creator when 'system' then '' else case A.creator when A.member_id then '' else (SELECT concat(B.member_name,' ',UPPER(B.authority)) from `member` B where B.member_id = A.creator) end end) AS regi_manager
                , (SELECT B.member_name FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_name
                , (SELECT B.authority FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_authority
                , (case (SELECT COUNT(*) FROM activity_member_mail where member_cd = A.member_cd) when 0 then 'N' else 'Y' end) AS appr_status
                , (SELECT createdate FROM activity_member_mail where member_cd = A.member_cd) AS appr_date
            FROM `member` A LEFT OUTER JOIN activity_member_mail D ON A.member_cd = D.member_cd
            WHERE 1 = 1
			AND A.deleteCheck = 'N'
    	");

        if($authType != null && $authType != 'All') {
            $set_query .= ' AND A.authority = \''.$authType.'\'';
        }

        if($requestType != null && $requestType != 'All') {
            $set_query .= ' AND A.emailConfirm = \''.$requestType.'\'';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }
        
        $authority = $this->session->userdata('authority');
        if($authority != 'op'){
        	$set_query .= ' AND A.member_cd != "MEM_0000000000000000"';
        }

        $set_query .= ' ORDER BY A.'.$sortWhere.' '.$sortType.' ';
        $set_query .= ' LIMIT ' .$startCount.', '.$readCount;

        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }
    
    function getMembertOnly($memberCd) {
    	$set_query = ("
			SELECT
				A.member_cd
                , A.member_name
                , A.member_email
                , A.member_hp
                , A.member_org
                , A.member_dep
                , A.member_team
                , A.member_brand
				, A.office_name
                , A.agree
                , A.authority
                , (SELECT COUNT(*) FROM brand_group WHERE brand_group_cd in (SELECT brand_group_cd from brand_group_member where member_cd = A.member_cd and brand_group_cd != '' and brand_group_cd is not null)) AS brand_count 
                , (SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_names
                , (SELECT GROUP_CONCAT(B.brand_group_cd SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_cds
                , A.emailConfirm
                , D.updatedate AS mail_date
                , A.createdate AS createdate
                , A.member_regdate AS member_regdate
                , member_id
                , (case A.creator when 'system' then '' else case A.creator when A.member_id then '' else (SELECT concat(B.member_name,' ',UPPER(B.authority)) from `member` B where B.member_id = A.creator) end end) AS regi_manager
                , (SELECT B.member_name FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_name
                , (SELECT B.authority FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_authority
                , (case (SELECT COUNT(*) FROM activity_member_mail where member_cd = A.member_cd) when 0 then 'N' else 'Y' end) AS appr_status
                , (SELECT createdate FROM activity_member_mail where member_cd = A.member_cd) AS appr_date
            FROM `member` A LEFT OUTER JOIN activity_member_mail D ON A.member_cd = D.member_cd
            WHERE 1=1
			AND A.member_cd = '".$memberCd."'
    	");
    	
    	
    	$result = $this->db->query($set_query);
    	$result_row = $result->row_array();
    	$result->free_result();
    	
    	return $result_row;
    }


    function getBrandMemberAllList($brand_group_cd, $searchType, $searchText) {

        $set_query = ("
			SELECT
                A.member_cd,
                A.member_name,
                A.member_email,
                A.member_brand,
                A.member_org,
                A.member_dep,
                A.member_team,
                A.member_regdate,
                A.emailConfirm,
                A.authority
            FROM member A
            WHERE 1 = 1
            AND A.member_cd not in (SELECT member_cd from brand_group_member where brand_group_cd = ?)
			AND A.deleteCheck = 'N'
    	");

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%","'.$searchText.'","%") ';
        }
        
        $authority = $this->session->userdata('authority');
        
        if($authority == 'pm') {
        	$set_query .= ' AND A.authority != \'op\' AND A.authority != \'mcm\'';
        }

        $set_query .= ' ORDER BY A.updatedate desc ';

        $result = $this->db->query($set_query, $brand_group_cd);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    function getMemberAllList($authType, $requestType, $searchType, $searchText, $sortWhere, $sortType) {
        $set_query = ("
			SELECT
			    A.member_cd
                , A.member_name
                , A.member_email
                , A.member_hp
                , A.member_org
                , A.member_dep
                , A.member_team
                , A.member_brand
				, A.office_name
                , A.agree
                , A.authority
                , (SELECT COUNT(*) from brand_group_member where member_cd = A.member_cd and brand_group_cd != '' and brand_group_cd is not null) AS brand_count 
                , (SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_names
                , (SELECT GROUP_CONCAT(B.brand_group_cd SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_cds
                , A.emailConfirm
                , D.updatedate AS mail_date
                , A.createdate AS createdate
                , A.member_regdate AS member_regdate
                , member_id
                , (case A.creator when 'system' then '' else case A.creator when A.member_id then '' else (SELECT B.member_name +' '+ B.authority from `member` B where B.member_id = A.creator) end end) AS regi_manager
                , (SELECT B.member_name FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_name
                , (SELECT B.authority FROM activity_member_mail C inner JOIN `member` B ON C.creator = B.member_id WHERE C.member_cd = A.member_cd) AS appr_authority
                , (case (SELECT COUNT(*) FROM activity_member_mail where member_cd = A.member_cd) when 0 then 'N' else 'Y' end) AS appr_status
                , (SELECT createdate FROM activity_member_mail where member_cd = A.member_cd) AS appr_date
            FROM `member` A LEFT OUTER JOIN activity_member_mail D ON A.member_cd = D.member_cd
            WHERE 1 = 1
			AND A.deleteCheck = 'N'
    	");

        if($authType != null && $authType != 'All') {
            $set_query .= ' AND A.authority = \''.$authType.'\'';
        }

        if($requestType != null && $requestType != 'All') {
            $set_query .= ' AND A.emailConfirm = \''.$requestType.'\'';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND A.'.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $set_query .= ' ORDER BY A.'.$sortWhere.' '.$sortType.' ';

        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    // 사용자가 속해있는 브랜드 그룹리스트
    function getMemberRelGroup($member_cd) {
        $set_query = ("
			SELECT
                B.brand_group_name,
                A.brand_group_member_cd,
                A.brand_group_cd,
                A.member_cd
            FROM brand_group_member A, brand_group B
            WHERE 1=1
            AND A.brand_group_cd = B.brand_group_cd
            AND A.member_cd = ?
    	");

        $result = $this->db->query($set_query, $member_cd);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    function getMemberCount($authType, $requestType, $searchType, $searchText) {

        $set_query = ("
			SELECT
                COUNT(*) AS cnt
            FROM member 
            WHERE 1 = 1
			AND deleteCheck = 'N'
    	");

        if($authType != null && $authType != 'All') {
            $set_query .= ' AND authority = \''.$authType.'\'';
        }

        if($requestType != null && $requestType != 'All') {
            $set_query .= ' AND emailConfirm = \''.$requestType.'\'';
        }

        if($searchText !=  null && $searchText != '') {
            $set_query .= ' AND '.$searchType.' LIKE CONCAT("%","'.$searchText.'","%")';
        }

        $result = $this->db->query($set_query);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    function getMemberRow($params = array()) {

        $set_query = ("
			SELECT 
				member_cd,
                member_id,
                member_pass,
                member_name,
                member_email,
                member_hp,
                member_org,
                member_dep,
                member_regdate,
                member_team,
                authority,
                emailConfirm,
                emailCert,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate
			FROM member 
			WHERE member_id = ? and member_pass = password(?)
			AND deleteCheck = 'N'
    	");

        $this->where = array($params['member_id'],$params['member_pass']);

        $result = $this->db->query($set_query, $this->where);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    function getEmailCertRow($emailCert) {
        $set_query = ("
			SELECT 
				member_cd,
                member_name,
                member_email,
                member_org,
                member_dep,
                member_team,
                authority,
                emailConfirm,
                emailCert,
                member_brand,
                session
			FROM member 
			WHERE emailCert = ?
			AND deleteCheck = 'N'
    	");

        $result = $this->db->query($set_query, $emailCert);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    // 사용자 상세 정보
    function getMemberDetail($member_cd) {

        $this->where = array();

        $set_query = ("
			SELECT
                member_cd,
                member_id,
                member_pass,
                member_name,
                member_email,
                member_hp,
                member_org,
                member_dep,
                member_regdate,
                member_team,
				office_name,
                (SELECT GROUP_CONCAT(B.brand_group_name SEPARATOR ',') AS branNames from brand_group_member C inner JOIN brand_group B on C.brand_group_cd = B.brand_group_cd where C.member_cd = A.member_cd) AS brand_names,
                authority,
                emailConfirm,
                emailCert,
				agree,
                creator,
                createdate AS createdate,
                updater,
                updatedate AS updatedate
            FROM member A
            WHERE 1=1
            AND member_cd = ?
			AND deleteCheck = 'N'
    	");

        $result = $this->db->query($set_query, $member_cd);
        $result_row = $result->row_array();
        $result->free_result();

        if( empty($result_row) )
            return array();

        return $result_row;
    }

    function getMemberUnique($member_id, $member_email) {

        $this->where = array();

        $set_query = ("
			SELECT
                member_cd,
                member_id,
                member_pass,
                member_name,
                member_email,
                member_hp,
                member_org,
                member_dep,
                member_regdate,
                creator,
                DATE_FORMAT(createdate, '%Y.%m.%d'),
                updater,
                DATE_FORMAT(updatedate, '%Y.%m.%d'),
                authority,
                (case when authority = 'op' then 'OP' when authority = 'pm' then 'PM' else 'PSR' end) AS authorityText,
                emailConfirm,
                emailCert,
                member_team
            FROM member
            WHERE 1=1
            AND member_id != 'admin'
            AND (member_id = ? OR member_email = ?)
			AND deleteCheck = 'N'
    	");

        $this->where = array($member_id, $member_email);

        $result = $this->db->query($set_query, $this->where );
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    function insertMemberData($data = array()) {

        foreach($data as $key => $value) {
            if( $key == 'member_pass' )
                $this->db->set($key, 'password("'.$value.'")', false);
            else
                $this->db->set($key, $value);
        }

        $this->db->insert("member");

        return $this->db->insert_id();
    }
    
    function insertMemberDataRejoin($data = array(),$member_email) {
    	
    	$member_id = $this->session->userdata('member_id');
    	
    	foreach($data as $key => $value) {
    		if($key == 'updatedate' || $key == 'createdate' || $key == 'member_regdate') {
    			$this->db->set($key, 'now()', false);
    		} else {
    			$this->db->set($key, $value);
    		}
    	}
    	
    	$this->db->where('member_id', $member_email);
    	
    	return $this->db->update("member");
    }

    function activityMailInsert($data = array()) {

        foreach($data as $key => $value) {
            $this->db->set($key, $value);
        }

        $this->db->insert("activity_member_mail");

        return $this->db->insert_id();
    }

    // 마이페이지에서 사용자 정보 업데이트
    function updateMemberData($member_cd = '', $data = array()) {
        $member_id = $this->session->userdata('member_id');

        foreach($data as $key => $value) {
            if( $key == 'newPw1' ) {
                if($value != null && $value != '') {
                    $this->db->set('member_pass', 'password("' . $value . '")', false);
                }
            } else {
                if( $key != 'member_pass' ) {
                    $this->db->set($key, $value);
                }
            }
        }

        if($member_id != null && $member_id != '') {
            $this->db->set('updater', $member_id);
        } else {
            $this->db->set('updater', 'system');
        }

        $this->db->set('updatedate', 'now()', false);

        $this->db->where('member_cd', $member_cd);

        return $this->db->update("member");
    }

    function updateEmailCert($emailCert) {

        $this->db->set('emailConfirm', 'true');
        $this->db->where('emailCert', $emailCert);

        return $this->db->update("member");
    }

    // 아이디 존재여부 확인
    function getMemberCountById($member_id) {

        $this->where = array();

        $set_query = ("
			SELECT 
				COUNT(member_cd) AS cnt
			FROM member 
			WHERE member_id = ?
			AND deleteCheck = 'N'
    	");

        $this->where = array($member_id);

        $result = $this->db->query($set_query, $this->where);
        $result_row = $result->row_array();
        $result->free_result();

        return empty($result_row['cnt']) ? 0 : intval($result_row['cnt']);
    }
    
    //사용자가 속해있는 브랜드 그룹 개수조회
    function getCheckMemberGroup($member_cd,$brand_group_cd) {
    	
    	$set_query = ("
			SELECT
				COUNT(*) AS cnt
			FROM brand_group_member
			WHERE member_cd = '".$member_cd."' 
			AND brand_group_cd = '".$brand_group_cd."'
    	");
    	
    	$this->where = array($member_id);
    	
    	$result = $this->db->query($set_query);
    	$result_row = $result->row_array();
    	$result->free_result();
    	
    	return $result_row['cnt'];
    }
    
    // 사용자 프라이머리키 찾기
    function getMemberCd($member_id) {
    	
    	$this->where = array();
    	
    	$set_query = ("
			SELECT
				member_cd
			FROM member
			WHERE member_id = ?
    	");
    	
    	$this->where = array($member_id);
    	
    	$result = $this->db->query($set_query, $this->where);
    	$result_row = $result->row_array();
    	$result->free_result();
    	
    	return $result_row;
    }

    // 이메일 존재 여부 확인
    function getMemberCountByEmail($member_email) {

        $this->where = array();

        $set_query = ("
			SELECT 
				COUNT(member_cd) AS cnt
			FROM member 
			WHERE member_email = ?
			AND deleteCheck = 'N'
    	");

        $this->where = array($member_email);

        $result = $this->db->query($set_query, $this->where);
        $result_row = $result->row_array();
        $result->free_result();

        return empty($result_row['cnt']) ? 0 : intval($result_row['cnt']);
    }
    
    // 새로 가입한건지 탈퇴했다가 다시 가입한건지 확인
    function getMemberCountByEmailReJoin($member_email) {
    	
    	$this->where = array();
    	
    	$set_query = ("
			SELECT
				COUNT(member_cd) AS cnt
			FROM member
			WHERE member_id = ?
    	");
    	
    	$this->where = array($member_email);
    	
    	$result = $this->db->query($set_query, $this->where);
    	$result_row = $result->row_array();
    	$result->free_result();
    	
    	return empty($result_row['cnt']) ? 0 : intval($result_row['cnt']);
    }

    // 비밀번호 임시발급
    function setPasswdChangeData($member_id, $member_pw) {

        $this->db->set('member_pass', 'password("'.$member_pw.'")', false);

        $this->db->where('member_id', $member_id);

        $this->db->update('member');

        if ($this->db->affected_rows() > 0) return TRUE;
        else
            return FALSE;

    }

    // 휴대폰 번호로 아이디 찾기
    function getSearchId($params) {
        $set_query = ("
			SELECT 
				member_id
			FROM member 
			WHERE 1=1
			AND member_name = ?
			AND member_hp = ? 
			AND deleteCheck = 'N'
    	");

        $result = $this->db->query($set_query, $params);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    // 이메일 주소로 아이디 찾기
    function getSearchEmailId($params) {
        $set_query = ("
			SELECT 
				member_id
			FROM member 
			WHERE 1=1
			AND member_name = ?
			AND member_id = ? 
			AND deleteCheck = 'N'
			LIMIT 1
    	");

        $result = $this->db->query($set_query, $params);
        $result_row = $result->row_array();
        $result->free_result();

        return $result_row;
    }

    // 비밀번호 찾기
    function getSearchPw($params) {
        $set_query = ("
			SELECT 
				*
			FROM member 
			WHERE  member_id = ? and member_name = ? 
			AND deleteCheck = 'N'
    	");

        $result = $this->db->query($set_query, $params);
        $result_row = $result->row_array();
        $result->free_result();

        if( empty($result_row) )
            return array();

        return $result_row;
    }
    
    //아이디 불러오기
    function getMemberId($member_cd) {
    	
    	$set_query = ("
			SELECT
				member_id
			FROM member
			WHERE  member_cd = '".$member_cd."'
			AND deleteCheck = 'N'
    	");
    	
    	$result = $this->db->query($set_query);
    	$result_row = $result->row_array();
    	$result->free_result();
    	
   		return $result_row;
    }
    
    // 선택된 사용자 권한 가져오기
    function getAuthority($member_cd) {
    	
    	$set_query = ("
			SELECT
                authority
            FROM member
            WHERE 1=1
            AND member_cd = '".$member_cd."'
    	");
    	
    	$result = $this->db->query($set_query);
    	$result_list = $result->result_array();
    	$result->free_result();
    	
    	return $result_list;
    }
    
    // 현재 사용자가 속해있는 브랜드 리스트
    function currentMemberGroups($member_cd) {
    	
    	$set_query = ("
			SELECT
                brand_group_cd
            FROM brand_group_member
            WHERE 1=1
            AND member_cd = '".$member_cd."'
    	");
    	
    	$result = $this->db->query($set_query);
    	$result_list = $result->result_array();
    	$result->free_result();
    	
    	return $result_list;
    }
    
    // 사용자 삭제
    function getMemberDel($member_cd) {
    	$this->db->set('deleteCheck', 'Y');
    	$this->db->set('office_name','');
    	$this->db->set('agree',null);
    	$this->db->where('member_cd', $member_cd);
    	return $this->db->update("member");
    }
    
    // 사용자 삭제 할때 사용자가 속해있는 그룹에서 삭제
    function getBrandGroupMemberDelMemberOnly($member_cd) {
    	$this->db->where('member_cd', $member_cd);
    	return $this->db->delete('brand_group_member');
    }
    
	// 브랜드 삭제
    function getBrandGroupDel($brand_group_cd) {
        $this->db->where('brand_group_cd', $brand_group_cd);
        return $this->db->delete('brand_group');
    }
    
    // 브랜드 삭제할때 안에 포함되어있는 멤버들도 삭제
    function getBrandGroupMemberDelGroupOnly($brand_group_cd) {
    	$this->db->where('brand_group_cd', $brand_group_cd);
    	return $this->db->delete('brand_group_member');
    }
    
    // 사용자를 특정 브랜드 그룹에서 제거
    function getBrandGroupMemberDel($brand_group_cd, $member_cd) {
    	$this->db->where('brand_group_cd', $brand_group_cd);
    	$this->db->where('member_cd', $member_cd);
    	return $this->db->delete('brand_group_member');
    }

    // 사용자 정보 업데이트 할때 일단 현재 가지고있는 브랜드 제거
    function memRelGroupDelete($brand_group_member_cd) {
        $this->db->where('brand_group_member_cd', $brand_group_member_cd);
        return $this->db->delete("brand_group_member");
    }

    
    function memGroupDelete($member_cd,$brand_group_cd) {
        $this->db->where('member_cd', $member_cd);
        $this->db->where('brand_group_cd', $brand_group_cd);
        return $this->db->delete("brand_group_member");
    }

    // 사용자가 속해있는 회사정보 조회
    function memberToOrgs($member_cd) {
        $set_query = ("
			SELECT
                B.org_id as org_id
                , B.org_name as org_name
            FROM brand_group B left outer JOIN brand_group_member A ON A.brand_group_cd = B.brand_group_cd
            WHERE 1=1
        ");

        $authority = $this->session->userdata('authority');

        if($authority != 'admin' && $authority != 'op') {
            $set_query .= ' AND A.member_cd = \''.$member_cd.'\' ';
        }

        $set_query .= ' GROUP BY B.org_id';


        $result = $this->db->query($set_query);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    //사용자가 속해있는 브랜드정보 조회
    function memberToBrands($member_cd) {
        $set_query = ("
            SELECT
                B.brand_group_cd as brand_group_cd
                , B.brand_group_id as brand_group_id
                , B.brand_group_name as brand_group_name
            FROM brand_group B left outer JOIN brand_group_member A ON A.brand_group_cd = B.brand_group_cd
            WHERE 1=1
        ");

        $authority = $this->session->userdata('authority');

        if($authority != 'mcm' && $authority != 'op') {
            $set_query .= ' AND A.member_cd = \''.$member_cd.'\' ';
        }

        $set_query .= ' GROUP BY B.brand_group_name';

        $result = $this->db->query($set_query, $this->where);
        $result_list = $result->result_array();
        $result->free_result();

        return $result_list;
    }

    // 활성화 메일 전송 후 삭제
    function activityMailDelete($member_cd) {
        $this->db->where('member_cd', $member_cd);
        return $this->db->delete("activity_member_mail");
    }
}

?>