<?php

/**
 * Class M_main
 *

 */

class Main_model extends CI_Model {

    private $where = array();

    function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+9:00'");
    }



    function getReport($admin) {

        $smember_id = $this->session->userdata('member_id');
        $authority = $this->session->userdata('authority');


        if($smember_id != null && $smember_id != '') {
            if ($authority == 'psr' && $admin != 'admin') {
                $set_query = ("
                    SELECT
                        (SELECT COUNT(*) FROM applicant A, symposium_info B WHERE 1=1
                          AND A.sympo_cd = B.sympo_cd
                          AND CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0 
	                        THEN CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_start_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_end_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE 'EN' END END END ELSE 'Y' END = 'I' 
	                      AND A.creator = '".$smember_id."'
	                    ) AS regiCount,
                        (SELECT COUNT(*) FROM applicant A, symposium_info B WHERE 1=1
                          AND A.sympo_cd = B.sympo_cd
                          AND CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0 
	                        THEN CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_start_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_end_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE 'EN' END END END ELSE 'Y' END = 'I'
                          AND A.creator = '".$smember_id."'
	                      AND A.regist_type = 'PSR'
	                    ) AS regiPSRCount,
                        (SELECT COUNT(*) FROM applicant A, symposium_info B WHERE 1=1
                          AND A.sympo_cd = B.sympo_cd
                          AND CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0 
	                        THEN CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_start_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_end_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE 'EN' END END END ELSE 'Y' END = 'I'
	                      AND A.creator = '".$smember_id."'
	                      AND A.regist_type = 'HCP'
	                    ) AS regiHCPCount
                    FROM dual
    	        ");
            } else {
                $set_query = ("
                    SELECT
                        (SELECT COUNT(*) FROM applicant A, symposium_info B WHERE 1=1
                          AND A.sympo_cd = B.sympo_cd
                          AND CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0 
	                        THEN CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_start_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_end_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE 'EN' END END END ELSE 'Y' END = 'I' 
	                    ) AS regiCount,
                        (SELECT COUNT(*) FROM applicant A, symposium_info B WHERE 1=1
                          AND A.sympo_cd = B.sympo_cd
                          AND CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0 
	                        THEN CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_start_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_end_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE 'EN' END END END ELSE 'Y' END = 'I'
	                      AND A.regist_type = 'PSR'
	                    ) AS regiPSRCount,
                        (SELECT COUNT(*) FROM applicant A, symposium_info B WHERE 1=1
                          AND A.sympo_cd = B.sympo_cd
                          AND CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegStartDateTime, '%Y.%m.%d'), now()) <= 0 
	                        THEN CASE WHEN DATEDIFF(DATE_FORMAT(B.event_PreRegEndDateTime, '%Y.%m.%d'), now()) >= 0 THEN 'I' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_start_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE CASE WHEN DATEDIFF(now(), DATE_FORMAT(B.event_end_date, '%Y.%m.%d')) <= 0 THEN 'EI' 
	                        ELSE 'EN' END END END ELSE 'Y' END = 'I'
	                      AND A.regist_type = 'HCP'
	                    ) AS regiHCPCount
                ");
            }
        }

        $result = $this->db->query($set_query);
        $result_list = $result->row_array();
        $result->free_result();

        return $result_list;
    }
    function getAuthority($member_cd) {
    	
    	$set_query = ("

					SELECT authority
					FROM member
					WHERE member_cd = '".$member_cd."'
					AND deleteCheck = 'N'
    	        ");
    	
    	$result = $this->db->query($set_query);
    	$result_list = $result->row_array();
    	$result->free_result();
    	
    	return $result_list;
    }

}

?>