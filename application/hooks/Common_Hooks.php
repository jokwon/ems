<?php

/**
 * Class Common_Hooks
 *
 * @property M_a_member_point M_a_member_point
 */
class Common_Hooks {

    private $CI;

    private $user_id = null;
    private $user_idx = null;

    private $admin_id = null;
    private $admin_idx = null;
    private $authority = null;

    private $admin_auth = null;

    private $business_id = null;
    private $business_idx = null;
    private $business_section = null;

    private $planner_id = null;
    private $planner_name = null;
    private $planner_idx = null;

    private $hall_id = null;
    private $hall_name = null;
    private $hall_idx = null;


    private $uri_arr = array(
        'key','hcp','hcpAdd', 'threadSend'
    );

    private $redirect_url = '';

    function __construct() {
        $this->CI =& get_instance();

    }

    function init() {
        $this->CI->load->helper('cookie');

        $this->member_id = $this->CI->session->userdata('member_id');
        $this->member_idx = $this->CI->session->userdata('member_cd');
        $this->authority = $this->CI->session->userdata('authority');
        
        
        $this->uri_arr = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        //$this->uri_arr = array_filter($this->uri_arr, create_function( '$val', 'if (!$val || trim($val)=="") return false; return $val;' ));


        $this->redirect_url = $_SERVER['REQUEST_URI'];


        /*if( !get_cookie('movbox') )
            set_cookie('movbox', 'todayClose', 60*60*24, '.'.$_SERVER['HTTP_HOST'], '/');*/

        define('MEMBER_ID', !$this->member_id ? null : $this->member_id);
        define('MEMBER_IDX', !$this->member_idx ? null : $this->member_idx);
        define('AUTHROITY', !$this->authority ? null : $this->authority);



        //define('URI_ARR', serialize($this->uri_arr));
       // define('SPECIAL_PKG', serialize(getTopSpecialPackage()));

        // IE10 이하 체크
        //$this->ie_check();

        // 로그인 체크
        $this->is_login();

    }

    /*function ie_check() {
        $is_ie = preg_match_all('/MSIE\s(?P<v>\d*)/i', $_SERVER['HTTP_USER_AGENT'], $B);
        $B['v'] = reset($B['v']);

        if ( $is_ie && intval($B['v']) < 10)
            select_alert(null, '/resources/html/browser_check.html');
    }*/

    function is_login() {
        if ($this->member_idx)
            return;
        if( !in_array('login', $this->uri_arr) && !in_array('login_exec', $this->uri_arr) ) {
            if(in_array('member', $this->uri_arr)){
               return;
            }

            if(in_array('notice', $this->uri_arr)){
                return;
            }

            if(in_array('key', $this->uri_arr)){
                return;
            }

            if(in_array('hcp_home', $this->uri_arr)){
                return;
            }
            
            if(in_array('hcp_detail', $this->uri_arr)){
            	return;
            }
            
            if(in_array('hcp_step1', $this->uri_arr)){
            	return;
            }
            
            if(in_array('hcp_step2', $this->uri_arr)){
            	return;
            }
            
            if(in_array('hcp_step3', $this->uri_arr)){
            	return;
            }

            if(in_array('hcp_step4', $this->uri_arr)){
                return;
            }
            
            if(in_array('hcp_complete', $this->uri_arr)){
            	return;
            }
            
            if(in_array('getCheckInvitation', $this->uri_arr)){
            	return;
            }
            
            if(in_array('threadSend', $this->uri_arr)){
                return;
            }

            if (!$this->member_idx) {
                select_alert(null, HOME_DIR."/login?redirect_url=" . $_SERVER['REQUEST_URI']);
            } else {
                select_alert(null, '/main');
            }
        }


    }

  
}
