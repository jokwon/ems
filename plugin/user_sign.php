<?php
$text = $_REQUEST['text'];

$textSize = 20;
$str_len = mb_strlen($text, 'UTF-8');

//Create image
$im = imagecreate(19*$str_len, 30);

// Black background and White text
$bg = imagecolorallocate($im, 255, 255, 255);
$textColor = imagecolorallocate($im, 0, 0, 0);

imagettftext($im, $textSize, 0, 5, $textSize, $textColor, $_SERVER['DOCUMENT_ROOT'].'/plugin/font/tvN_Medium.ttf', $text);

// Output the image
header('Content-type: image/png');
imagepng($im);
imagedestroy($im);
?>