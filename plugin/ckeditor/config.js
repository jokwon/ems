/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    config.language = 'ko';
    // config.uiColor = '#AADC6E';

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbar = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Scayt' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'tools', items: [ 'Maximize' ] },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
        { name: 'others', items: [ '-' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
        { name: 'styles', items: [ 'Styles', 'Format' ] },
        { name: 'about', items: [ 'About' ] }
        { name: 'document', items: [ 'Source', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        '/',
        { name: 'paragraph', items: [ 'Image', 'Bold', 'TextColor', 'BGColor' ] }
    ];
    /*config.toolbar = [
        { name: 'document', items: [ 'Source', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        '/',
        { name: 'paragraph', items: [ 'Image', 'Bold', 'TextColor', 'BGColor' ] }
    ];*/

// Toolbar groups configuration.
    /*config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' },
        { name: 'about' }
    ];*/
    config.toolbarGroups = [];
    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced;';
    config.linkShowAdvancedTab = false;
    config.linkShowTargetTab = false;

    config.image_previewText = ' ';

    config.enterMode = CKEDITOR.ENTER_BR;

};

CKEDITOR.on( 'dialogDefinition', function( ev ) {
    // Take the dialog name and its definition from the event data.
    var dialogName = ev.data.name;
    var dialog = ev.data.definition.dialog;
    var dialogDefinition = ev.data.definition;
    //var dialog = CKEDITOR.dialog.getCurrent();
    //alert( dialog.getName() );

    // Check if the definition is from the dialog we are interested in (the 'link' dialog)
    if(dialogName == 'link') {

        dialogDefinition.onShow = function () {
            var dialog = CKEDITOR.dialog.getCurrent();
            //dialog.hidePage( 'target' ); // now via config
            //dialog.hidePage( 'advanced' ); // now via config
            elem = dialog.getContentElement('info','anchorOptions');
            elem.getElement().hide();
            elem = dialog.getContentElement('info','emailOptions');
            elem.getElement().hide();
            var elem = dialog.getContentElement('info','linkType');
            elem.getElement().hide();
            elem = dialog.getContentElement('info','protocol');
            elem.disable();
        };

    }
    /* if you have any plugin of your own and need to remove ok button
    else if(dialogName == 'my_own_plugin') {
        // remove ok button (this was hard to find!)
        dialogDefinition.onShow = function () {
           // this is a hack
           document.getElementById(this.getButton('ok').domId).style.display='none';
        };
    }*/
    else if(dialogName == 'image') {
      dialog.on('show', function (obj) {
      this.selectPage('Upload'); //업로드텝으로 바로 이동
    });

      dialogDefinition.removeContents( 'advanced' ); // 자세히 탭 삭제
      dialogDefinition.removeContents( 'Link' ); // 링크 탭 삭제

    }
    else if(dialogName == 'table') {
        dialogDefinition.removeContents('advanced');
    }

});
